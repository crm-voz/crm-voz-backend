<?php

namespace Tests\Helpers;

class TestsSingleton {
    private static $instances = [];
    protected $id_temp;

    /**
     * O construtor do Singleton deve ser sempre privado para prevenir
     * chamadas diretas ao construtor com o operador 'new'.
     */
    protected function __construct() { }

    /**
     * Singletons não devem ser clonados.
     */
    protected function __clone() { }

    /**
     * Singletons não devem ser restaurados por string.
     */
    public function __wakeup()
    {
        throw new \Exception("Cannot unserialize a singleton.");
    }

    /**
     * Este é o método estático que controla o acesso à instância singleton.
     * Na primeira chamada, ele irá criar
     * This is the static method that controls the access to the singleton
     * instance. On the first run, it creates a singleton object and places it
     * into the static field. On subsequent runs, it returns the client existing
     * object stored in the static field.
     *
     * This implementation lets you subclass the Singleton class while keeping
     * just one instance of each subclass around.
     */
    public static function getInstance(): TestsSingleton
    {
        $cls = static::class;
        if (!isset(self::$instances[$cls])) {
            self::$instances[$cls] = new static();
        }

        return self::$instances[$cls];
    }

    public function getId(){
        return $this->id_temp;
    }

    public function setId($newId){
        $this->id_temp = $newId;
    }
}
