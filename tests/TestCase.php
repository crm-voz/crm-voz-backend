<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    protected static function setId($newId){
        $singleton = \Tests\Helpers\TestsSingleton::getInstance();
        $singleton->setId($newId);
    }

    protected static function getId(){
        $singleton = \Tests\Helpers\TestsSingleton::getInstance();
        return $singleton->getId();
    }
}
