<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;
use Laravel\Passport\Passport;

class ClientesTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_get_principal_retorna_200_json_correto()
    {
        Passport::actingAs(
            User::first()
        );

        $this->get('/api/v1/clientes')
            ->assertStatus(200)
            ->assertJsonStructure([
                'dados',
                'mensagem'
            ]);
    }

    public function test_get_pagina_create_retorna_200_json_correto()
    {
        Passport::actingAs(
            User::first()
        );

        $response = $this->get('/api/v1/clientes/create')
                        ->assertStatus(200)
                        ->assertJsonStructure([
                            'dados',
                            'mensagem'
                        ]);

    }

    public function test_store_post_valido_retorna_200_json_correto()
    {
        Passport::actingAs(
            User::first()
        );

        $response = $this->json('POST', '/api/v1/clientes/store', [
                                    "nome" => "Teste",
                                    "cpf_cnpj" => "60860860868",
                                    "endereco" => "Rua João de Barro",
                                    "cep" => "65900-630"
                                ])
                        ->assertStatus(200)
                        ->assertJsonStructure(['dados', 'mensagem']);

        $this->setId($response->getData()->dados->id);
        // $response->dd();
    }

    public function test_get_pagina_edit_id_valido_retorna_200_json_correto()
    {
        Passport::actingAs(
            User::first()
        );

        $response = $this->get("/api/v1/clientes/edit/" . $this->getId())
                        ->assertStatus(200)
                        ->assertJsonStructure([
                            'dados',
                            'mensagem'
                        ]);
    }

    public function test_filtro_json_correto_retorna_200_json_correto()
    {
        Passport::actingAs(
            User::first()
        );

        $response = $this->json('GET', '/api/v1/clientes/filtro', [
                                    'items_pag' => 10,
                                    'pag' => 1
                                ])
                        ->assertStatus(200)
                        ->assertJsonStructure(['dados', 'mensagem']);
    }

    public function test_update_json_valido_retorna_200_json_correto()
    {
        Passport::actingAs(
            User::first()
        );

        $response = $this->json('PUT', '/api/v1/clientes/update/'  . $this->getId(), [
                                    "nome" => "Teste ",
                                    "cpf_cnpj" => "00000000000",
                                    "endereco" => "Rua Teste de Teste",
                                    "cep" => "65900-630"
                                ])
                        ->assertStatus(200)
                        ->assertJsonStructure(['dados', 'mensagem']);
    }

    public function test_delete_id_valido_retorna_200_json_correto()
    {
        Passport::actingAs(
            User::first()
        );

        $response = $this->json('DELETE', '/api/v1/clientes/delete/'  . $this->getId());

        if($response->getStatusCode() == 422) {
            $response->assertStatus(422);
        } else {
            $response->assertStatus(200);
        }
    }
}
