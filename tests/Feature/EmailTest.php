<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;
use App\Models\BackOffice\Cliente;
use Laravel\Passport\Passport;

class EmailTest extends TestCase
{

    public function test_store_post_valido_retorna_200_json_correto()
    {
        Passport::actingAs(
            User::first()
        );

        $response = $this->json('POST', '/api/v1/clientes/emails/store', [
                        'email' => "admin@teste.com.br",
                        "cliente_id" => Cliente::first()->id
                    ])
                ->assertStatus(200)
                ->assertJsonStructure([
                    'dados',
                    'mensagem'
                ]);

        $this->setId($response->getData()->dados->id);
    }

    public function test_get_principal_retorna_200_json_correto()
    {
        Passport::actingAs(
            User::first()
        );

        $response = $this->get('/api/v1/clientes/emails/' . $this->getId())
                        ->assertStatus(200)
                        ->assertJsonStructure([
                            'dados',
                            'mensagem'
                        ]);
    }

    public function test_get_edit_retorna_200_json_correto()
    {
        Passport::actingAs(
            User::first()
        );

        $response = $this->get('/api/v1/clientes/emails/edit/' . $this->getId())
                        ->assertStatus(200)
                        ->assertJsonStructure([
                            'dados',
                            'mensagem'
                        ]);
    }

    public function test_update_retorna_200_json_correto()
    {
        Passport::actingAs(
            User::first()
        );

        $response = $this->json('PUT', '/api/v1/clientes/emails/update/' . $this->getId(), [
                                    'email' => "admin_test_feature@teste.com.br",
                                    'cliente_id' => Cliente::first()->id
                                ])
                        ->assertStatus(200)
                        ->assertJsonStructure([
                            'dados',
                            'mensagem'
                        ])
                        ;
        // $response->dd();
    }

    public function test_get_adicionar_blacklist_retorna_200_json_correto()
    {
        Passport::actingAs(
            User::first()
        );

        $response = $this->get('/api/v1/clientes/emails/adicionar_blacklist/' . $this->getId())
                        ->assertStatus(200)
                        ->assertJsonStructure([
                            'dados',
                            'mensagem'
                        ]);
    }

    public function test_get_retirar_blacklist_retorna_200_json_correto()
    {
        Passport::actingAs(
            User::first()
        );

        $response = $this->get('/api/v1/clientes/emails/retirar_blacklist/' . $this->getId())
                        ->assertStatus(200)
                        ->assertJsonStructure([
                            'dados',
                            'mensagem'
                        ]);
    }

    public function test_delete_email_retorna_200_json_correto()
    {
        Passport::actingAs(
            User::first()
        );

        $response = $this->delete('/api/v1/clientes/emails/delete/' . $this->getId())
                        ->assertStatus(200)
                        ->assertJsonStructure([
                            'dados',
                            'mensagem'
                        ]);
    }
}
