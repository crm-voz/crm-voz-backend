<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;
use Laravel\Passport\Passport;

class RemessasTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_get_principal_retorna_200_json_correto()
    {
        Passport::actingAs(
            User::factory()->create()
        );

        $this->get('/api/v1/remessas')
            ->assertStatus(200)
            ->assertJsonStructure([
                'sucesso',
                'dados',
                'mensagem'
            ]);
    }

    public function test_get_pagina_create_retorna_200_json_correto()
    {
        Passport::actingAs(
            User::factory()->create()
        );

        $response = $this->get('/api/v1/remessas/create')
                        ->assertStatus(200)
                        ->assertJsonStructure([
                            'sucesso',
                            'dados',
                            'mensagem'
                        ]);
    }

    public function test_store_post_valido_retorna_200_json_correto()
    {
        Passport::actingAs(
            User::find(1)
        );

        $response = $this->json('POST', '/api/v1/remessas/store', [
                                    "data_entrada" => "2021-05-12 00:00:00",
                                    "nome_arquivo" => "Teste de Banco de dados",
                                    "credor_id" => 1
                                ])
                        ->assertStatus(200)
                        ->assertJsonStructure(['sucesso', 'dados', 'mensagem']);

        $this->setId($response->getData()->dados->id);

    }

    public function test_get_pagina_edit_id_valido_retorna_200_json_correto()
    {
        Passport::actingAs(
            User::factory()->create()
        );
        $singleton = \Tests\Helpers\TestsSingleton::getInstance();

        $response = $this->get("/api/v1/remessas/edit/" . $this->getId())
                        ->assertStatus(200)
                        ->assertJsonStructure([
                            'sucesso',
                            'dados',
                            'mensagem'
                        ]);
    }

    public function test_update_json_valido_retorna_200_json_correto()
    {
        Passport::actingAs(
            User::factory()->create()
        );
        $singleton = \Tests\Helpers\TestsSingleton::getInstance();

        $response = $this->json('PUT', '/api/v1/remessas/update/' . $this->getId(), [
                                    "numero_remessa" => "654321",
                                    "data_remessa" => "2021-05-12 00:00:00",
                                    "data_entrada" => "2021-05-12 00:00:00",
                                    "nome_arquivo" => "Teste de Banco de dados",
                                    "credor_id" => 1,
                                    "usuario_empresa_id" => 1
                                ])
                        ->assertStatus(200)
                        ->assertJsonStructure(['sucesso', 'dados', 'mensagem']);
    }

    public function test_delete_id_valido_retorna_200_json_correto()
    {
        Passport::actingAs(
            User::factory()->create()
        );
        $singleton = \Tests\Helpers\TestsSingleton::getInstance();

        $response = $this->json('DELETE', '/api/v1/remessas/delete/' . $this->getId())
                        ->assertStatus(200);
    }
}
