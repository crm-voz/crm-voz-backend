<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;
use App\Models\BackOffice\Cliente;
use Laravel\Passport\Passport;

class TelefoneTest extends TestCase
{

    public function test_store_post_valido_retorna_200_json_correto()
    {
        Passport::actingAs(
            User::first()
        );

        $response = $this->json('POST', '/api/v1/clientes/telefones/store', [
                        "cliente_id" => Cliente::first()->id,
                        "telefone" => "99991654782",
                        "origem" => "Credor",
                        "tags" => "Móvel/Whatsapp",
                        "status" => true
                    ])
                ->assertStatus(200)
                ->assertJsonStructure([
                    'dados',
                    'mensagem'
                ]);

        $this->setId($response->getData()->dados->id);
    }

    public function test_get_principal_retorna_200_json_correto()
    {
        Passport::actingAs(
            User::first()
        );

        $response = $this->get('/api/v1/clientes/telefones/' . $this->getId())
                        ->assertStatus(200)
                        ->assertJsonStructure([
                            'dados',
                            'mensagem'
                        ]);
    }

    public function test_get_edit_retorna_200_json_correto()
    {
        Passport::actingAs(
            User::first()
        );

        $response = $this->get('/api/v1/clientes/telefones/edit/' . $this->getId())
                        ->assertStatus(200)
                        ->assertJsonStructure([
                            'dados',
                            'mensagem'
                        ]);
    }

    public function test_update_retorna_200_json_correto()
    {
        Passport::actingAs(
            User::first()
        );

        $response = $this->json('PUT', '/api/v1/clientes/telefones/update/' . $this->getId(), [
                                    "cliente_id" => Cliente::first()->id,
                                    "telefone" => "99991654782",
                                    "origem" => "Credor",
                                    "tags" => "Móvel",
                                    "status" => false
                                ])
                        // ->assertStatus(200)
                        // ->assertJsonStructure([
                        //     'dados',
                        //     'mensagem'
                        // ])
                        ;
        // $response->dd();
    }

    public function test_status_update_retorna_200_json_correto() {
        Passport::actingAs(
            User::first()
        );

        $response = $this->json('PUT', '/api/v1/clientes/telefones/status/update/' . $this->getId(), [
                                    "status_log_telefone_id" => 1,
                                    "observacao" => "Obs Teste Automatizado"
                                ])
                        // ->assertStatus(200)
                        // ->assertJsonStructure([
                        //     'dados',
                        //     'mensagem'
                        // ])
                        ;
        // $response->dd();
    }

    public function test_get_adicionar_blacklist_retorna_200_json_correto()
    {
        Passport::actingAs(
            User::first()
        );

        $response = $this->get('/api/v1/clientes/telefones/adicionar_blacklist/' . $this->getId())
                        ->assertStatus(200)
                        ->assertJsonStructure([
                            'dados',
                            'mensagem'
                        ]);
    }

    public function test_get_retirar_blacklist_retorna_200_json_correto()
    {
        Passport::actingAs(
            User::first()
        );

        $response = $this->get('/api/v1/clientes/telefones/retirar_blacklist/' . $this->getId())
                        ->assertStatus(200)
                        ->assertJsonStructure([
                            'dados',
                            'mensagem'
                        ]);
    }

    public function test_delete_email_retorna_200_json_correto()
    {
        Passport::actingAs(
            User::first()
        );

        $response = $this->delete('/api/v1/clientes/telefones/delete/' . $this->getId())
                        ->assertStatus(200)
                        ->assertJsonStructure([
                            'dados',
                            'mensagem'
                        ]);
    }
}
