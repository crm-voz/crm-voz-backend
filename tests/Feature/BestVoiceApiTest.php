<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Gateway\Sms\BestVoice;

class BestVoiceApiTest extends TestCase
{
    /**
     * Teste de envios de SMS único BestVoice
     *
     * @return void
     */
    public function test_enviar_unico_sms_retorna_200()
    {
        $json = [
            "celular" => "99991997205",
            "mensagem" => "Teste da API single - BestVoice"
        ];

        $bv = new BestVoice();
        $this->assertTrue($bv->enviar_sms($json)->successful());
    }

    /**
     * Teste de envios de SMS em lote BestVoice
     *
     * @return void
     */
    public function test_enviar_bulk_sms_retorna_200()
    {
        $json = [
            [
                "celular" => "99991997205",
                "mensagem" => "Teste da API bulk - BestVoice"
            ],
            [
                "celular" => "99991806627",
                "mensagem" => "Teste da API bulk - BestVoice"
            ]

        ];

        $bv = new BestVoice();
        $this->assertTrue($bv->enviar_sms($json)->successful());
    }
}
