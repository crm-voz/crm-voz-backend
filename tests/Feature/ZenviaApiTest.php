<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Gateway\Sms\Zenvia;

class ZenviaApiTest extends TestCase
{
    /**
     * Teste de envios de SMS único Zenvia
     *
     * @return void
     */
    public function test_enviar_unico_sms_retorna_200()
    {
        $json = [
            "celular" => "5599991997205",
            "mensagem" => "Teste da API single - Zenvia"
        ];

        $zv = new Zenvia();
        $this->assertTrue($zv->enviar_sms($json)->successful());
        // dd(($zv->enviar_sms($json)));
    }

    /**
     * Teste de envios de SMS em lote Zenvia
     *
     * @return void
     */
    public function test_enviar_bulk_sms_retorna_200()
    {
        $json = [
            [
                "celular" => "5599991997205",
                "mensagem" => "Teste da API bulk - Zenvia"
            ],
            [
                "celular" => "5599991171112",
                "mensagem" => "Teste da API bulk - Zenvia"
            ]

        ];

        $zv = new Zenvia();
        $this->assertTrue($zv->enviar_sms($json)->successful());
        // dd($zv->enviar_sms($json));
    }
}
