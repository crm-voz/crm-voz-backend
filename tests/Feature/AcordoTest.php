<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Passport\Passport;
use Tests\TestCase;

class AcordoTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_get_principal_retorna_200_json_correto()
    {
        Passport::actingAs(
            User::first()
        );

        $this->get('/api/v1/acordos/2')
            ->assertStatus(200)
            ->assertJsonStructure([
                'dados',
                'mensagem'
            ]);
    }

    public function test_store_acordo_sem_boleo_json_correto(){
        Passport::actingAs(
            User::first()
        );
        $response = $this->json('POST', '/api/v1/clientes/store', [
            "acordo" => [
                    "status" => "Negociado",
                    "valor_nominal" => 911.12,
                    "juros" => 41.04,
                    "multas" => 22.80,
                    "honorarios" => 50.00,
                    "desconto" => 0.00,
                    "valor_entrada" => 500,
                    "quantidade_parcelas" => "3",
                    "valor_parcela" => 303.70,
                    "cliente_id" => 53324,
                    "executivo_cobranca_id" => 8,
                    "juros_porcento" => 2,
                    "multas_porcento" => 2,
                    "honorarios_porcento" => 20,
                    "desconto_porcento" => 15,
                    "valor_entrada_porcento" => 30
                ],
                "operacoes" => [
                    [
                      "id" => 359698,
                      "numero_operacao" => "123542152",
                      "data_atualizacao" => "2021-07-22",
                      "valor_atualizado" => 55,
                      "valor_juros" => 41.04,
                      "valor_multa" => 22.8,
                      "valor_honorario" => 25,
                      "desconto" => 0
                    ]
                  ],
                "parcelas" => [
                        [
                            "valor_nominal" => 50,
                            "numero_operacao" => "15422652",
                            "data_vencimento" => "2021-07-22",
                            "numero_parcela" => 1,
                            "valor_juros" => 2,
                            "valor_multa" => 1,
                            "valor_honorario" => 25,
                            "forma_pagamento" => "Boleto"
                        ],
                        [
                            "valor_nominal"=> "50",
                            "data_vencimento"=> "2021-08-22",
                            "numero_parcela"=> 2,
                            "valor_juros"=> 2,
                            "valor_multa"=> 1,
                            "valor_honorario"=> 25,
                            "forma_pagamento"=> "Boleto"
                        ],
                        [
                            "valor_nominal"=> "50",
                            "data_vencimento"=> "2021-08-22",
                            "numero_parcela"=> 3,
                            "valor_juros"=> 2,
                            "valor_multa"=> 1,
                            "valor_honorario"=> 25,
                            "forma_pagamento"=> "Boleto"
                        ]
                    ],
                "parametros_contato" => [
                        "cpf" => "61037007360",
                        "telefone" => "099991171112",
                        "call_id" => "13058984",
                        "origem" => "MANUAL"
                    ]
                ])
                        ->assertStatus(200)
                        ->assertJsonStructure(['dados', 'mensagem']);
    }

}
