<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;
use Laravel\Passport\Passport;

class OperacoesTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_get_principal_retorna_200_json_correto()
    {
        Passport::actingAs(
            User::factory()->create()
        );

        $this->get('/api/v1/operacoes')
            ->assertStatus(200)
            ->assertJsonStructure([
                'sucesso',
                'dados',
                'mensagem'
            ]);
    }

    public function test_store_post_valido_retorna_200_json_correto()
    {
        Passport::actingAs(
            User::find(1)
        );

        $response = $this->json('POST', '/api/v1/operacoes/store', [
                                    "numero_operacao"=> 100,
                                    "valor_nominal"=> 12.35,
                                    "data_vencimento"=> "2021-06-17",
                                    "descricao"=> "Apenas um teste de 12,35 reais",
                                    "cliente_id"=> 2,
                                    "status_operacao_id"=> 1,
                                    "remessa_id"=> 1
                                ])
                        ->assertStatus(200)
                        ->assertJsonStructure(['sucesso', 'dados', 'mensagem']);

        $this->setId($response->getData()->dados->id);

    }

    public function test_get_pagina_create_retorna_200_json_correto()
    {
        Passport::actingAs(
            User::factory()->create()
        );

        $id_cliente = \App\Models\Backoffice\Cliente::limit(1)->get()[0]['id'];
        
        $response = $this->get('/api/v1/operacoes/create/' . $id_cliente)
                        ->assertStatus(200)
                        ->assertJsonStructure([
                            'sucesso',
                            'dados',
                            'mensagem'
                        ]);
    }

    public function test_get_pagina_edit_id_valido_retorna_200_json_correto()
    {
        Passport::actingAs(
            User::factory()->create()
        );
        $singleton = \Tests\Helpers\TestsSingleton::getInstance();

        $response = $this->get("/api/v1/operacoes/edit/" . $this->getId())
                        ->assertStatus(200)
                        ->assertJsonStructure([
                            'sucesso',
                            'dados',
                            'mensagem'
                        ]);
    }

    public function test_update_json_valido_retorna_200_json_correto()
    {
        Passport::actingAs(
            User::factory()->create()
        );
        $singleton = \Tests\Helpers\TestsSingleton::getInstance();

        $response = $this->json('PUT', '/api/v1/operacoes/update/' . $this->getId(), [
                                "numero_operacao"=> 100,
                                "valor_nominal"=> 12.35,
                                "data_vencimento"=> "2021-06-17",
                                "descricao"=> "Apenas um teste de 12,35 reais",
                                "cliente_id"=> 2,
                                "status_operacao_id"=> 1,
                                "remessa_id"=> 1
                                ])
                        ->assertStatus(200)
                        ->assertJsonStructure(['sucesso', 'dados', 'mensagem']);
    }

    public function test_delete_id_valido_retorna_200_json_correto()
    {
        Passport::actingAs(
            User::factory()->create()
        );
        $singleton = \Tests\Helpers\TestsSingleton::getInstance();

        $response = $this->json('DELETE', '/api/v1/operacoes/delete/' . $this->getId())
                        ->assertStatus(200);
    }
}
