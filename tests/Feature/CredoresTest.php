<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;
use App\Models\BackOffice\Remessa;
use App\Models\BackOffice\Credor;
use Laravel\Passport\Passport;

class CredoresTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_get_principal_retorna_200_json_correto()
    {
        Passport::actingAs(
            User::first()
        );

        $this->get('/api/v1/credores')
            ->assertStatus(200)
            ->assertJsonStructure([
                'dados',
                'mensagem'
            ]);
    }

    public function test_get_pagina_create_retorna_200_json_correto()
    {
        Passport::actingAs(
            User::first()
        );

        $response = $this->get('/api/v1/credores/create')
                        ->assertStatus(200)
                        ->assertJsonStructure([
                            'dados',
                            'mensagem'
                        ]);
    }

    public function test_store_post_valido_retorna_200_json_correto()
    {
        Passport::actingAs(
            User::first()
        );

        $response = $this->json('POST', '/api/v1/credores/store', [
                                    "nome"=> "WANIA DRINKS",
                                    "razao_social"=> "WANIA ENTRETENIMENTOS LTDA",
                                    "cnpj"=> "12365-5",
                                    "cidade_id"=> 1,
                                    "email"=> "wania@drinks.com",
                                    "empresa_id"=> 1
                                ])
                        ->assertStatus(200)
                        ->assertJsonStructure(['dados', 'mensagem']);

        $this->setId($response->getData()->dados->id);

    }

    public function test_get_pagina_edit_id_valido_retorna_200_json_correto()
    {
        Passport::actingAs(
            User::first()
        );
        $singleton = \Tests\Helpers\TestsSingleton::getInstance();

        $response = $this->get("/api/v1/credores/edit/" . $this->getId())
                        ->assertStatus(200)
                        ->assertJsonStructure([
                            'dados',
                            'mensagem'
                        ]);
    }

    public function test_update_json_valido_retorna_200_json_correto()
    {
        Passport::actingAs(
            User::first()
        );
        $singleton = \Tests\Helpers\TestsSingleton::getInstance();

        $response = $this->json('PUT', '/api/v1/credores/update/' . $this->getId(), [
                                    "nome"=> "WANIA DRINKS Atualizado",
                                    "razao_social"=> "WANIA ENTRETENIMENTOS LTDA",
                                    "cnpj"=> "12365-5",
                                    "cidade_id"=> 1,
                                    "email"=> "wania@drinks.com",
                                    "empresa_id"=> 1
                                ])
                        ->assertStatus(200)
                        ->assertJsonStructure(['dados', 'mensagem']);
    }


    public function test_get_pagina_clientes_retorna_200_json_correto()
    {
        Passport::actingAs(
            User::first()
        );

        $response = $this->get('/api/v1/credores/clientes/' . $this->getId())
                        ->assertStatus(200)
                        ->assertJsonStructure([
                            'dados',
                            'mensagem'
                        ]);
    }

    public function test_get_pagina_remessas_retorna_200_json_correto()
    {
        Passport::actingAs(
            User::first()
        );

        $response = $this->get('/api/v1/credores/remessas/' . Remessa::first()->id)
                        ->assertStatus(200)
                        ->assertJsonStructure([
                            'dados',
                            'mensagem'
                        ]);
    }

    public function test_get_pagina_filtro_retorna_200_json_correto()
    {
        Passport::actingAs(
            User::first()
        );

        $response = $this->get('/api/v1/credores/filtro?filtro=' . substr(str_shuffle('abcdefghijk'), 0, rand(3,11)))
                        ->assertStatus(200)
                        ->assertJsonStructure([
                            'dados',
                            'mensagem'
                        ]);
    }

    public function test_store_parametros_valido_retorna_200_json_correto()
    {
        Passport::actingAs(
            User::first()
        );

        $response = $this->json('POST', '/api/v1/credores/storeparametros/' . $this->getId(), [
                                    '2018.1' => [
                                    0 => [
                                        'avista' => [
                                        'juros' => '0',
                                        'multa' => '0',
                                        'desconto' => '50',
                                        'honorarios' => '10',
                                        ],
                                        'parcelado' => [
                                        'boleto' => [
                                            'juros' => '0',
                                            'multa' => '0',
                                            'honorarios' => '10',
                                            'maximo_parcelas_restantes' => '5',
                                        ],
                                        'cartao_credito' => [
                                            'juros' => '0',
                                            'multa' => '0',
                                            'honorarios' => '10',
                                            'maximo_parcelas_restantes' => '6',
                                        ],
                                        ],
                                    ],
                                    ],
                                    '2018.2' => [
                                    0 => [
                                        'avista' => [
                                        'juros' => '0',
                                        'multa' => '0',
                                        'desconto' => '30',
                                        'honorarios' => '10',
                                        ],
                                        'parcelado' => [
                                        'boleto' => [
                                            'juros' => '0',
                                            'multa' => '0',
                                            'honorarios' => '10',
                                            'maximo_parcelas_restantes' => '5',
                                        ],
                                        'cartao_credito' => [
                                            'juros' => '0',
                                            'multa' => '0',
                                            'honorarios' => '10',
                                            'maximo_parcelas_restantes' => '6',
                                        ],
                                        ],
                                    ],
                                    ],
                                    '2019.2' => [
                                    0 => [
                                        'avista' => [
                                        'juros' => '0',
                                        'multa' => '0',
                                        'desconto' => '30',
                                        'honorarios' => '10',
                                        ],
                                        'parcelado' => [
                                        'boleto' => [
                                            'juros' => '0',
                                            'multa' => '0',
                                            'honorarios' => '10',
                                            'maximo_parcelas_restantes' => '5',
                                        ],
                                        'cartao_credito' => [
                                            'juros' => '0',
                                            'multa' => '0',
                                            'honorarios' => '10',
                                            'maximo_parcelas_restantes' => '6',
                                        ],
                                        ],
                                    ],
                                    ],
                                    '2020.1' => [
                                    0 => [
                                        'avista' => [
                                        'juros' => '0',
                                        'multa' => '0',
                                        'desconto' => '10',
                                        'honorarios' => '10',
                                        ],
                                        'parcelado' => [
                                        'boleto' => [
                                            'juros' => '0',
                                            'multa' => '0',
                                            'honorarios' => '10',
                                            'maximo_parcelas_restantes' => '5',
                                        ],
                                        'cartao_credito' => [
                                            'juros' => '0',
                                            'multa' => '0',
                                            'honorarios' => '10',
                                            'maximo_parcelas_restantes' => '6',
                                        ],
                                        ],
                                    ],
                                    ],
                                    '2020.2' => [
                                    0 => [
                                        'avista' => [
                                        'juros' => '1',
                                        'multa' => '2',
                                        'desconto' => '0',
                                        'honorarios' => '20',
                                        ],
                                        'parcelado' => [
                                        'boleto' => [
                                            'juros' => '1',
                                            'multa' => '2',
                                            'honorarios' => '20',
                                            'entrada_minima' => '40',
                                            'maximo_parcelas_restantes' => '3',
                                        ],
                                        'cartao_credito' => [
                                            'juros' => '1',
                                            'multa' => '2',
                                            'honorarios' => '20',
                                            'entrada_minima' => '0',
                                            'maximo_parcelas_restantes' => '6',
                                        ],
                                        ],
                                    ],
                                    ],
                                    '2021.1' => [
                                    0 => [
                                        'avista' => [
                                        'juros' => '1',
                                        'multa' => '2',
                                        'desconto' => '0',
                                        'honorarios' => '20',
                                        ],
                                        'parcelado' => [
                                        'boleto' => [
                                            'juros' => '1',
                                            'multa' => '2',
                                            'honorarios' => '20',
                                            'entrada_minima' => '40',
                                            'maximo_parcelas_restantes' => '3',
                                        ],
                                        'cartao_credito' => [
                                            'juros' => '1',
                                            'multa' => '2',
                                            'honorarios' => '20',
                                            'entrada_minima' => '0',
                                            'maximo_parcelas_restantes' => '6',
                                        ],
                                        ],
                                    ],
                                    ],
                                    'default' => [
                                    'juros' => '1',
                                    'multa' => '2',
                                    'desconto' => '0',
                                    'honorarios' => '20',
                                    'desconto_baixa_credor' => '10',
                                    'prazo_envio_remessa' => '30',
                                    'convenio' => '2793032',
                                    'cod_banco' => '2031',
                                    ],
                                ])
                        ->assertStatus(200)
                        ->assertJsonStructure(['dados', 'mensagem']);

        $this->setId($response->getData()->dados->id);

    }

    public function test_get_pagina_snapshot_retorna_200_json_correto()
    {
        Passport::actingAs(
            User::first()
        );

        $response = $this->json('GET', '/api/v1/credores/stats', [
                                    "credor" => Credor::first()->id
                                ])
                        ->assertStatus(200)
                        ->assertJsonStructure([
                            'dados',
                            'mensagem'
                        ]);
    }

    public function test_delete_id_valido_retorna_200_json_correto()
    {
        Passport::actingAs(
            User::first()
        );
        $singleton = \Tests\Helpers\TestsSingleton::getInstance();

        $response = $this->json('DELETE', '/api/v1/credores/delete/' . $this->getId())
                        ->assertStatus(200);
    }
}
