<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;
use Laravel\Passport\Passport;

class DevolucaoTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_get_principal_retorna_200_json_correto()
    {
        Passport::actingAs(
            User::factory()->create()
        );

        $this->get('/api/v1/devolucao')
            ->assertStatus(200)
            ->assertJsonStructure([
                'sucesso',
                'dados',
                'mensagem'
            ]);
    }

    public function test_get_pagina_create_retorna_200_json_correto()
    {
        Passport::actingAs(
            User::factory()->create()
        );

        $response = $this->get('/api/v1/devolucao/create')
                        ->assertStatus(200)
                        ->assertJsonStructure([
                            'sucesso',
                            'dados',
                            'mensagem'
                        ]);
    }

    public function test_store_post_valido_retorna_200_json_correto()
    {
        Passport::actingAs(
            User::find(1)
        );

        $response = $this->json('POST', '/api/v1/devolucao/store', [
                                    "motivo_devolucao_id" => 1,
                                    "descricao" => "Devolução de teste"
                                ])
                        ->assertStatus(200)
                        ->assertJsonStructure(['sucesso', 'dados', 'mensagem']);

        $this->setId($response->getData()->dados->id);
    }

    public function test_get_pagina_edit_id_valido_retorna_200_json_correto()
    {
        Passport::actingAs(
            User::factory()->create()
        );
        $singleton = \Tests\Helpers\TestsSingleton::getInstance();

        $response = $this->get("/api/v1/devolucao/detalhe/" . $this->getId())
                        ->assertStatus(200)
                        ->assertJsonStructure([
                            'sucesso',
                            'dados',
                            'mensagem'
                        ]);
    }
}
