<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;

class AuthTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_autenticar_com_credenciais_incorretas_responde_400()
    {
        $user = User::factory()->create();

        $response = $this->json('POST' ,'/api/login', ['email' => $user->email, 'password' => $user->password])
                        ->assertStatus(400);
    }

    public function test_autenticar_com_credenciais_invalidas_responde_422()
    {
        $response = $this->json('POST' ,'/api/login', ['email' => 'any_name_not_a_email', 'password' => ''])
                        ->assertStatus(422)
                        ->assertJsonStructure(['message', 'errors']);
    }

    public function test_autenticar_com_credenciais_corretas_responde_200()
    {
        $user = User::factory()->create();

        $response = $this->json('POST' ,'/api/login', ['email' => 'admin@grupovoz.com.br', 'password' => '123456789'])
                        ->assertStatus(200);
    }
}
