<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Gateway\Email\Sendinblue;

class SendinblueTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_multiplos_emails_tem_sucesso()
    {
        $em = new Sendinblue();
        $arr = [
                    "contatos" => [
                        [
                            "email" => "richardlucasfm@gmail.com",
                            "nome" => "Richard Lucas",
                        ],
                        [
                            "email" => "richard@grupovoz.com.br",
                            "nome" => "Richard Lucas",
                        ]
                    ],
                    "parametros" => [
                        "nome"   => "Nome teste",
                        "credor" => "Credor",
                        "codigo" => 10
                    ],
                    "template" => 25,
                ];
        $this->assertTrue($em->enviar_email($arr)->successful());
    }

    public function test_email_unico_tem_sucesso()
    {
        $em = new Sendinblue();
        $arr = [
                    "contatos" => [
                        [
                            "email" => "richardlucasfm@gmail.com",
                            "nome" => "Richard Lucas",
                        ]
                    ],
                    "parametros" => [
                        "nome"   => "Richard Lucas",
                        "credor" => "VOZ Gestão",
                        "codigo" => 1010
                    ],
                    "template" => 25,
                ];
        $this->assertTrue($em->enviar_email($arr)->successful());
    }
}
