
@extends('layouts.auth')

@section('title')
    {{ config('app.name', 'Laravel') }}
@endsection

@section('content')
<div class="auth-wrapper">
        <div class="auth-content">
            <div class="auth-bg">
                <span class="r"></span>
                <span class="r s"></span>
                <span class="r s"></span>
                <span class="r"></span>
            </div>
            <div class="card">
                <form method="POST" action="{{ route('login') }}">
                    @csrf
                    <div class="card-body text-center">
                        <div class="mb-4">
                            <i class="feather icon-unlock auth-icon"></i>
                        </div>
                        <h3 class="mb-4">Login</h3>
                        <div class="input-group mb-3">
                            <input type="email" class="form-control" placeholder="Email" id="email" name="email">
                        </div>
                        <div class="input-group mb-4">
                            <input type="password" class="form-control" placeholder="password" id="password" name="password">
                        </div>
                        <div class="form-group text-left">
                            <div class="checkbox checkbox-fill d-inline">
                                <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}/>
                                <label for="remember" class="cr" > {{ __('Lembrar-me') }} </label>
                            </div>
                        </div>
                        <button class="btn btn-primary shadow-2 mb-4">{{ __('Login') }}</button>
                        <p class="mb-2 text-muted">Recuperar senha? <a href="{{ route('password.request') }}">{{  __('Reset') }}</a></p>
                        <p class="mb-0 text-muted">Não tem conta? <a href="{{ route('register') }}">Criar conta</a></p>
                    </div>
                </form>
            </div>
        </div>

</div>
@endsection
