
@extends('layouts.auth')

@section('title')
    {{ config('app.name', 'Laravel') }}
@endsection

@section('content')

<div class="auth-wrapper">
    <div class="auth-content">
        <div class="auth-bg">
            <span class="r"></span>
            <span class="r s"></span>
            <span class="r s"></span>
            <span class="r"></span>
        </div>
        <div class="card">
            <form method="POST" action="{{ route('password.email') }}">
                @csrf
                <div class="card-body text-center">
                    <div class="mb-4">
                        <i class="feather icon-mail auth-icon"></i>
                    </div>
                    <h3 class="mb-4">{{ __('Reset Password') }}</h3>
                    <div class="input-group mb-3">
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>

                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <button class="btn btn-primary mb-4 shadow-2">{{ __('Send Password Reset Link') }}</button>
                    <p class="mb-0 text-muted">Não possui conta? <a href="{{ route('register') }}">Criar conta</a></p>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
