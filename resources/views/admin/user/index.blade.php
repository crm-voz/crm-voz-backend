@extends('layouts.admin')
@section('title', config('app.name', 'Voz CRM'))

@section('css')
    <style>
        .card .card-block, .card .card-body {
            text-align: -webkit-center !important;
        }
    </style>
@endsection

@section('PageHeader')
    Perfil
@endsection

@section('breadcrumb')
<div class="row">
    <div class="col-8">
        <ul class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}"><i class="feather icon-home"></i></a></li>
            <li class="breadcrumb-item"><a href="#!">Perfil</a></li>
        </ul>
    </div>
    <div class="col-4">
        <div class="text-right">
            {{-- <a href="{{route('admin.empresas.create')}}" class="btn btn-primary" title="" data-toggle="tooltip"
            data-original-title="Criar nova empresa">Nova Empresa</a> --}}
        </div>
    </div>
</div>
@endsection

@section('TituloCard')
    Perfil do Usuário
@endsection

@section('content')
<div class="auth-wrapper">
        <div class="auth-bg">
            <span class="r"></span>
            <span class="r s"></span>
            <span class="r s"></span>
            <span class="r"></span>
        </div>
    <div class="col-xl-6 col-md-6">
        <div class="card">
            <div style="text-align: -webkit-left !important;" class="card-block p-0">
                <div class="text-center project-main">
                    <img class="rounded-circle" style="width:25%;" alt="activity-user"
                        src="{{asset('../storage/app/public/users/profile/'.auth()->user()->url_perfil)}}">
                    <h5 class="mt-4">
                        @guest
                            <span>Nenhum usuário logado</span>
                        @else
                            <span>{{auth()->user()->name}}</span>
                        @endguest
                    </h5>
                    <span>
                        <form class="form-group col-md-12" action="{{route('admin.user.update',[auth()->user()->id])}}" enctype="multipart/form-data" method="POST">
                            <input name="url_perfil" type="file" accept="image/*" class="label theme-bg text-white f-12" required>
                            {{-- <a href="#" type="submit" class="label theme-bg text-white f-12"><i class="fa fa-save"></i> Salvar Foto</a> --}}
                    </span>
                </div>

                <div class="border-top"></div>
                <div class="project-main">
                    <div style="padding: 2%;" class="form-row">
                            {{-- <form class="form-group col-md-12" action="{{route('admin.user.update',[auth()->user()->id])}}" method="PUT"> --}}
                            @csrf
                            <div class="form-group col-md-12">
                                <label for="name">Nome:</label><label style="color:red">*</label>
                                <input id="name" value="@isset(auth()->user()->name){{auth()->user()->name}}@endisset" name="name"
                                    type="text" placeholder="Nome"
                                    class="form-control @error('name') is-invalid @enderror">
                                @if($errors->has('name'))
                                    <label class="error jquery-validation-error small form-text invalid-feedback" style="color:red">
                                        @error('name')
                                            <span class="error">
                                                {{ $message }}
                                            </span>
                                        @enderror
                                    </label>
                                @endif
                            </div>

                            <div class="form-group col-md-12">
                                <label for="email">Email:</label><label style="color:red">*</label>
                                <input id="email" value="@isset(auth()->user()->email){{auth()->user()->email}}@endisset" name="email"
                                    type="text" placeholder="email"
                                    class="form-control @error('email') is-invalid @enderror">
                                @if($errors->has('email'))
                                    <label class="error jquery-validation-error small form-text invalid-feedback" style="color:red">
                                        @error('email')
                                            <span class="error">
                                                {{ $message }}
                                            </span>
                                        @enderror
                                    </label>
                                @endif
                            </div>

                            <div class="form-group col-md-12">
                                <label for="password">Senha:</label><label style="color:red">*</label>
                                <input id="password" value="" name="password"
                                    type="password" placeholder="Senha"
                                    class="form-control @error('password') is-invalid @enderror">
                                    <small id="passwordHelpBlock" class="form-text text-muted">Exemplo: Grupovoz2021$. Exemplo caracteres especiais: #?!@$%^&*-_.,</small>
                                @if($errors->has('password'))
                                    <label class="error jquery-validation-error small form-text invalid-feedback" style="color:red">
                                        @error('password')
                                            <span class="error">
                                                {{ $message }}
                                            </span>
                                        @enderror
                                    </label>
                                @endif
                            </div>

                            <div class="form-group col-md-12">
                                <label for="confirmar_senha">Confirmar Senha:</label><label style="color:red">*</label>
                                <input id="confirmar_senha" value="" name="confirmar_senha"
                                    type="password" placeholder="Confirmar Senha"
                                    class="form-control @error('confirmar_senha') is-invalid @enderror">
                                @if($errors->has('confirmar_senha'))
                                    <label class="error jquery-validation-error small form-text invalid-feedback" style="color:red">
                                        @error('confirmar_senha')
                                            <span class="error">
                                                {{ $message }}
                                            </span>
                                        @enderror
                                    </label>
                                @endif
                            </div>
                            <div class="form-group col-md-12">
                                <button type="submit" class="btn btn-primary  text-uppercase btn-block">Salvar</button>
                            </div>
                        </form>
                    </div>

                    <div class="row text-center">
                        <div class="col-md-4 col-6">
                            <h6 class="text-muted"><i class="fas fa-map-marker-alt m-r-5"></i>BRL</h6>
                        </div>
                        <div class="col-md-4 col-6">
                            <h6 class="text-muted">
                                <i class="far fa-calendar-alt"></i>
                                @php
                                    $mytime = Carbon\Carbon::now();
                                    echo $mytime->format('d/m/Y');
                                @endphp
                            </h6>
                        </div>
                        <div class="col-md-4 col-12">
                            <h6 class="text-muted m-0"><i class="far fa-clock"></i>
                                @php
                                    $mytime = Carbon\Carbon::now();
                                    echo $mytime-> toTimeString();
                                @endphp
                            </h6>
                        </div>

                        <div class="card-block">
                            <ul class="task-list">
                                <li>
                                    <i class="task-icon bg-c-green"></i>
                                    <h6>Última Atualização: &nbsp
                                        <span class="float-right text-muted">
                                            @isset($usuario){{auth()->user()->updated_at->format('d/m/Y H:i:s')}}@endisset
                                        </span>
                                    </h6>
                                    <p class="text-muted">@isset($usuario){{auth()->user()->name}}@endisset…</p>
                                </li>
                                <li>
                                    <i class="task-icon bg-c-green"></i>
                                    <h6>Criado:
                                        <span class="float-right text-muted">
                                            @isset($usuario){{auth()->user()->created_at->format('d/m/Y H:i:s')}}@endisset
                                        </span>
                                    </h6>
                                    <p class="text-muted">@isset($usuario){{auth()->user()->name}}@endisset…</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
    <script src="{{asset('assets/js/pages/dashboard-custom.js')}}"></script>
    <script>
        // Função para desabilitar ação do enter no formulário.
        $(document).ready(function () {
            $('input').keypress(function (e) {
                    var code = null;
                    code = (e.keyCode ? e.keyCode : e.which);
                    return (code == 13) ? false : true;
            });
        });
    </script>
@endsection
