@extends('layouts.admin')
@section('title', config('app.name', 'Voz CRM'))

@section('css')

@endsection

@section('PageHeader','Cadastro de Classificação Negativação')

@section('breadcrumb')
    <div class="row">
        <div class="col-8">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}"><i class="feather icon-home"></i></a></li>
                <li class="breadcrumb-item"><a href="{{route('admin.classificacao_negativacao.index')}}">Classificação Negativação</a></li>
                <li class="breadcrumb-item"><a href="#!">Nova Classificação Negativação</a></li>
            </ul>
        </div>
        <div class="col-4">
            <div class="text-right">
                <a href="{{route('admin.classificacao_negativacao.index')}}" class="btn btn-success" title="" data-toggle="tooltip" data-original-title="Voltar">Voltar</a>
            </div>
        </div>
    </div>
@endsection

@section('TituloCard','Nova Classificação Negativação')

@section('content')
    <form action="{{route('admin.classificacao_negativacao.store')}}" method="post">
        @csrf
        @include('admin.administrador.classificacao_negativacao.formulario')
    </form>
@endsection

@section('js')

@endsection
