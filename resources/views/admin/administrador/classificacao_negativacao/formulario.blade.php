<div class="form-row">
    <div class="form-group col-md-6">
        <label for="nome">Nome:</label><label style="color:red">*</label>
        <input id="nome" value="@isset($dados){{$dados->nome}}@endisset" name="nome" type="text" placeholder="Nome Classificação Negativação"  class="form-control @error('nome') is-invalid @enderror">
        @if($errors->has('nome'))
            <label class="error jquery-validation-error small form-text invalid-feedback"
            style="color:red">
                @error('nome')
                    <span class="error">
                        {{ $message }}
                    </span>
                @enderror
            </label>
        @endif
    </div>
</div>
<button id="acao" name="acao" type="submit" class="btn btn-primary">Salvar</button>


