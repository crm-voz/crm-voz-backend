@extends('layouts.admin')
@section('title', config('app.name', 'Voz CRM'))

@section('css')
    <!-- material datetimepicker css -->
    <link rel="stylesheet" href="{{asset('assets/plugins/material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}">
    <!-- Bootstrap datetimepicker css -->
    <link rel="stylesheet" href="{{asset('assets/plugins/bootstrap-datetimepicker/css/bootstrap-datepicker3.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/fonts/material/css/materialdesignicons.min.css')}}">
    <!-- minicolors css -->
    <link rel="stylesheet" href="{{asset('assets/plugins/mini-color/css/jquery.minicolors.css')}}">
@endsection

@section('PageHeader')
    Motivo Pendência
@endsection

@section('breadcrumb')
<div class="row">
    <div class="col-8">
        <ul class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}"><i class="feather icon-home"></i></a></li>
            <li class="breadcrumb-item"><a href="#!">Motivo Pendência</a></li>
        </ul>
    </div>
    <div class="col-4">
        <div class="text-right">
            <a href="{{route('admin.motivo_pendencia.create')}}" class="btn btn-primary"
            title="" data-toggle="tooltip" data-original-title="Criar novo motivo pendência">Novo Motivo Pendência</a>
        </div>
    </div>
</div>
@endsection

@section('TituloCard')
    Lista de Motivo Pendência
@endsection

@section('content')
    <div class="table-responsive">
        <table id="key-act-button" class="display table nowrap table-striped table-hover" style="width:100%">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Nome</th>
                    <th>Cor</th>
                    <th>Status</th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($dados as $motivo_pendencia)
                    <tr role="row" class="odd">
                        <td class="">{{$motivo_pendencia->id}}</td>
                        <td class="">{{$motivo_pendencia->nome}}</td>
                        <td class="">
                            <label style="color:{{$motivo_pendencia->cor}}">
                                <button  data-toggle="tooltip" data-original-title="Cor: {{$motivo_pendencia->cor}}"
                                    class="btn btn-icon btn-rounded btn-primary"
                                    style="background-color:{{$motivo_pendencia->cor}} !important;border-color:#fff !important;"
                                    type="button">
                                </button>
                            </label>
                        </td>
                        <td class="">
                            @if ($motivo_pendencia->status == true)
                                Ativo
                            @else
                                Inativo
                            @endif
                        </td>

                        <th class="" style="width: 10px">
                            <a href="{{route('admin.motivo_pendencia.edit',[$motivo_pendencia->id])}}"
                                class="btn btn-icon btn-warning" data-toggle="tooltip" data-original-title="Atualizar Motivo Pendência">
                                <i class="far fa-edit"></i>
                            </a>
                        </th>
                        <th class="" style="width: 10px">
                            <form action="{{route('admin.motivo_pendencia.delete',[$motivo_pendencia->id])}}" method="post">
                                @csrf
                                @method('delete')
                                <a href="#" class="btn btn-icon btn-danger btn-delete" data-toggle="tooltip"
                                    data-original-title="Excluir Motivo Pendência: {{$motivo_pendencia->nome}}">
                                    <i class="fas fa-trash-alt"></i>
                                </a>
                            </form>
                        </th>
                    </tr>
                @endforeach
            </tbody>

        </table>
    </div>
@endsection

@section('js')
    <script type="text/javascript">
        $(document).on('click', '.btn-delete', function (event) {
            event.preventDefault();

            var button = $(this);

            swal({
                buttons: {
                    cancel: "Cancelar!",
                    catch: {
                        text: "Sim",
                    },
                    defeat: true,
                },
                title: "Deseja excluir o registro?",
                text: "Você não poderá recuperar essa informação.",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then(function(isConfirm){
                if (isConfirm) {
                    button.closest("form").submit();
                }
            });
        });
    </script>
    <!-- material datetimepicker Js -->
    <script src="http://momentjs.com/downloads/moment-with-locales.min.js"></script>
    <script src="{{asset('assets/plugins/material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}"></script>

    <!-- minicolors Js -->
    <script src="{{asset('assets/plugins/mini-color/js/jquery.minicolors.min.js')}}"></script>

    <!-- form-picker-custom Js -->
    <script src="{{asset('assets/js/pages/form-picker-custom.js')}}"></script>
@endsection
