@extends('layouts.admin')
@section('title', config('app.name', 'Voz CRM'))

@section('css')
    <!-- material datetimepicker css -->
    <link rel="stylesheet" href="{{asset('assets/plugins/material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}">
    <!-- Bootstrap datetimepicker css -->
    <link rel="stylesheet" href="{{asset('assets/plugins/bootstrap-datetimepicker/css/bootstrap-datepicker3.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/fonts/material/css/materialdesignicons.min.css')}}">
    <!-- minicolors css -->
    <link rel="stylesheet" href="{{asset('assets/plugins/mini-color/css/jquery.minicolors.css')}}">
@endsection

@section('PageHeader','Cadastro de Motivo Pedência')

@section('breadcrumb')
    <div class="row">
        <div class="col-8">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}"><i class="feather icon-home"></i></a></li>
                <li class="breadcrumb-item"><a href="{{route('admin.motivo_pendencia.index')}}">Motivo Pendência</a></li>
                <li class="breadcrumb-item"><a href="#!">Novo Motivo Pendência</a></li>
            </ul>
        </div>
        <div class="col-4">
            <div class="text-right">
                <a href="{{route('admin.motivo_pendencia.index')}}" class="btn btn-success" title=""
                data-toggle="tooltip" data-original-title="Voltar">Voltar</a>
            </div>
        </div>
    </div>
@endsection

@section('TituloCard','Novo Motivo Pendência')

@section('content')
    <form action="{{route('admin.motivo_pendencia.store')}}" method="post">
        @csrf
        @include('admin.administrador.motivo_pendencia.formulario')
    </form>
@endsection

@section('js')
    <!-- material datetimepicker Js -->
    <script src="http://momentjs.com/downloads/moment-with-locales.min.js"></script>
    <script src="{{asset('assets/plugins/material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}"></script>

    <!-- minicolors Js -->
    <script src="{{asset('assets/plugins/mini-color/js/jquery.minicolors.min.js')}}"></script>

    <!-- form-picker-custom Js -->
    <script src="{{asset('assets/js/pages/form-picker-custom.js')}}"></script>
@endsection
