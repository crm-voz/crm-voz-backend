<div class="form-row">
    <div class="form-group col-md-6">
        <label for="nome">Nome:</label><label style="color:red">*</label>
        <input id="nome" value="@isset($cidade){{$cidade->nome}}@endisset" name="nome" type="text" placeholder="Nome Cidade"  class="form-control @error('nome') is-invalid @enderror">
        @if($errors->has('nome'))
            <label class="error jquery-validation-error small form-text invalid-feedback"
            style="color:red">
                @error('nome')
                    <span class="error">
                        {{ $message }}
                    </span>
                @enderror
            </label>
        @endif
    </div>

    <div class="form-group col-md-2">
        <label for="uf">UF:</label><label style="color:red">*</label>
        <select id="uf" name="uf" placeholder="Nome UF" class="form-control @error('uf') is-invalid @enderror">
            <option id="0" selected="selected">@isset($cidade){{$cidade->uf}}@endisset</option>
                <option id="1">AC</option>
                    <option id="2">AL</option>
                        <option id="3">AP</option>
                            <option id="4">AM</option>
                                <option id="5">BA</option>
                                    <option id="6">CE</option>
                                        <option id="7">DF</option>
                                            <option id="8">ES</option>
                                                <option id="9">GO</option>
                                                    <option id="10">MA</option>

                <option id="11">MT</option>
                    <option id="12">MS</option>
                        <option id="13">MG</option>
                            <option id="14">PA</option>
                                <option id="15">PB</option>
                                    <option id="16">PR</option>
                                        <option id="17">PE</option>
                                            <option id="18">PI</option>
                                                <option id="19">RJ</option>
                                                    <option id="20">RN</option>
                                                        <option id="21">RS</option>
                                                            <option id="22">RO</option>
                                                                <option id="23">RR</option>
                                                                    <option id="24">SC</option>
                                                                        <option id="25">SP</option>
                                                                            <option id="26">SE</option>
                                                                                <option id="27">TO</option>
        </select>
        @if($errors->has('uf'))
            <label class="error jquery-validation-error small form-text invalid-feedback"
            style="color:red">
                @error('uf')
                    <span class="error">
                        {{ $message }}
                    </span>
                @enderror
            </label>
        @endif
    </div>

    <div class="form-group col-md-4">
        <label for="codigo_ibge">Código IBGE:</label><label style="color:red">*</label>
        <input id="codigo_ibge" value="@isset($cidade){{$cidade->codigo_ibge}}@endisset" name="codigo_ibge" placeholder="Código IBGE" type="text" class="form-control @error('codigo_ibge') is-invalid @enderror">
        @if($errors->has('codigo_ibge'))
            <label class="error jquery-validation-error small form-text invalid-feedback"
            style="color:red">
                @error('codigo_ibge')
                    <span class="error">
                        {{ $message }}
                    </span>
                @enderror
            </label>
        @endif
    </div>

</div>
    <button id="acao" name="acao" type="submit" class="btn btn-primary">Salvar</button>

