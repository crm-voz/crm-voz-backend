@extends('layouts.admin')
@section('title', config('app.name', 'Voz CRM'))

@section('css')

@endsection

@section('PageHeader')
    Cidades
@endsection

@section('breadcrumb')
<div class="row">
    <div class="col-8">
        <ul class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}"><i class="feather icon-home"></i></a></li>
            <li class="breadcrumb-item"><a href="#!">Cidades</a></li>
        </ul>
    </div>
    <div class="col-4">
        <div class="text-right">
            <a href="{{route('admin.cidade.create')}}" class="btn btn-primary" title="" data-toggle="tooltip" data-original-title="Criar nova cidade">Nova Cidade</a>
        </div>
    </div>
</div>
@endsection

@section('TituloCard')
    Lista de Cidades
@endsection

@section('content')
    <div class="table-responsive">
        <table id="key-act-button" class="display table nowrap table-striped table-hover" style="width:100%">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Nome</th>
                    <th>UF</th>
                    <th>Código IBGE</th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($dados as $cidade)
                    <tr role="row" class="odd">
                        <td class="">{{$cidade->id}}</td>
                        <td class="">{{$cidade->nome}}</td>
                        <td class="">{{$cidade->uf}}</td>
                        <td class="">{{$cidade->codigo_ibge}}</td>
                        <th class="" style="width: 10px"><a href="{{route('admin.cidade.edit',[$cidade->id])}}" class="btn btn-icon btn-warning" data-toggle="tooltip" data-original-title="Atualizar Cidade"><i class="far fa-edit"></i></a></th>
                        <th class="" style="width: 10px">
                            <form action="{{route('admin.cidade.delete',[$cidade->id])}}" method="post">
                                @csrf
                                @method('delete')
                                <a href="#" class="btn btn-icon btn-danger btn-delete" data-toggle="tooltip" data-original-title="Excluir cidade: {{$cidade->nome}}"><i class="fas fa-trash-alt"></i></a>
                            </form>
                        </th>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection

@section('js')
<script type="text/javascript">
    $(document).on('click', '.btn-delete', function (event) {
        event.preventDefault();

        var button = $(this);

        swal({
            buttons: {
                cancel: "Cancelar!",
                catch: {
                    text: "Sim",
                },
                defeat: true,
            },
            title: "Deseja excluir o registro?",
            text: "Você não poderá recuperar essa informação.",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then(function(isConfirm){
            if (isConfirm) {
                button.closest("form").submit();
            }
        });
    });
</script>
@endsection
