@extends('layouts.admin')
@section('title', config('app.name', 'Voz CRM'))

@section('css')

@endsection

@section('PageHeader','Cadastro de Cidade')

@section('breadcrumb')
    <div class="row">
        <div class="col-8">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}"><i class="feather icon-home"></i></a></li>
                <li class="breadcrumb-item"><a href="{{route('admin.cidade.index')}}">Cidades</a></li>
                <li class="breadcrumb-item"><a href="#!">Nova Cidade</a></li>
            </ul>
        </div>
        <div class="col-4">
            <div class="text-right">
                <a href="{{route('admin.cidade.index')}}" class="btn btn-success" title="" data-toggle="tooltip" data-original-title="Voltar">Voltar</a>
            </div>
        </div>
    </div>
@endsection

@section('TituloCard','Nova Cidade')

@section('content')
    <form action="{{route('admin.cidade.store')}}" method="post">
        @csrf
        @include('admin.administrador.cidade.formulario')
    </form>
@endsection

@section('js')

@endsection
