<div class="form-row">
    <div class="form-group col-md-6">
        <label class="col-form-label">Nome:</label><label style="color:red">*</label>
        <input id="nome" value="@isset($motivo_devolucao){{$motivo_devolucao->nome}}@endisset" name="nome" type="text"
            placeholder="Nome Motivo Devolução"  class="form-control @error('nome') is-invalid @enderror">
        @if($errors->has('nome'))
            <label class="error jquery-validation-error small form-text invalid-feedback"
            style="color:red">
                @error('nome')
                    <span class="error">
                        {{ $message }}
                    </span>
                @enderror
            </label>
        @endif
    </div>

    <div class="form-group col-md-2">
        <label class="col-form-label">Cor:</label>
        <input style="height:44px !important" name="cor" type="text" id="cor" class="form-control demo" data-control="brightness"
            value="@isset($motivo_devolucao){{$motivo_devolucao->cor}}@endisset">
    </div>

    <div class="form-group col-md-2">
        <label class="col-form-label">Status:</label><label style="color:red">*</label>
        <br>
        <div class="switch switch-success d-inline m-r-10">
             <input value="true" name="status" type="checkbox" id="status"@isset($motivo_devolucao) @if ($motivo_devolucao->status) checked @endif @endisset>
            <label for="status" class="cr"></label>
        </div>
        @if($errors->has('status'))
            <label class="error jquery-validation-error small form-text invalid-feedback"
            style="color:red">
                @error('status')
                    <span class="error">
                        {{ $message }}
                    </span>
                @enderror
            </label>
        @endif
    </div>

</div>
<button id="acao" name="acao" type="submit" class="btn btn-primary">Salvar</button>


