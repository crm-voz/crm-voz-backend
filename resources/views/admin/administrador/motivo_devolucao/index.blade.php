@extends('layouts.admin')
@section('title', config('app.name', 'Voz CRM'))

@section('css')

@endsection

@section('PageHeader')
    Motivo Devolução
@endsection

@section('breadcrumb')
<div class="row">
    <div class="col-8">
        <ul class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">
                <i class="feather icon-home"></i></a></li>
            <li class="breadcrumb-item"><a href="#!">Motivo Devolução</a></li>
        </ul>
    </div>
    <div class="col-4">
        <div class="text-right">
            <a href="{{route('admin.motivo_devolucao.create')}}" class="btn btn-primary"
            title="" data-toggle="tooltip" data-original-title="Criar novo motivo devolução">Novo Motivo Devolução</a>
        </div>
    </div>
</div>
@endsection

@section('TituloCard')
    Lista de Motivo Devolução
@endsection

@section('content')
    <div class="table-responsive">
        <table id="key-act-button" class="display table nowrap table-striped table-hover" style="width:100%">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Nome</th>
                    <th>Cor</th>
                    <th>Status</th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($dados as $motivo_devolucao)
                    <tr role="row" class="odd">
                        <td class="">{{$motivo_devolucao->id}}</td>
                        <td class="">{{$motivo_devolucao->nome}}</td>
                        <td class="">
                            <label style="color:{{$motivo_devolucao->cor}}">
                                <button  data-toggle="tooltip" data-original-title="Cor: {{$motivo_devolucao->cor}}"
                                    class="btn btn-icon btn-rounded btn-primary"
                                    style="background-color:{{$motivo_devolucao->cor}} !important;border-color:#fff !important;"
                                    type="button">
                                </button>
                            </label>
                        </td>
                        <td class="">
                            @if ($motivo_devolucao->status == true)
                                Ativo
                            @endif
                            @if ($motivo_devolucao->status == false)
                                Inativo
                            @endif
                        </td>
                        <th class="" style="width: 10px">
                            <a href="{{route('admin.motivo_devolucao.edit',[$motivo_devolucao->id])}}"
                                class="btn btn-icon btn-warning" data-toggle="tooltip"
                                data-original-title="Atualizar Motivo Devolução">
                                <i class="far fa-edit"></i>
                            </a>
                        </th>
                        <th class="" style="width: 10px">
                            <form action="{{route('admin.motivo_devolucao.delete',[$motivo_devolucao->id])}}" method="post">
                                @csrf
                                @method('delete')
                                <a href="#" class="btn btn-icon btn-danger btn-delete" data-toggle="tooltip"
                                    data-original-title="Excluir Motivo Devolução: {{$motivo_devolucao->nome}}">
                                    <i class="fas fa-trash-alt"></i>
                                </a>
                            </form>
                        </th>
                    </tr>
                @endforeach
            </tbody>

        </table>
    </div>
@endsection

@section('js')
    <script type="text/javascript">
        $(document).on('click', '.btn-delete', function (event) {
            event.preventDefault();

            var button = $(this);

            swal({
                buttons: {
                    cancel: "Cancelar!",
                    catch: {
                        text: "Sim",
                    },
                    defeat: true,
                },
                title: "Deseja excluir o registro?",
                text: "Você não poderá recuperar essa informação.",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then(function(isConfirm){
                if (isConfirm) {
                    button.closest("form").submit();
                }
            });
        });
    </script>

@endsection
