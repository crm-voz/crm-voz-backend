@extends('layouts.admin')
@section('title', config('app.name', 'Voz CRM'))

@section('css')

@endsection

@section('PageHeader','Atualização de Funcionários')

@section('breadcrumb')
    <div class="row">
        <div class="col-8">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}"><i class="feather icon-home"></i></a></li>
                <li class="breadcrumb-item"><a href="{{route('admin.funcionarios.index')}}">Funcionários</a></li>
                <li class="breadcrumb-item"><a href="#!">Atualizar Funcionário</a></li>
            </ul>
        </div>
        <div class="col-4">
            <div class="text-right">
                <a href="{{route('admin.funcionarios.index')}}" class="btn btn-success" title=""
                data-toggle="tooltip" data-original-title="Voltar">Voltar</a>
            </div>
        </div>
    </div>
@endsection

@section('TituloCard','Atualizar Empresas')

@section('content')
<form action="{{route('admin.funcionarios.update',[$funcionario->id])}}" method="post">
    @csrf
    @method('put')
    @include('admin.administrador.funcionarios.formulario')
</form>
@endsection

@section('js')

@endsection
