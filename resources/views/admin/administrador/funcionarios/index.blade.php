@extends('layouts.admin')
@section('title', config('app.name', 'Voz CRM'))

@section('css')

@endsection

@section('PageHeader')
    Funcionários
@endsection

@section('breadcrumb')
<div class="row">
    <div class="col-8">
        <ul class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}"><i class="feather icon-home"></i></a></li>
            <li class="breadcrumb-item"><a href="#!">Funcionários</a></li>
        </ul>
    </div>
    <div class="col-4">
        <div class="text-right">
            <a href="{{route('admin.funcionarios.create')}}" class="btn btn-primary" title="" data-toggle="tooltip"
            data-original-title="Criar nova empresa">Novo Funcionário</a>
        </div>
    </div>
</div>
@endsection

@section('TituloCard')
    Lista de Funcionários
@endsection

@section('content')
    <div class="table-responsive">
        <table id="key-act-button" class="display table nowrap table-striped table-hover" style="width:100%">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Nome</th>
                    <th>CPF</th>
                    <th>RG</th>
                    <th>Endereço</th>
                    <th>Observação</th>
                    <th>Cidade</th>
                    <th>UF</th>
                    <th>Função</th>
                    <th>Status</th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($dados as $funcionario)
                    <tr role="row" class="odd">
                        <td>{{$funcionario->id}}</td>
                        <td>{{$funcionario->nome}}</td>
                        <td>{{$funcionario->cpf}}</td>
                        <td>{{$funcionario->rg}}</td>
                        <td>{{$funcionario->endereco}}</td>
                        <td>{{$funcionario->observacao}}</td>
                        <td>
                           {{$funcionario->cidade->nome }}
                        </td>
                        <td>
                            {{$funcionario->cidade->uf }}
                         </td>
                        <td>
                            {{ $funcionario->funcao->nome }}
                        </td>
                        <td>
                            {{ $funcionario->status ? 'Ativo' : 'Inativo' }}
                        </td>

                        <th style="width: 10px">
                            <a href="{{route('admin.funcionarios.edit',[$funcionario->id])}}"
                                class="btn btn-icon btn-warning" data-toggle="tooltip"
                                data-original-title="Atualizar Funcionário">
                                <i class="far fa-edit"></i>
                            </a>
                        </th>
                        <th style="width: 10px">
                            <form action="{{route('admin.funcionarios.delete',[$funcionario->id])}}" method="post">
                                @csrf
                                @method('delete')
                                <a href="#" class="btn btn-icon btn-danger btn-delete" data-toggle="tooltip"
                                    data-original-title="Excluir Funcionario: {{$funcionario->nome}}">
                                    <i class="fas fa-trash-alt"></i>
                                </a>
                            </form>
                        </th>
                    </tr>
                @endforeach
            </tbody>

        </table>
    </div>
@endsection

@section('js')
    <script type="text/javascript">
        $(document).on('click', '.btn-delete', function (event) {
            event.preventDefault();

            var button = $(this);

            swal({
                buttons: {
                    cancel: "Cancelar!",
                    catch: {
                        text: "Sim",
                    },
                    defeat: true,
                },
                title: "Deseja excluir o registro?",
                text: "Você não poderá recuperar essa informação.",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then(function(isConfirm){
                if (isConfirm) {
                    button.closest("form").submit();
                }
            });
        });
    </script>
    
@endsection
