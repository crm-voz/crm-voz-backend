<div class="form-row">
    <div class="form-group col-md-6">
        <label for="nome">Nome:</label><label style="color:red">*</label>
        <input id="nome_fantasia" value="@isset($funcionario->nome){{$funcionario->nome}}@endisset" name="nome"
            type="text" placeholder="Nome Funcionário"
            class="form-control @error('nome') is-invalid @enderror">
        @if($errors->has('nome'))
            <label class="error jquery-validation-error small form-text invalid-feedback" style="color:red">
                @error('nome')
                    <span class="error">
                        {{ $message }}
                    </span>
                @enderror
            </label>
        @endif
    </div>

    <div class="form-group col-md-2">
        <label for="cpf">CPF:</label><label style="color:red">*</label>
        <input id="cpf" value="@isset($funcionario->cpf){{$funcionario->cpf}}@endisset" name="cpf"
            type="text" placeholder="CPF"
            class="form-control cpf @error('cpf') is-invalid @enderror">
        @if($errors->has('cpf'))
            <label class="error jquery-validation-error small form-text invalid-feedback"
            style="color:red">
                @error('cpf')
                    <span class="error">
                        {{ $message }}
                    </span>
                @enderror
            </label>
        @endif
    </div>

    <div class="form-group col-md-4">
        <label for="rg">RG:</label>
        <input id="rg" value="@isset($funcionario->rg){{$funcionario->rg}}@endisset" name="rg"
            type="text" placeholder="RG"
            class="form-control @error('rg') is-invalid @enderror">
    </div>

    <div class="form-group col-md-6">
        <label for="endereco">Endereço:</label>
        <input id="endereco" value="@isset($funcionario->endereco){{$funcionario->endereco}}@endisset" name="endereco"
            type="text" placeholder="Endereço"
            class="form-control @error('endereco') is-invalid @enderror">
    </div>

    <div class="form-group col-md-6">
        <label for="observacao">Observação:</label>
        <input id="observacao" value="@isset($funcionario->observacao){{$funcionario->observacao}}@endisset" name="observacao"
            type="text" placeholder="Observação"
            class="form-control @error('observacao') is-invalid @enderror">
    </div>

    <div class="form-group col-md-4">
        <label for="cidade">Cidade:</label>
            <select id="cidade_id" name="cidade_id" class="select2 form-control @error('cidade_id') is-invalid @enderror">
                @foreach ($cidades as $cidade)
                    <option value="{{$cidade->id}}"
                    @isset($empresas->cidade_id)
                        @if ($empresas->cidade_id == old('cidade_id', $cidade->id))
                            selected="selected"
                        @endif
                    @endisset
                    >{{$cidade->nome}}</option>
                @endforeach
            </select>

    </div>

    <div class="form-group col-md-4">
        <label for="funcao">Função:</label>
            <select id="funcao_id" name="funcao_id" class="form-control @error('funcao_id') is-invalid @enderror">
                @foreach ($funcoes as $funcao)
                    <option value="{{$funcao->id}}"
                    @isset($funcionario->funcao_id)
                        @if ($funcionario->funcao_id == old('funcao_id', $funcao->id))
                            selected="selected"
                        @endif
                    @endisset
                    >{{$funcao->nome}}</option>
                @endforeach
            </select>
    </div>


    <div class="form-group col-md-0">
        <label for="add"><br/></label>
        <button  data-toggle="modal"
        data-target="#novaFuncao" id="add" name="add"
        type="button" class="form-control btn-icon btn-warning"
        data-original-title="Cadastrar Função" title="Cadastrar Função">
        <i class="feather icon-plus"></i>
        </button>
    </div>

    <div class="form-group col-md-2">
        <label for="status">Status:</label>
        <br>
        <div class="switch switch-success d-inline m-r-10">
            <input name="status" type="checkbox" id="status"  @isset($funcionario)   @if ($funcionario->status) checked  @endif @endisset>
            <label for="status" class="cr"></label>
        </div>
    </div>

</div>
    <button id="acao" name="acao" type="submit" class="btn btn-primary">Salvar</button>

</form>


<div wire:ignore.self class="modal fade" id="novaFuncao" tabindex="-1" role="dialog"
    aria-labelledby="novaFuncao" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"  id="titulo">Cadastrar Nova Função</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
                <form name="formulario" id="formulario" action="{{route('admin.funcao.store')}}" method="post" >
                    @csrf
                    <div class="form-group">
                        <label for="nome_funcao">Nome:</label><label style="color:red">*</label>
                        <input type="hidden" name="check_edit" value="true" id="check_edit">
                        <input id="nome_funcao" value="@isset($funcao){{$funcao->nome_funcao}}@endisset"
                            name="nome_funcao" type="text" placeholder="Nome Função"
                            class="form-control @error('nome_funcao') is-invalid @enderror" required>
                            @if($errors->has('nome_funcao'))
                                <label class="error jquery-validation-error small form-text invalid-feedback" style="color:red">
                                    @error('nome_funcao')
                                        <span class="error">
                                            {{ $message }}
                                        </span>
                                    @enderror
                                </label>
                            @endif
                    </div>

                    <table id="scrolling-table" class="display table nowrap table-striped table-hover" style="width:100%">
                        <thead>
                            <tr>
                                <th>Nome</th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($funcoes as $funcao)
                                <tr role="row" class="odd">
                                    <td>{{$funcao->nome}}</td>
                                    <td style="width: 10px">
                                        <a id="teste" href="#" class="btn btn-icon btn-warning" data-toggle="tooltip"
                                            onclick="funcao('{{$funcao->nome}}')">
                                            <i class="far fa-edit"></i>
                                        </a>
                                    </td>
                                    <td style="width: 10px">
                                        <form action="{{route('admin.funcao.delete',[$funcao->id])}}" method="post">
                                            @csrf
                                            @method('delete')
                                            <a href="#" class="btn btn-icon btn-danger btn-delete" data-toggle="tooltip"
                                                data-original-title="Excluir Função: {{$funcao->nome}}">
                                                <i class="fas fa-trash-alt"></i>
                                            </a>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Sair</button>
                        <button type="submit" class="btn btn-primary btsalvar">Salvar</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>
@section('js')
    <script>
        $('#novaFuncao').on('shown.bs.modal', function(event) {
            $("#nome_funcao").focus();
        })
    </script>
    <script type="text/javascript">
        $(document).on('click', '.btn-delete', function (event) {
            event.preventDefault();

            var button = $(this);

            swal({
                buttons: {
                    cancel: "Cancelar!",
                    catch: {
                        text: "Sim",
                    },
                    defeat: true,
                },
                title: "Deseja excluir o registro?",
                text: "Você não poderá recuperar essa informação.",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then(function(isConfirm){
                if (isConfirm) {
                    button.closest("form").submit();
                }
            });
        });
    </script>
    <script>
        "use strict";
        function funcao(e) {
            $('#nome_funcao').val(e);
            $('#check_edit').val(e);
            $('#titulo').html("Editar Nova Função");
        }

    </script>

    <!-- Input mask Js -->
    <script src="{{asset('assets/plugins/inputmask/js/inputmask.min.js')}}"></script>
    <script src="{{asset('assets/plugins/inputmask/js/jquery.inputmask.min.js')}}"></script>
    <script src="{{asset('assets/plugins/inputmask/js/autoNumeric.js')}}"></script>
    <!-- form-picker-custom Js -->
    <script src="{{asset('assets/js/pages/form-masking-custom.js')}}"></script>
    <!-- Função validar Cpf_cnpj -->
    <script src="{{asset('assets/js/verifica_cpf_cnpj.js')}}"></script>

    <script>
        // Chamando a função validar CPF_CNPJ
        $("#cpf").focusout(function(){
            if($(this).val() != ""){
                if (formata_cpf_cnpj($("#cpf").val().trim()) ) {
                        // Deu certo não faz nada!
                    } else {
                        swal("Atenção","CPF Inválido!","warning",{
                        button: false,
                        });
                        $("#cpf").focus();
                    }
                }
        })
    </script>

    <script>
        // Função para desabilitar ação do enter no formulário.
        $(document).ready(function () {
            $('input').keypress(function (e) {
                    var code = null;
                    code = (e.keyCode ? e.keyCode : e.which);
                    return (code == 13) ? false : true;
            });
        });
    </script>

@endsection

