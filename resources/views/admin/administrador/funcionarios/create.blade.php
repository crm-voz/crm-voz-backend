@extends('layouts.admin')
@section('title', config('app.name', 'Voz CRM'))

@section('css')

@endsection

@section('PageHeader','Cadastro de Empresas')

@section('breadcrumb')
    <div class="row">
        <div class="col-8">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}"><i class="feather icon-home"></i></a></li>
                <li class="breadcrumb-item"><a href="{{route('admin.funcionarios.index')}}">Funcionários</a></li>
                <li class="breadcrumb-item"><a href="#!">Novo Funcionário</a></li>
            </ul>
        </div>
        <div class="col-4">
            <div class="text-right">
                <a href="{{route('admin.funcionarios.index')}}" class="btn btn-success" title=""
                    data-toggle="tooltip"
                        data-original-title="Voltar">Voltar</a>
            </div>
        </div>
    </div>
@endsection

@section('TituloCard','Novo Funcionário')

@section('content')
    <form action="{{route('admin.funcionarios.store')}}" method="post">
        @csrf
        @include('admin.administrador.funcionarios.formulario')
    </form>
@endsection

@section('js')

@endsection
