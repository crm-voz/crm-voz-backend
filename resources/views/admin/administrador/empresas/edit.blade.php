@extends('layouts.admin')
@section('title', config('app.name', 'Voz CRM'))

@section('css')

@endsection

@section('PageHeader','Atualização de Empresas')

@section('breadcrumb')
    <div class="row">
        <div class="col-8">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}"><i class="feather icon-home"></i></a></li>
                <li class="breadcrumb-item"><a href="{{route('admin.empresas.index')}}">Empresas</a></li>
                <li class="breadcrumb-item"><a href="#!">Atualizar Empresas</a></li>
            </ul>
        </div>
        <div class="col-4">
            <div class="text-right">
                <a href="{{route('admin.empresas.index')}}" class="btn btn-success" title=""
                data-toggle="tooltip" data-original-title="Voltar">Voltar</a>
            </div>
        </div>
    </div>
@endsection

@section('TituloCard','Atualizar Empresas')

@section('content')
<form action="{{route('admin.empresas.update',[$empresa->id])}}" method="post">
    @csrf
    @method('put')
    @include('admin.administrador.empresas.formulario')
</form>
@endsection

@section('js')

@endsection
