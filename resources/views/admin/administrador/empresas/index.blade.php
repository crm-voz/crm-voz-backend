@extends('layouts.admin')
@section('title', config('app.name', 'Voz CRM'))

@section('css')

@endsection

@section('PageHeader')
    Empresas
@endsection

@section('breadcrumb')
<div class="row">
    <div class="col-8">
        <ul class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}"><i class="feather icon-home"></i></a></li>
            <li class="breadcrumb-item"><a href="#!">Empresas</a></li>
        </ul>
    </div>
    <div class="col-4">
        <div class="text-right">
            <a href="{{route('admin.empresas.create')}}" class="btn btn-primary" title="" data-toggle="tooltip"
            data-original-title="Criar nova empresa">Nova Empresa</a>
        </div>
    </div>
</div>
@endsection

@section('TituloCard')
    Lista de Empresas
@endsection

@section('content')
    <div class="table-responsive">
        <table id="key-act-button" class="display table nowrap table-striped table-hover" style="width:100%">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Nome Fantasia</th>
                    <th>Razão Social</th>
                    <th>CNPJ</th>
                    <th>Inscrição Estadual</th>
                    <th>Cidade</th>
                    <th>UF</th>
                    <th>Endereço</th>
                    <th>Telefone</th>
                    <th>Celular</th>
                    <th>E-mail</th>
                    <th>Status</th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($dados as $empresa)
                    <tr role="row" class="odd">
                        <td>{{$empresa->id}}</td>
                        <td>{{$empresa->nome_fantasia}}</td>
                        <td>{{$empresa->razao_social}}</td>
                        <td>{{$empresa->cnpj}}</td>
                        <td>{{$empresa->inscricao_estadual}}</td>
                        <td>{{$empresa->cidade->nome}}</td>
                        <td>{{$empresa->cidade->uf}}</td>
                        <td>{{$empresa->endereco}}</td>
                        <td>{{$empresa->telefone}}</td>
                        <td>{{$empresa->celular}}</td>
                        <td>{{$empresa->email}}</td>
                        <td>
                            {{ $empresa->status ? 'Ativo' : 'Inativo' }}
                        </td>

                        <th style="width: 10px">
                            <a href="{{route('admin.empresas.edit',[$empresa->id])}}"
                                class="btn btn-icon btn-warning" data-toggle="tooltip" data-original-title="Atualizar Empresas">
                                <i class="far fa-edit"></i>
                            </a>
                        </th>

                        <th style="width: 10px">
                            <form action="{{route('admin.empresas.delete',[$empresa->id])}}" method="post">
                                @csrf
                                @method('delete')
                                <a href="#" class="btn btn-icon btn-danger btn-delete" data-toggle="tooltip"
                                    data-original-title="Excluir Empresa: {{$empresa->nome_fantasia}}">
                                    <i class="fas fa-trash-alt"></i>
                                </a>
                            </form>
                        </th>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection

@section('js')
    <script type="text/javascript">
        $(document).on('click', '.btn-delete', function (event) {
            event.preventDefault();

            var button = $(this);

            swal({
                buttons: {
                    cancel: "Cancelar!",
                    catch: {
                        text: "Sim",
                    },
                    defeat: true,
                },
                title: "Deseja excluir o registro?",
                text: "Você não poderá recuperar essa informação.",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then(function(isConfirm){
                if (isConfirm) {
                    button.closest("form").submit();
                }
            });
        });
    </script>

@endsection
