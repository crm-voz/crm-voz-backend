<div wire:ignore.self class="modal fade" id="editStatusOperacao" tabindex="-1" role="dialog"
    aria-labelledby="editStatusOperacao" style="display: none;" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"  id="titulo">Editar Novo Status Operação</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
                <form action="{{route('admin.status_operacao.update')}}" method="POST" name="formulario">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label for="nome">Nome:</label><label style="color:red">*</label>
                        <input type="hidden" id="edit_id" name="edit_id">
                        <input id="nome_s" name="nome" type="text" placeholder="Nome Status Operação"
                            class="form-control @error('nome') is-invalid @enderror" required>
                        @if($errors->has('nome'))
                            <label class="error jquery-validation-error small form-text invalid-feedback" style="color:red">
                                @error('nome')
                                    <span class="error">
                                        {{ $message }}
                                    </span>
                                @enderror
                            </label>
                        @endif
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Sair</button>
                        <button type="submit" class="btn btn-primary btsalvar">Salvar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@section('js')
    @parent
    <script>
        "use strict";
        function funcao(e,f) {
            $('#nome_s').val(e);
            $('#edit_id').val(f);
        }
    </script>

@endsection
