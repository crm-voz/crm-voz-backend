@extends('layouts.admin')
@section('title', config('app.name', 'Voz CRM'))

@section('css')

@endsection

@section('PageHeader')
    Status Operação
@endsection

@section('breadcrumb')
<div class="row">
    <div class="col-8">
        <ul class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}"><i class="feather icon-home"></i></a></li>
            <li class="breadcrumb-item"><a href="#!">Status Operação</a></li>
        </ul>
    </div>
    <div class="col-4">
        <div class="text-right">
            <button data-toggle="modal"
            data-target="#novoStatusOperacao" class="btn btn-primary" title="" data-toggle="tooltip"
            data-original-title="Criar novo status operação">Novo Status Operação</button>
        </div>
    </div>
</div>
@endsection

@section('TituloCard')
    Lista de Status Operação
@endsection

@section('content')

    @include('admin.administrador.status_operacao.create')
    @include('admin.administrador.status_operacao.edit')

    <div class="table-responsive">
        <table id="key-act-button" class="display table nowrap table-striped table-hover" style="width:100%">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Nome</th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($dados as $status_operacao)
                    <tr role="row" class="odd">
                        <td >{{$status_operacao->id}}</td>
                        <td>{{$status_operacao->nome}}</td>

                        <th style="width: 10px">
                            <a
                                href="#" data-toggle="modal" data-target="#editStatusOperacao"
                                class="btn btn-icon btn-warning" data-toggle="tooltip"
                                data-original-title="Atualizar Status Operação"
                                onclick="funcao('{{$status_operacao->nome}}','{{$status_operacao->id}}')">
                                <i class="far fa-edit"></i>
                            </a>
                        </th>
                        <th style="width: 10px">
                            <form action="{{route('admin.status_operacao.delete',[$status_operacao->id])}}" method="PUT">
                                <a  href="#" class="btn btn-icon btn-danger btn-delete" data-toggle="tooltip"
                                    data-original-title="Excluir Funcionário: {{$status_operacao->nome}}">
                                    <i class="fas fa-trash-alt"></i>
                                </a>
                            </form>
                        </th>
                    </tr>
                @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <th>Id</th>
                    <th>Numero Remessa</th>
                    <th></th>
                    <th></th>
                </tr>
            </tfoot>
        </table>
    </div>
@endsection

@section('js')
    <script type="text/javascript">
        $(document).on('click', '.btn-delete', function (event) {
            event.preventDefault();

            var button = $(this);

            swal({
                buttons: {
                    cancel: "Cancelar!",
                    catch: {
                        text: "Sim",
                    },
                    defeat: true,
                },
                title: "Deseja excluir o registro?",
                text: "Você não poderá recuperar essa informação.",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then(function(isConfirm){
                if (isConfirm) {
                    button.closest("form").submit();
                }
            });
        });
    </script>
@endsection
