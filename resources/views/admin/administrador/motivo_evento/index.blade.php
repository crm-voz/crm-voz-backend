@extends('layouts.admin')
@section('title', config('app.name', 'Voz CRM'))

@section('css')

@endsection

@section('PageHeader')
    Motivo Evento
@endsection

@section('breadcrumb')
<div class="row">
    <div class="col-8">
        <ul class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}"><i class="feather icon-home"></i></a></li>
            <li class="breadcrumb-item"><a href="#!">Motivo Evento</a></li>
        </ul>
    </div>
    <div class="col-4">
        <div class="text-right">
            <a href="{{route('admin.motivo_evento.create')}}" class="btn btn-primary" title="" data-toggle="tooltip"
            data-original-title="Criar novo motivo evento">Novo Motivo Evento</a>
        </div>
    </div>
</div>
@endsection

@section('TituloCard')
    Lista de Motivo Evento
@endsection

@section('content')
    <div class="table-responsive">
        <table id="key-act-button" class="display table nowrap table-striped table-hover" style="width:100%">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Nome</th>
                    <th>Cor</th>
                    <th>Status</th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($dados as $motivoEvento)
                    <tr role="row" class="odd">
                        <td>{{$motivoEvento->id}}</td>
                        <td>{{$motivoEvento->nome}}</td>
                        <td class="">
                            <label style="color:{{$motivoEvento->cor}}">
                                <button  data-toggle="tooltip" data-original-title="Cor: {{$motivoEvento->cor}}"
                                    class="btn btn-icon btn-rounded btn-primary"
                                    style="background-color:{{$motivoEvento->cor}} !important;border-color:#fff !important;"
                                    type="button">
                                </button>
                            </label>
                        </td>
                        <td>
                            {{ $motivoEvento->status ? 'Ativo' : 'Inativo' }}
                        </td>

                        <th style="width: 10px">
                            <a href="{{route('admin.motivo_evento.edit',[$motivoEvento->id])}}"
                                class="btn btn-icon btn-warning" data-toggle="tooltip"
                                data-original-title="Atualizar Motivo Evento">
                                <i class="far fa-edit"></i>
                            </a>
                        </th>
                        <th style="width: 10px">
                            <form action="{{route('admin.motivo_evento.delete',[$motivoEvento->id])}}" method="post">
                                @csrf
                                @method('delete')
                                <a href="#" class="btn btn-icon btn-danger btn-delete" data-toggle="tooltip"
                                    data-original-title="Excluir Motivo Evento: {{$motivoEvento->nome}}">
                                    <i class="fas fa-trash-alt"></i>
                                </a>
                            </form>
                        </th>
                    </tr>
                @endforeach
            </tbody>

        </table>
    </div>
@endsection

@section('js')
    <script type="text/javascript">
        $(document).on('click', '.btn-delete', function (event) {
            event.preventDefault();

            var button = $(this);

            swal({
                buttons: {
                    cancel: "Cancelar!",
                    catch: {
                        text: "Sim",
                    },
                    defeat: true,
                },
                title: "Deseja excluir o registro?",
                text: "Você não poderá recuperar essa informação.",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then(function(isConfirm){
                if (isConfirm) {
                    button.closest("form").submit();
                }
            });
        });
    </script>
@endsection
