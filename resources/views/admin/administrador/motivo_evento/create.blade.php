@extends('layouts.admin')
@section('title', config('app.name', 'Voz CRM'))

@section('css')
    <link rel="stylesheet" href="{{asset('assets/plugins/mini-color/css/jquery.minicolors.css')}}">
@endsection

@section('PageHeader','Cadastro de Motivo Evento')

@section('breadcrumb')
    <div class="row">
        <div class="col-8">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}"><i class="feather icon-home"></i></a></li>
                <li class="breadcrumb-item"><a href="{{route('admin.motivo_evento.index')}}">Motivo Evento</a></li>
                <li class="breadcrumb-item"><a href="#!">Novo Motivo Evento</a></li>
            </ul>
        </div>
        <div class="col-4">
            <div class="text-right">
                <a href="{{route('admin.motivo_evento.index')}}" class="btn btn-success"
                    title="" data-toggle="tooltip" data-original-title="Voltar">Voltar
                </a>
            </div>
        </div>
    </div>
@endsection

@section('TituloCard','Novo Motivo Evento')

@section('content')
    <form action="{{route('admin.motivo_evento.store')}}" method="post">
        @csrf
        @include('admin.administrador.motivo_evento.formulario')
    </form>
@endsection

@section('js')
<script src="http://momentjs.com/downloads/moment-with-locales.min.js"></script>
<script src="{{asset('assets/plugins/material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}"></script>

<!-- minicolors Js -->
<script src="{{asset('assets/plugins/mini-color/js/jquery.minicolors.min.js')}}"></script>

<!-- form-picker-custom Js -->
<script src="{{asset('assets/js/pages/form-picker-custom.js')}}"></script>
@endsection
