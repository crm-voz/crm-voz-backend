@extends('layouts.admin')
@section('title', config('app.name', 'Voz CRM'))

@section('css')

@endsection

@section('PageHeader')
    Status Localização
@endsection

@section('breadcrumb')
<div class="row">
    <div class="col-8">
        <ul class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">
                <i class="feather icon-home"></i></a></li>
            <li class="breadcrumb-item"><a href="#!">Status Localização</a></li>
        </ul>
    </div>
    <div class="col-4">
        <div class="text-right">
            <a href="{{route('admin.status_localizacao.create')}}" class="btn btn-primary" title=""
            data-toggle="tooltip" data-original-title="Criar novo Status Localização">Novo Status Localização</a>
        </div>
    </div>
</div>
@endsection

@section('TituloCard')
    Lista de Status Localização
@endsection

@section('content')
    <div class="table-responsive">
        <table id="key-act-button" class="display table nowrap table-striped table-hover" style="width:100%">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Nome</th>
                    <th>Cor</th>
                    <th>Status</th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($dados as $status_localizacao)
                    <tr role="row" class="odd">
                        <td class="">{{$status_localizacao->id}}</td>
                        <td class="">{{$status_localizacao->nome}}</td>
                        <td class="">
                            <label style="color:{{$status_localizacao->cor}}">
                                <button  data-toggle="tooltip" data-original-title="Cor: {{$status_localizacao->cor}}"
                                    class="btn btn-icon btn-rounded btn-primary"
                                    style="background-color:{{$status_localizacao->cor}} !important;border-color:#fff !important;"
                                    type="button">
                                </button>
                            </label>
                        </td>
                        <td class="">
                            @if ($status_localizacao->status == true)
                                Ativo
                            @endif
                            @if ($status_localizacao->status == false)
                                Inativo
                            @endif
                        </td>
                        <th class="" style="width: 10px">
                            <a href="{{route('admin.status_localizacao.edit',[$status_localizacao->id])}}"
                                class="btn btn-icon btn-warning" data-toggle="tooltip"
                                data-original-title="Atualizar Status Localização">
                                <i class="far fa-edit"></i>
                            </a>
                        </th>
                        <th class="" style="width: 10px">
                            <form action="{{route('admin.status_localizacao.delete',[$status_localizacao->id])}}" method="post">
                                @csrf
                                @method('delete')
                                <a href="#" class="btn btn-icon btn-danger btn-delete"
                                    data-toggle="tooltip" data-original-title="Excluir Status Localização: {{$status_localizacao->nome}}">
                                    <i class="fas fa-trash-alt"></i>
                                </a>
                            </form>
                        </th>
                    </tr>
                @endforeach
            </tbody>

        </table>
    </div>
@endsection

@section('js')
    <script type="text/javascript">
        $(document).on('click', '.btn-delete', function (event) {
            event.preventDefault();

            var button = $(this);

            swal({
                buttons: {
                    cancel: "Cancelar!",
                    catch: {
                        text: "Sim",
                    },
                    defeat: true,
                },
                title: "Deseja excluir o registro?",
                text: "Você não poderá recuperar essa informação.",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then(function(isConfirm){
                if (isConfirm) {
                    button.closest("form").submit();
                }
            });
        });
    </script>

@endsection
