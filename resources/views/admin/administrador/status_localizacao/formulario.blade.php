<div class="form-row">

    <div class="form-group col-md-6">
        <label for="nome">Nome:</label><label style="color:red">*</label>
        <input id="nome" value="@isset($status_localizacao){{$status_localizacao->nome}}@endisset" name="nome"
            type="text" placeholder="Nome Status localização"
            class="form-control @error('nome') is-invalid @enderror">
        @if($errors->has('nome'))
            <label class="error jquery-validation-error small form-text invalid-feedback"
            style="color:red">
                @error('nome')
                    <span class="error">
                        {{ $message }}
                    </span>
                @enderror
            </label>
        @endif
    </div>

    <div class="form-group col-md-2">
        <label for="cor">Cor:</label>
        <input style="height:44px !important" name="cor" type="text" id="brightness-demo" class="form-control demo"
            data-control="brightness" value="@isset($status_localizacao){{$status_localizacao->cor}}@endisset">
    </div>

    <div class="form-group col-md-2">
        <label for="status">Status:</label><label style="color:red">*</label>
        <br>
        <div class="switch switch-success d-inline m-r-10">
            @isset($status_localizacao)
                @if ($status_localizacao->status == true)
                    <input value="true" name="status" type="checkbox" id="switch-s-1" checked>
                @endif
                @if ($status_localizacao->status == false)
                    <input value="true" name="status" type="checkbox" id="switch-s-1">
                @endif
            @else
                <input value="true" name="status" type="checkbox" id="switch-s-1" checked>
            @endisset

            <label for="switch-s-1" class="cr"></label>
        </div>
        @if($errors->has('status'))
            <label class="error jquery-validation-error small form-text invalid-feedback"
            style="color:red">
                @error('status')
                    <span class="error">
                        {{ $message }}
                    </span>
                @enderror
            </label>
        @endif
    </div>

</div>
    <button id="acao" name="acao" type="submit" class="btn btn-primary">Salvar
    </button>


