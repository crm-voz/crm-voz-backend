@extends('layouts.admin')
@section('title', config('app.name', 'Voz CRM'))

@section('css')

@endsection

@section('PageHeader')
    Remessas
@endsection

@section('breadcrumb')
<div class="row">
    <div class="col-8">
        <ul class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}"><i class="feather icon-home"></i></a></li>
            <li class="breadcrumb-item"><a href="#!">Remessas</a></li>
        </ul>
    </div>
    <div class="col-4">
        <div class="text-right">
            <a href="{{route('admin.remessas.create')}}" class="btn btn-primary" title="" data-toggle="tooltip"
            data-original-title="Criar nova função">Nova Remessa</a>
        </div>
    </div>
</div>
@endsection

@section('TituloCard')
    Lista de Remessas
@endsection

@section('content')
    <div class="table-responsive">
        <table id="key-act-button" class="display table nowrap table-striped table-hover" style="width:100%">
            <thead>
                <tr>
                    <th>Numero Remessa</th>
                    <th>Data da Remessa</th>
                    <th>Data da Entrada</th>
                    <th>Valor Total</th>
                    <th>Nome do Arquivo</th>
                    <th>Credor</th>
                    <th>Usuário</th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($dados as $remessas)

                    <tr role="row" class="odd">

                        <td>{{$remessas->numero_remessa}}</td>
                        <td>{{ $remessas->data_remessa ?  $remessas->data_remessa->format('d/m/Y') : ''}}</td>
                        <td>{{ $remessas->data_entrada ? $remessas->data_entrada->format('d/m/Y') : ''}}</td>
                        <td>
                           @if ($remessas->operacoes->count() > 0)
                                   {{number_format($remessas->operacoes->sum('valor_nominal'),'2',',','.')}}
                           @else
                                R$ 0.00
                           @endif
                        </td>
                        <td>{{$remessas->nome_arquivo}}</td>
                        <td>
                            {{ $remessas->credor->nome }}
                        </td>
                        <td>
                           {{ $remessas->usuario_empresa->user->name }}
                        </td>

                        <th style="width: 10px">
                            <a href="{{route('admin.remessas.edit',[$remessas->id])}}"
                                class="btn btn-icon btn-warning" data-toggle="tooltip"
                                data-original-title="Atualizar Remessa">
                                <i class="far fa-edit"></i>
                            </a>
                        </th>

                        <th style="width: 10px">
                            <a href="#"
                                class="btn btn-icon btn-danger" data-toggle="tooltip"
                                data-original-title="Cancelar Remessa">
                                <i class="far fa-times-circle"></i>
                            </a>
                        </th>

                        <th style="width: 10px">
                            <form action="{{route('admin.remessas.delete',[$remessas->id])}}" method="post">
                                @csrf
                                @method('delete')
                                <a href="#" class="btn btn-icon btn-danger btn-delete" data-toggle="tooltip"
                                    data-original-title="Excluir Remessa: {{$remessas->nome}}">
                                    <i class="fas fa-trash-alt"></i>
                                </a>
                            </form>
                        </th>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection

@section('js')

    <script>
        $('.select2').select();
    </script>
    <script type="text/javascript">
        $(document).on('click', '.btn-delete', function (event) {
            event.preventDefault();

            var button = $(this);

            swal({
                buttons: {
                    cancel: "Cancelar!",
                    catch: {
                        text: "Sim",
                    },
                    defeat: true,
                },
                title: "Deseja excluir o registro?",
                text: "Você não poderá recuperar essa informação.",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then(function(isConfirm){
                if (isConfirm) {
                    button.closest("form").submit();
                }
            });
        });
    </script>
@endsection
