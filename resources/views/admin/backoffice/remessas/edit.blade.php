@extends('layouts.admin')
@section('title', config('app.name', 'Voz CRM'))

@section('css')

@endsection

@section('PageHeader','Atualização de Remessas')

@section('breadcrumb')
    <div class="row">
        <div class="col-8">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}"><i class="feather icon-home"></i></a></li>
                <li class="breadcrumb-item"><a href="{{route('admin.remessas.index')}}">Remessas</a></li>
                <li class="breadcrumb-item"><a href="#!">Atualizar Remessa</a></li>
            </ul>
        </div>
        <div class="col-4">
            <div class="text-right">
                <a href="{{route('admin.remessas.index')}}" class="btn btn-success" title=""
                data-toggle="tooltip" data-original-title="Voltar">Voltar</a>
            </div>
        </div>
    </div>
@endsection

@section('TituloCard','Atualizar Remessa')

@section('content')
<form action="{{route('admin.remessas.update',[$dados->id])}}" method="POST">
    @csrf
    @method('PUT')
    @include('admin.backoffice.remessas.formulario')
</form>
@endsection

@section('js')

@endsection
