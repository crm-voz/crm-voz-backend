<div class="form-row">

    <div class="form-group col-md-8">
        <label for="nome_arquivo">Nome do Arquivo</label>
        <input type="file" name="nome_arquivo" class="form-control">
    </div>

    <div class="form-group col-md-4">
        <label for="credor_id">Credor:</label><label style="color:red">*</label>
            <select id="credor_id" name="credor_id" class="form-control select2 @error('credor_id') is-invalid @enderror">
                @foreach ($credores as $nome_credor)
                    <option value="{{$nome_credor->id}}"
                    @isset($remessas->credor_id)
                        @if ($remessas->credor_id == old('credor_id', $nome_credor->id))
                            selected="selected"
                        @endif
                    @endisset
                    >{{$nome_credor->nome}}</option>
                @endforeach
            </select>
        @if($errors->has('credor_id'))
            <label class="error jquery-validation-error small form-text invalid-feedback"
            style="color:red">
                @error('credor_id')
                    <span class="error">
                        {{ $message }}
                    </span>
                @enderror
            </label>
        @endif
    </div>
</div>
    <button id="acao" name="acao" type="submit" class="btn btn-primary">Salvar</button>

