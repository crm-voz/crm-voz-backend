@extends('layouts.admin')
@section('title', config('app.name', 'Voz CRM'))

@section('css')

@endsection

@section('PageHeader','Cadastro de Cliente')

@section('breadcrumb')
<div class="row">
    <div class="col-8">
        <ul class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}"><i class="feather icon-home"></i></a></li>
            <li class="breadcrumb-item"><a href="{{route('admin.clientes.index')}}">Clientes</a></li>
            <li class="breadcrumb-item"><a href="#!">Novo Cliente</a></li>
       </ul>
    </div>
    <div class="col-4">
        <div class="text-right">
            <a href="{{route('admin.clientes.index')}}" class="btn btn-success" title="" data-toggle="tooltip" data-original-title="Voltar">Voltar</a>
        </div>
    </div>
</div>

@endsection

@section('TituloCard','Novo Cliente')

@section('content')
<form action="{{route('admin.clientes.store')}}" method="POST">
    @csrf
    @include('admin.backoffice.clientes.formulario')
</form>
@endsection

@section('js')

@endsection
