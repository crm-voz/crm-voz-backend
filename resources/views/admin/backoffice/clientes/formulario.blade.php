
<div class="form-row">
    <div class="form-group col-md-6">
        <label for="nome">Nome</label>
        <input type="text" class="form-control @if($errors->has('nome')) is-invalid @endif" id="nome" name="nome" placeholder="Nome do cliente" value="{{ (isset($cliente) ? $cliente->nome : '') }}">
        @if($errors->has('nome'))
            <label id="nome" class="error jquery-validation-error small form-text invalid-feedback" for="nome">{{ $errors->first('nome') }}</label>
        @endif

    </div>
    <div class="form-group col-md-6">
        <label for="cpf_cnpj">Cpf/CNPJ</label>
        <input type="text" class="form-control
            @if($errors->has('cpf_cnpj')) is-invalid @endif"
            id="cpf_cnpj" name="cpf_cnpj" placeholder="Cpf/Cnpj"
            value="{{ (isset($cliente) ? $cliente->cpf_cnpj : '') }}">
        @if($errors->has('cpf_cnpj'))
            <label id="cpf_cnpj" class="error jquery-validation-error small form-text invalid-feedback" for="cpf_cnpj">{{ $errors->first('cpf_cnpj') }}</label>
        @endif
    </div>
</div>
<div class="form-group">
    <label for="endereco">Endereço</label>
    <input type="text" class="form-control" id="endereco" name="endereco" placeholder="Endereço do cliente" value="{{ (isset($cliente) ? $cliente->endereco : '') }}">
</div>
<div class="form-row">
    <div class="form-group col-md-6">
        <label for="inputCity">City</label>
        <select id="cidades_id" name="cidades_id" class="select2 form-control">
            @foreach ($cidades as $cidade)
                <option value="{{$cidade->id}}"
                    @if(isset($cliente))
                        @if( $cliente->cidade_id == $cidade->id) selected @endif
                    @endif
                    >{{$cidade->nome}}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group col-md-4">
        <label for="bairro">Bairro</label>
        <input type="text" class="form-control  @if($errors->has('bairro')) is-invalid @endif" id="bairro" name="bairro" placeholder="Bairro do cliente" value="{{ (isset($cliente) ? $cliente->bairro : '') }}">
        @if($errors->has('bairro'))
            <label id="bairro" class="error jquery-validation-error small form-text invalid-feedback" for="bairro">{{ $errors->first('bairro') }}</label>
        @endif

    </div>
    <div class="form-group col-md-2">
        <label for="cep">Cep</label>
        <input type="text" class="form-control @if($errors->has('cep')) is-invalid @endif" id="cep" name="cep" placeholder="Cep do cliente" value="{{ (isset($cliente) ? $cliente->cep : '') }}">
        @if($errors->has('cep'))
            <label id="cep" class="error jquery-validation-error small form-text invalid-feedback" for="cep">{{ $errors->first('cep') }}</label>
        @endif
    </div>
</div>

<button type="submit" class="btn btn-primary">Salvar</button>

@section('js')
  <script>
     $(".select2").select2();
  </script>

  <!-- Função validar Cpf_cnpj -->
  <script src="{{asset('assets/js/verifica_cpf_cnpj.js')}}"></script>

  <script>
      // Chamando a função validar CPF_CNPJ
      $("#cpf_cnpj").focusout(function(){
          if($(this).val() != ""){
              if (formata_cpf_cnpj($("#cpf_cnpj").val().trim()) ) {
                      // Deu certo não faz nada!
                  } else {
                      swal("Atenção","CPF/CNPJ Inválido!","warning",{
                      button: false,
                      });
                      $("#cpf_cnpj").focus();
                  }
              }
      })
  </script>

  <script>
      // Função para desabilitar ação do enter no formulário.
      $(document).ready(function () {
          $('input').keypress(function (e) {
                  var code = null;
                  code = (e.keyCode ? e.keyCode : e.which);
                  return (code == 13) ? false : true;
          });
      });
  </script>
@endsection
