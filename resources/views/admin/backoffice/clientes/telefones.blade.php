@extends('layouts.admin')
@section('title', config('app.name', 'Voz CRM'))

@section('css')

@endsection

@section('PageHeader')
  Telefones do Cliente: {{ $cliente->nome}}
@endsection

@section('breadcrumb')
<div class="row">
    <div class="col-9">
        <ul class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}"><i class="feather icon-home"></i></a></li>
            <li class="breadcrumb-item"><a href="{{route('admin.clientes.index')}}">Clientes</a></li>
            <li class="breadcrumb-item"><a href="#!">Telefones</a></li>
        </ul>
    </div>
    <div class="col-2">
        <div class="text-right">
            <a href="{{ route('admin.clientes.telefones.create',[$cliente->id]) }}" class="btn btn-primary" title="">Novo Telefone</a>
        </div>
    </div>
    <div class="col-1">
        <div class="text-right">
            <a href="{{route('admin.clientes.index')}}" class="btn btn-success" title="" data-toggle="tooltip" data-original-title="Voltar">Voltar</a>
        </div>
    </div>
</div>

@endsection

@section('TituloCard')
    Lista de Telefones
@endsection

@section('content')
    <div id="key-act-button_wrapper" class="dataTables_wrapper dt-bootstrap4">
         <table id="key-act-button" class="display table nowrap table-striped table-hover dataTable" style="width: 100%;" role="grid" aria-describedby="key-act-button_info">
            <thead>
                <tr role="row">
                    <th class="sorting" tabindex="0" aria-controls="key-act-button" rowspan="1" colspan="1" aria-label="telefone: activate to sort column ascending">Telefone</th>
                    <th class="sorting" tabindex="0" aria-controls="key-act-button" rowspan="1" colspan="1" aria-label="origem: activate to sort column ascending">Origem</th>
                    <th class="sorting" tabindex="0" aria-controls="key-act-button" rowspan="1" colspan="1" aria-label="confirmado: activate to sort column ascending">Confirmado</th>
                    <th class="sorting" tabindex="0" aria-controls="key-act-button" rowspan="1" colspan="1" aria-label="confirmado: activate to sort column ascending">Tag</th>
                    <th class="sorting" tabindex="0" aria-controls="key-act-button" rowspan="1" colspan="1" aria-label="Data cadastro: activate to sort column ascending">Data cadastro</th>
                    <th class="sorting" tabindex="0" aria-controls="key-act-button" rowspan="1" colspan="1" aria-label="Status: activate to sort column ascending">Status</th>
                    <th class="sorting" tabindex="0" aria-controls="key-act-button" rowspan="1" colspan="1" aria-label="Observação: activate to sort column ascending">Observação</th>
                    <th class="" style="width: 10px"></th>
                    <th class="" style="width: 10px"></th>
                    <th class="" style="width: 10px"></th>
                </tr>
            </thead>
            <tbody>
              @if (!is_null($cliente->telefones))
                @foreach ($cliente->telefones as $telefone)
                    <tr role="row" class="odd">
                        <td class="" data-nome="{{ $telefone->telefone }}" >{{$telefone->telefone}}</td>
                        <td class="">{{$telefone->origem}}</td>
                        <td class="">
                            {{ $telefone->status ? 'True' : 'False'}}
                        </td>
                        <td class="">{{$telefone->tags}}</td>
                        <td class="">{{$telefone->created_at->format('d/m/Y')}}</td>
                        <td class="">
                            @if ($telefone->log_telefone->count() > 0)
                                {{ $telefone->log_telefone->last()->nome }}
                            @endif
                        </td>
                        <td class="">
                            @if ($telefone->log->count() > 0)
                                {{ $telefone->log->last()->observacao }}
                            @endif
                        </td>
                        <td class="" style="width: 10px">
                            <a href="{{ route('admin.clientes.telefones.edit',[$telefone->id]) }}" class="btn btn-icon btn-secondary load"><i class="fas fa-edit"></i></a>
                         </td>
                         <td class="" style="width: 10px">
                           <a href="#" class="btn btn-icon btn-info status" id="{{ $telefone->id }}" data-toggle="modal" data-original-title="Adicionar status ao telefone" data-target="#statusTelefone" data-whatever="@getbootstrap"><i class="fas fa-receipt"></i></a>
                        </td>
                        <td class="" style="width: 10px">
                            <form action="{{route('admin.clientes.telefones.delete',[$telefone->id])}}" method="post">
                                @csrf
                                @method('DELETE')
                                <a href="#" class="btn btn-icon btn-danger btn-delete" data-toggle="tooltip" data-original-title="Excluir telefone"><i class="fas fa-trash-alt"></i></a>
                            </form>
                        </td>
                    </tr>
                @endforeach
        @endif
            </tbody>
        </table>
    </div>
    @include('admin.backoffice.clientes.modal.telefone_status')
@endsection

@section('js')
<script src="{{ asset('assets/plugins/multi-select/js/jquery.quicksearch.js') }}"></script>
<script src="{{ asset('assets/plugins/multi-select/js/jquery.multi-select.js') }}"></script>
<script type="text/javascript">

    $( ".status" ).click(function() {
        $( "#telefone_id" ).val($(this).attr('id'));
    });

    $( ".btstatus" ).click(function() {
        $( "#status_form" ).submit();
    });

    $(document).on('click', '.btn-delete', function (event) {
        event.preventDefault();
        var button = $(this);

        swal({
            buttons: {
                cancel: "Cancelar!",
                catch: {
                    text: "Sim",
                },
                defeat: true,
            },
            title: "Deseja excluir o registro?",
            text: "Você não poderá recuperar essa informação.",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then(function(isConfirm){
            if (isConfirm) {
                button.closest("form").submit();
            }
        });
    });
</script>

@endsection
