<div class="modal fade" id="enviarEmail" tabindex="-1" role="dialog" aria-labelledby="novoEmail" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="enviarEmailLabel">Enviar Email</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
                <form id="email_enviar" method="POST" action="{{ route('admin.clientes.email.enviar',[$cliente->id]) }}" method="POST">
                    @csrf
                    <div class="form-group">
                        <input type="hidden" value="{{$cliente->id}}" id="cliente_id" name="cliente_id">
                        <label for="assunto" class="col-form-label">Assunto</label>
                        <input type="assunto" class="form-control" id="assunto" name="assunto" value="Email de Cobrança">
                    </div>
                    <div class="form-group">
                        <label for="conteudo" class="col-form-label">Conteúdo</label>
                        <textarea class="form-control" id="conteudo" name="conteudo" rows="3">Olá {{ $cliente->nome }}</textarea>
                    </div>
                    <div class="form-group">
                        <h4 class="col-form-label text-center">Lista de títulos</h4>
                    </div>
                    <table class="display table nowrap table-striped table-hover">
                        <thead>
                            <tr>
                                <td>Data Vencimento</td>
                                <td>Descrição</td>
                                <td>Valor R$</td>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($cliente->operacoes as $operacao)
                                <tr>
                                    <td >{{ $operacao->data_vencimento->format('d/m/Y') }}</td>
                                    <td >{{ $operacao->descricao }}</td>
                                    <td style="text-align: right">{{ number_format($operacao->valor_nominal,2,',','.') }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <td></td>
                                <td align="right">Total R$</td>
                                <td align="right">{{ number_format($cliente->operacoes->sum('valor_nominal'),2,',','.')}}</td>
                            </tr>
                        </tfoot>
                    </table>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-success btenviar">Enviar</button>
            </div>
        </div>
    </div>
</div>
