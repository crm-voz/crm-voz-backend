<div class="modal fade" id="novoEmail" tabindex="-1" role="dialog" aria-labelledby="novoEmail" style="display: none;" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Novo Email</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
                <form id="form_email" method="POST" action="{{route('admin.clientes.emails.add')}}">
                    @csrf
                    <div class="form-group">
                        <input type="hidden" value="{{$cliente->id}}" id="cliente_id" name="cliente_id">
                        <label for="email" class="col-form-label">Email</label>
                        <input type="email" class="form-control" id="email" name="email">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary btsalvar">Salvar</button>
            </div>
        </div>
    </div>
</div>
