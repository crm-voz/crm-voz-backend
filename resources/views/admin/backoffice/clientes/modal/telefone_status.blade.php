<div class="modal fade" id="statusTelefone" tabindex="-1" role="dialog" aria-labelledby="statusTelefone" style="display: none;" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Status Telefone</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
                <form id="status_form" method="POST" action="{{route('admin.clientes.telefones.status.update')}}">
                    @csrf
                    @method('put')
                    <div class="form-group">
                        <input type="hidden" name="telefone_id" id="telefone_id" value="">
                        <label for="telefone" class="col-form-label">Status</label>
                        <select name="status_id" id="status_id" class="form-control select2">
                            @foreach ($status as $st)
                                <option value="{{ $st->id }}">{{ $st->nome }}</option>
                            @endforeach
                        </select>

                    </div>
                    <div class="form-group">
                        <label for="telefone" class="col-form-label">Observação</label>
                        <textarea class="form-control" id="observacao" name="observacao"></textarea>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary btstatus">Salvar</button>
            </div>
        </div>
    </div>
</div>
