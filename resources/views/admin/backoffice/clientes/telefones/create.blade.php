@extends('layouts.admin')
@section('title', config('app.name', 'Voz CRM'))

@section('css')

@endsection

@section('PageHeader','Cadastro de Telefones')

@section('breadcrumb')
<div class="row">
    <div class="col-8">
        <ul class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}"><i class="feather icon-home"></i></a></li>
            <li class="breadcrumb-item"><a href="{{route('admin.clientes.index')}}">Clientes</a></li>
            <li class="breadcrumb-item"><a href="{{route('admin.clientes.telefones',[$cliente->id])}}">Telefones</a></li>
            <li class="breadcrumb-item"><a href="#!">Novo Telefone</a></li>
       </ul>
    </div>
    <div class="col-4">
        <div class="text-right">
            <a href="{{route('admin.clientes.telefones',[$cliente->id])}}" class="btn btn-success" title="" data-toggle="tooltip" data-original-title="Voltar">Voltar</a>
        </div>
    </div>
</div>

@endsection

@section('TituloCard','Novo Telefone')

@section('content')
<form id="form_email" method="POST" action="{{route('admin.clientes.telefones.post')}}">
    @csrf
    <div class="form-group">
        <input type="hidden" value="{{$cliente->id}}" id="cliente_id" name="cliente_id">
        <label for="telefone" class="col-form-label">Telefone</label>
        <input type="tel" class="form-control" id="telefone" name="telefone">
    </div>
    <div class="form-group">
        <label for="origem" class="col-form-label">Origem dos Dados</label>
        <select name="origem" id="origem" class="form-control">
            <option value="Credor">Credor</option>
            <option value="Enriquecimento">Enriquecimento</option>
            <option value="Atualização Cadastral">Atualização Cadastral</option>
        </select>
    </div>
    <div class="form-group">
        <label for="Tags" class="col-form-label">Tags</label>
        <select name="tags" id="tags" class="form-control">
            <option value="Fixo">Fixo</option>
            <option value="Móvel">Móvel</option>
            <option value="Móvel/Whatsapp">Móvel/Whatsapp</option>
        </select>
    </div>
    <div class="form-group">
        <label class="col-form-label">Confirmado</label><label style="color:red">*</label>
        <br>
        <div class="switch switch-success d-inline m-r-10">
            <input name="status" type="checkbox" id="status">
            <label for="status" class="cr"></label>
        </div>
    </div>
    <button type="submit" class="btn btn-primary">Salvar</button>
</form>

@endsection

@section('js')

@endsection
