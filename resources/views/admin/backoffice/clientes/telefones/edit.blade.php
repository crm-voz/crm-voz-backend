@extends('layouts.admin')
@section('title', config('app.name', 'Voz CRM'))

@section('css')

@endsection

@section('PageHeader','Atualizar Telefone')

@section('breadcrumb')
<div class="row">
    <div class="col-8">
        <ul class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}"><i class="feather icon-home"></i></a></li>
            <li class="breadcrumb-item"><a href="{{route('admin.clientes.index')}}">Clientes</a></li>
            <li class="breadcrumb-item"><a href="{{route('admin.clientes.telefones',[$cliente->id])}}">Telefones</a></li>
            <li class="breadcrumb-item"><a href="#!">Atualizar Telefone</a></li>
       </ul>
    </div>
    <div class="col-4">
        <div class="text-right">
            <a href="{{route('admin.clientes.telefones',[$cliente->id])}}" class="btn btn-success" title="" data-toggle="tooltip" data-original-title="Voltar">Voltar</a>
        </div>
    </div>
</div>

@endsection

@section('TituloCard','Atualizar Telefone')

@section('content')
<form id="form_email" method="POST" action="{{route('admin.clientes.telefones.update')}}">
    @csrf
    @method('put')

    <div class="form-group">
        <input type="hidden" value="{{$telefone->id}}" id="telefone_id" name="telefone_id">
        <label for="telefone" class="col-form-label">Telefone</label>
        <input type="tel" class="form-control" id="telefone" name="telefone" value="{{ $telefone->telefone }}">
    </div>
    <div class="form-group">
        <label for="origem" class="col-form-label">Origem dos Dados</label>
        <select name="origem" id="origem" class="form-control">
            <option value="Credor" {{ $telefone->origem_dados == 'Credor' ? 'selected' : '' }}>Credor</option>
            <option value="Enriquecimento" {{ $telefone->origem_dados == 'Enriquecimento' ? 'selected' : '' }}>Enriquecimento</option>
            <option value="Atualização Cadastral" {{ $telefone->origem_dados == 'Atualização Cadastral' ? 'selected' : '' }}>Atualização Cadastral</option>
        </select>
    </div>
    <div class="form-group">
        <label for="Tags" class="col-form-label">Tags</label>
        <select name="tags" id="tags" class="form-control">
            <option value="Fixo" {{ $telefone->tags == 'Fixo' ? 'selected' : '' }}>Fixo</option>
            <option value="Móvel" {{ $telefone->tags == 'Móvel' ? 'selected' : '' }}>Móvel</option>
            <option value="Móvel/Whatsapp" {{ $telefone->tags == 'Móvel/Whatsapp' ? 'selected' : '' }}>Móvel/Whatsapp</option>
        </select>
    </div>
    <div class="form-group">
        <label class="col-form-label">Confirmado</label><label style="color:red">*</label>
        <br>
        <div class="switch switch-success d-inline m-r-10">
            <input name="status" type="checkbox" id="status" {{ $telefone->status ? 'checked' : ''}}>
            <label for="status" class="cr"></label>
        </div>
    </div>
    <button type="submit" class="btn btn-primary">Salvar</button>
</form>

@endsection

@section('js')

@endsection
