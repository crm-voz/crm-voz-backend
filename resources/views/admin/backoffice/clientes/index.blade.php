@extends('layouts.admin')
@section('title', config('app.name', 'Voz CRM'))

@section('css')

@endsection

@section('PageHeader')
    Clientes
@endsection

@section('breadcrumb')
    <div class="row">
        <div class="col-8">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}"><i class="feather icon-home"></i></a></li>
                <li class="breadcrumb-item"><a href="#!">Clientes</a></li>
            </ul>
        </div>
        <div class="col-4">
            <div class="text-right">
                <a href="{{route('admin.clientes.create')}}" class="btn btn-primary" title="" data-toggle="tooltip" data-original-title="Criar novo cliente">Novo Cliente</a>
            </div>
        </div>
    </div>
@endsection

@section('TituloCard')
       Lista de Clientes
@endsection

@section('content')
<div class="table-responsive">
    <div id="key-act-button_wrapper" class="dataTables_wrapper dt-bootstrap4">
        <table id="key-act-button" class="display table nowrap table-striped table-hover dataTable" style="width: 100%;" role="grid" aria-describedby="key-act-button_info">
            <thead>
                <tr role="row">
                    <th class="sorting" tabindex="0" aria-controls="key-act-button" rowspan="1" colspan="1" aria-label="Name: activate to sort column ascending">Nome</th>
                    <th class="sorting" tabindex="0" aria-controls="key-act-button" rowspan="1" colspan="1" aria-label="cpf/Cnpj: activate to sort column ascending">Cpf/Cnpj</th>
                    <th class="sorting" tabindex="0" aria-controls="key-act-button" rowspan="1" colspan="1" aria-label="Endereço: activate to sort column ascending">Endereço</th>
                    <th class="sorting" tabindex="0" aria-controls="key-act-button" rowspan="1" colspan="1" aria-label="Cidade: activate to sort column ascending">Cidade</th>
                    <th class="sorting" tabindex="0" aria-controls="key-act-button" rowspan="1" colspan="1" aria-label="UF date: activate to sort column ascending">UF</th>
                    <th class="" style="width: 10px"></th>
                    <th class="" style="width: 10px"></th>
                    <th class="" style="width: 10px"></th>
                    <th class="" style="width: 10px"></th>
                    <th class="" style="width: 20px"></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($clientes as $cliente)
                    <tr role="row" class="odd">
                        <td class="">{{$cliente->nome}}</td>
                        <td class="">{{$cliente->cpf_cnpj}}</td>
                        <td class="">{{$cliente->endereco}}</td>
                        <td class="">{{$cliente->cidades->nome}}</td>
                        <td class="">{{$cliente->cidades->uf}}</td>
                        <th class="" style="width: 10px"><a href="{{route('admin.clientes.edit',[$cliente->id])}}" class="btn btn-icon btn-warning" data-toggle="tooltip" data-original-title="Atualizar cliente"><i class="far fa-edit"></i></a></th>
                        <th class="" style="width: 10px">
                            <form action="{{route('admin.clientes.delete',[$cliente->id])}}" method="post">
                                <a href="#" class="btn btn-icon btn-danger btn-delete" data-toggle="tooltip" data-original-title="Excluir cliente: {{$cliente->nome}}"><i class="fas fa-trash-alt"></i></a>
                            </form>
                        </th>
                        <td class="" style="width: 10px"><a href="{{route('admin.clientes.emails',[$cliente->id])}}" class="btn btn-icon btn-primary" data-toggle="tooltip" data-original-title="Lista de emails do cliente"><i class="fas fa-envelope-square"></i></a></td>
                        <td class="" style="width: 10px"><a href="{{route('admin.clientes.telefones',[$cliente->id])}}" class="btn btn-icon btn-info" data-toggle="tooltip" data-original-title="Lista de telefones do cliente"><i class="fas fa-phone"></i></a></td>
                        <td class="" style="width: 20px"><a href="{{route('admin.operacoes.index',[$cliente->id])}}" class="btn shadow-2 btn-success" data-toggle="tooltip" data-original-title="Operações do cliente">Operações</a></td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

@endsection

@section('js')
<script type="text/javascript">
    $(document).on('click', '.btn-delete', function (event) {
        event.preventDefault();

        var button = $(this);

        swal({
            buttons: {
                cancel: "Cancelar!",
                catch: {
                    text: "Sim",
                },
                defeat: true,
            },
            title: "Deseja excluir o registro?",
            text: "Você não poderá recuperar essa informação.",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then(function(isConfirm){
            if (isConfirm) {
                button.closest("form").submit();
            }
        });
    });
</script>

@endsection
