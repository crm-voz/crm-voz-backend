@extends('layouts.admin')
@section('title', config('app.name', 'Voz CRM'))

@section('css')

@endsection

@section('PageHeader')
    Emails do Cliente: {{ $cliente->nome}}
@endsection

@section('breadcrumb')
<div class="row">
    <div class="col-7">
        <ul class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}"><i class="feather icon-home"></i></a></li>
            <li class="breadcrumb-item"><a href="{{route('admin.clientes.index')}}">Clientes</a></li>
            <li class="breadcrumb-item"><a href="#!">Emails</a></li>
        </ul>
    </div>
    <div class="col-4">
        <div class="text-right">
            <a href="#" class="btn btn-warning" data-toggle="modal" data-target="#enviarEmail" data-whatever="@getbootstrap"><i class="far fa-envelope"></i>Enviar Email</a>
            <a href="#" class="btn btn-primary" title=""  data-toggle="modal" data-target="#novoEmail" data-whatever="@getbootstrap">Novo Email</a>
        </div>
    </div>
    <div class="col-1">
        <div class="text-right">
            <a href="{{route('admin.clientes.index')}}" class="btn btn-success" title="" data-toggle="tooltip" data-original-title="Voltar">Voltar</a>
        </div>
    </div>
</div>
@endsection

@section('TituloCard')
    Lista de Emails
@endsection

@section('content')
    <div id="key-act-button_wrapper" class="dataTables_wrapper dt-bootstrap4">
        <table id="key-act-button" class="display table nowrap table-striped table-hover dataTable" style="width: 100%;" role="grid" aria-describedby="key-act-button_info">
            <thead>
                <tr role="row">
                    <th class="sorting" tabindex="0" aria-controls="key-act-button" rowspan="1" colspan="1" aria-label="Email: activate to sort column ascending">Email</th>
                    <th class="sorting" tabindex="0" aria-controls="key-act-button" rowspan="1" colspan="1" aria-label="Data cadastro: activate to sort column ascending">Data cadastro</th>
                    <th class="" style="width: 20px"></th>
                </tr>
            </thead>
            <tbody>
              @if (!is_null($cliente->emails))
                @foreach ($cliente->emails as $email)
                    <tr role="row" class="odd">
                        <td class="">{{$email->email}}</td>
                        <td class="">{{$email->created_at->format('d/m/Y')}}</td>
                        <td class="" style="width: 20px">
                            <form action="{{route('admin.clientes.emails.delete',[$email->id])}}" method="post">
                                @csrf
                                @method('DELETE')
                                <a href="#" class="btn btn-icon btn-danger btn-delete"><i class="fas fa-trash-alt"></i></a>
                            </form>
                        </td>
                    </tr>
                @endforeach
        @endif
            </tbody>
        </table>
    </div>

    @include('admin.backoffice.clientes.modal.add_email')
    @include('admin.backoffice.clientes.modal.enviar_email')

@endsection

@section('js')
<script type="text/javascript">
    $( ".btsalvar" ).click(function() {
        $( "#form_email" ).submit();
    });

    $( ".btenviar" ).click(function() {
        $( "#email_enviar" ).submit();
    });

    $(document).on('click', '.btn-delete', function (event) {
        event.preventDefault();
        var button = $(this);
        swal({
            buttons: {
                cancel: "Cancelar!",
                catch: {
                    text: "Sim",
                },
                defeat: true,
            },
            title: "Deseja excluir o registro?",
            text: "Você não poderá recuperar essa informação.",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then(function(isConfirm){
            if (isConfirm) {
                button.closest("form").submit();
            }
        });
    });
</script>
@endsection
