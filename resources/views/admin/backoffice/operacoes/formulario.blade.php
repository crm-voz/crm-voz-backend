<div class="form-row">
    <input type="hidden" name="cliente_id" id="cliente_id" value="{{ $cliente[0]->id }}">
    <div class="col-md-6">
        <label for="credores_id">Credor</label>
        <select name="credores_id" id="credores_id" class="form-control select2">
            @foreach ($credores as $credor)
                <option value="{{$credor->id}}"
                    @if(isset($operacao))
                      @if ($credor->id == $operacao->remessa->credor_id) selected @endif
                    @endif
                >{{$credor->nome}}</option>

            @endforeach
        </select>
    </div>
    <div class="col-md-6">
        <label for="remessa_id">Remessas</label>
        <select name="remessa_id" id="remessa_id" class="form-control select2">
            @foreach ($remessas as $remessa)
                <option value="{{$remessa->id}}"
                    @if(isset($operacao))
                    @if ($remessa->id == $operacao->remessa->id) selected @endif
                    @endif
                >{{$remessa->numero_remessa}}</option>
            @endforeach
        </select>
    </div>
</div>
<div class="form-row">
    <div class="form-group col-md-3">
        <label for="numero_operacao">Número Operação</label>
        <input type="text" class="form-control @if($errors->has('numero_operacao')) is-invalid @endif"
        id="numero_operacao" name="numero_operacao" placeholder="Número da operação"
        value=" {{ isset($operacao) ? $operacao->numero_operacao : ''}}">
        @if($errors->has('numero_operacao'))
            <label id="numero_operacao" class="error jquery-validation-error small form-text invalid-feedback" for="numero_operacao">{{ $errors->first('numero_operacao') }}</label>
        @endif

    </div>

    <div class="form-group col-md-3">
        <label for="valor_nominal">Valor Nominal R$</label>
        <input type="text" class="form-control @if($errors->has('valor_nominal')) is-invalid @endif"
        id="valor_nominal" name="valor_nominal" placeholder="Valor Nominal R$"
        value=" {{ isset($operacao) ? $operacao->valor_nominal : ''}}">
        @if($errors->has('valor_nominal'))
            <label id="valor_nominal" class="error jquery-validation-error small form-text invalid-feedback"
            for="valor_nominal">{{ $errors->first('valor_nominal') }}</label>
        @endif
    </div>
    <div class="form-group col-md-3">
        <label for="valor_atualizado">Valor Atualizado R$</label>
        <input type="text" class="form-control  @if($errors->has('valor_atualizado')) is-invalid @endif"
        id="valor_divida" name="valor_atualizado" placeholder="Valor atualizado R$"
        value=" {{isset($operacao) ?  $operacao->valor_atualizado : ''}}">
        @if($errors->has('valor_atualizado'))
            <label id="valor_atualizado" class="error jquery-validation-error small form-text invalid-feedback"
            for="valor_atualizado">{{ $errors->first('valor_atualizado') }}</label>
        @endif
    </div>
    <div class="form-group col-md-3">
        <label for="data_vencimento">Data de Vencimento</label>
        <input type="date" class="form-control @if($errors->has('data_vencimento')) is-invalid @endif"
        id="data_vencimento" name="data_vencimento" placeholder="Data de vencimento"
        value="{{ isset($operacao) ? $operacao->data_vencimento->format('Y-m-d') : ''}}">
        @if($errors->has('data_vencimento'))
            <label id="data_vencimento" class="error jquery-validation-error small form-text invalid-feedback"
            for="data_vencimento">{{ $errors->first('data_vencimento') }}</label>
        @endif
    </div>

</div>
<div class="form-row">
    <div class="col-md-4">
        <label for="motivo_pendencia_id">Motivo Pendência</label>
        <select id="motivo_pendencia_id" name="motivo_pendencia_id" class="select2 form-control">
            @foreach ($MotivoPendencia as $mp)
                <option value="{{$mp->id}}"
                    @if(isset($operacao))
                        @if ($mp->id == $operacao->motivo_pendencia->id) selected @endif
                    @endif
                >{{$mp->nome}}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group col-md-4">
        <label for="status_operacao">Status da Operação</label>
        <select id="status_operacao_id" name="status_operacao_id" class="select2 form-control">
            @foreach ($statusOperacao as $so)
                <option value="{{$so->id}}"
                    @if(isset($operacao))
                        @if ($so->id == $operacao->status_operacao->id) selected @endif
                    @endif
                >{{$so->nome}}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group col-md-4">
        <label for="classificacao_negativacao">Classificação Negativação</label>
        <select id="classificacao_negativacao_id" name="classificacao_negativacao_id" class="select2 form-control">
            @foreach ($classificacaoNegativacao as $cn)
                <option value="{{$cn->id}}"
                    @if(isset($operacao))
                        @if ($cn->id == $operacao->classificacao_negativacao->id) selected @endif
                    @endif
                >{{$cn->nome}}</option>
            @endforeach
        </select>
    </div>
</div>


<div class="form-group">
    <label for="descricao">Descrição</label>
    <input type="text" class="form-control @if($errors->has('descricao')) is-invalid @endif" id="descricao" name="descricao" placeholder="Descrição da operação" value=" {{ isset($operacao)? $operacao->descricao : ''}}">
    @if($errors->has('descricao'))
        <label id="descricao" class="error jquery-validation-error small form-text invalid-feedback" for="descricao">{{ $errors->first('descricao') }}</label>
    @endif
</div>


<button type="submit" class="btn btn-primary">Salvar</button>

@section('js')
  <script>
     $(".select2").select2();

     $(document).on('click', '#credores_id', function (event) {
         console.log('value' + $(this).val());
     });
  </script>
@endsection
