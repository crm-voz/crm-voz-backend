@extends('layouts.admin')
@section('title', config('app.name', 'Voz CRM'))

@section('css')

@endsection

@section('PageHeader')
    Operaçãoes
@endsection

@section('breadcrumb')
<div class="row">
    <div class="col-8">
        <ul class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}"><i class="feather icon-home"></i></a></li>
            <li class="breadcrumb-item"><a href="{{route('admin.clientes.index')}}">Clientes</a></li>
            <li class="breadcrumb-item"><a href="#">Operações</a></li>
        </ul>
    </div>
    <div class="col-4">
        <div class="text-right">
            <div class="text-right">
                <a href="{{route('admin.operacoes.create',[$cliente[0]->id])}}" class="btn btn-primary" title="" data-toggle="tooltip" data-original-title="Cria uma nova operação">Nova Operação</a>
            </div>
        </div>
    </div>
</div>

@endsection

@section('TituloCard')
    Lista de operaçãoes:  {{$cliente[0]->nome}}
@endsection

@section('content')
<div class="table-responsive">
    <div id="key-act-button_wrapper" class="dataTables_wrapper dt-bootstrap4">
        <table id="key-act-button" class="display table nowrap table-striped table-hover dataTable" style="width: 100%;" role="grid" aria-describedby="key-act-button_info">
            <thead>
                <tr role="row">
                    <th class="sorting" tabindex="0" aria-controls="key-act-button" rowspan="1" colspan="1" aria-label="Operação: activate to sort column ascending">Operação</th>
                    <th class="sorting" tabindex="0" aria-controls="key-act-button" rowspan="1" colspan="1" aria-label="Remessa: activate to sort column ascending">Remessa</th>
                    <th class="sorting" tabindex="0" aria-controls="key-act-button" rowspan="1" colspan="1" aria-label="Data Vencimento: activate to sort column ascending">Data Vencimento</th>
                    <th class="sorting" tabindex="0" aria-controls="key-act-button" rowspan="1" colspan="1" aria-label="Data Processamento: activate to sort column ascending">Data Processamento</th>
                    <th class="sorting" tabindex="0" aria-controls="key-act-button" rowspan="1" colspan="1" aria-label="Valor Nominal: activate to sort column ascending">Valor Nominal R$</th>
                    <th class="sorting" tabindex="0" aria-controls="key-act-button" rowspan="1" colspan="1" aria-label="Valor Atualizado: activate to sort column ascending">Valor Atualizado R$</th>
                    <th class="sorting" tabindex="0" aria-controls="key-act-button" rowspan="1" colspan="1" aria-label="Status operação: activate to sort column ascending">Status</th>
                    <th class="" style="width: 10px"></th>
                    <th class="" style="width: 10px"></th>

                </tr>
            </thead>
            <tbody>
                @foreach ($operacoes as $operacao)
                    <tr role="row" class="odd">
                        <td class="">{{$operacao->numero_operacao}}</td>
                        <td class="">{{$operacao->remessa->numero_remessa}}</td>
                        <td class="">{{$operacao->data_vencimento->format('d/m/Y')}}</td>
                        <td class="">{{$operacao->data_processamento->format('d/m/Y')}}</td>
                        <td class="">R$ {{ number_format($operacao->valor_nominal,'2',',','.') }}</td>
                        <td class="">R$ {{ number_format($operacao->valor_atualizado,'2',',','.') }}</td>
                        <td class=""><span class="badge badge-primary">{{ $operacao->status_operacao->nome }}</span></td>
                        <th class="" style="width: 10px"><a href="{{route('admin.operacoes.edit',[$operacao->id])}}" class="btn btn-icon btn-warning" data-toggle="tooltip" data-original-title="Atualizar Operação"><i class="far fa-edit"></i></a></th>
                        <th class="" style="width: 10px">
                            <form action="{{route('admin.operacoes.delete',[$operacao->id])}}" method="post">
                                @csrf
                                @method('DELETE')
                                <a href="#" class="btn btn-icon btn-danger btn-delete" data-toggle="tooltip" data-original-title="Excluir Operação: {{$operacao->numero_operacao}}"><i class="fas fa-trash-alt"></i></a>
                            </form>
                        </th>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

@endsection

@section('js')
<script type="text/javascript">
    $(document).on('click', '.btn-delete', function (event) {
        event.preventDefault();

        var button = $(this);

        swal({
            buttons: {
                cancel: "Cancelar!",
                catch: {
                    text: "Sim",
                },
                defeat: true,
            },
            title: "Deseja excluir o registro?",
            text: "Você não poderá recuperar essa informação.",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then(function(isConfirm){
            if (isConfirm) {
                button.closest("form").submit();
            }
        });
    });
</script>

@endsection
