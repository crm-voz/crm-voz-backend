@extends('layouts.admin')
@section('title', config('app.name', 'Voz CRM'))

@section('css')

@endsection

@section('PageHeader','Operações do Cliente')

@section('breadcrumb')
<div class="row">
    <div class="col-8">
        <ul class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}"><i class="feather icon-home"></i></a></li>
            <li class="breadcrumb-item"><a href="{{route('admin.clientes.index')}}">Clientes</a></li>
            <li class="breadcrumb-item"><a href="{{route('admin.operacoes.index')}}">Operações</a></li>
            <li class="breadcrumb-item"><a href="#!">Nova Operação</a></li>
        </ul>
    </div>
    <div class="col-4">
        <div class="text-right">
            <a href="{{route('admin.clientes.index')}}" class="btn btn-success" title="" data-toggle="tooltip"
            data-original-title="Voltar">Voltar</a>
        </div>
    </div>
</div>
@endsection

@section('TituloCard','Cliente: ' . $cliente[0]->nome)

@section('content')
    <form action="{{route('admin.operacoes.store')}}" method="POST">
        @csrf
        @include('admin.backoffice.operacoes.formulario')
    </form>
@endsection
