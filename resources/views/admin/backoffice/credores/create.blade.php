@extends('layouts.admin')
@section('title', config('app.name', 'Voz CRM'))

@section('css')

@endsection

@section('PageHeader','Cadastro de Credores')

@section('breadcrumb')
    <div class="row">
        <div class="col-8">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}"><i class="feather icon-home"></i></a></li>
                <li class="breadcrumb-item"><a href="{{route('admin.credores.index')}}">Credores</a></li>
                <li class="breadcrumb-item"><a href="#!">Nova Credor</a></li>
            </ul>
        </div>
        <div class="col-4">
            <div class="text-right">
                <a href="{{route('admin.credores.index')}}" class="btn btn-success" title="" data-toggle="tooltip"
                data-original-title="Voltar">Voltar</a>
            </div>
        </div>
    </div>
@endsection

@section('TituloCard','Nova Empresa')

@section('content')
    <form action="{{route('admin.credores.store')}}" method="post">
        @csrf
        @include('admin.backoffice.credores.formulario')
    </form>
@endsection

@section('js')

@endsection
