<div class="form-row">
    <div class="form-group col-md-6">
        <label for="nome">Nome:</label><label style="color:red">*</label>
        <input id="nome" value="@isset($credores){{$credores->nome}}@endisset" name="nome"
            type="text" placeholder="Nome Credor"
            class="form-control @error('nome') is-invalid @enderror">
        @if($errors->has('nome'))
            <label class="error jquery-validation-error small form-text invalid-feedback" style="color:red">
                @error('nome')
                    <span class="error">
                        {{ $message }}
                    </span>
                @enderror
            </label>
        @endif
    </div>

    <div class="form-group col-md-6">
        <label for="razao_social">Razão Social:</label><label style="color:red">*</label>
        <input id="razao_social" value="@isset($credores){{$credores->razao_social}}@endisset" name="razao_social"
            type="text" placeholder="Razão Social"
            class="form-control @error('razao_social') is-invalid @enderror">
        @if($errors->has('razao_social'))
            <label class="error jquery-validation-error small form-text invalid-feedback"
            style="color:red">
                @error('razao_social')
                    <span class="error">
                        {{ $message }}
                    </span>
                @enderror
            </label>
        @endif
    </div>

    <div class="form-group col-md-2">
        <label for="cnpj">CNPJ:</label><label style="color:red">*</label>
        <input id="cnpj" value="@isset($credores){{$credores->cnpj}}@endisset" name="cnpj"
            type="text" placeholder="CNPJ"
            class="form-control cnpj @error('cnpj') is-invalid @enderror">
        @if($errors->has('cnpj'))
            <label class="error jquery-validation-error small form-text invalid-feedback"
            style="color:red">
                @error('cnpj')
                    <span class="error">
                        {{ $message }}
                    </span>
                @enderror
            </label>
        @endif
    </div>

    <div class="form-group col-md-2">
        <label for="inscricao_estadual">Inscrição Estadual:</label>
        <input id="inscricao_estadual" value="@isset($credores){{$credores->inscricao_estadual}}@endisset" name="inscricao_estadual"
            type="text" placeholder="Inscrição Estadual"
            class="form-control @error('inscricao_estadual') is-invalid @enderror">
        @if($errors->has('inscricao_estadual'))
            <label class="error jquery-validation-error small form-text invalid-feedback"
            style="color:red">
                @error('inscricao_estadual')
                    <span class="error">
                        {{ $message }}
                    </span>
                @enderror
            </label>
        @endif
    </div>

    <div class="form-group col-md-4">
        <label for="cidade">Cidade:</label><label style="color:red">*</label>
            <select id="cidade_id" name="cidade_id" class="form-control @error('cidade_id') is-invalid @enderror">
                @foreach ($cidades as $cidade)
                    <option value="{{$cidade->id}}"
                    @isset($credores->cidade_id)
                        @if ($credores->cidade_id == old('cidade_id', $cidade->id))
                            selected="selected"
                        @endif
                    @endisset
                    >{{$cidade->nome}}</option>
                @endforeach
            </select>
        @if($errors->has('cidade'))
            <label class="error jquery-validation-error small form-text invalid-feedback"
            style="color:red">
                @error('cidade')
                    <span class="error">
                        {{ $message }}
                    </span>
                @enderror
            </label>
        @endif
    </div>

    <div class="form-group col-md-4">
        <label for="endereco">Endereço:</label>
        <input id="endereco" value="@isset($credores){{$credores->endereco}}@endisset" name="endereco"
        type="text" placeholder="Endereço"
        class="form-control @error('endereco') is-invalid @enderror">
        @if($errors->has('endereco'))
            <label class="error jquery-validation-error small form-text invalid-feedback"
            style="color:red">
                @error('endereco')
                    <span class="error">
                        {{ $message }}
                    </span>
                @enderror
            </label>
        @endif
    </div>

    <div class="form-group col-md-2">
        <label for="telefone">Telefone:</label>
        <input id="telefone" value="@isset($credores){{$credores->telefone}}@endisset" name="telefone"
        type="text" placeholder="Telefone" data-mask="(99) 99999-9999"
        class="form-control telphone_with_code @error('telefone') is-invalid @enderror">
        @if($errors->has('telefone'))
            <label class="error jquery-validation-error small form-text invalid-feedback"
            style="color:red">
                @error('telefone')
                    <span class="error">
                        {{ $message }}
                    </span>
                @enderror
            </label>
        @endif
    </div>

    <div class="form-group col-md-2">
        <label for="celular">Celular:</label>
        <input id="celular" value="@isset($credores){{$credores->celular}}@endisset" name="celular"
        type="text" placeholder="Celular" data-mask="(99) 99999-9999"
        class="form-control telphone_with_code @error('celular') is-invalid @enderror">
        @if($errors->has('celular'))
            <label class="error jquery-validation-error small form-text invalid-feedback"
            style="color:red">
                @error('celular')
                    <span class="error">
                        {{ $message }}
                    </span>
                @enderror
            </label>
        @endif
    </div>

    <div class="form-group col-md-4">
        <label for="email">E-mail:</label><label style="color:red">*</label>
        <input id="email" value="@isset($credores){{$credores->email}}@endisset" name="email"
        type="text" placeholder="E-mail"
        class="form-control @error('email') is-invalid @enderror">
        @if($errors->has('email'))
            <label class="error jquery-validation-error small form-text invalid-feedback"
            style="color:red">
                @error('email')
                    <span class="error">
                        {{ $message }}
                    </span>
                @enderror
            </label>
        @endif
    </div>

    <div class="form-group col-md-4">
        <label for="empresa_id">Empresa:</label><label style="color:red">*</label>
            <select id="empresa_id" name="empresa_id" class="form-control @error('empresa_id') is-invalid @enderror">
                @foreach ($empresas as $empresa)
                    <option value="{{$empresa->id}}"
                    @isset($credores->empresa_id)
                        @if ($credores->empresa_id == old('empresa_id', $empresa->id))
                            selected="selected"
                        @endif
                    @endisset
                    >{{$empresa->nome_fantasia}}</option>
                @endforeach
            </select>
        @if($errors->has('empresa_id'))
            <label class="error jquery-validation-error small form-text invalid-feedback"
            style="color:red">
                @error('empresa_id')
                    <span class="error">
                        {{ $message }}
                    </span>
                @enderror
            </label>
        @endif
    </div>

    <div class="form-group col-md-2">
        <label for="status">Status:</label><label style="color:red">*</label>
        <br>
        <div class="switch switch-success d-inline m-r-10">
            <input name="status" type="checkbox" id="status"  @isset($credores)  @if ($credores->status) checked @endif @endisset checked>
            <label for="status" class="cr"></label>
        </div>
        @if($errors->has('status'))
            <label class="error jquery-validation-error small form-text invalid-feedback"
            style="color:red">
                @error('status')
                    <span class="error">
                        {{ $message }}
                    </span>
                @enderror
            </label>
        @endif
    </div>

</div>
    <button id="acao" name="acao" type="submit" class="btn btn-primary">Salvar</button>

    @section('js')
    <!-- Input mask Js -->
    <script src="{{asset('assets/plugins/inputmask/js/inputmask.min.js')}}"></script>
    <script src="{{asset('assets/plugins/inputmask/js/jquery.inputmask.min.js')}}"></script>
    <script src="{{asset('assets/plugins/inputmask/js/autoNumeric.js')}}"></script>
    <!-- form-picker-custom Js -->
    <script src="{{asset('assets/js/pages/form-masking-custom.js')}}"></script>
    <!-- Função validar Cpf_cnpj -->
    <script src="{{asset('assets/js/verifica_cpf_cnpj.js')}}"></script>

    <script>
        // Chamando a função validar CPF_CNPJ
        $("#cnpj").focusout(function(){
            if($(this).val() != ""){
                if (formata_cpf_cnpj($("#cnpj").val().trim()) ) {
                        // Deu certo não faz nada!
                    } else {
                        swal("Atenção","CNPJ Inválido!","warning",{
                        button: false,
                        });
                        $("#cnpj").focus();
                    }
                }
        })
    </script>

    <script>
        // Função para desabilitar ação do enter no formulário.
        $(document).ready(function () {
            $('input').keypress(function (e) {
                    var code = null;
                    code = (e.keyCode ? e.keyCode : e.which);
                    return (code == 13) ? false : true;
            });
        });
    </script>

@endsection
