@extends('layouts.admin')
@section('title', config('app.name', 'Voz CRM'))

@section('css')

@endsection

@section('PageHeader','Devoluções')

@section('breadcrumb')
<div class="row">
    <div class="col-8">
        <ul class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}"><i class="feather icon-home"></i></a></li>
            <li class="breadcrumb-item"><a href="{{route('admin.operacoes.index')}}">Devoluão</a></li>
            <li class="breadcrumb-item"><a href="#!">Nova Devolução</a></li>
        </ul>
    </div>
    <div class="col-4">
        <div class="text-right">
            <a href="{{route('admin.clientes.index')}}" class="btn btn-success" title="" data-toggle="tooltip"
            data-original-title="Voltar">Voltar</a>
        </div>
    </div>
</div>
@endsection

@section('TituloCard','Devolução de Operações')

@section('content')
    @include('admin.backoffice.devolucao.formulario')
@endsection

@section('js')
    <script type="text/javascript">
        $(".select2").select2();
    </script>
@endsection
