<form id="form_devolucao" method="POST" action="{{route('admin.devolucao.store')}}">
    @csrf
    <div class="form-group">
        <label for="motivo_devolucao_id">Motivo da devolução</label>
        <select id="motivo_devolucao_id" name="motivo_devolucao_id" class="select2 form-control">
            @foreach ($motivos_devolucao as $motivo_devolucao)
                <option value="{{$motivo_devolucao->id}}">{{$motivo_devolucao->nome}}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label for="descricao" class="col-form-label">Descrição</label>
        <textarea name="descricao" id="descricao"class="form-control"></textarea>
    </div>

    <div class="form-group">
        <div class="table-responsive">
            <div id="key-act-button_wrapper" class="dataTables_wrapper dt-bootstrap4">
                <table id="key-act-button" class="display table nowrap table-striped table-hover dataTable" style="width: 100%;" role="grid" aria-describedby="key-act-button_info">
                    <thead>
                        <tr role="row">
                            <th style="width: 3px">
                                <div class="checkbox checkbox-primary checkbox-fill d-inline">
                                    <input type="checkbox" name="check_todos" id="check_todos">
                                    <label for="check_todos" class="cr"></label>
                                </div>
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="key-act-button" rowspan="1" colspan="1" aria-label="Operação: activate to sort column ascending">Operação</th>
                            <th class="sorting" tabindex="0" aria-controls="key-act-button" rowspan="1" colspan="1" aria-label="Remessa: activate to sort column ascending">Remessa</th>
                            <th class="sorting" tabindex="0" aria-controls="key-act-button" rowspan="1" colspan="1" aria-label="Cliente: activate to sort column ascending">Cliente</th>
                            <th class="sorting" tabindex="0" aria-controls="key-act-button" rowspan="1" colspan="1" aria-label="Data Vencimento: activate to sort column ascending">Data Vencimento</th>
                            <th class="sorting" tabindex="0" aria-controls="key-act-button" rowspan="1" colspan="1" aria-label="Data Processamento: activate to sort column ascending">Data Processamento</th>
                            <th class="sorting" tabindex="0" aria-controls="key-act-button" rowspan="1" colspan="1" aria-label="Valor Nominal: activate to sort column ascending">Valor Nominal R$</th>
                            <th class="sorting" tabindex="0" aria-controls="key-act-button" rowspan="1" colspan="1" aria-label="Valor Atualizado: activate to sort column ascending">Valor Atualizado R$</th>
                            <th class="sorting" tabindex="0" aria-controls="key-act-button" rowspan="1" colspan="1" aria-label="Status operação: activate to sort column ascending">Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($operacoes as $operacao)
                            <tr role="row" class="odd">
                                <td style="width: 3px" id="{{ $operacao->id }}">
                                    <div class="checkbox checkbox-primary checkbox-fill d-inline">
                                        <input type="checkbox" name="ch{{ $operacao->id }}" id="ch{{ $operacao->id }}">
                                        <label for="ch{{ $operacao->id }}" class="cr"></label>
                                    </div>
                                </td>
                                <td class="">{{$operacao->numero_operacao}}</td>
                                <td class="">{{$operacao->remessa->numero_remessa}}</td>
                                <td class="">{{$operacao->cliente->nome}}</td>
                                <td class="">{{$operacao->data_vencimento->format('d/m/Y')}}</td>
                                <td class="">{{$operacao->data_processamento->format('d/m/Y')}}</td>
                                <td class="">R$ {{ number_format($operacao->valor_nominal,'2',',','.') }}</td>
                                <td class="">R$ {{ number_format($operacao->valor_atualizado,'2',',','.') }}</td>
                                <td class=""><span class="badge badge-primary">{{ $operacao->status_operacao->nome }}</span></td>
                            </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-10">

        </div>
        <div class="col-md-2 text-right">
            <button type="submit" class="btn btn-primary">Salvar</button>
        </div>
    </div>
</form>

@section('js')
<script type="text/javascript">

    $('#check_todos').click(function(){
         if($(this).is(':checked')){
            $('table tbody tr').each(function(index){
                $(this).find("input[type='checkbox']").attr({checked: true});
            });
         }else{
            $('table tbody tr').each(function(index){
                $(this).find("input[type='checkbox']").attr({checked: false});
            });
         }
    });

</script>

@endsection
