@extends('layouts.admin')
@section('title', config('app.name', 'Voz CRM'))

@section('css')

@endsection

@section('PageHeader')
    Devoluções
@endsection

@section('breadcrumb')
<div class="row">
    <div class="col-8">
        <ul class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}"><i class="feather icon-home"></i></a></li>
            <li class="breadcrumb-item"><a href="#">Devolução</a></li>
        </ul>
    </div>
    <div class="col-4">
        <div class="text-right">
            <div class="text-right">
                <a href="{{ route('admin.devolucao.create') }}" class="btn btn-primary" title="" data-toggle="tooltip"
                data-original-title="Devolver cobrança">Nova Devolução</a>
            </div>
        </div>
    </div>
</div>

@endsection

@section('TituloCard')
    Devolução
@endsection

@section('content')
<div class="table-responsive">
    <div id="key-act-button_wrapper" class="dataTables_wrapper dt-bootstrap4">
        <table id="key-act-button" class="display table nowrap table-striped table-hover dataTable" style="width: 100%;" role="grid" aria-describedby="key-act-button_info">
            <thead>
                <tr role="row">
                    <th class="sorting" tabindex="0" aria-controls="key-act-button" rowspan="1" colspan="1" aria-label="Status: activate to sort column ascending">Motivo Devolução</th>
                    <th class="sorting" tabindex="0" aria-controls="key-act-button" rowspan="1" colspan="1" aria-label="Cliente: activate to sort column ascending">Descrição</th>
                    <th class="sorting" tabindex="0" aria-controls="key-act-button" rowspan="1" colspan="1" aria-label="Cliente: activate to sort column ascending">Usuario</th>
                    <th class="sorting" tabindex="0" aria-controls="key-act-button" rowspan="1" colspan="1" aria-label="Operação: activate to sort column ascending">Data</th>
                    <th class="sorting" tabindex="0" aria-controls="key-act-button" rowspan="1" colspan="1" aria-label="Operação: activate to sort column ascending">Valor R$</th>
                    <th class=""></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($devolucoes as $devolucao)
                    <tr role="row" class="odd">
                        <td class="">{{$devolucao->motivo_devolucao->nome}}</td>
                        <td class="">{{$devolucao->descricao}}</td>
                        <td class="">{{$devolucao->usuario_empresas->user->name}}</td>
                        <td class="">{{$devolucao->created_at->format('d/m/Y h:m:s')}}</td>
                        <td class="">
                            @if ($devolucao->has('operacoes'))
                                R$ {{number_format($devolucao->operacoes->sum('valor_nominal'),'2',',','.')}}
                            @endif
                        </td>
                        <td class=""><a href="{{route('admin.devolucao.detalhe',[$devolucao->id])}}" class="btn btn-icon btn-secondary" data-toggle="tooltip" data-original-title="Detalhes da Devolução"><i class="far fa-edit"></i></a></td>
                     </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

@endsection

@section('js')
<script type="text/javascript">
    $(".select2").select2();

    $(document).on('click', '.btn-delete', function (event) {
        event.preventDefault();

        var button = $(this);

        swal({
            buttons: {
                cancel: "Cancelar!",
                catch: {
                    text: "Sim",
                },
                defeat: true,
            },
            title: "Deseja excluir o registro?",
            text: "Você não poderá recuperar essa informação.",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then(function(isConfirm){
            if (isConfirm) {
                button.closest("form").submit();
            }
        });
    });
</script>

@endsection
