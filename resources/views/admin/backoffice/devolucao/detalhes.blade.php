@extends('layouts.admin')
@section('title', config('app.name', 'Voz CRM'))

@section('css')

@endsection

@section('PageHeader')
    Detalhe da Devolução
@endsection

@section('breadcrumb')
<div class="row">
    <div class="col-8">
        <ul class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}"><i class="feather icon-home"></i></a></li>
            <li class="breadcrumb-item"><a href="{{route('admin.devolucao.index')}}">Devolução</a></li>
            <li class="breadcrumb-item"><a href="#">Detalhe da devolução</a></li>
        </ul>
    </div>
    <div class="col-4">
        <div class="text-right">

        </div>
    </div>
</div>

@endsection

@section('TituloCard')
    Devolução Número: <strong>{{ $devolucao[0]->id }} </strong> - Data: <strong>{{ $devolucao[0]->created_at->format('d/m/Y H:m:s') }}</strong>
@endsection

@section('content')
<div id="key-act-button_wrapper" class="dataTables_wrapper dt-bootstrap4">
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label for="descricao">Descrição da devolução</label>
                <textarea class="form-control">{{ $devolucao[0]->descricao }}</textarea>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <div class="form-group">
                <label for="motivo">Motivo da devolução</label>
                <span class="form-control">{{ $devolucao[0]->motivo_devolucao->nome }}</span>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="usuario">Usuário Responsável</label>
                <span class="form-control">{{ $devolucao[0]->usuario_empresas->user->name }}</span>
            </div>
        </div>
    </div>

</div>
<div class="table-responsive">

        <table id="key-act-button" class="display table nowrap table-striped table-hover dataTable" style="width: 100%;" role="grid" aria-describedby="key-act-button_info">
            <thead>
                <tr role="row">
                    <th class="sorting" tabindex="0" aria-controls="key-act-button" rowspan="1" colspan="1" aria-label="Operação: activate to sort column ascending">Operação</th>
                    <th class="sorting" tabindex="0" aria-controls="key-act-button" rowspan="1" colspan="1" aria-label="Remessa: activate to sort column ascending">Remessa</th>
                    <th class="sorting" tabindex="0" aria-controls="key-act-button" rowspan="1" colspan="1" aria-label="Remessa: activate to sort column ascending">Usuário Remessa</th>
                    <th class="sorting" tabindex="0" aria-controls="key-act-button" rowspan="1" colspan="1" aria-label="Data Vencimento: activate to sort column ascending">Data Vencimento</th>
                    <th class="sorting" tabindex="0" aria-controls="key-act-button" rowspan="1" colspan="1" aria-label="Data Processamento: activate to sort column ascending">Data Processamento</th>
                    <th class="sorting" tabindex="0" aria-controls="key-act-button" rowspan="1" colspan="1" aria-label="Valor Nominal: activate to sort column ascending">Valor Nominal R$</th>
                    <th class="sorting" tabindex="0" aria-controls="key-act-button" rowspan="1" colspan="1" aria-label="Valor Atualizado: activate to sort column ascending">Valor Atualizado R$</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($devolucao[0]->operacoes as $operacao)
                    <tr role="row" class="odd">
                        <td class="">{{$operacao->numero_operacao}}</td>
                        <td class="">{{$operacao->remessa->numero_remessa}}</td>
                        <td class="">{{$operacao->remessa->usuario_empresa->user->name}}</td>
                        <td class="">{{$operacao->data_vencimento->format('d/m/Y')}}</td>
                        <td class="">{{$operacao->data_processamento->format('d/m/Y')}}</td>
                        <td class="">R$ {{ number_format($operacao->valor_nominal,'2',',','.') }}</td>
                        <td class="">R$ {{ number_format($operacao->valor_atualizado,'2',',','.') }}</td>

                    </tr>
                @endforeach
            </tbody>
        </table>

</div>

@endsection

@section('js')
<script type="text/javascript">
    $(".select2").select2();

    $(document).on('click', '.btn-delete', function (event) {
        event.preventDefault();

        var button = $(this);

        swal({
            buttons: {
                cancel: "Cancelar!",
                catch: {
                    text: "Sim",
                },
                defeat: true,
            },
            title: "Deseja excluir o registro?",
            text: "Você não poderá recuperar essa informação.",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then(function(isConfirm){
            if (isConfirm) {
                button.closest("form").submit();
            }
        });
    });
</script>

@endsection
