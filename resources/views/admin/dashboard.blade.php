@extends('layouts.admin')
@section('title')
    Voz CRM
@endsection

@section('css')

@endsection

@section('PageHeader')
    Dashboard
@endsection

@section('breadcrumb')
<div class="row">
    <div class="col-8">
        <ul class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}"><i class="feather icon-home"></i></a></li>
            <li class="breadcrumb-item"><a href="#!">Sample Page</a></li>
        </ul>
    </div>
    <div class="col-4">
        <div class="text-right">
            <div class="text-right">
                <a href="#" class="btn btn-primary" title="" data-toggle="tooltip" data-original-title="Cria uma nova operação">Nova Operação</a>
            </div>
        </div>
    </div>
</div>

@endsection

@section('TituloCard')
    Meu card incrivel
@endsection

@section('content')

@endsection

@section('js')

@endsection
