@component('mail::message')
<p> {{ $dados['conteudo'] }} </p>

@component('mail::button', ['url' => 'https://grupovoz.com.br/'])
  Acesse nosso site
@endcomponent

Atenciosamente
{{ config('app.name') }}
@endcomponent
