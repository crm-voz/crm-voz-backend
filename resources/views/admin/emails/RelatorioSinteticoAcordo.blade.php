@component('mail::message')
{{-- <p> {{ $data['obrservacao'] }}</p> --}}

@component('mail::table')
    <table summary="Demonstrativo de Acordo Sintético">
        <caption>VOZ GESTÃO DE COBRANÇA </caption>
        <thead>
            <tr>
                <th colspan="6">Demonstrativo Sintético Negociação</th>
            </tr>
            <tr>
                <td colspan="6">
                    <b>Credor:</b> {{ $data['credor'] }} <br>
                    <b>Cliente:</b> {{ $data['cliente'] }}
                    <b>CPF:</b> {{ $data['cpf_cnpj'] }}<br>
                    <b>Endereço:</b> {{ $data['endereco'] }} <br>
                    <b>Cidade:</b> {{ $data['cidade'] }}-{{ $data['uf'] }}
                </td>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <td colspan="6"></td>
            </tr>
        </tfoot>
        <tr>
            <th colspan="6">Acordos</th>
        </tr>
        <tbody style='text-align:center;'>
            <tr>
                <th><b>Nº. Acordo</b></th>
                <th><b>Dt. Acordo</b></th>
                <th><b>Valor</b></th>
                <th><b>Desconto</b></th>
                <th><b>Vlr. Total</b></th>
            </tr>
            <tr >
                <td>{{ $data['numero_acordo'] }}</td>
                <td>{{ date( 'd/m/Y' , strtotime($data['data_acordo']))}}</td>
                <td>{{ $data['valor_acordo'] }} </td>
                <td>{{ $data['desconto_acordo'] }} </td>
                <td>{{ $data['valor_parcela_acordo'] }} </td>
            </tr>
            <tr>
                <th colspan="6">Parcelas</th>
            </tr>
            <tr>
                <th><b>Nº Parcela<b></th>
                <th><b>Dt.Vencimento<b></th>
                <th><b>Valor<b></th>
                <th><b>Vlr. Recebido<b></th>
                <th><b>Dt. Recebido<b></th>
            </tr>
            @foreach($datad as $parcelas)
                <tr>
                    <td>{{ $parcelas->numero_parcela }}</td>
                    <td>{{ date( 'd/m/Y' , strtotime($parcelas->data_vencimento))}}</td>
                    <td>{{ $parcelas->valor_nominal }}</td>
                    <td>{{ $parcelas->valor_baixado }}</td>
                    @if(empty($parcelas->data_baixa))
                        <td></td>
                    @else
                    <td>{{ date( 'd/m/Y' , strtotime($parcelas->data_baixa))}}</td>
                    @endif
                </tr>
                <tr>
                    <td colspan="6">L. Digitável: {{ $parcelas->linha_digitavel }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endcomponent

@component('mail::panel')
    {{-- observações a ser observado. --}}
@endcomponent

@component('mail::button', ['url' => 'https://grupovoz.com.br/'], ['color' => 'blue'])
  Acesse nosso site
@endcomponent

Atenciosamente,
    Grupo Voz.
@endcomponent
