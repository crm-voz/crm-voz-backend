@component('mail::message')
<p> {{ $dados['conteudo'] }} </p>

@component('mail::table')
    <table class="display table nowrap table-striped table-hover">
        <thead>
            <tr>
                <td>Data Vencimento</td>
                <td>Descrição</td>
                <td>Valor R$</td>
            </tr>
        </thead>
        <tbody>
            @foreach($operacoes as $operacao)
                <tr>
                    <td >{{ $operacao->data_vencimento->format('d/m/Y') }}</td>
                    <td >{{ $operacao->descricao }}</td>
                    <td style="text-align: right">{{ number_format($operacao->valor_nominal,2,',','.') }}</td>
                </tr>
            @endforeach
        </tbody>
        <tfoot>
            <tr>
                <td></td>
                <td align="right">Total R$</td>
                <td align="right">{{ number_format($operacoes->sum('valor_nominal'),2,',','.')}}</td>
            </tr>
        </tfoot>
    </table>
@endcomponent

@component('mail::button', ['url' => 'https://grupovoz.com.br/'])
  Acesse nosso site
@endcomponent

Atenciosamente
{{ config('app.name') }}
@endcomponent
