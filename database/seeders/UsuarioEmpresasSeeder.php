<?php

namespace Database\Seeders;

use App\Models\Core\UsuarioEmpresa;
use Illuminate\Database\Seeder;

class UsuarioEmpresasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        UsuarioEmpresa::create([
           'id'=> 1,
            'user_id' => 1,
            'status' => true,
            'empresa_id' => 1
        ]);

        UsuarioEmpresa::create([
            'id'=> 2,
            'user_id' => 2,
            'status' => true,
            'empresa_id' => 1
        ]);

        UsuarioEmpresa::create([
            'id'=> 3,
            'user_id' => 3,
            'status' => true,
            'empresa_id' => 1
        ]);

        UsuarioEmpresa::create([
            'id'=> 4,
            'user_id' => 4,
            'status' => true,
            'empresa_id' => 1
        ]);

        UsuarioEmpresa::create([
            'id'=> 5,
            'user_id' => 5,
            'status' => true,
            'empresa_id' => 1
        ]);

        UsuarioEmpresa::create([
            'id'=> 6,
            'user_id' => 6,
            'status' => true,
            'empresa_id' => 1
        ]);

        UsuarioEmpresa::create([
            'id'=> 7,
            'user_id' => 7,
            'status' => true,
            'empresa_id' => 1
        ]);

        UsuarioEmpresa::create([
            'id'=> 8,
            'user_id' => 8,
            'status' => true,
            'empresa_id' => 1
        ]);

        UsuarioEmpresa::create([
            'id'=> 9,
            'user_id' => 9,
            'status' => true,
            'empresa_id' => 1
        ]);

        UsuarioEmpresa::create([
            'id'=> 10,
            'user_id' => 10,
            'status' => true,
            'empresa_id' => 1
        ]);

        UsuarioEmpresa::create([
            'id'=> 11,
            'user_id' => 11,
            'status' => true,
            'empresa_id' => 1
        ]);

        UsuarioEmpresa::create([
            'id'=> 12,
            'user_id' => 12,
            'status' => true,
            'empresa_id' => 1
        ]);

        UsuarioEmpresa::create([
            'id'=> 13,
            'user_id' => 13,
            'status' => true,
            'empresa_id' => 1
        ]);

        UsuarioEmpresa::create([
            'id'=> 14,
            'user_id' => 14,
            'status' => true,
            'empresa_id' => 1
        ]);

        UsuarioEmpresa::create([
            'id'=> 15,
            'user_id' => 15,
            'status' => true,
            'empresa_id' => 1
        ]);

        UsuarioEmpresa::create([
            'id'=> 16,
            'user_id' => 16,
            'status' => true,
            'empresa_id' => 1
        ]);

        UsuarioEmpresa::create([
            'id'=> 17,
            'user_id' => 17,
            'status' => true,
            'empresa_id' => 1
        ]);

        UsuarioEmpresa::create([
            'id'=> 18,
            'user_id' => 18,
            'status' => true,
            'empresa_id' => 1
        ]);

        UsuarioEmpresa::create([
            'id'=> 19,
            'user_id' => 19,
            'status' => true,
            'empresa_id' => 1
        ]);

        UsuarioEmpresa::create([
            'id'=> 20,
            'user_id' => 20,
            'status' => true,
            'empresa_id' => 1
        ]);

        UsuarioEmpresa::create([
            'id'=> 21,
            'user_id' => 21,
            'status' => true,
            'empresa_id' => 1
        ]);
    }
}
