<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\BackOffice\Cliente;

class ClientesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Cliente::create([
            "nome" => "Teste",
            "cpf_cnpj" => "60860860868",
            "endereco" => "Rua João de Barro",
            "cep" => "65900-630",
            "cidades_id" => 1,
            'localizacao' => 'Localizado'
        ]);
    }
}
