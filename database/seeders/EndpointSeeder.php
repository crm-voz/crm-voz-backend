<?php

namespace Database\Seeders;

use App\Models\Core\Endpoints;
use Illuminate\Database\Seeder;

class EndpointSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Endpoints de remessas
        Endpoints::create([
            'grupo' => 'Remessa',
            'nome' => 'index',
            'url' => 'remessas/'
        ]);
        Endpoints::create([
            'grupo' => 'Remessa',
            'nome' => 'create',
            'url' => 'remessas/create'
        ]);
        Endpoints::create([
            'grupo' => 'Remessa',
            'nome' => 'update',
            'url' => 'remessas/edit/'
        ]);
        Endpoints::create([
            'grupo' => 'Remessa',
            'nome' => 'excluir',
            'url' => 'remessas/delete/'
        ]);
        Endpoints::create([
            'grupo' => 'Remessa',
            'nome' => 'store',
            'url' => 'remessas/store/'
        ]);
        Endpoints::create([
            'grupo' => 'Remessa',
            'nome' => 'importar',
            'url' => 'remessas/importar'
        ]);
        Endpoints::create([
            'grupo' => 'Remessa',
            'nome' => 'cancelar',
            'url' => 'remessas/cancelar/'
        ]);
        Endpoints::create([
            'grupo' => 'Remessa',
            'nome' => 'credor',
            'url' => 'operacoes/remessasCredor/'
        ]);
        Endpoints::create([
            'grupo' => 'Remessa',
            'nome' => 'chart',
            'url' => 'remessas/total_remessas_periodo_chart'
        ]);

        //Endpoints de clientes
        Endpoints::create([
            'grupo' => 'Clientes',
            'nome' => 'index',
            'url' => 'clientes/'
        ]);
        Endpoints::create([
            'grupo' => 'Clientes',
            'nome' => 'create',
            'url' => 'clientes/create'
        ]);
        Endpoints::create([
            'grupo' => 'Clientes',
            'nome' => 'update',
            'url' => 'clientes/edit/'
        ]);
        Endpoints::create([
            'grupo' => 'Clientes',
            'nome' => 'delete',
            'url' => 'clientes/delete/'
        ]);
        Endpoints::create([
            'grupo' => 'Clientes',
            'nome' => 'store',
            'url' => 'clientes/store'
        ]);
        Endpoints::create([
            'grupo' => 'Clientes',
            'nome' => 'filtro',
            'url' => 'clientes/filtro/'
        ]);
        Endpoints::create([
            'grupo' => 'Clientes',
            'nome' => 'updateLoteStatusClientes',
            'url' => 'clientes/updatelot/'
        ]);
        Endpoints::create([
            'grupo' => 'Clientes',
            'nome' => 'updateLoteStatusClientesMult',
            'url' => 'clientes/update_lot_status_cliente_mult/'
        ]);
        Endpoints::create([
            'grupo' => 'clientes',
            'nome' => 'eventos',
            'url' => 'clientes/eventos'
        ]);
        Endpoints::create([
            'grupo' => 'Clientes',
            'nome' => 'filtroGeral',
            'url' => 'clientes/filtro_geral'
        ]);
        Endpoints::create([
            'grupo' => 'Clientes',
            'nome' => 'updateLoteLocalizacaoCliente',
            'url' => 'clientes/filtro_geral'
        ]);
        //endpoint emails
        Endpoints::create([
            'grupo' => 'Emails',
            'nome' => 'index',
            'url' => 'clientes/emails/'
        ]);
        Endpoints::create([
            'grupo' => 'Emails',
            'nome' => 'update',
            'url' => 'clientes/emails/update/'
        ]);
        Endpoints::create([
            'grupo' => 'Emails',
            'nome' => 'store',
            'url' => 'clientes/emails/store/'
        ]);
        Endpoints::create([
            'grupo' => 'Emails',
            'nome' => 'delete',
            'url' => 'clientes/emails/delete/'
        ]);
        //endpoint telefones
        Endpoints::create([
            'grupo' => 'Telefones',
            'nome' => 'index',
            'url' => 'clientes/telefones/'
        ]);
        Endpoints::create([
            'grupo' => 'Telefones',
            'nome' => 'update',
            'url' => 'clientes/telefones/update/'
        ]);
        Endpoints::create([
            'grupo' => 'Telefones',
            'nome' => 'store',
            'url' => 'clientes/telefones/store/'
        ]);
        Endpoints::create([
            'grupo' => 'Telefones',
            'nome' => 'delete',
            'url' => 'clientes/telefones/delete/'
        ]);
        //endpoint cliente chart
        Endpoints::create([
            'grupo' => 'Chart',
            'nome' => 'clienteLocalizado',
            'url' => 'clientes/cliente_localizacao_chart'
        ]);
        Endpoints::create([
            'grupo' => 'Chart',
            'nome' => 'statusCliente',
            'url' => 'clientes/status_cliente_chart'
        ]);
        Endpoints::create([
            'grupo' => 'Chart',
            'nome' => 'totalClientePeriodo',
            'url' => 'clientes/total_cliente_periodo_chart'
        ]);

        //endpoint Credores
        Endpoints::create([
            'grupo' => 'Credores',
            'nome' => 'index',
            'url' => 'credores'
        ]);
        Endpoints::create([
            'grupo' => 'Credores',
            'nome' => 'create',
            'url' => 'credores/create'
        ]);
        Endpoints::create([
            'grupo' => 'Credores',
            'nome' => 'store',
            'url' => 'credores/store'
        ]);
        Endpoints::create([
            'grupo' => 'Credores',
            'nome' => 'delete',
            'url' => 'credores/delete/'
        ]);
        Endpoints::create([
            'grupo' => 'Credores',
            'nome' => 'remessas',
            'url' => 'credores/remessas/'
        ]);
        Endpoints::create([
            'grupo' => 'Credores',
            'nome' => 'storeParametros',
            'url' => 'credores/storeparametros/'
        ]);
        Endpoints::create([
            'grupo' => 'Credores',
            'nome' => 'clientes',
            'url' => 'credores/clientes/'
        ]);
        //endpoint devolucao
        Endpoints::create([
            'grupo' => 'Devolução',
            'nome' => 'index',
            'url' => 'devolucao/'
        ]);
        Endpoints::create([
            'grupo' => 'Devolução',
            'nome' => 'create',
            'url' => 'devolucao/create/'
        ]);
        Endpoints::create([
            'grupo' => 'Devolução',
            'nome' => 'detalhe',
            'url' => 'devolucao/detalhe/'
        ]);
        Endpoints::create([
            'grupo' => 'Devolução',
            'nome' => 'store',
            'url' => 'devolucao/store/'
        ]);
        Endpoints::create([
            'grupo' => 'operaçõesCredor',
            'nome' => 'credor',
            'url' => 'operacoes/operacoesCredor/'
        ]);
        //endpoint Operações Chart
        Endpoints::create([
            'grupo' => 'operaçõesChart',
            'nome' => 'chart',
            'url' => 'operacoes/status_operacao_chart'
        ]);
        Endpoints::create([
            'grupo' => 'operaçõesChart',
            'nome' => 'chart',
            'url' => 'operacoes/motivo_pendencia_chart'
        ]);
        Endpoints::create([
            'grupo' => 'operaçõesChart',
            'nome' => 'chart',
            'url' => 'operacoes/motivo_devolucao_chart'
        ]);
        Endpoints::create([
            'grupo' => 'operaçõesChart',
            'nome' => 'chart',
            'url' => 'operacoes/valor_nominal_ano_vencimento_chart'
        ]);
        Endpoints::create([
            'grupo' => 'operaçõesChart',
            'nome' => 'chart',
            'url' => 'operacoes/titulo_aberto_periodo_chart'
        ]);
        //endpoint Operações
        Endpoints::create([
            'grupo' => 'Operações',
            'nome' => 'index',
            'url' => 'operacoes'
        ]);
        Endpoints::create([
            'grupo' => 'Operações',
            'nome' => 'create',
            'url' => 'operacoes/create/'
        ]);
        Endpoints::create([
            'grupo' => 'Operações',
            'nome' => 'edit',
            'url' => 'operacoes/edit/'
        ]);
        Endpoints::create([
            'grupo' => 'Operações',
            'nome' => 'filtro',
            'url' => 'operacoes/filtro'
        ]);
        Endpoints::create([
            'grupo' => 'Operações',
            'nome' => 'store',
            'url' => 'operacoes/store'
        ]);
        Endpoints::create([
            'grupo' => 'Operações',
            'nome' => 'updateLotPendencia',
            'url' => 'operacoes/update_lot_motivo_pendencia/'
        ]);
        Endpoints::create([
            'grupo' => 'Operações',
            'nome' => 'updateLotStatusOperacao',
            'url' => 'operacoes/update_lot_status_operacao/'
        ]);
        Endpoints::create([
            'grupo' => 'Operações',
            'nome' => 'updateLotNegativado',
            'url' => 'operacoes/update_lot_negativado_operacao/'
        ]);
        Endpoints::create([
            'grupo' => 'Operações',
            'nome' => 'operacoesNegativadosCliente',
            'url' => 'operacoes/operacoes_negativados_cliente/'
        ]);
        //endpoint Campanha
        Endpoints::create([
            'grupo' => 'Campanhas',
            'nome' => 'campanhasCredor',
            'url' => 'operacoes/campanhasCredor/'
        ]);
        Endpoints::create([
            'grupo' => 'Campanhas',
            'nome' => 'index',
            'url' => 'campanha/'
        ]);
        Endpoints::create([
            'grupo' => 'Campanhas',
            'nome' => 'edit',
            'url' => 'campanha/edit/'
        ]);
        Endpoints::create([
            'grupo' => 'Campanhas',
            'nome' => 'store',
            'url' => 'campanha/store'
        ]);
        Endpoints::create([
            'grupo' => 'Campanhas',
            'nome' => 'update',
            'url' => 'campanha/update/'
        ]);
        Endpoints::create([
            'grupo' => 'Campanhas',
            'nome' => 'delete',
            'url' => 'campanha/delete/'
        ]);
        Endpoints::create([
            'grupo' => 'Campanhas',
            'nome' => 'lista',
            'url' => 'campanha/lista'
        ]);
        //endpoint Sms
        Endpoints::create([
            'grupo' => 'Sms',
            'nome' => 'enviar',
            'url' => 'sms/enviar'
        ]);
        //endpoint Email
        Endpoints::create([
            'grupo' => 'Sms',
            'nome' => 'enviar',
            'url' => 'email/enviar'
        ]);
        Endpoints::create([
            'grupo' => 'Sms',
            'nome' => 'enviarAcordoSintetico',
            'url' => 'email/enviar_acordo_sintetico'
        ]);
        //endpoint Descontos
        Endpoints::create([
            'grupo' => 'Descontos',
            'nome' => 'index',
            'url' => 'descontos/'
        ]);
        Endpoints::create([
            'grupo' => 'Descontos',
            'nome' => 'edit',
            'url' => 'descontos/edit/'
        ]);
        Endpoints::create([
            'grupo' => 'Descontos',
            'nome' => 'store',
            'url' => 'descontos/store'
        ]);
        Endpoints::create([
            'grupo' => 'Descontos',
            'nome' => 'update',
            'url' => 'descontos/update/'
        ]);
        Endpoints::create([
            'grupo' => 'Descontos',
            'nome' => 'delete',
            'url' => 'descontos/delete/'
        ]);
        Endpoints::create([
            'grupo' => 'Descontos',
            'nome' => 'importar',
            'url' => 'descontos/importar'
        ]);
        //endpoint Lote Operações
        Endpoints::create([
            'grupo' => 'loteOperações',
            'nome' => 'importar',
            'url' => 'lote_operacoes/importar'
        ]);
        // Pasta Administrador
        // endpoint Cidades
        Endpoints::create([
            'grupo' => 'Cidades',
            'nome' => 'index',
            'url' => 'cidades'
        ]);
        Endpoints::create([
            'grupo' => 'Cidades',
            'nome' => 'edit',
            'url' => 'cidades/edit/'
        ]);
        Endpoints::create([
            'grupo' => 'Cidades',
            'nome' => 'store',
            'url' => 'cidades/store'
        ]);
        Endpoints::create([
            'grupo' => 'Cidades',
            'nome' => 'update',
            'url' => 'cidades/update/'
        ]);
        Endpoints::create([
            'grupo' => 'Cidades',
            'nome' => 'delete',
            'url' => 'cidades/delete/'
        ]);
        //endpoint Empresas
        Endpoints::create([
            'grupo' => 'Empresas',
            'nome' => 'index',
            'url' => 'empresas'
        ]);
        Endpoints::create([
            'grupo' => 'Empresas',
            'nome' => 'edit',
            'url' => 'empresas/edit/'
        ]);
        Endpoints::create([
            'grupo' => 'Empresas',
            'nome' => 'create',
            'url' => 'empresas/create'
        ]);
        Endpoints::create([
            'grupo' => 'Empresas',
            'nome' => 'store',
            'url' => 'empresas/store'
        ]);
        Endpoints::create([
            'grupo' => 'Empresas',
            'nome' => 'update',
            'url' => 'empresas/update/'
        ]);
        Endpoints::create([
            'grupo' => 'Empresas',
            'nome' => 'delete',
            'url' => 'empresas/delete/'
        ]);
        // endpoint Funcionarios
        Endpoints::create([
            'grupo' => 'Funcionarios',
            'nome' => 'index',
            'url' => 'funcionarios'
        ]);
        Endpoints::create([
            'grupo' => 'Funcionarios',
            'nome' => 'edit',
            'url' => 'funcionarios/edit/'
        ]);
        Endpoints::create([
            'grupo' => 'Funcionarios',
            'nome' => 'create',
            'url' => 'funcionarios/create'
        ]);
        Endpoints::create([
            'grupo' => 'Funcionarios',
            'nome' => 'store',
            'url' => 'funcionarios/store'
        ]);
        Endpoints::create([
            'grupo' => 'Funcionarios',
            'nome' => 'update',
            'url' => 'funcionarios/update/'
        ]);
        Endpoints::create([
            'grupo' => 'Funcionarios',
            'nome' => 'delete',
            'url' => 'funcionarios/delete/'
        ]);
        Endpoints::create([
            'grupo' => 'Funcionarios',
            'nome' => 'storefull',
            'url' => 'funcionarios/storefull'
        ]);
        //endpoint Motivo Evento
        Endpoints::create([
            'grupo' => 'motivoEvento',
            'nome' => 'index',
            'url' => 'motivo_evento'
        ]);
        Endpoints::create([
            'grupo' => 'motivoEvento',
            'nome' => 'edit',
            'url' => 'motivo_evento/edit/'
        ]);
        Endpoints::create([
            'grupo' => 'motivoEvento',
            'nome' => 'store',
            'url' => 'motivo_evento/store'
        ]);
        Endpoints::create([
            'grupo' => 'motivoEvento',
            'nome' => 'update',
            'url' => 'motivo_evento/update/'
        ]);
        Endpoints::create([
            'grupo' => 'motivoEvento',
            'nome' => 'delete',
            'url' => 'motivo_evento/delete/'
        ]);
        //endpoint Motivo Devolução
        Endpoints::create([
            'grupo' => 'motivoDevolução',
            'nome' => 'index',
            'url' => 'motivo_devolucao'
        ]);
        Endpoints::create([
            'grupo' => 'motivoDevolução',
            'nome' => 'edit',
            'url' => 'motivo_devolucao/edit/'
        ]);
        Endpoints::create([
            'grupo' => 'motivoDevolução',
            'nome' => 'store',
            'url' => 'motivo_devolucao/store'
        ]);
        Endpoints::create([
            'grupo' => 'motivoDevolução',
            'nome' => 'update',
            'url' => 'motivo_devolucao/update/'
        ]);
        Endpoints::create([
            'grupo' => 'motivoDevolução',
            'nome' => 'delete',
            'url' => 'motivo_devolucao/delete/'
        ]);
//endpoint createificação Negativação
        Endpoints::create([
            'grupo' => 'createificaçãoNegativação',
            'nome' => 'index',
            'url' => 'createificacao_negativacao'
        ]);
        Endpoints::create([
            'grupo' => 'createificaçãoNegativação',
            'nome' => 'edit',
            'url' => 'createificacao_negativacao/edit/'
        ]);
        Endpoints::create([
            'grupo' => 'createificaçãoNegativação',
            'nome' => 'store',
            'url' => 'createificacao_negativacao/store'
        ]);
        Endpoints::create([
            'grupo' => 'createificaçãoNegativação',
            'nome' => 'update',
            'url' => 'createificacao_negativacao/update/'
        ]);
        Endpoints::create([
            'grupo' => 'createificaçãoNegativação',
            'nome' => 'delete',
            'url' => 'createificacao_negativacao/delete/'
        ]);
        //endpoint Motivo Pendência
        Endpoints::create([
            'grupo' => 'motivoPendência',
            'nome' => 'index',
            'url' => 'motivo_pendencia/'
        ]);
        Endpoints::create([
            'grupo' => 'motivoPendência',
            'nome' => 'edit',
            'url' => 'motivo_pendencia/edit/'
        ]);
        Endpoints::create([
            'grupo' => 'motivoPendência',
            'nome' => 'store',
            'url' => 'motivo_pendencia/store'
        ]);
        Endpoints::create([
            'grupo' => 'motivoPendência',
            'nome' => 'update',
            'url' => 'motivo_pendencia/update/'
        ]);
        Endpoints::create([
            'grupo' => 'motivoPendência',
            'nome' => 'delete',
            'url' => 'motivo_pendencia/delete/'
        ]);
        //endpoint Localização
        Endpoints::create([
            'grupo' => 'Localização',
            'nome' => 'index',
            'url' => 'status_localizacao'
        ]);
        Endpoints::create([
            'grupo' => 'Localização',
            'nome' => 'edit',
            'url' => 'status_localizacao/edit/'
        ]);
        Endpoints::create([
            'grupo' => 'Localização',
            'nome' => 'store',
            'url' => 'status_localizacao/store'
        ]);
        Endpoints::create([
            'grupo' => 'Localização',
            'nome' => 'update',
            'url' => 'status_localizacao/update/'
        ]);
        Endpoints::create([
            'grupo' => 'Localização',
            'nome' => 'delete',
            'url' => 'status_localizacao/delete/'
        ]);
        //endpoint Função
        Endpoints::create([
            'grupo' => 'Função',
            'nome' => 'index',
            'url' => 'funcao'
        ]);
        Endpoints::create([
            'grupo' => 'Função',
            'nome' => 'edit',
            'url' => 'funcao/edit/'
        ]);
        Endpoints::create([
            'grupo' => 'Função',
            'nome' => 'store',
            'url' => 'funcao/store'
        ]);
        Endpoints::create([
            'grupo' => 'Função',
            'nome' => 'update',
            'url' => 'funcao/update/'
        ]);
        Endpoints::create([
            'grupo' => 'Função',
            'nome' => 'delete',
            'url' => 'funcao/delete/'
        ]);
        //endpoint Users
        Endpoints::create([
            'grupo' => 'Users',
            'nome' => 'index',
            'url' => 'users'
        ]);
        Endpoints::create([
            'grupo' => 'Users',
            'nome' => 'edit',
            'url' => 'users/edit/'
        ]);
        Endpoints::create([
            'grupo' => 'Users',
            'nome' => 'store',
            'url' => 'users/store'
        ]);
        Endpoints::create([
            'grupo' => 'Users',
            'nome' => 'update',
            'url' => 'users/update/'
        ]);
        Endpoints::create([
            'grupo' => 'Users',
            'nome' => 'delete',
            'url' => 'users/delete/'
        ]);
        Endpoints::create([
            'grupo' => 'Users',
            'nome' => 'uploadFoto',
            'url' => 'users/uploadFoto'
        ]);
        Endpoints::create([
            'grupo' => 'Users',
            'nome' => 'exibirFoto',
            'url' => 'users/exibirFoto/'
        ]);
        Endpoints::create([
            'grupo' => 'Users',
            'nome' => 'atachePerfil',
            'url' => 'users/atache/perfil'
        ]);
        Endpoints::create([
            'grupo' => 'Users',
            'nome' => 'detachePerfil',
            'url' => 'users/detache/perfil'
        ]);
        //endpoint Eventos
        Endpoints::create([
            'grupo' => 'Eventos',
            'nome' => 'index',
            'url' => 'eventos/'
        ]);
        Endpoints::create([
            'grupo' => 'Eventos',
            'nome' => 'edit',
            'url' => 'eventos/edit/'
        ]);
        Endpoints::create([
            'grupo' => 'Eventos',
            'nome' => 'store',
            'url' => 'eventos/store'
        ]);
        Endpoints::create([
            'grupo' => 'Eventos',
            'nome' => 'update',
            'url' => 'eventos/update/'
        ]);
        Endpoints::create([
            'grupo' => 'Eventos',
            'nome' => 'delete',
            'url' => 'eventos/delete/'
        ]);

//endpoint Boleto
        Endpoints::create([
            'grupo' => 'Boleto',
            'nome' => 'index',
            'url' => 'boletos/'
        ]);
        Endpoints::create([
            'grupo' => 'Eventos',
            'nome' => 'edit',
            'url' => 'boletos/edit/'
        ]);
        Endpoints::create([
            'grupo' => 'Boleto',
            'nome' => 'store',
            'url' => 'boletos/store'
        ]);
        Endpoints::create([
            'grupo' => 'Boleto',
            'nome' => 'update',
            'url' => 'boletos/update/'
        ]);
        Endpoints::create([
            'grupo' => 'Boleto',
            'nome' => 'delete',
            'url' => 'boletos/delete/'
        ]);
        Endpoints::create([
            'grupo' => 'Boleto',
            'nome' => 'dataBoleto',
            'url' => 'boletos/data_boleto'
        ]);
        Endpoints::create([
            'grupo' => 'Boleto',
            'nome' => 'viewBoleto',
            'url' => 'boletos/viewBoleto'
        ]);
        //endpoint Status Localização
        Endpoints::create([
            'grupo' => 'statusLocalização',
            'nome' => 'index',
            'url' => 'status_localizacao/'
        ]);
        Endpoints::create([
            'grupo' => 'statusLocalização',
            'nome' => 'edit',
            'url' => 'status_localizacao/edit/'
        ]);
        Endpoints::create([
            'grupo' => 'statusLocalização',
            'nome' => 'store',
            'url' => 'status_localizacao/store'
        ]);
        Endpoints::create([
            'grupo' => 'statusLocalização',
            'nome' => 'update',
            'url' => 'status_localizacao/update/'
        ]);
        Endpoints::create([
            'grupo' => 'statusLocalização',
            'nome' => 'delete',
            'url' => 'status_localizacao/delete/'
        ]);
        //endpoint Parametro Boleto
        Endpoints::create([
            'grupo' => 'parametroBoleto',
            'nome' => 'index',
            'url' => 'parametro_boleto'
        ]);
        Endpoints::create([
            'grupo' => 'parametroBoleto',
            'nome' => 'edit',
            'url' => 'parametro_boleto/edit/'
        ]);
        Endpoints::create([
            'grupo' => 'parametroBoleto',
            'nome' => 'store',
            'url' => 'parametro_boleto/store'
        ]);
        Endpoints::create([
            'grupo' => 'parametroBoleto',
            'nome' => 'update',
            'url' => 'parametro_boleto/update/'
        ]);
        Endpoints::create([
            'grupo' => 'parametroBoleto',
            'nome' => 'delete',
            'url' => 'parametro_boleto/delete/'
        ]);
        //endpoint Template Sms
        Endpoints::create([
            'grupo' => 'templateSms',
            'nome' => 'index',
            'url' => 'template_sms/'
        ]);
        Endpoints::create([
            'grupo' => 'templateSms',
            'nome' => 'edit',
            'url' => 'template_sms/edit/'
        ]);
        Endpoints::create([
            'grupo' => 'templateSms',
            'nome' => 'store',
            'url' => 'template_sms/store'
        ]);
        Endpoints::create([
            'grupo' => 'templateSms',
            'nome' => 'update',
            'url' => 'template_sms/update/'
        ]);
        Endpoints::create([
            'grupo' => 'templateSms',
            'nome' => 'delete',
            'url' => 'template_sms/delete/'
        ]);
        Endpoints::create([
            'grupo' => 'templateSms',
            'nome' => 'preencherSms',
            'url' => 'template_sms/preenchersms'
        ]);
        //endpoint Template Email
        Endpoints::create([
            'grupo' => 'templateEmail',
            'nome' => 'index',
            'url' => 'template_email/'
        ]);
        Endpoints::create([
            'grupo' => 'templateEmail',
            'nome' => 'edit',
            'url' => 'template_email/edit/'
        ]);
        Endpoints::create([
            'grupo' => 'templateEmail',
            'nome' => 'store',
            'url' => 'template_email/store'
        ]);
        Endpoints::create([
            'grupo' => 'templateEmail',
            'nome' => 'update',
            'url' => 'template_email/update/'
        ]);
        Endpoints::create([
            'grupo' => 'templateEmail',
            'nome' => 'delete',
            'url' => 'template_email/delete/'
        ]);
        Endpoints::create([
            'grupo' => 'templateEmail',
            'nome' => 'preencherEmail',
            'url' => 'template_email/preencheremail'
        ]);
        //endpoint Perfil Usuário
        Endpoints::create([
            'grupo' => 'perfilUsuário',
            'nome' => 'index',
            'url' => 'perfil_usuario/'
        ]);
        Endpoints::create([
            'grupo' => 'perfilUsuário',
            'nome' => 'edit',
            'url' => 'perfil_usuario/edit/'
        ]);
        Endpoints::create([
            'grupo' => 'perfilUsuário',
            'nome' => 'store',
            'url' => 'perfil_usuario/store'
        ]);
        Endpoints::create([
            'grupo' => 'perfilUsuário',
            'nome' => 'update',
            'url' => 'perfil_usuario/update/'
        ]);
        Endpoints::create([
            'grupo' => 'perfilUsuário',
            'nome' => 'delete',
            'url' => 'perfil_usuario/delete/'
        ]);
        //endpoint Endpoints
        Endpoints::create([
            'grupo' => 'perfilUsuário',
            'nome' => 'index',
            'url' => 'perfil_usuario/'
        ]);
        Endpoints::create([
            'grupo' => 'perfilUsuário',
            'nome' => 'edit',
            'url' => 'endpoints/edit/'
        ]);
        Endpoints::create([
            'grupo' => 'perfilUsuário',
            'nome' => 'store',
            'url' => 'endpoints/store'
        ]);
        Endpoints::create([
            'grupo' => 'perfilUsuário',
            'nome' => 'update',
            'url' => 'endpoints/update/'
        ]);
        Endpoints::create([
            'grupo' => 'perfilUsuário',
            'nome' => 'delete',
            'url' => 'endpoints/delete/'
        ]);
        // Pasta Informações
        Endpoints::create([
            'grupo' => 'Informações',
            'nome' => 'statusOperacao',
            'url' => 'info/statusoperacao'
        ]);
        Endpoints::create([
            'grupo' => 'Informações',
            'nome' => 'createificacaoNegativacao',
            'url' => 'info/createificacaonegativacao'
        ]);
        Endpoints::create([
            'grupo' => 'Informações',
            'nome' => 'createificacaoCliente',
            'url' => 'info/createificacaocliente'
        ]);
        Endpoints::create([
            'grupo' => 'Informações',
            'nome' => 'motivoPendencia',
            'url' => 'info/motivopendencia'
        ]);
        //Pasta Cobrança
        //endpoint Acordo Credor
        Endpoints::create([
            'grupo' => 'acordoCredor',
            'nome' => 'credor',
            'url' => 'operacoes/acordosCredor/'
        ]);
        //endpoint Acordo Chart
        Endpoints::create([
            'grupo' => 'acordoChart',
            'nome' => 'chart',
            'url' => 'acordo/acordo_periodo_chart'
        ]);
        //endpoint Acordo
        Endpoints::create([
            'grupo' => 'Acordo',
            'nome' => 'index',
            'url' => 'acordo/'
        ]);
        Endpoints::create([
            'grupo' => 'Acordo',
            'nome' => 'edit',
            'url' => 'acordo/edit/'
        ]);
        Endpoints::create([
            'grupo' => 'Acordo',
            'nome' => 'store',
            'url' => 'acordo/store'
        ]);
        Endpoints::create([
            'grupo' => 'Acordo',
            'nome' => 'update',
            'url' => 'acordo/update/'
        ]);
        Endpoints::create([
            'grupo' => 'Acordo',
            'nome' => 'delete',
            'url' => 'acordo/delete/'
        ]);
        Endpoints::create([
            'grupo' => 'Acordo',
            'nome' => 'filtroExecutio',
            'url' => 'acordo/executivo/'
        ]);
        Endpoints::create([
            'grupo' => 'Acordo',
            'nome' => 'filtroCliente',
            'url' => 'acordo/cliente/'
        ]);
        Endpoints::create([
            'grupo' => 'Acordo',
            'nome' => 'filtroNomeAluno',
            'url' => 'acordo/nome_aluno'
        ]);
        Endpoints::create([
            'grupo' => 'Acordo',
            'nome' => 'filtroDataAcordo',
            'url' => 'acordo/data_acordo'
        ]);
        Endpoints::create([
            'grupo' => 'Acordo',
            'nome' => 'Negociação',
            'url' => 'acordo/negociacao/'
        ]);
        Endpoints::create([
            'grupo' => 'Acordo',
            'nome' => 'consultarFaturaCartão',
            'url' => 'acordo/fatura_cartao/'
        ]);
        Endpoints::create([
            'grupo' => 'Acordo',
            'nome' => 'editarClienteAcordo',
            'url' => 'acordo/cliente/editar/'
        ]);
        Endpoints::create([
            'grupo' => 'Acordo',
            'nome' => 'updateClienteAcordo',
            'url' => 'acordo/cliente/update/'
        ]);
        //endpoint Parcelas
        Endpoints::create([
            'grupo' => 'Parcelas',
            'nome' => 'index',
            'url' => 'parcelas/'
        ]);
        Endpoints::create([
            'grupo' => 'Parcelas',
            'nome' => 'edit',
            'url' => 'parcelas/edit/'
        ]);
        Endpoints::create([
            'grupo' => 'Parcelas',
            'nome' => 'listAcordo',
            'url' => 'parcelas/indexAcordo/'
        ]);
        Endpoints::create([
            'grupo' => 'Parcelas',
            'nome' => 'store',
            'url' => 'parcelas/store'
        ]);
        Endpoints::create([
            'grupo' => 'Parcelas',
            'nome' => 'update',
            'url' => 'parcelas/update/'
        ]);
        Endpoints::create([
            'grupo' => 'Parcelas',
            'nome' => 'delete',
            'url' => 'parcelas/delete/'
        ]);
        //Pasta Financeiro
        //endpoint Baixar Boletos
        Endpoints::create([
            'grupo' => 'baixarBoletos',
            'nome' => 'baixarBoleto',
            'url' => 'baixar_boleto/boleto'
        ]);
        Endpoints::create([
            'grupo' => 'baixarBoletos',
            'nome' => 'index',
            'url' => 'baixar_boleto/index'
        ]);
        //Pasta Webhook
        //endpoint Recebimento
        Endpoints::create([
            'grupo' => 'Recebimento',
            'nome' => 'cartao',
            'url' => 'webhook/cartao/recebimento/'
        ]);
        //endpoint Dashboard
        Endpoints::create([
            'grupo' => 'Dashboard',
            'nome' => 'dashboard',
            'url' => 'dashboard'
        ]);

        //fornecedores
        Endpoints::create([
            'grupo' => 'Fornecedores',
            'nome' => 'index',
            'url' => 'fornecedores'
        ]);
        Endpoints::create([
            'grupo' => 'Fornecedores',
            'nome' => 'edit',
            'url' => 'fornecedores/edit/'
        ]);
        Endpoints::create([
            'grupo' => 'Fornecedores',
            'nome' => 'create',
            'url' => 'fornecedores/create'
        ]);
        Endpoints::create([
            'grupo' => 'Fornecedores',
            'nome' => 'store',
            'url' => 'fornecedores/store'
        ]);
        Endpoints::create([
            'grupo' => 'Fornecedores',
            'nome' => 'update',
            'url' => 'fornecedores/update/'
        ]);
        Endpoints::create([
            'grupo' => 'Fornecedores',
            'nome' => 'delete',
            'url' => 'fornecedores/delete/'
        ]);

    }
}
