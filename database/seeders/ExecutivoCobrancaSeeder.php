<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Cobranca\ExecutivoCobranca;

class ExecutivoCobrancaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ExecutivoCobranca::create([
            'funcionarios_id' => 9
        ]);

        ExecutivoCobranca::create([
            'funcionarios_id' => 10
        ]);

        ExecutivoCobranca::create([
            'funcionarios_id' => 11
        ]);

        ExecutivoCobranca::create([
            'funcionarios_id' => 12
        ]);

        ExecutivoCobranca::create([
            'funcionarios_id' => 13
        ]);

        ExecutivoCobranca::create([
            'funcionarios_id' => 15
        ]);

        ExecutivoCobranca::create([
            'funcionarios_id' => 16
        ]);

        ExecutivoCobranca::create([
            'funcionarios_id' => 17
        ]);

        ExecutivoCobranca::create([
            'funcionarios_id' => 18
        ]);

        ExecutivoCobranca::create([
            'funcionarios_id' => 19
        ]);

        ExecutivoCobranca::create([
            'funcionarios_id' => 20
        ]);

    }
}
