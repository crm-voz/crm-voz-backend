<?php

namespace Database\Seeders;

use App\Models\Core\TemplateSms;
use Illuminate\Database\Seeder;

class TemplateSmsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @see o campo credores_id é opcional!
     */
    public function run()
    {
        TemplateSms::create([
            'titulo' => 'BOLETO ENVIADO',
            'mensagem' => 'Sr.(a) $CLIENTE$, seu boleto $CREDOR$ foi enviado com sucesso Quaisquer duvida estaremos a disposicao no0800 940 6011.',
            'status' => true,
            'credores_id' => 1
        ]);
    }
}
