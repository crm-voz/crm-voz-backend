<?php

namespace Database\Seeders;

use App\Models\Core\StatusOperacao;
use Illuminate\Database\Seeder;

class StatusOperacaoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        StatusOperacao::create([
            'nome'=>'Cobrança',
            'sigla' => 'C'
        ]);
        StatusOperacao::create([
            'nome' => 'Liquidada',
            'sigla' => 'L'
        ]);
        StatusOperacao::create([
            'nome' => 'Devolvida',
            'sigla' => 'D'
        ]);
        StatusOperacao::create([
            'nome' => 'Negociada',
            'sigla' => 'N'

        ]);
        StatusOperacao::create([
            'nome' => 'Aguardando',
            'sigla' => 'A'
        ]);



    }
}
