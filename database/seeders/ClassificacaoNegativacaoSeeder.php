<?php

namespace Database\Seeders;

use App\Models\Core\ClassificacaoNegativacao;
use Illuminate\Database\Seeder;

class ClassificacaoNegativacaoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ClassificacaoNegativacao::create([
            'nome' => 'Negativado',
        ]);

        ClassificacaoNegativacao::create([
            'nome' => 'Retirada por quitação',
        ]);

        ClassificacaoNegativacao::create([
            'nome' => 'Retirada por negociação',
        ]);

        ClassificacaoNegativacao::create([
            'nome' => 'Retirada por solicitacao do credor',
        ]);

        ClassificacaoNegativacao::create([
            'nome' => 'Retirada por inclusão indevida',
        ]);

        ClassificacaoNegativacao::create([
            'nome' => 'Retirada por prescrição',
        ]);
    }
}
