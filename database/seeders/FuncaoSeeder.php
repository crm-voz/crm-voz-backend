<?php

namespace Database\Seeders;
use Illuminate\Database\Seeder;
use App\Models\Core\Funcao;

class FuncaoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Funcao::create([
            'nome'=>'Executivo de Cobrança'
        ]);

        Funcao::create([
            'nome'=>'Analista de Sistemas Júnior'
        ]);

        Funcao::create([
            'nome'=>'Analista de Desenvolvimento de Sistemas Pleno'
        ]);

        Funcao::create([
            'nome'=>'Desenvolvedor Web'
        ]);

        Funcao::create([
            'nome'=>'Analista de Web Design'
        ]);

        Funcao::create([
            'nome'=>'Gerente de Projetos TI'
        ]);

        Funcao::create([
            'nome'=>'Analista de Sistemas Sênior'
        ]);

        Funcao::create([
            'nome' =>'Supervisora Financeira'
        ]);

        Funcao::create([
            'nome' =>'Supervisora Administrativo'
        ]);

        Funcao::create([
            'nome' =>'Analista de Planejamento Financeiro Júnior'
        ]);

        Funcao::create([
            'nome' =>'Analista de Informações Gerenciais Júnior'
        ]);

        Funcao::create([
            'nome' =>'Supervisora de Informações Gerenciais'
        ]);

        Funcao::create([
            'nome' =>'Supervisora de Informações Gerenciais'
        ]);

        Funcao::create([
            'nome' =>'Estagiário de TI'
        ]);

        Funcao::create([
            'nome' =>'Supervisora Cobrança'
        ]);

    }
}
