<?php

namespace Database\Seeders;

use App\Models\Core\Empresa;
use Illuminate\Database\Seeder;

class EmpresasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Empresa::create([
            'nome_fantasia' => 'VOZ GESTÃO DE COBRANCA',
            'razao_social' => 'MOTA E SILVA LTDA',
            'cnpj' => '05492078000194',
            'inscricao_estadual' => '',
            'cidade_id' => 1,
            'endereco' => 'RUA BOM FUTURO, 1300',
            'telefone' => '99 32218450',
            'celular' => '',
            'email' => 'atendimento@grupovoz.com.br',
            'status' => true
        ]);
    }
}
