<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\BackOffice\Campanha;

class CampanhasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Campanha::create([
            'tipo_campanha' => 'sms',
            'data_inicio_campanha' => '2021-07-22',
            'data_final_campanha' => '2021-07-30',
            'respiro' => 10,
            'usuario_empresas_id' => 1
        ]);
    }
}
