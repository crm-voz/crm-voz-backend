<?php

namespace Database\Seeders;

use App\Models\Cobranca\ExecutivoCobranca;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsuarioSeeder::class);
        $this->call(CidadesSeeder::class);
        $this->call(EmpresasSeeder::class);
        $this->call(UsuarioEmpresasSeeder::class);
        // $this->call(ClassificacaoMarcacaoSeeder::class);
        $this->call(ClassificacaoNegativacaoSeeder::class);
        $this->call(MotivoPendenciaSeeder::class);
        $this->call(StatusLocalizacaoSeeder::class);
        $this->call(FuncaoSeeder::class);
        $this->call(StatusOperacaoSeeder::class);
        $this->call(CredorSeeder::class);
        // $this->call(GrupoAcessoSeeder::class);
        $this->call(FuncionarioSeeder::class);
        $this->call(MotivoDevolucaoSeeder::class);
        $this->call(MotivoEventoSeeder::class);
        $this->call(ClientesSeeder::class);
        $this->call(RemessasSeeder::class);
        $this->call(OperacoesSeeder::class);
        $this->call(ExecutivoCobrancaSeeder::class);
        $this->call(CampanhasSeeder::class);
        $this->call(TemplateSmsSeeder::class);
        $this->call(TemplateEmailSeeder::class);
    }
}
