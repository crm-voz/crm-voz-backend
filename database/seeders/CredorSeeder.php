<?php

namespace Database\Seeders;

use App\Models\BackOffice\Credor;
use App\Models\Core\Empresa;
use App\Models\Core\UsuarioEmpresa;
use GuzzleHttp\Promise\Create;
use Illuminate\Database\Seeder;

class CredorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Empresa::create([
            'nome_fantasia' => 'UNISULMA LTDA',
            'razao_social' => 'UNISULMA LTDA',
            'cnpj' => '89.678.945/0001-70',
            'inscricao_estadual' => '1020304050',
            'endereco' => 'Av. Dorgival Pinheiro de Sousa, XXX',
            'telefone' => '(99)9999-9999',
            'celular' => '(99)9999-9999',
            'email' => 'xxxx@xxxx.com.br',
            //'status' => true,
            'cidade_id' => 1
        ]);

        // UsuarioEmpresa::create(['users_id' => 1, 'empresa_id' => 2]);

        Credor::create([
            'nome' => 'UNISULMA FACULDADE',
            'razao_social' => 'UNISULMA',
            'cnpj' => '10.177.133/0001-92',
            'inscricao_estadual' => '10203040500',
            'endereco' => 'Av. Dorgival Pinheiro de Sousa, XXX',
            'telefone' => '(99)9999-9999',
            'celular' => '(99)9999-9999',
            'email' => 'xxxx@xxxx.com.br',
           // 'status' => true,
            'cidade_id' => 1,
            'empresa_id' => 1
        ]);
    }
}
