<?php

namespace Database\Seeders;

use App\Models\Core\Funcionario;
use Illuminate\Database\Seeder;

class FuncionarioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Funcionario::create(
            [
                 'id'=> 1,
                 'nome' => 'Jairo Rodrigues',
                 'cpf' => '01076601219',
                 'rg' =>  '405404956',
                 'cidade_id' => '1',
                 'funcao_id' => '5',
                //  'grupo_acesso_id' => '1',
                 'usuario_empresas_id' => '1'
            ]
        );

        Funcionario::create(
            [
                'id'=> 2,
                'nome' => 'Welliton Cunha',
                'cpf' => '04601835321',
                'rg' =>  '405404956',
                'cidade_id' => '1',
                'funcao_id' => '3',
                // 'grupo_acesso_id' => '1',
                'usuario_empresas_id' => '2'
            ]
        );

        Funcionario::create(
            [
                'id'=> 3,
                'nome' => 'Rafael Gigante',
                'cpf' => '61037007360',
                'rg' =>  '405404956',
                'cidade_id' => '1',
                'funcao_id' => '4',
                // 'grupo_acesso_id' => '1',
                'usuario_empresas_id' => '3'
            ]
        );

        Funcionario::create(
            [
                'id'=> 4,
                'nome' => 'Nonilton Alves',
                'cpf' => '83344837320',
                'rg' =>  '405404956',
                'cidade_id' => '1',
                'funcao_id' => '6',
                // 'grupo_acesso_id' => '1',
                'usuario_empresas_id' => '4'
            ]
        );

        Funcionario::create(
            [
                'id'=> 5,
                'nome' => 'Richard Lucas',
                'cpf' => '60864786352',
                'rg' =>  '4201123216',
                'cidade_id' => '1',
                'funcao_id' => '4',
                // 'grupo_acesso_id' => '1',
                'usuario_empresas_id' => '5'
            ]
        );

        Funcionario::create(
            [
                'id'=> 6,
                'nome' => 'Ricardo Hítalo',
                'cpf' => '06982749380',
                'rg' =>  '4201123216',
                'cidade_id' => '1',
                'funcao_id' => '4',
                // 'grupo_acesso_id' => '1',
                'usuario_empresas_id' => '6'
            ]
        );

        Funcionario::create(
            [
                'id'=> 7,
                'nome' => 'Credor Unisulma',
                'cpf' => '95010962039',
                'rg' =>  '4201123216',
                'cidade_id' => '1',
                'funcao_id' => '1',
                // 'grupo_acesso_id' => '8',
                'usuario_empresas_id' => '7'
            ]
        );

        Funcionario::create(
            [
                'id'=> 8,
                'nome' => 'Hadassa Castro',
                'cpf' => '40114202044',
                'rg' =>  '405404956',
                'cidade_id' => '1',
                'funcao_id' => '15',
                // 'grupo_acesso_id' => '4',
                'usuario_empresas_id' => '8'
            ]
        );

        Funcionario::create(
            [
                'id'=> 9,
                'nome' => 'Andreia Sousa da Silva',
                'cpf' => '04855500321',
                'rg' =>  '405404956',
                'cidade_id' => '1',
                'funcao_id' => '1',
                // 'grupo_acesso_id' => '5',
                'usuario_empresas_id' => '9'
            ]
        );

        Funcionario::create(
            [
                'id'=> 10,
                'nome' => 'Carmina Santos Freires',
                'cpf' => '60410392332',
                'rg' =>  '405404956',
                'cidade_id' => '1',
                'funcao_id' => '1',
                // 'grupo_acesso_id' => '5',
                'usuario_empresas_id' => '10'
            ]
        );

        Funcionario::create(
            [
                'id'=> 11,
                'nome' => 'Ester Sampaio Sousa',
                'cpf' => '61365134393',
                'rg' =>  '405404956',
                'cidade_id' => '1',
                'funcao_id' => '1',
                // 'grupo_acesso_id' => '5',
                'usuario_empresas_id' => '11'
            ]
        );

        Funcionario::create(
            [
                'id'=> 12,
                'nome' => 'Ivanda Costa Santos',
                'cpf' => '61596517301',
                'rg' =>  '4201123216',
                'cidade_id' => '1',
                'funcao_id' => '1',
                // 'grupo_acesso_id' => '5',
                'usuario_empresas_id' => '12'
            ]
        );

        Funcionario::create(
            [
                'id'=> 13,
                'nome' => 'Marcos Madjer Souza Morais',
                'cpf' => '00761012311',
                'rg' =>  '4201123216',
                'cidade_id' => '1',
                'funcao_id' => '1',
                // 'grupo_acesso_id' => '5',
                'usuario_empresas_id' => '13'
            ]
        );

        Funcionario::create(
            [
                'id'=> 14,
                'nome' => 'Náthila Rodrigues Silva Viana',
                'cpf' => '05023830381',
                'rg' =>  '4201123216',
                'cidade_id' => '1',
                'funcao_id' => '15',
                // 'grupo_acesso_id' => '6',
                'usuario_empresas_id' => '14'
            ]
        );

        Funcionario::create(
            [
                'id'=> 15,
                'nome' => 'Daniella Lima Oliveira',
                'cpf' => '03661847396',
                'rg' =>  '405404956',
                'cidade_id' => '1',
                'funcao_id' => '1',
                // 'grupo_acesso_id' => '5',
                'usuario_empresas_id' => '15'
            ]
        );

        Funcionario::create(
            [
                'id'=> 16,
                'nome' => 'Nadja Farias da Rocha',
                'cpf' => '05298149359',
                'rg' =>  '405404956',
                'cidade_id' => '1',
                'funcao_id' => '1',
                // 'grupo_acesso_id' => '5',
                'usuario_empresas_id' => '16'
            ]
        );

        Funcionario::create(
            [
                'id'=> 17,
                'nome' => 'Natalia de Sousa Cursino',
                'cpf' => '60557347378',
                'rg' =>  '405404956',
                'cidade_id' => '1',
                'funcao_id' => '1',
                // 'grupo_acesso_id' => '5',
                'usuario_empresas_id' => '17'
            ]
        );

        Funcionario::create(
            [
                'id'=> 18,
                'nome' => 'Pâmula Rejane Ciqueira de Sousa',
                'cpf' => '04521644350',
                'rg' =>  '405404956',
                'cidade_id' => '1',
                'funcao_id' => '1',
                // 'grupo_acesso_id' => '5',
                'usuario_empresas_id' => '18'
            ]
        );

        Funcionario::create(
            [
                'id'=> 19,
                'nome' => 'Raíla Borges Carvalho',
                'cpf' => '04227312345',
                'rg' =>  '4201123216',
                'cidade_id' => '1',
                'funcao_id' => '1',
                // 'grupo_acesso_id' => '5',
                'usuario_empresas_id' => '19'
            ]
        );

        Funcionario::create(
            [
                'id'=> 20,
                'nome' => 'Sandra Regina da Silva Gomes Nunes',
                'cpf' => '05370577307',
                'rg' =>  '4201123216',
                'cidade_id' => '1',
                'funcao_id' => '1',
                // 'grupo_acesso_id' => '5',
                'usuario_empresas_id' => '20'
            ]
        );

        Funcionario::create(
            [
                'id'=> 21,
                'nome' => 'Adriana Castro Cavalcante Sena',
                'cpf' => '05284985338',
                'rg' =>  '4201123216',
                'cidade_id' => '1',
                'funcao_id' => '1',
                // 'grupo_acesso_id' => '5',
                'usuario_empresas_id' => '21'
            ]
        );

    }
}
