<?php

namespace Database\Seeders;

use App\Models\Core\TemplateEmail;
use Illuminate\Database\Seeder;

class TemplateEmailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @see o campo credores_id é opcional!
     */
    public function run()
    {
        TemplateEmail::create([
            'titulo' => 'BOLETO CLIENTE',
            'mensagem' => 'Prezado (a) Senhor (a), $CLIENTE$

            Conforme acordado, segue anexo boleto(s) bancário(s) e demonstrativo da negociação realizada.

            Como garantia de segurança, é necessário digitar os 11 dígitos do seu CPF como senha para imprimir o(s) boleto(s). Ex.: 01234567891

            Antes de efetuar o pagamento, por gentileza, conferir os dados bancários, o banco, o beneficiário emissor do boleto e o demonstrativo de negociação.

            Lembramos que o prazo de compensação bancária é de até 03 (três) dias úteis.

            Informamos que não utilizamos plataformas de pagamento como Mercado Pago ou PagSeguro.

            Caso o pagamento já tenha sido efetuado, ou você desconheça essa informação, por favor, desconsidere este e-mail.

            *** Esta é uma mensagem automática, portanto, não responda este e-mail, para dúvidas ou demais solicitações, ligue 0800 591 6011 ou acesse nosso site www.grupovoz.com.br ***

            Para visualização e impressão do(s) anexo(s), será necessário programa para abrir e ler PDF, como Adobe Acrobat Reader ou similar instalado.

            Atenciosamente,
            VOZ GESTÃO DE COBRANÇA
            0800 591 6011',
            'status' => true,
            'credores_id' => 1
        ]);

        TemplateEmail::create([
            'titulo' => 'BOLETO CREDOR',
            'mensagem' => 'Prezado (a),

            A(O) responsável $CLIENTE$, CPF nº $CPF$, comparecerá na instituição $DATA_RETIRADA$ para retirada do boleto da negociação realizada conosco, conforme padrões do demonstrativo em anexo.

            Como garantia de segurança, é necessário digitar os 11 dígitos do CPF do cliente como senha para imprimir o(s) boleto(s). Ex.: 01234567891.

            Esta é uma mensagem automática, portanto, não responda este e-mail. Quaisquer dúvidas ou maiores informações, entrar em contato com à Supervisão de Cobrança nos seguintes e-mails:
            nathila@grupovoz.com.br e rodrigo.alvarenga@grupovoz.com.br

            *** Para visualização e impressão do(s) anexo(s), será necessário programa para abrir e ler PDF, como Adobe Acrobat Reader ou similar instalado. ***

            Atenciosamente,
            VOZ GESTÃO DE COBRANÇA
            0800 591 6011',
            'status' => true,
            'credores_id' => null
        ]);
    }
}
