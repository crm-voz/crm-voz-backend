<?php

namespace Database\Seeders;

use App\Models\Core\MotivoPendencia;
use Illuminate\Database\Seeder;

//motivo_pendencia
class MotivoPendenciaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MotivoPendencia::create([
            'nome' => 'ALEGAÇÃO DE PAGAMENTO',
            'status' => true,
             'cor' => null
        ]);

        MotivoPendencia::create([
            'nome' => 'ALEGAÇÃO DE BOLSA ESTUDANTIL',
            'status' => true,
            'cor' => null
        ]);

        MotivoPendencia::create([
            'nome' => 'ALEGAÇÃO DE DIVERGÊNCIA NO VALOR ORIGINAL',
            'status' => true,
            'cor' => null
        ]);

        MotivoPendencia::create([
            'nome' => 'SOLICITAÇÃO DE BAIXA CREDOR',
            'status' => true,
            'cor' => null
        ]);

        MotivoPendencia::create([
            'nome' => 'ALEGAÇÃO QUE DESCONHECE DÉBITO',
            'status' => true,
            'cor' => null
        ]);

        MotivoPendencia::create([
            'nome' => 'ALEGAÇÃO QUE ENCONTRA-SE NO JURÍDICO',
            'status' => true,
            'cor' => null
        ]);

        MotivoPendencia::create([
            'nome' => 'ALEGAÇÃO DE FALECIMENTO DO CLIENTE',
            'status' => true,
            'cor' => null
        ]);

        MotivoPendencia::create([
            'nome' => 'ALEGAÇÃO DE DUPLICIDADE DE DÉBITO',
            'status' => true,
            'cor' => null
        ]);

        MotivoPendencia::create([
            'nome' => 'COMPROVANTE ENVIADO PARA ANÁLISE',
            'status' => true,
            'cor' => null
        ]);

        MotivoPendencia::create([
            'nome' => 'ALINHAMENTO DE BAIXA CREDOR',
            'status' => true,
            'cor' => null
        ]);

    }
}
