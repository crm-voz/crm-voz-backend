<?php

namespace Database\Seeders;

use App\Models\Core\MotivoEvento;
use Illuminate\Database\Seeder;

class MotivoEventoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MotivoEvento::create([
            'id' => 1,
            'nome' => 'COBRANÇA TELEFONE',
             'cor' => null,
             'status' => true,
             'grupo' => 1
        ]);

        MotivoEvento::create([
            'id' => 2,
            'nome' => 'LIGAÇÃO RECEBIDA',
            'cor' => null,
             'status' => true,
             'grupo' => 1
        ]);

        MotivoEvento::create([
            'id' => 3,
            'nome' => 'PAGAMENTO CONFIRMADO',
            'cor' => null,
             'status' => true,
             'grupo' => 1
        ]);

        MotivoEvento::create([
            'id' => 4,
            'nome' => 'TENTATIVAS SEM SUCESSO',
            'cor' => null,
            'status' => true,
             'grupo' => 2
        ]);

        MotivoEvento::create([
            'id' => 5,
            'nome' => 'ACORDO REALIZADO',
            'cor' => null,
             'status' => true,
             'grupo' => 1
        ]);

        MotivoEvento::create([
            'id' => 6,
            'nome' => 'NEGOCIAÇÃO DE PARCELAS',
            'cor' => null,
             'status' => true,
             'grupo' => 3
        ]);

        MotivoEvento::create([
            'id' => 7,
            'nome' => 'SEM PREVISÃO DE PAGAMENTO',
            'cor' => null,
             'status' => true,
             'grupo' => 2
        ]);

        MotivoEvento::create([
            'id' => 8,
            'nome' => 'CADASTRO CONFIRMADO',
            'cor' => null,
             'status' => true,
             'grupo' => 3
        ]);

        MotivoEvento::create([
            'id' => 9,
            'nome' => 'PROPOSTA EM ANALISE DA SUPERVISÃO',
            'cor' => null,
             'status' => true,
             'grupo' => 1
        ]);

        MotivoEvento::create([
            'id' => 10,
            'nome' => 'TENTATIVA SEM SUCESSO - DISCADOR',
            'cor' => null,
             'status' => true,
             'grupo' => 1
        ]);

        MotivoEvento::create([
            'id' => 11,
            'nome' => 'ATENÇÃO',
            'cor' => null,
             'status' => true,
             'grupo' => 1
        ]);

        MotivoEvento::create([
            'id' => 12,
            'nome' => 'PENDÊNCIA',
            'cor' => null,
             'status' => true,
             'grupo' => 3
        ]);

        MotivoEvento::create([
            'id' => 13,
            'nome' => 'AGUARDANDO ANALISE DA INSTITUIÇÃO',
            'cor' => null,
             'status' => true,
             'grupo' => 3
        ]);

        MotivoEvento::create([
            'id' => 14,
            'nome' => 'RETORNO DA INSTITUIÇÃO',
            'cor' => null,
             'status' => true,
             'grupo' => 3
        ]);

        MotivoEvento::create([
            'id' => 15,
            'nome' => 'ANALISE URGENTE',
            'cor' => null,
             'status' => true,
             'grupo' => 3
        ]);

        MotivoEvento::create([
            'id' => 16,
            'nome' => 'ATENDIMENTO ONLINE',
            'cor' => null,
             'status' => true,
             'grupo' => 3
        ]);

        MotivoEvento::create([
            'id' => 17,
            'nome' => 'CONTRA PROPOSTA EFETUADA PELO CLIENTE',
            'cor' => null,
             'status' => true,
             'grupo' => 3
        ]);

        MotivoEvento::create([
            'id' => 18,
            'nome' => 'COMUNICAÇÃO INTERNA',
            'cor' => null,
             'status' => true,
             'grupo' => 3
        ]);

        MotivoEvento::create([
            'id' => 19,
            'nome' => 'ATENDIMENTO VIA WHATSAPP',
            'cor' => null,
             'status' => true,
             'grupo' => 3
        ]);

        MotivoEvento::create([
            'id' => 20,
            'nome' => 'SIMULAÇÃO DE NEGOCIAÇÃO',
            'cor' => null,
             'status' => true,
             'grupo' => 2
        ]);

        MotivoEvento::create([
            'id' => 21,
            'nome' => 'AGENDAMENTO COM O CLIENTE',
            'cor' => null,
             'status' => true,
             'grupo' => 3
        ]);

        MotivoEvento::create([
            'id' => 22,
            'nome' => 'ENVIO DE BOLETOS A VENCER',
            'cor' => null,
             'status' => true,
             'grupo' => 1
        ]);

        MotivoEvento::create([
            'id' => 23,
            'nome' => 'TIROU A BAIXA',
            'cor' => null,
             'status' => true,
             'grupo' => 3
        ]);

        MotivoEvento::create([
            'id' => 24,
            'nome' => 'MUDOU DATA DO VENCIMENTO SEM ATUALIZAR PARCELAS',
            'cor' => null,
             'status' => true,
             'grupo' => 3
        ]);

        MotivoEvento::create([
            'id' => 25,
            'nome' => 'RECADO COM TERCEIROS',
            'cor' => null,
             'status' => true,
             'grupo' => 3
        ]);

        MotivoEvento::create([
            'id' => 26,
            'nome' => 'LEMBRETE DE VENCIMENTO',
            'cor' => null,
             'status' => true,
             'grupo' => 3
        ]);

        MotivoEvento::create([
            'id' => 27,
            'nome' => 'SOLICITAÇÃO DE LIGAÇÃO VIA SITE',
            'cor' => null,
             'status' => true,
             'grupo' => 3
        ]);

        MotivoEvento::create([
            'id' => 28,
            'nome' => 'VALORES DE SIMULAÇÃO DE DIVIDA ENVIADOS',
            'cor' => null,
             'status' => true,
             'grupo' => 1
        ]);


        MotivoEvento::create([
            'id' => 29,
            'nome' => 'ENVIO DE SIMULAÇÃO DE DIVIDA',
            'cor' => null,
             'status' => true,
             'grupo' => 3
        ]);

        MotivoEvento::create([
            'id' => 30,
            'nome' => 'ENVIO DE LINK VOZ',
            'cor' => null,
             'status' => true,
             'grupo' => 3
        ]);

        MotivoEvento::create([
            'id' => 31,
            'nome' => 'ENVIO DE BOLETO PARA CREDOR',
            'cor' => null,
             'status' => true,
             'grupo' => 3
        ]);

        MotivoEvento::create([
            'id' => 32,
            'nome' => 'ENVIO DE CARTÃO CREDOR',
            'cor' => null,
             'status' => true,
             'grupo' => 3
        ]);

        MotivoEvento::create([
            'id' => 33,
            'nome' => 'FALECIDO',
            'cor' => null,
             'status' => true,
             'grupo' => 1
        ]);

        MotivoEvento::create([
            'id' => 34,
            'nome' => 'DEVOLUÇÃO',
            'cor' => null,
             'status' => true,
             'grupo' => 3
        ]);

        MotivoEvento::create([
            'id' => 35,
            'nome' => 'RETORNO DE E-MAIL',
            'cor' => null,
             'status' => true,
             'grupo' => 3
        ]);

        MotivoEvento::create([
            'id' => 36,
            'nome' => 'ALTERAÇÃO NA OPERAÇÃO',
            'cor' => null,
            'status' => true,
            'grupo' => 3
        ]);

        MotivoEvento::create([
            'id' => 37,
            'nome' => 'ENVIO DE SMS EM LOTE',
            'cor' => null,
            'status' => true,
            'grupo' => 3
        ]);
        MotivoEvento::create([
            'id' => 38,
            'nome' => 'ENVIO DE E-MAIL EM LOTE',
            'cor' => null,
            'status' => true,
            'grupo' => 3
        ]);

        MotivoEvento::create([
            'id' => 101,
            'nome' => 'OBSERVAÇÃO',
            'cor' => null,
             'status' => true,
             'grupo' => 3
        ]);

        MotivoEvento::create([
            'id' => 102,
            'nome' => 'TELEFONE ENVIADO PARA A BLACKLIST',
            'cor' => null,
             'status' => true,
             'grupo' => 3
        ]);

        MotivoEvento::create([
            'id' => 103,
            'nome' => 'EMAIL ENVIADO PARA A BLACKLIST',
            'cor' => null,
             'status' => true,
             'grupo' => 3
        ]);

        MotivoEvento::create([
            'id' => 104,
            'nome' => 'TELEFONE RETIRADO DA BLACKLIST',
            'cor' => null,
             'status' => true,
             'grupo' => 3
        ]);

        MotivoEvento::create([
            'id' => 105,
            'nome' => 'EMAIL RETIRADO DA BLACKLIST',
            'cor' => null,
             'status' => true,
             'grupo' => 3
        ]);

        MotivoEvento::create([
            'id' => 106,
            'nome' => 'ALTERAÇÃO NA OPERAÇÃO',
            'cor' => null,
             'status' => true,
             'grupo' => 3
        ]);

        MotivoEvento::create([
            'id' => 107,
            'nome' => 'ALTERAÇÃO NO CLIENTE',
            'cor' => null,
             'status' => true,
             'grupo' => 3
        ]);

    }
}
