<?php

namespace Database\Seeders;

use App\Models\Core\GrupoAcesso;
use Illuminate\Database\Seeder;

class GrupoAcessoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        GrupoAcesso::create([
            'descricao'=>'TI'
        ]);

        GrupoAcesso::create([
            'descricao'=>'ADMINISTRATIVO'
        ]);

        GrupoAcesso::create([
            'descricao'=>'FINANCEIRO'
        ]);

        GrupoAcesso::create([
            'descricao'=>'BACKOFFICE'
        ]);

        GrupoAcesso::create([
            'descricao'=>'COBRANÇA'
        ]);

        GrupoAcesso::create([
            'descricao'=>'SUPERVISÃO COBRANÇA'
        ]);

        GrupoAcesso::create([
            'descricao'=>'COMERCIAL'
        ]);

        GrupoAcesso::create([
            'descricao'=>'CREDOR'
        ]);

        GrupoAcesso::create([
            'descricao'=>'SUPERVISORES'
        ]);

        GrupoAcesso::create([
            'descricao'=>'RH'
        ]);

        GrupoAcesso::create([
            'descricao'=>'DIRETORES'
        ]);

    }
}
