<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\BackOffice\Remessa;
use App\Models\BackOffice\Operacao;
use App\Models\BackOffice\Cliente;
use Carbon\Carbon;

class OperacoesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $remessa_id = Remessa::limit(1)->get()[0]->id;
        $cliente_id = Cliente::limit(1)->get()[0]->id;

        Operacao::create([
            "numero_operacao" => 900,
            "valor_nominal" => 15,
            "data_vencimento" => Carbon::now(),
            "descricao" => "Apenas um teste de 12,35 reais",
            "cliente_id" => $cliente_id,
            "status_operacao_id" => 1,
            "remessa_id" => $remessa_id,
            "data_processamento" => Carbon::now(),
            "usuario_empresas_id" => 1,
        ]);

        Operacao::create([
            "numero_operacao" => 910,
            "valor_nominal" => 15,
            "data_vencimento" => Carbon::now(),
            "descricao" => "Apenas um teste de 12,35 reais",
            "cliente_id" => $cliente_id,
            "status_operacao_id" => 1,
            "remessa_id" => $remessa_id,
            "data_processamento" => Carbon::now(),
            "usuario_empresas_id" => 1,
        ]);

        Operacao::create([
            "numero_operacao" => 920,
            "valor_nominal" => 15,
            "data_vencimento" => Carbon::now(),
            "descricao" => "Apenas um teste de 12,35 reais",
            "cliente_id" => $cliente_id,
            "status_operacao_id" => 1,
            "remessa_id" => $remessa_id,
            "data_processamento" => Carbon::now(),
            "usuario_empresas_id" => 1,
        ]);
    }
}
