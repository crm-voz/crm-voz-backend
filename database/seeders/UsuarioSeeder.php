<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UsuarioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        User::create([
            'id'=> 1,
            'name' => 'Jairo Rodrigues',
            'cpf' => '01076601219',
            'email' => 'jairo@grupovoz.com.br',
            'password' => bcrypt('123456789'),
            'master' => false,
            'credor_id' => null
        ]);

        User::create([
            'id'=> 2,
            'name' => 'Welliton Cunha',
            'cpf' => '04601835321',
            'email' => 'welliton@grupovoz.com.br',
            'password' => bcrypt('123456789'),
            'master' => false,
            'credor_id' => null
        ]);

        User::create([
            'id'=> 3,
            'name' => 'Rafhael Gigante',
            'cpf' => '61037007360',
            'email' => 'rafael@grupovoz.com.br',
            'password' => bcrypt('123456789'),
            'master' => false,
            'credor_id' => null
        ]);

        User::create([
            'id'=> 4,
            'name' => 'Nonilton Alves',
            'cpf' => '83344837320',
            'email' => 'nonilton@grupovoz.com.br',
            'password' => bcrypt('123456789'),
            'master' => false,
            'credor_id' => null
        ]);

        User::create([
            'id'=> 5,
            'name' => 'Richard Lucas',
            'email' => 'richard@grupovoz.com.br',
            'cpf' => '60864786352',
            'password' => bcrypt('123456789'),
            'master' => false,
            'credor_id' => null
        ]);

        User::create([
            'id'=> 6,
            'name' => 'Ricardo Hítalo',
            'email' => 'ricardo@grupovoz.com.br',
            'cpf' => '06982749380',
            'password' => bcrypt('123456789'),
            'master' => false,
            'credor_id' => null
        ]);

        User::create([
            'id'=> 7,
            'name' => 'Credor Unisulma',
            'email' => 'credor@unisulma.com.br',
            'cpf' => '95010962039',
            'password' => bcrypt('123456789'),
            'master' => false,
            'credor_id' => 1
        ]);

        User::create([
            'id'=> 8,
            'name' => 'Hadassa Castro',
            'cpf' => '40114202044',
            'email' => 'hadassa@grupovoz.com.br',
            'password' => bcrypt('123456789'),
            'master' => false,
            'credor_id' => null
        ]);

        User::create([
            'id'=> 9,
            'name' => 'Andreia Sousa da Silva',
            'cpf' => '04855500321',
            'email' => 'andreia.sousa@grupovoz.com.br',
            'password' => bcrypt('123456789'),
            'master' => false,
            'credor_id' => null
        ]);

        User::create([
            'id'=> 10,
            'name' => 'Carmina Santos Freires',
            'cpf' => '60410392332',
            'email' => 'carmina.santos@grupovoz.com.br',
            'password' => bcrypt('123456789'),
            'master' => false,
            'credor_id' => null
        ]);

        User::create([
            'id'=> 11,
            'name' => 'Ester Sampaio Sousa',
            'cpf' => '61365134393',
            'email' => 'ester.sampaio@grupovoz.com.br',
            'password' => bcrypt('123456789'),
            'master' => false,
            'credor_id' => null
        ]);

        User::create([
            'id'=> 12,
            'name' => 'Ivanda Costa Santos',
            'cpf' => '61596517301',
            'email' => 'ivanda.costa@grupovoz.com.br',
            'password' => bcrypt('123456789'),
            'master' => false,
            'credor_id' => null
        ]);

        User::create([
            'id'=> 13,
            'name' => 'Marcos Madjer Souza Morais',
            'cpf' => '00761012311',
            'email' => 'marcos.madjer@grupovoz.com.br',
            'password' => bcrypt('123456789'),
            'master' => false,
            'credor_id' => null
        ]);

        User::create([
            'id'=> 14,
            'name' => 'Náthila Rodrigues Silva Viana',
            'cpf' => '05023830381',
            'email' => 'nathila@grupovoz.com.br',
            'password' => bcrypt('123456789'),
            'master' => false,
            'credor_id' => null
        ]);

        User::create([
            'id'=> 15,
            'name' => 'Daniella Lima Oliveira',
            'cpf' => '03661847396',
            'email' => 'daniella.lima@grupovoz.com.br',
            'password' => bcrypt('123456789'),
            'master' => false,
            'credor_id' => null
        ]);

        User::create([
            'id'=> 16,
            'name' => 'Nadja Farias da Rocha',
            'cpf' => '05298149359',
            'email' => 'nadja.farias@grupovoz.com.br',
            'password' => bcrypt('123456789'),
            'master' => false,
            'credor_id' => null
        ]);

        User::create([
            'id'=> 17,
            'name' => 'Natalia de Sousa Cursino',
            'cpf' => '60557347378',
            'email' => 'natalia.sousa@grupovoz.com.br',
            'password' => bcrypt('123456789'),
            'master' => false,
            'credor_id' => null
        ]);

        User::create([
            'id'=> 18,
            'name' => 'Pâmula Rejane Ciqueira de Sousa',
            'cpf' => '04521644350',
            'email' => 'pamula.rejane@grupovoz.com.br',
            'password' => bcrypt('123456789'),
            'master' => false,
            'credor_id' => null
        ]);

        User::create([
            'id'=> 19,
            'name' => 'Raíla Borges Carvalho',
            'cpf' => '04227312345',
            'email' => 'raila.borges@grupovoz.com.br',
            'password' => bcrypt('123456789'),
            'master' => false,
            'credor_id' => null
        ]);

        User::create([
            'id'=> 20,
            'name' => 'Sandra Regina da Silva Gomes Nunes',
            'cpf' => '05370577307',
            'email' => 'sandra.regina@grupovoz.com.br',
            'password' => bcrypt('123456789'),
            'master' => false,
            'credor_id' => null
        ]);

        User::create([
            'id'=> 21,
            'name' => 'Adriana Castro Cavalcante Sena',
            'cpf' => '05284985338',
            'email' => 'adriana.castro@grupovoz.com.br',
            'password' => bcrypt('123456789'),
            'master' => false,
            'credor_id' => null
        ]);
    }
}
