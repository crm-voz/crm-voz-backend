<?php

namespace Database\Seeders;

use App\Models\Core\StatusLocalizacao;
use Illuminate\Database\Seeder;

class StatusLocalizacaoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        StatusLocalizacao::create([
            'nome' => 'LOCALIZADO',
            'cor' => null,
            'status' => true
        ]);

        StatusLocalizacao::create([
            'nome' => 'NÃO LOCALIZADO',
            'cor' => null,
            'status' => true
        ]);

        StatusLocalizacao::create([
            'nome' => 'FALECIDO',
            'cor' => null,
            'status' => true
        ]);

        StatusLocalizacao::create([
            'nome' => 'DEVOLUÇÃO EM LOTE CREDOR',
            'cor' => null,
            'status' => true
        ]);

        StatusLocalizacao::create([
            'nome' => 'SEM PREVISÃO DE PAGAMENTO',
            'cor' => null,
            'status' => true
        ]);

        StatusLocalizacao::create([
            'nome' => 'PROPOSTA EM ANÁLISE DA SUPERVISÃO',
            'cor' => null,
            'status' => true
        ]);

        StatusLocalizacao::create([
            'nome' => 'AGUARDANDO ANÁLISE DA INSTITUIÇÃO',
            'cor' => null,
            'status' => true
        ]);

        StatusLocalizacao::create([
            'nome' => 'SOLICITAÇÃO _ ENVIO DE DEMONSTRATIVO',
            'cor' => null,
            'status' => true
        ]);

        StatusLocalizacao::create([
            'nome' => 'TRATAMENTO _ PENDÊNCIA URGENTE',
            'cor' => null,
            'status' => true
        ]);

        StatusLocalizacao::create([
            'nome' => 'RETORNO DA INSTITUIÇÃO',
            'cor' => null,
            'status' => true
        ]);

        StatusLocalizacao::create([
            'nome' => 'COMPROVANTE RECEBIDO',
            'cor' => null,
            'status' => true
        ]);

        StatusLocalizacao::create([
            'nome' => 'COBRANÇA SUSPENSA',
            'cor' => null,
            'status' => true
        ]);

        StatusLocalizacao::create([
            'nome' => 'GERENCIAMENTO DE PARCELAS',
            'cor' => null,
            'status' => true
        ]);

        StatusLocalizacao::create([
            'nome' => 'TÍTULOS PRESCRITOS',
            'cor' => null,
            'status' => true
        ]);

        StatusLocalizacao::create([
            'nome' => 'CLIENTE ADVERTIDO _ UNIMED RETENÇÃO',
            'cor' => null,
            'status' => true
        ]);

        StatusLocalizacao::create([
            'nome' => 'JURÍDICO',
            'cor' => null,
            'status' => true
        ]);

        StatusLocalizacao::create([
            'nome' => 'COBRANCA SUSPENSA - UNIMED',
            'cor' => null,
            'status' => true
        ]);

        StatusLocalizacao::create([
            'nome' => 'CADASTRO PARA TESTES',
            'cor' => null,
            'status' => true
        ]);

        StatusLocalizacao::create([
            'nome' => 'SOLICITAÇÃO DE ENVIO DE BOLETO - ASM',
            'cor' => null,
            'status' => true
        ]);

        StatusLocalizacao::create([
            'nome' => 'AGUARDANDO BOLETO - ASM',
            'cor' => null,
            'status' => true
        ]);

        StatusLocalizacao::create([
            'nome' => 'SOLICITACAO DE ENVIO DO TERMO - ASM',
            'cor' => null,
            'status' => true
        ]);

        StatusLocalizacao::create([
            'nome' => 'TERMO EM CONSTRUÇÃO - ASM',
            'cor' => null,
            'status' => true
        ]);

        StatusLocalizacao::create([
            'nome' => 'AGUARDANDO ASSINATURA DO TERMO - ASM',
            'cor' => null,
            'status' => true
        ]);

    }
}
