<?php

namespace Database\Seeders;

use App\Models\Core\MotivoDevolucao;
use Illuminate\Database\Seeder;

class MotivoDevolucaoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MotivoDevolucao::create([
            'nome' => 'CANCELAMENTO DE DÉBITO SISTEMA UNIMED',
            'status' => true,
             'cor' => null
        ]);

        MotivoDevolucao::create([
            'nome' => 'ENVIO PARA JURÍDICO',
            'status' => true,
             'cor' => null
        ]);

        MotivoDevolucao::create([
            'nome' => 'SOLICITAÇÃO CREDOR EM CARÁTER DE EXCEÇÃO',
            'status' => true,
             'cor' => null
        ]);

        MotivoDevolucao::create([
            'nome' => 'PAGAMENTO REALIZADO ANTES DO PRAZO CONTRATUAL DE ENVIO DA REMESSA',
            'status' => true,
             'cor' => null
        ]);

        MotivoDevolucao::create([
            'nome' => 'DEVOLUÇÃO PARA CREDOR',
            'status' => true,
             'cor' => null
        ]);

        MotivoDevolucao::create([
            'nome' => 'RESCISÃO CONTRATUAL',
            'status' => true,
             'cor' => null
        ]);

        MotivoDevolucao::create([
            'nome' => 'BAIXA SISTEMA ANTIGO',
            'status' => true,
             'cor' => null
        ]);

        MotivoDevolucao::create([
            'nome' => 'DÉBITO GERADO INDEVIDAMENTE',
            'status' => true,
             'cor' => null
        ]);

        MotivoDevolucao::create([
            'nome' => 'ALINHAMENTO DE BASE DEVOLUÇÃO AO CREDOR',
            'status' => true,
             'cor' => null
        ]);

        MotivoDevolucao::create([
            'nome' => 'TÍTULO PRESCRITO',
            'status' => true,
             'cor' => null
        ]);

        MotivoDevolucao::create([
            'nome' => 'DUPLICIDADE DE OPERAÇÃO',
            'status' => true,
             'cor' => null
        ]);

        MotivoDevolucao::create([
            'nome' => 'FIES/PROUNI',
            'status' => true,
             'cor' => null
        ]);

        MotivoDevolucao::create([
            'nome' => 'RENEGOCIAÇÃO',
            'status' => true,
             'cor' => null
        ]);

        MotivoDevolucao::create([
            'nome' => 'BOLSA ESTUDANTIL',
            'status' => true,
             'cor' => null
        ]);

        MotivoDevolucao::create([
            'nome' => 'NÃO CURSOU DÉBITOS CANCELADOS NA INSTITUIÇÃO',
            'status' => true,
             'cor' => null
        ]);

        MotivoDevolucao::create([
            'nome' => 'PERMUTA',
            'status' => true,
             'cor' => null
        ]);

        MotivoDevolucao::create([
            'nome' => 'LANÇAMENTO INDEVIDO VOZ',
            'status' => true,
             'cor' => null
        ]);

        MotivoDevolucao::create([
            'nome' => 'TÍTULO IMPRATICÁVEL PARA COBRANÇA',
            'status' => true,
             'cor' => null
        ]);

        MotivoDevolucao::create([
            'nome' => 'ALTERAÇÃO DE RESPONSÁVEL FINANCEIRO',
            'status' => true,
             'cor' => null
        ]);

        MotivoDevolucao::create([
            'nome' => 'FALECIMENTO',
            'status' => true,
             'cor' => null
        ]);

        MotivoDevolucao::create([
            'nome' => 'DUPLICIDADE DE CADASTRO',
            'status' => true,
             'cor' => null
        ]);

    }
}
