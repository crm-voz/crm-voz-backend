<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\BackOffice\Remessa;
use Carbon\Carbon;

class RemessasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Remessa::create([
            'numero_remessa' => Carbon::now()->format('Ymdhms'),
            'data_remessa' => Carbon::now(),
            'usuario_empresa_id' => 1,
            'data_entrada' => Carbon::now(),
            'nome_arquivo' => 'Teste de Remessa',
            'credor_id' => 1
        ]);
    }
}
