<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExecutivoCobrancaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('executivo_cobranca', function (Blueprint $table) {
            $table->id();
            $table->string('usuario',50)->nullable();
            $table->string('senha',50)->nullable();
            $table->string('ramal',50)->nullable();
            $table->foreignid('funcionarios_id')->references('id')->on('funcionarios');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('executivo_cobranca');
    }
}
