<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsParcelasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('parcelas', function (Blueprint $table) {
            $table->double('valor_desconto')->nullable()->default(0);
            $table->double('desconto_percentual')->nullable()->default(0);
            $table->double('valor_juros_original')->nullable()->default(0);
            $table->double('valor_multa_original')->nullable()->default(0);
            $table->double('valor_honorario_original')->nullable()->default(0);
            $table->double('juros_porcento_original')->nullable()->default(0);
            $table->double('multa_porcento_original')->nullable()->default(0);
            $table->double('honorarios_porcento_original')->nullable()->default(0);
            $table->dateTime('vencimento_original')->nullable();
            $table->string('status')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
