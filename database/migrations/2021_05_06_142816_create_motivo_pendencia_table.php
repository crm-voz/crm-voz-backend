<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMotivoPendenciaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('motivo_pendencia', function (Blueprint $table) {
            $table->id();
            $table->string('nome',100)->nullable(false);
            $table->boolean('status')->nullable(true)->default(true);
            $table->string('cor',20)->nullable(true);
            $table->integer('grupo')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('motivo_pendencia');
    }
}
