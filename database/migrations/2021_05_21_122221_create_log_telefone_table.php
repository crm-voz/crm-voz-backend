<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLogTelefoneTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_telefone', function (Blueprint $table) {
            $table->id();
            $table->foreignId('telefone_id')->references('id')->on('telefones')->onDelete('cascade');
            $table->foreignId('status_log_telefone_id')->references('id')->on('status_log_telefone')->onDelete('cascade');
            $table->text('observacao')->nullable(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_telefone');
    }
}
