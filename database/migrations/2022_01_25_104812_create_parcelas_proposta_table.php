<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateParcelasPropostaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parcelas_proposta', function (Blueprint $table) {
            $table->id();
            $table->double('valor_nominal')->nullable(false);
            $table->double('valor_previsto')->nullable(false);
            $table->date('data_vencimento')->nullable(false);
            $table->integer('numero_parcela')->nullable(false);
            $table->double('valor_juros')->nullable();
            $table->double('valor_multa')->nullable();
            $table->double('valor_honorario')->nullable();
            $table->double('valor_comissao')->nullable();
            $table->string('forma_pagamento')->nullable(false);
            $table->foreignId('proposta_id')->references('id')->on('proposta');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parcelas_proposta');
    }
}
