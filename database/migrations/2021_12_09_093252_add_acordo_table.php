<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAcordoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('acordo', function (Blueprint $table) {
            $table->double('juros_porcento')->nullable();
            $table->double('multas_porcento')->nullable();
            $table->double('honorarios_porcento')->nullable();
            $table->double('desconto_porcento')->nullable();
            $table->double('valor_entrada_porcento')->nullable();
            $table->timestamp('data_cancelou')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
