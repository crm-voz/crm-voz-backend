<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClienteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cliente', function (Blueprint $table) {
            $table->id();
            $table->string('nome',100)->nullable(false);
            $table->string('cpf_cnpj',20)->nullable(false);
            $table->string('endereco',100)->nullable();
            $table->string('bairro',50)->nullable();
            $table->string('cep',12)->nullable();
            $table->foreignid('cidades_id')->nullable(true)->references('id')->on('cidades');
            // $table->json('status_cliente_id')->nullable();
            // $table->foreignid('classificacao_cliente_id')->nullable(true)->references('id')->on('classificacao_cliente');
            $table->json('status_cliente_id')->nullable();
            $table->foreignid('status_localizacao_id')->nullable(true)->references('id')->on('status_localizacao');
            $table->enum('localizacao',['Localizado','Nao Localizado','Em Processo de Localizacao'])->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cliente');
    }
}
