<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOperacaoAcordoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('operacao_acordo', function (Blueprint $table) {
            $table->id();
            $table->foreignId('acordos_id')->references('id')->on('acordo');
            $table->foreignId('operacoes_id')->references('id')->on('operacoes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('operacao_acordo');
    }
}
