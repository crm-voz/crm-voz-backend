<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDescontosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('descontos', function (Blueprint $table) {
            $table->id();
            $table->string('tipo',45)->nullable();
            $table->double('desconto_padrao')->default(0)->nullable(false);
            $table->double('desconto_maximo')->default(0)->nullable(false);
            $table->double('desconto_minimo')->default(0)->nullable(false);
            $table->date('data_inicio')->nullable(false);
            $table->date('data_final')->nullable(false);
            $table->boolean('status')->nullable();
            $table->foreignId('operacoes_id')->references('id')->on('operacoes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('descontos');
    }
}
