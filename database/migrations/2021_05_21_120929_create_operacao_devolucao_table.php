<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOperacaoDevolucaoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('operacao_devolucao', function (Blueprint $table) {
            $table->id();
            $table->foreignId('devolucao_id')->references('id')->on('devolucao')->onDelete('cascade');
            $table->foreignId('operacao_id')->references('id')->on('operacoes')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('operacao_devolucao');
    }
}
