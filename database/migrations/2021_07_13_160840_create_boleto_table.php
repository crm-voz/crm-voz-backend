<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBoletoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('boleto', function (Blueprint $table) {
            $table->id();
            $table->string('linha_digitavel',255)->nullable();
            $table->string('codigo_barra',255)->nullable();
            $table->string('nome_beneficiario',255)->nullable();
            $table->string('cnpj_beneficiario',255)->nullable();
            $table->string('endereco_beneficiario',255)->nullable();
            $table->string('carteira',50)->nullable();
            $table->string('agencia',50)->nullable();
            $table->string('numero_documento',100)->nullable();
            $table->double('deducoes')->nullable();
            $table->double('juros')->nullable();
            $table->double('acrescimos')->nullable();
            $table->string('nome_pagador',255)->nullable();
            $table->string('cpf_pagador',15)->nullable();
            $table->string('nome_avaliador',255)->nullable();
            $table->integer('codigo_credor')->nullable();
            $table->string('nosso_numero',45)->nullable();
            $table->date('vencimento')->nullable();
            $table->date('baixa')->nullable();
            $table->double('valor')->nullable();
            $table->double('valor_pago')->nullable();
            $table->double('multa')->nullable();
            $table->double('desconto')->nullable();
            $table->string('id_pix')->nullable();
            $table->text('chave_pix')->nullable();
            $table->string('convenio')->nullable();
            $table->string('codigo_banco')->nullable();
            $table->foreignId('parcelas_id')->references('id')->on('parcelas');
            $table->foreignid('usuario_empresas_id')->references('id')->on('usuario_empresas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('boleto');
    }
}
