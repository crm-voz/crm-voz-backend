<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFuncionariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('funcionarios', function (Blueprint $table) {
            $table->id();
            $table->string('nome',100)->nullable(false);
            $table->string('cpf',15)->nullable(false);
            $table->string('rg',30)->nullable();
            $table->string('endereco',100)->nullable();
            $table->string('observacao')->nullable();
            $table->foreignId('cidade_id')->nullable(true)->references('id')->on('cidades');
            $table->foreignid('funcao_id')->nullable(true)->references('id')->on('funcao');
            // $table->foreignid('grupo_acesso_id')->nullable(false)->references('id')->on('grupo_acesso');
            $table->foreignid('usuario_empresas_id')->nullable(true)->references('id')->on('usuario_empresas');
            $table->boolean('status')->nullable()->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('funcionarios');
    }
}

