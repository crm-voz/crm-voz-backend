<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePermissoesPerfilTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permissoes_perfil', function (Blueprint $table) {
            $table->id();
            $table->boolean('modificar')->nullable();
            $table->foreignId('perfil_usuario_id')->references('id')->on('perfil_usuario');
            $table->foreignId('endpoints_id')->references('id')->on('endpoints');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permissoes_perfil');
    }
}
