<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDevolucaoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('devolucao', function (Blueprint $table) {
            $table->id();
            $table->text('descricao')->nullable();
            $table->foreignId('motivo_devolucao_id')->references('id')->on('motivo_devolucao');
            $table->foreignId('usuario_empresas_id')->references('id')->on('usuario_empresas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('devolucao');
    }
}
