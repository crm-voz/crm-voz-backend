<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterOperacoesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('operacoes', function (Blueprint $table) {
            $table->double('desconto')->default(0);
            $table->string('origem_desconto')->nullable();
            $table->string('aluno')->nullable();
            $table->string('competencia')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('operacoes', function (Blueprint $table) {
            $table->dropColumn('desconto');
        });
    }
}
