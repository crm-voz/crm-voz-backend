<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eventos', function (Blueprint $table) {
            $table->id();
            $table->text('descricao')->nullable(false);
            $table->foreignId('cliente_id')->references('id')->on('cliente');
            $table->foreignId('usuario_empresas_id')->references('id')->on('usuario_empresas');
            $table->foreignId('motivo_evento_id')->references('id')->on('motivo_evento');
            $table->foreignId('credores_id')->references('id')->on('credores');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eventos');
    }
}
