<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBaixaBoletoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('baixa_boleto', function (Blueprint $table) {
            $table->id();
            $table->string('descricao',100)->nullable(false);
            $table->string('banco',100)->nullable(false);
            $table->string('convenio',45)->nullable(false);
            $table->integer('cnab')->nullable(false);
            $table->integer('quantidade_baixada')->nullable(false);
            $table->double('valor_total_baixado')->nullable(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('baixa_boleto');
    }
}
