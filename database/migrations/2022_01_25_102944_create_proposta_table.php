<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePropostaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proposta', function (Blueprint $table) {
            $table->id();
            $table->string('numero_proposta',45)->nullable(false);
            $table->double('valor_nominal')->nullable(false);
            $table->double('juros')->nullable();
            $table->double('multas')->nullable();
            $table->double('honorarios')->nullable();
            $table->double('desconto')->nullable();
            $table->double('valor_entrada')->nullable();
            $table->double('quantidade_parcelas')->nullable();
            $table->double('valor_parcela')->nullable();
            $table->foreignId('cliente_id')->references('id')->on('cliente');
            $table->foreignId('executivo_cobranca_id')->references('id')->on('executivo_cobranca')->nullable();
            $table->bigInteger('supervisor_cobranca_id')->unsigned()->nullable(true);
            $table->foreign('supervisor_cobranca_id')->references('id')->on('executivo_cobranca');
            $table->double('juros_porcento')->nullable();
            $table->double('multas_porcento')->nullable();
            $table->double('honorarios_porcento')->nullable();
            $table->double('desconto_porcento')->nullable();
            $table->double('valor_entrada_porcento')->nullable();
            $table->enum('status',['Aprovado', 'Analise', 'Reprovado'])->default('Analise');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proposta');
    }
}
