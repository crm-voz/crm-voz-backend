<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFornecedorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fornecedor', function (Blueprint $table) {
            $table->id();
            $table->string('nome_fantasia')->nullable(false);
            $table->string('razao_social')->nullable(false);
            $table->string('cnpj')->nullable(false);
            $table->string('inscricao_estadual')->nullable(true);
            $table->foreignId('cidade_id')->nullable(true)->references('id')->on('cidades');
            $table->string('endereco')->nullable();
            $table->string('cep')->nullable(true);
            $table->string('representante')->nullable(true);
            $table->string('contato_representante')->nullable(true);
            $table->string('chave_pix')->nullable(true);
            $table->string('telefone')->nullable(true);
            $table->string('celular')->nullable(true);
            $table->string('email')->nullable(true);
            $table->string('site')->nullable(true);
            $table->string('contato')->nullable(true);
            $table->text('observacao')->nullable();
            $table->foreignid('empresa_id')->nullable(true)->references('id')->on('empresas');
            $table->boolean('status')->nullable()->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fornecedor');
    }
}
