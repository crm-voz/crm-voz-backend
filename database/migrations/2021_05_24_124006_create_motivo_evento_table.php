<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMotivoEventoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('motivo_evento', function (Blueprint $table) {
            $table->id();
            $table->string('nome',150)->nullable(false);
            $table->string('cor',20)->nullable(true);
            $table->boolean('status')->nullable(true)->default(true);
            $table->integer('grupo')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('motivo_evento');
    }
}
