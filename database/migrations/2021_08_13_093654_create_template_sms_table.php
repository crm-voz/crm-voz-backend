<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTemplateSmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('template_sms', function (Blueprint $table) {
            $table->id();
            $table->string('titulo',45)->nullable(false);
            $table->text('mensagem')->nullable(false);
            $table->boolean('status')->default(true)->nullable();
            $table->foreignid('credores_id')->nullable(true)->references('id')->on('credores');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('template_sms');
    }
}
