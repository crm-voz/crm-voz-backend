<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAcordoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('acordo', function (Blueprint $table) {
            $table->id();
            $table->string('numero_acordo',45)->nullable(false);
            $table->string('status',30)->nullable();
            $table->double('valor_nominal')->nullable(false);
            $table->double('juros')->nullable();
            $table->double('multas')->nullable();
            $table->double('honorarios')->nullable();
            $table->double('desconto')->nullable();
            $table->double('valor_entrada')->nullable();
            $table->double('quantidade_parcelas')->nullable();
            $table->double('valor_parcela')->nullable();
            $table->text('descricao_cancelamento')->nullable();
            $table->json('transacao_cartao')->nullable();
            $table->foreignId('cliente_id')->references('id')->on('cliente');
            $table->foreignId('executivo_cobranca_id')->references('id')->on('executivo_cobranca')->nullable();
            $table->bigInteger('usuario_cancelou_id')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('acordo');
    }
}
