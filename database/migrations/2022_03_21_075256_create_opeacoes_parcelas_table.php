<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOpeacoesParcelasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('operacoes_parcelas', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('credor_id')->unsigned();
            $table->foreign('credor_id')->references('id')->on('credores')->onUpdate('cascade');
            $table->bigInteger('operacao_id')->unsigned()->nullable(true);
            $table->foreign('operacao_id')->references('id')->on('operacoes')->onUpdate('cascade');
            $table->bigInteger('parcela_id')->unsigned()->nullable(true);
            $table->foreign('parcela_id')->references('id')->on('parcelas')->onUpdate('cascade');
            $table->string('instituicao',255)->nullable(false);
            $table->string('cliente',255)->nullable(false);
            $table->string('aluno',255)->nullable(false);
            $table->string('documento',50)->nullable();
            $table->string('numero_operacao',50)->nullable();
            $table->date('vencimento_original',20)->nullable();
            $table->date('vencimento')->nullable();
            $table->date('recebimento')->nullable();
            $table->string('parcela',50)->nullable();
            $table->float('valor_negociado',10,2)->nullable();
            $table->float('valor_repasse',10,2)->nullable();
            $table->string('origem',255)->nullable();
            $table->float('principal',10,2)->nullable();
            $table->float('juros',10,2)->nullable();
            $table->float('multa',10,2)->nullable();
            $table->float('correcao',10,2)->nullable();
            $table->float('desconto',10,2)->nullable();
            $table->float('honorarios',10,2)->nullable();
            $table->float('realizado',10,2)->nullable();
            $table->float('taxa_cartao',10,2)->nullable();
            $table->float('comissao',10,2)->nullable();
            $table->float('repasse',10,2)->nullable();
            $table->string('forma',100)->nullable();
            $table->string('referencia',100)->nullable();
            $table->string('numero_documento',250)->nullable();
            $table->float('desconto_acordo',10,2)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('operacoes_parcelas');
    }
}
