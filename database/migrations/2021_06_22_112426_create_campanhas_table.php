<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCampanhasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campanhas', function (Blueprint $table) {
            $table->id();
            $table->enum('tipo_campanha',['sms','email','ativo']);
            $table->datetime('data_inicio_campanha')->nullable();
            $table->datetime('data_final_campanha')->nullable();
            $table->integer('respiro')->nullable();
            $table->json('filtro')->nullable();
            $table->integer('template_id')->default(0)->nullable();
            $table->integer('template_email_id')->default(0)->nullable();
            $table->string('tipo_filtro')->default('operacao');
            $table->string('servico')->nullable();
            $table->boolean('primeira_execucao')->default(false);
            $table->foreignid('usuario_empresas_id')->references('id')->on('usuario_empresas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campanhas');
    }
}
