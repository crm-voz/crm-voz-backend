<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLogPendenciasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_pendencias', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('operacao_id')->unsigned();
            $table->foreign('operacao_id')->references('id')->on('operacoes')->onDelete('cascade');
            $table->bigInteger('usuario_empresa_id')->unsigned();
            $table->foreign('usuario_empresa_id')->references('id')->on('usuario_empresas');
            $table->text('observacao')->nullable(true);
            $table->enum('status',['REGISTRO','INICIADO','EM ANDAMENTO','CONCLUIDO'])->default('REGISTRO');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_pendencias');
    }
}
