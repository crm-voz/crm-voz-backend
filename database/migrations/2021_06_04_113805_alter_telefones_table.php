<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTelefonesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('telefones', function (Blueprint $table) {
            $table->enum('origem',['Credor','Enriquecimento','Atualização Cadastral']);
            $table->enum('tags',['Fixo','Móvel','Móvel/Whatsapp'])->default('Móvel');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropColumns('telefones',['origem','tags']);
    }
}
