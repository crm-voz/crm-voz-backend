<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterOperacoesParcelaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('parcelas', function(Blueprint $table){
            $table->float('valor_nominal_original',10,2)->nullable(true)->default(0);
        });

        Schema::table('parcelas', function(Blueprint $table){
            $table->dropColumn('valor_juros_original');
            $table->dropColumn('valor_multa_original');
            $table->dropColumn('valor_honorario_original');
            $table->dropColumn('juros_porcento_original');
            $table->dropColumn('multa_porcento_original');
            $table->dropColumn('honorarios_porcento_original');
            $table->dropColumn('json_parcela');
        });

        Schema::table('operacoes_parcelas', function (Blueprint $table){
            $table->float('valor_juros_original', 10,2)->nullable(true)->default(0);
            $table->float('valor_multa_original', 10,2)->nullable(true)->default(0);
            $table->float('valor_honorario_original', 10,2)->nullable(true)->default(0);
            $table->float('valor_desconto_original', 10,2)->nullable(true)->default(0);
            $table->float('valor_comissao_original', 10,2)->nullable(true)->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
