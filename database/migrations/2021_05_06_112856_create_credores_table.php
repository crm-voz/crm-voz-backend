<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCredoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('credores', function (Blueprint $table) {
            $table->id();
            $table->string('nome',100)->nullable(false);
            $table->string('razao_social', 100)->nullable(false);
            $table->string('cnpj',20)->nullable(false);
            $table->string('inscricao_estadual',30)->nullable();
            $table->string('cep', 45)->nullable();
            $table->string('endereco', 100)->nullable();
            $table->string('bairro', 100)->nullable();
            $table->string('numero', 45)->nullable();
            $table->string('telefone',20)->nullable();
            $table->string('celular', 20)->nullable();
            $table->string('email', 50)->nullable();
            $table->boolean('status')->default(true);
            $table->json('parametros_config')->nullable();
            $table->foreignId('empresa_id')->references('id')->on('empresas');
            $table->foreignId('cidade_id')->references('id')->on('cidades');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('credores');
    }
}
