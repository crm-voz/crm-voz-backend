<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStatusLocalizacaoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('status_localizacao', function (Blueprint $table) {
            $table->id();
            $table->String('nome',40)->nullable(false);
            $table->string('cor',45)->nullable(true);
            $table->boolean('status')->nullable(true)->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('status_localizacao');
    }
}
