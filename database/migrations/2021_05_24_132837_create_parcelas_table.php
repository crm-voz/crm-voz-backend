<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateParcelasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parcelas', function (Blueprint $table) {
            $table->id();
            $table->double('valor_nominal')->nullable(false);
            $table->date('data_vencimento')->nullable(false);
            $table->integer('numero_parcela')->nullable(false);
            $table->dateTime('data_baixa')->nullable();
            $table->double('valor_baixado')->nullable();
            $table->double('valor_juros')->nullable();
            $table->double('valor_multa')->nullable();
            $table->double('valor_honorario')->nullable();
            $table->double('valor_comissao')->nullable();
            $table->string('forma_pagamento')->nullable(false);
            $table->json('transacao_parcela')->nullable();
            $table->foreignId('acordos_id')->references('id')->on('acordo');
            $table->foreignId('usuario_empresas_id')->references('id')->on('usuario_empresas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parcelas');
    }
}
