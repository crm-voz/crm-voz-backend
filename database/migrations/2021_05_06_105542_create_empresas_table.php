<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmpresasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empresas', function (Blueprint $table) {
            $table->id();
            $table->string('nome_fantasia',100)->nullable(false);
            $table->string('razao_social',100)->nullable(false);
            $table->string('cnpj',20)->nullable(false)->unique();
            $table->string('inscricao_estadual',30)->nullable();
            $table->foreignId('cidade_id')->references('id')->on('cidades');
            $table->string('endereco',100)->nullable();
            $table->string('telefone', 20)->nullable();
            $table->string('celular',20)->nullable();
            $table->string('email', 50)->nullable();
            $table->boolean('status')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empresas');
    }
}
