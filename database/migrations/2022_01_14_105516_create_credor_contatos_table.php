<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCredorContatosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('credor_contatos', function (Blueprint $table) {
            $table->id();
            $table->string('nome',60)->nullable();
            $table->string('telefone',20)->nullable();
            $table->string('email',100)->nullable();
            $table->text('observacoes')->nullable();
            $table->foreignId('credor_id')->references('id')->on('credores');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('credor_contatos');
    }
}
