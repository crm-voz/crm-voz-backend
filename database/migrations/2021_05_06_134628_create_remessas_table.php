<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRemessasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('remessas', function (Blueprint $table) {
            $table->id();
            $table->string('numero_remessa',45)->nullable(false);
            $table->dateTime('data_remessa')->nullable();
            $table->dateTime('data_entrada')->nullable(false);
            $table->string('nome_arquivo',100)->nullable();
            $table->foreignid('credor_id')->references('id')->on('credores');
            $table->foreignid('usuario_empresa_id')->references('id')->on('usuario_empresas');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('remessas');
    }
}
