<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCredorCarteiraTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('credor_carteira', function (Blueprint $table) {
            $table->id();
            $table->foreignId('carteira_id')->references('id')->on('carteira')->onUpdate('cascade');
            $table->foreignId('credor_id')->references('id')->on('credores')->onUpdate('cascade');
            $table->double('meta')->default(0)->nullable(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('credor_carteira');
    }
}
