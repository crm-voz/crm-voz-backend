<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExecutivosCarteiraTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('executivos_carteira', function (Blueprint $table) {
            $table->id();
            $table->foreignId('executivo_cobranca_id')->references('id')->on('executivo_cobranca')->onUpdate('cascade');
            $table->foreignId('carteira_id')->references('id')->on('carteira')->onUpdate('cascade');
            $table->double('meta')->default(0)->nullable(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('executivos_carteira');
    }
}
