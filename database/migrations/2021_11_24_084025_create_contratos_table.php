<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContratosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contratos', function (Blueprint $table) {
            $table->id();
            $table->date('data_inicio')->nullable(false);
            $table->date('data_final')->nullable(false);
            $table->text('descricao')->nullable();
            $table->text('objeto')->nullable();
            $table->string('responsavel',100)->nullable();
            $table->string('telefone_responsavel',20)->nullable();
            $table->string('email_responsavel',100)->nullable();
            $table->boolean('status');
            $table->foreignId('fornecedor_id')->references('id')->on('fornecedor');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contratos');
    }
}
