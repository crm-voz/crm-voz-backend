<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOperacoesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('operacoes', function (Blueprint $table) {
            $table->id();
            $table->String('numero_operacao',50)->nullable(false);
            $table->datetime('data_atualizacao')->nullable();
            $table->date('data_vencimento')->nullable(false);
            $table->datetime('data_processamento')->nullable();
            $table->double('valor_nominal')->default(0)->nullable(false);
            $table->double('valor_atualizado')->default(0)->nullable();
            $table->double('valor_juros')->default(0)->nullable();
            $table->double('valor_multa')->default(0)->nullable();
            $table->double('valor_honorario')->default(0)->nullable();
            $table->text('descricao')->nullable();
            $table->foreignid('cliente_id')->references('id')->on('cliente');
            $table->foreignid('motivo_pendencia_id')->nullable(true)->references('id')->on('motivo_pendencia');
            $table->foreignid('usuario_empresas_id')->references('id')->on('usuario_empresas');
            $table->foreignid('status_operacao_id')->references('id')->on('status_operacao');
            $table->foreignid('classificacao_negativacao_id')->nullable(true)->references('id')->on('classificacao_negativacao');
            $table->foreignid('remessa_id')->references('id')->on('remessas');
            $table->boolean('divida_reconhecida')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('operacoes');
    }
}
