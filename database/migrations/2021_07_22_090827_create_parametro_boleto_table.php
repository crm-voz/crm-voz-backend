<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateParametroBoletoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parametro_boleto', function (Blueprint $table) {
            $table->id();
            $table->integer('codigo_banco')->nullable();
            $table->string('nome_banco',100)->nullable();
            $table->integer('convenio')->nullable();
            $table->string('carteira',45)->nullable();
            $table->string('agencia',45)->nullable();
            $table->string('conta',45)->nullable();
            $table->json('parametro')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parametro_boleto');
    }
}
