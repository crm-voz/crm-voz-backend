<?php

namespace App\Repository\Financeiro;

use App\Models\Core\Boleto;
use App\Repository\Backoffice\BaseRepository;
use Carbon\Carbon;

class BoletoRepository extends BaseRepository
{
    public function __construct(Boleto $boleto)
    {
        $this->model = $boleto;
    }

    public function getAll()
    {
       $boletos = $this->model->all();
        foreach ($boletos as $value) {
            $value->valor = intval($value->valor);
            $value->valor_pago = intval($value->valor_pago);
            $value->multa = intval($value->multa);
            $value->desconto = intval($value->desconto);
            $value->deducoes = intval($value->deducoes);
            $value->juros = intval($value->juros);
            $value->acrescimos = intval($value->acrescimos);
        }
        return $boletos;
    }

    public function baixarBoleto(Boleto $boleto, $dataBaixa, $valorPago){
       return $boleto->update([
            'valor_pago' => ($boleto?->valor ==  $valorPago) ? $boleto->valor : $valorPago,
            'baixa' => Carbon::createFromFormat('d/m/Y',$dataBaixa)->format('Y-m-d'),
        ]);
    }

    public function retirarBaixaBoleto(Boleto $boleto)
    {
        return $boleto->update(['valor_pago' => null,'baixa' => null,]);
    }

    public function getDataBoleto($dtInicio, $dtFinal)
    {
        return $this->model->whereBetween('boleto.vencimento', [$dtInicio, $dtFinal])
                           ->get();
    }
}
