<?php

namespace App\Repository\Cobranca;

use App\Models\Cobranca\Parcela;
use App\Models\Core\Boleto;
use App\Repository\Backoffice\BaseRepository;
use App\Repository\Backoffice\EventosRepository;
use App\Repository\Financeiro\BoletoRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class ParcelaRepository extends BaseRepository {

    protected $parcelaRepository;
    protected const EVENTO_RETIRADA_BAIXA = 23;
    protected const EVENTO_INCLUIR_BAIXA = 39;

    public function __construct(Parcela $model,
                                BoletoRepository $boletoRepository,
                                OperacoesParcelasRepository $operacoesParcelasRepository,
                                EventosRepository $eventosRepository)
    {
       parent::__construct($model);
       $this->operacoesParcelasRepository = $operacoesParcelasRepository;
       $this->boletoRepository = $boletoRepository;
       $this->eventosRepository = $eventosRepository;
    }

    protected function retirarBaixaOperacoesParcela($parcela){
        return $this->operacoesParcelasRepository->retirarBaixaOperacoesParcela($parcela);
    }

    protected function retirarBaixaBoleto(Boleto $boleto){
        return $this->boletoRepository->retirarBaixaBoleto($boleto);
    }

    protected function baixarBoleto(Boleto $boleto, $dataBaixa, $valorPago){
        return $boleto->update([
                                    'valor_pago' => ($boleto?->valor ==  $valorPago) ? $boleto->valor : $valorPago,
                                    'baixa' => Carbon::createFromFormat('d/m/Y',$dataBaixa)->format('Y-m-d'),
                                ]);
    }

    protected function retirarBaixaParcela(Parcela $parcela){
        #retirar baixa da operação da parcela
        $this->retirarBaixaOperacoesParcela($parcela);
        return  $parcela->update(['data_baixa' => null, 'valor_baixado' =>  null, 'valor_comissao' => null,'status' => null]);
    }

    protected function baixarParcela(Parcela $parcela, $dataBaixa, $valorComissao, $valorPago){
        return $parcela->update(['data_baixa' => Carbon::createFromFormat('d/m/Y', $dataBaixa)->format('Y-m-d H:i:s'),
                                 'valor_baixado' =>  ($parcela?->valor_nominal == $valorPago) ? $parcela?->valor_nominal : $valorPago,
                                 'valor_comissao' => $valorComissao,
                                 'status' => 'Liquidado']);
    }

    public function getParcelas(Array $ids){
        return $this->model->whereIn('id', $ids)->get();
    }

    public function retirarBaixa($parcelas){
        try {
            foreach($parcelas as $parcela){
                DB::beginTransaction();
                #retira a baixa da parcela
                $this->retirarBaixaParcela($parcela);

                #recupera o boleto da parcela
                $boleto = $parcela->boleto->first();
                #se existir retira a baixa
                if ($boleto){
                    $this->boletoRepository->retirarBaixaBoleto($boleto);
                }

                #devolve o status do acordo para aguardando
                $parcela->acordos->update(['status' => 'Aguardando']);
                #marca todas as operacoes do acordo como negociadas.
                foreach($parcela->acordos->operacoes_acordo as $op) {
                    $op->operacao->update([
                        'status_operacao_id' => 4
                    ]);
                }

                $this->eventosRepository->createEventos(
                    $parcela->acordos->cliente_id,
                    "RETIRADA DE BAIXA MANUAL DE PARCELA. ACORDO: " . $parcela->acordos->numero_acordo
                                                        . " PARCELA " . $parcela->numero_parcela
                                                        . " VALOR: " . number_format($parcela->valor_nominal, 2,'.',','),
                    self::EVENTO_RETIRADA_BAIXA
                );

                DB::commit();
            }
            return ['result' => true];
        } catch (\Throwable $th) {
            DB::rollBack();
            return ['result' => false, 'erro' => $th->getMessage()];
        }
    }

    public function incluirBaixa($parcelas, $dataBaixa, $valorComissao, $valorPago, $gerarEvento = false){
        try {
            foreach($parcelas as $parcela){
                DB::beginTransaction();
                #executa a baixa da parcela
                $this->baixarParcela($parcela, $dataBaixa, $valorComissao, $valorPago);
                #recupera o boleto da parcela
                $boleto = $parcela->boleto->first();

                #executa a baixa do boleto
                if ($boleto){
                    $this->boletoRepository->baixarBoleto($boleto, $dataBaixa, $valorPago);
                }

                #altera a data de recebimento das parcelas da operação
                $operacoesParcelas = $this->operacoesParcelasRepository->get($parcela);
                foreach($operacoesParcelas as $opPar){
                    $opPar->update([
                        'recebimento' => Carbon::createFromFormat('d/m/Y', $dataBaixa)->format('Y-m-d'),
                    ]);
                }

                if ($gerarEvento){
                    $this->eventosRepository->createEventos(
                        $parcela->acordos->cliente_id,
                        "BAIXA MANUAL DE PARCELA. ACORDO: " . $parcela->acordos->numero_acordo
                        . " PARCELA " . $parcela->numero_parcela
                        . " VALOR: " . number_format($parcela->valor_nominal, 2,'.',','),
                        self::EVENTO_INCLUIR_BAIXA
                    );
                }
                DB::commit();

                #verificar se todas as parcelas do acordo da parcela estão pagas.
                $parcelasAbertas = $this->model->where('acordos_id', $parcela->acordos->id)
                                               ->where('status', 'Liquidado')->get();

                DB::beginTransaction();
                if ($parcelasAbertas->count() == $parcela->acordos->quantidade_parcelas) {
                    $parcela->acordos->update(['status' => 'Liquidado']);
                    foreach($parcela->acordos->operacoes_acordo as $op){
                        $op->operacao->update([
                            'status_operacao_id' => 2
                        ]);
                    }
                }else{
                    $parcela->acordos->update(['status' => 'Negociado']);
                }

                DB::commit();
            }
            return ['result' => true];
        } catch (\Throwable $th) {
            DB::rollBack();
            return ['result' => false, 'erro' => $th->getMessage()];
        }
    }

//TODO: REFATORAR METODO FILTRARPARCELAS E REMOVER O SQL INTERNO DO METODO
    public function filtrarParcelas($dados){
        $parcela = DB::table('parcelas')
                    ->select('parcelas.*','cliente.id as cliente_id','cliente.nome as nome_cliente','credores.id as credor_id','credores.nome as nome_credor')
                    ->join('acordo','acordo.id','=','parcelas.acordos_id')
                    ->join('cliente','cliente.id','=','acordo.cliente_id')
                    ->join('operacoes','operacoes.cliente_id','=','cliente.id')
                    ->join('remessas','remessas.id','=','operacoes.remessa_id')
                    ->join('credores','credores.id','=','remessas.credor_id')
                    ->where(function($query) use($dados){
                        if(($dados->data_inicial) && ($dados->data_final)){
                            $query->where('parcelas.data_vencimento','>',$dados->data_inicial);
                            $query->where('parcelas.data_vencimento','<',$dados->data_final);
                        }
                    })->distinct(DB::raw('operacoes.numero_operacao'))->get();

        if(!$parcela){
            return false;
        }

        return $parcela;
    }

    public function filtrar($filtro){
        $parcelas = $this->model->query();
        if(array_key_exists('credores', $filtro)) {
            $parcelas->whereHas('acordos', function($qry) use($filtro){
                $qry->whereHas('operacoes_acordo', function($query) use($filtro){
                    $query->whereHas('operacao', function($q) use($filtro){
                        $q->whereHas('remessa', function($qr) use($filtro){
                            $qr->whereIn('credor_id', $filtro['credores']);
                        });
                    });
                });
            });

        }
        if(array_key_exists('classificacao_cliente', $filtro)){
            $parcelas->whereHas('acordos', function ($query) use($filtro) {
                 $query->whereHas('cliente', function($q) use($filtro){
                    if($filtro['classificacao_cliente']['exceto'] == 0) {
                        $q->whereIn('status_cliente_id->title', $filtro['classificacao_cliente']['valor']);
                    } else {
                        $q->whereNotIn('status_cliente_id->title', $filtro['classificacao_cliente']['valor']);
                    }

                    if ($filtro['periodo_contato']['exceto'] == 0) {
                        $q->whereBetween('ultimo_contato', [$filtro['periodo_contato']['data_inicio'], $filtro['periodo_contato']['data_final']]);
                    }else{
                        $q->whereNotBetween('ultimo_contato', [$filtro['periodo_contato']['data_inicio'], $filtro['periodo_contato']['data_final']]);
                    }

                    if ($filtro['localizacao']['exceto'] == 0) {
                        $q->where('localizacao', [$filtro['localizacao']['valor']]);
                    }else{
                        $q->whereNot('localizacao', [$filtro['localizacao']['valor']]);
                    }
                 });
            });
        }

        if (array_key_exists('vencimento', $filtro)){
            if ($filtro['vencimento']['exceto'] == 0) {
                $parcelas->whereBetween('data_vencimento', [$filtro['vencimento']['data_inicio'],$filtro['vencimento']['data_final']]);
            }else{
                $parcelas->whereNotBetween('data_vencimento', [$filtro['vencimento']['data_inicio'],$filtro['vencimento']['data_final']]);
            }
        }
        return $parcelas->get();
    }
}
