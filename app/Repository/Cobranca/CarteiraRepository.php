<?php

namespace App\Repository\Cobranca;

use App\Models\Core\Carteira;
use App\Models\Core\CredoresCarteira;
use App\Models\Core\ExecutivosCarteira;
use App\Repository\Backoffice\BaseRepository;
use App\Repository\Backoffice\CredorRepository;
use App\Repository\Backoffice\ExecutivoCobrancaRepository;
use App\Services\CarteiraServices;
use Exception;
use Illuminate\Support\Facades\DB;

class CarteiraRepository extends BaseRepository
{
    public function __construct(Carteira $carteira,
                                ExecutivosCarteira $executivosCarteira,
                                CredoresCarteira $credoresCarteira,
                                ExecutivoCobrancaRepository $executivoCobrancaRepository,
                                CredorRepository $credorRepository)
    {
        parent::__construct($carteira);
        $this->executivosCarteira = $executivosCarteira;
        $this->credoresCarteira = $credoresCarteira;
        $this->executivoCobrancaRepository = $executivoCobrancaRepository;
        $this->credorRepository = $credorRepository;
    }

    public function getDadosCreate(){
        return ['executivos' => $this->executivoCobrancaRepository->ListarExecutivos(),
                'credores' => $this->credorRepository->ListarCredores(),
                'supervisores' => $this->executivoCobrancaRepository->listarSupervisores()];
    }

    public function getCarteira($idCarteira){
        $carteira = $this->model->find($idCarteira);
        if(!$carteira){
            throw new Exception('Carteira não localizada');
        }
        $service = app(CarteiraServices::class);
        return $service->calcularDesempenho($carteira);
    }

    public function listarCarteiras(){
        $carteiras = $this->model->orderBy('id','desc')->get();
        $service = app(CarteiraServices::class);
        $lista = collect();
        foreach($carteiras as $carteira){
            $lista->add($service->calcularDesempenho($carteira));
        }

        return $lista;
    }

    public function criarCarteira($dados){
        try {
            DB::beginTransaction();

            $carteira = $this->model->create([
                'executivo_cobranca_id' => $dados['executivo_cobranca_id'],
                'descricao' => $dados['descricao'],
                'meta' => $dados['meta'],
            ]);

            foreach( json_decode($dados['executivos']) as $executivo){
                $this->executivosCarteira->create([
                    'executivo_cobranca_id' => $executivo->id,
                    'meta' => str_replace(',','.', $executivo->meta),
                    'carteira_id' => $carteira->id,
                ]);
            }

            foreach(json_decode($dados['credores']) as $credor){
                $this->credoresCarteira->create([
                    'credor_id' => $credor->id,
                    'carteira_id' => $carteira->id,
                    'meta' => str_replace(',','.', $credor->meta),
                ]);
            }
            DB::commit();

        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
        return $carteira;
    }

    public function excluirCarteira($id)
    {
        try {
            $carteira = $this->model->find($id);
            DB::beginTransaction();
            if (!$carteira){
                $carteira->credoresCarteira->delete();
                $carteira->executivos->delete();
                $carteira->delete();
            }
            DB::commit();
        } catch (\Throwable $th) {
            DB::rollBack();
            throw $th;
        }
    }

    public function supervisorDoExecutivo($executivoId){
        $executivo = $this->executivosCarteira->where('executivo_cobranca_id', $executivoId)
                          ->orderBy('carteira_id','desc')->first();
        return $executivo->carteira->supervisor;
    }

    public function carteiraDoExecutivo($executivoId){
        $executivo = $this->executivosCarteira->where('executivo_cobranca_id', $executivoId)
                                              ->orderBy('carteira_id','desc')->first();
        return $executivo->carteira;
    }


}
