<?php

namespace App\Repository\Cobranca;

use App\Models\Cobranca\OperacoesParcelas;
use App\Models\Cobranca\Parcela;
use App\Repository\Backoffice\BaseRepository;
use App\Services\OperacoesParcelaServices;

class OperacoesParcelasRepository extends BaseRepository
{
    public function __construct(OperacoesParcelas $operacaoParcela,
                                OperacoesParcelaServices $operacoesParcelaServices)
    {
        parent::__construct($operacaoParcela);
        $this->operacoesParcelaServices = $operacoesParcelaServices;
    }

    public function filter(Array $filter) {
        $query = $this->model->query();

        if (array_key_exists('credor_id', $filter)){
            $query->where('credor_id', $filter['credor_id']);
        }

        if(array_key_exists('data_inicio', $filter) && array_key_exists('data_final', $filter)){
            $query->whereBetween('recebimento',[$filter['data_inicio'],$filter['data_final']]);
        }

        if (array_key_exists('forma_pagamento', $filter)){
            $query->where('forma', $filter['forma_pagamento']);
        }
         return $this->operacoesParcelaServices->listar( $query->get() );
    }

    public function get(Parcela $parcela){
        return $this->model->where('parcela_id', $parcela->id)->get();
    }

    public function retirarBaixaOperacoesParcela($parcela){
        $OperacoesParcelas = $this->get($parcela);
        foreach($OperacoesParcelas as $operacaoParcela){
            $operacaoParcela->update([
                'recebimento' => null,
            ]);
        }
    }
}
