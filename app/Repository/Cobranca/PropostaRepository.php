<?php

namespace App\Repository\Cobranca;

use App\Models\BackOffice\Operacao;
use App\Models\Cobranca\OperacaoProposta;
use App\Models\Cobranca\ParcelasProposta;
use App\Models\Cobranca\Proposta;
use App\Repository\Backoffice\BaseRepository;
use App\Repository\Backoffice\EventosRepository;
use App\Services\MessagesServices;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class PropostaRepository extends BaseRepository
{
    public function __construct(Proposta $proposta,
                                EventosRepository $eventosRepository,
                                CarteiraRepository $carteiraRepository)
    {
        parent::__construct($proposta);
        $this->eventosRepository = $eventosRepository;
        $this->carteiraRepository = $carteiraRepository;
    }

    public function listarPropostaPorSupervisor($status, $id){
        try {
            $result = $this->model->select('proposta.*','cliente.nome as nome_cliente','credores.nome as nome_credor')
                      ->join('cliente','cliente.id','=','proposta.cliente_id')
                      ->join('operacoes','operacoes.cliente_id','=','proposta.cliente_id')
                      ->join('remessas','remessas.id','=','operacoes.remessa_id')
                      ->join('credores','credores.id','=','remessas.credor_id')
                      ->where(function($query) use($status, $id){
                        if($status){
                                    $query->where('proposta.status',$status);
                                }
                        if ($id){
                            $query->where('proposta.supervisor_cobranca_id', $id);
                        }
                      })->distinct(DB::raw('id'))->get();

            return $result;
        } catch (\Throwable $th) {
            throw $th;
        }

    }

    public function listarPropostaPorExecutivo($status, $id){
        try {
            $result = $this->model->select('proposta.*','cliente.nome as nome_cliente','credores.nome as nome_credor')
                      ->join('cliente','cliente.id','=','proposta.cliente_id')
                      ->join('operacoes','operacoes.cliente_id','=','proposta.cliente_id')
                      ->join('remessas','remessas.id','=','operacoes.remessa_id')
                      ->join('credores','credores.id','=','remessas.credor_id')
                      ->where(function($query) use($status, $id){
                        if($status){
                                    $query->where('proposta.status',$status);
                                }
                        if ($id){
                            $query->where('proposta.executivo_cobranca_id', $id);
                        }
                      })->distinct(DB::raw('id'))->get();

            return $result;
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function createProposta($dados){
        $proposta  = $dados['proposta'];
        $operacoes = $dados['operacoes'];
        $parcelas  = $dados['parcelas'];
        $proposta['numero_proposta'] = Carbon::now()->format('Ymdhs');

        try {
            $supervisor = $this->carteiraRepository->supervisorDoExecutivo($proposta['executivo_cobranca_id']);
            DB::beginTransaction();
            $nova_proposta = $this->model->create([
                    'status' => $proposta['status'],
                    'valor_nominal' => $proposta['valor_nominal'],
                    'juros' => $proposta['juros'],
                    'multas' => $proposta['multas'],
                    'honorarios' => $proposta['honorarios'],
                    'desconto' => $proposta['desconto'],
                    'valor_entrada' => $proposta['valor_entrada'],
                    'quantidade_parcelas' => $proposta['quantidade_parcelas'],
                    'valor_parcela' => $proposta['valor_parcela'],
                    'cliente_id' => $proposta['cliente_id'],
                    'executivo_cobranca_id' => $proposta['executivo_cobranca_id'],
                    'juros_porcento' => $proposta['juros_porcento'],
                    'multas_porcento' => $proposta['multas_porcento'],
                    'honorarios_porcento' => $proposta['honorarios_porcento'],
                    'desconto_porcento' => $proposta['desconto_porcento'],
                    'numero_proposta' => $proposta['numero_proposta'],
                    'supervisor_cobranca_id' => $supervisor->id
            ]);

            $ops = []; //array contento numereo de operacoes
            foreach($operacoes as $op){
                $ops[] = Operacao::find($op['id'])->numero_operacao;
                OperacaoProposta::create([
                    'proposta_id' => $nova_proposta->id,
                    'operacoes_id' => $op['id'],
                ]);
            }

            foreach($parcelas as $parcela){
                ParcelasProposta::create([
                    'proposta_id'     => $nova_proposta->id,
                    'valor_nominal'   => $parcela['valor_nominal'],
                    'valor_previsto'  => $parcela['valor_nominal'],
                    'data_vencimento' => $parcela['data_vencimento'],
                    'numero_parcela'  => $parcela['numero_parcela'],
                    'valor_juros'     => $parcela['valor_juros'],
                    'valor_multa'     => $parcela['valor_multa'],
                    'valor_honorario' => $parcela['valor_honorario'],
                    'valor_comissao'  => $parcela['valor_comissao'],
                    'forma_pagamento' => $parcela['forma_pagamento'],
                ]);
            }

            $operacao = Operacao::find($operacoes[0]['id']);
            $this->eventosRepository->createEventos($nova_proposta->cliente_id,
                                                    MessagesServices::generateMessagesProposta($proposta, $parcelas, $ops),
                                                    9, //evento para proposta do cliente
                                                    $operacao->remessa->credor_id);

            DB::commit();
            return $nova_proposta;
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }


    }

}
