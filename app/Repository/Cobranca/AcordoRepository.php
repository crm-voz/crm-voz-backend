<?php

namespace App\Repository\Cobranca;

use App\Gateway\Sms\BestVoice;
use App\Gateway\Sms\iSms;
use App\Models\BackOffice\Campanha;
use App\Models\BackOffice\Email;
use App\Models\Cobranca\Acordo;
use App\Models\Cobranca\OperacaoAcordo;
use App\Models\Cobranca\Parcela;
use App\Models\BackOffice\Operacao;
use App\Models\BackOffice\Telefone;
use App\Models\Cobranca\ExecutivoCobranca;
use App\Models\Cobranca\OperacoesParcelas;
use App\Models\Core\Boleto;
use App\Models\Core\Eventos;
use App\Models\User;
use App\Repository\Backoffice\BaseRepository;
use App\Repository\Backoffice\EventosRepository;
use App\Repository\Backoffice\OperacaoRepository;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

class AcordoRepository extends BaseRepository {

    protected iSms $plataforma_padrao;
    protected $operacaoRepository;
    protected $eventosRepository;

    protected const EVENTO_ACORDO_REALIZADO = 5;
    protected const EVENTO_ACORDO_CANCELADO = 40;
    protected const STATUS_OPERACAO_CANCELAR_ACORDO = 1;
    protected const PRAZO = 5;

    public function __construct(Acordo $model, BestVoice $plataforma, OperacaoRepository $opR, EventosRepository $eventos)
    {
       parent::__construct($model);
       $this->plataforma_padrao = new $plataforma;
       $this->operacaoRepository = $opR;
       $this->eventosRepository = $eventos;
    }

    public function acordosByCliente($clienteId)
    {
        return $this->model->here('cliente_id', $clienteId)->get();
    }

    public function filtrar($filtro)
    {
        //dd($filtro);
        $acordos = $this->model->query();
        if(array_key_exists('credores', $filtro)) {
            $acordos->whereHas('operacoes_acordo', function($query) use($filtro){
                $query->whereHas('operacao', function($q) use($filtro){
                    $q->whereHas('remessa', function($qr) use($filtro){
                        $qr->whereIn('credor_id', $filtro['credores']);
                    });
                });
            });
        }
        if(array_key_exists('classificacao_cliente', $filtro)){
            $acordos->whereHas('cliente', function($q) use($filtro){
                if($filtro['classificacao_cliente']['exceto'] == 0) {
                    $q->whereIn('status_cliente_id->title', $filtro['classificacao_cliente']['valor']);
                } else {
                    $q->whereNotIn('status_cliente_id->title', $filtro['classificacao_cliente']['valor']);
                }

                if ($filtro['periodo_contato']['exceto'] == 0) {
                    $q->whereBetween('ultimo_contato', [$filtro['periodo_contato']['data_inicio'], $filtro['periodo_contato']['data_final']]);
                }else{
                    $q->whereNotBetween('ultimo_contato', [$filtro['periodo_contato']['data_inicio'], $filtro['periodo_contato']['data_final']]);
                }

                if ($filtro['localizacao']['exceto'] == 0) {
                    $q->whereIn('localizacao', $filtro['localizacao']['valor']);
                }else{
                    $q->whereNotIn('localizacao', [$filtro['localizacao']['valor']]);
                }
            });
        }

        if (array_key_exists('parcelas', $filtro)){
            $acordos->whereHas('parcelas', function($query) use($filtro) {
                if ($filtro['parcelas']['exceto'] == 0) {
                    $query->whereBetween('data_vencimento', [$filtro['parcelas']['data_inicio'],$filtro['parcelas']['data_final']]);
                }else{
                    $query->whereNotBetween('data_vencimento', [$filtro['parcelas']['data_inicio'],$filtro['parcelas']['data_final']]);
                }

                if (array_key_exists('status', $filtro['parcelas'])){
                    if ($filtro['parcelas']['status']['exceto'] == 0) {
                        $query->where('status', $filtro['parcelas']['status']['valor']);
                    }else{
                        $query->whereNot('status', $filtro['parcelas']['status']['valor']);
                    }
                }
            });
        }

        if (array_key_exists('exceto',$filtro)){
            if ($filtro['exceto'] == 0){
                $acordos->where('status', $filtro['status']);
            }else{
                $acordos->whereNot('status', $filtro['status']);
            }
        }
        //dd($acordos->get());
        return $acordos->get();
    }

    public function generateNumeroAcordo(){
       $sequencia = DB::select("select nextval('acordo_id_seq')");
       return $sequencia[0]->nextval;
    }

    public function pesquisaNomeAluno($nomeAluno)
    {
         return $this->model->whereHas('operacoes_acordo', function($query) use($nomeAluno){
            $query->whereHas('operacao', function($q) use($nomeAluno){
                $q->where('aluno','ilike','%'.$nomeAluno.'%');
            });
         })->get();
    }

    public function acordosPorData($dtInicio, $dtfinal)
    {
        $result = $this->model
                       ->whereBetween('acordo.created_at', [$dtInicio, $dtfinal])
                       ->with('cliente','parcelas','operacoes_acordo.operacao')->get();

        foreach ($result as $value) {
            if ($value->usuario_cancelou_id){
                $value->usuario_cancelou_id = User::select('name')->find($value?->usuario_cancelou_id);
            }
                if ($value->executivo_cobranca_id){
                    $value->executivo_cobranca_id = ExecutivoCobranca::query()
                    ->with(['funcionarios' => function ($query) {
                        $query->select('id', 'nome');
                    }])
                    ->where('id',$value?->executivo_cobranca_id)->get();
                    foreach ($value->executivo_cobranca_id as $exec) {
                        $value->executivo_cobranca_id = [
                            'id' => $exec?->id,
                            'nome' => $exec?->funcionarios?->nome
                        ];
                    }
                }
            }

        return $result;
    }

    public function filtroAcordo($filtro_arr){
                $sql = $this->model->query();
      $result = $sql->select('cliente.nome as cliente','acordo.*',
                             'credores.nome as credor','funcionarios.nome as executivo')
                ->join('cliente','cliente.id','=','acordo.cliente_id')
                ->join('operacoes','operacoes.cliente_id','=','acordo.cliente_id')
                //->join('operacao_acordo','operacao_acordo.operacoes_id','=','operacoes.id')
                ->join('remessas','remessas.id','=','operacoes.remessa_id')
                ->join('credores','credores.id','=','remessas.credor_id')
                ->join('executivo_cobranca','executivo_cobranca.id','=','acordo.executivo_cobranca_id')
                ->join('funcionarios','funcionarios.id','=','executivo_cobranca.funcionarios_id')
                ->where(function($query) use($filtro_arr){
                    if(!empty($filtro_arr['filtros']['credores'])){
                        $query->whereIn('credores.id', $filtro_arr['filtros']['credores']);
                    }
                    if(!empty($filtro_arr['filtros']['executivo'])){
                        $query->whereIn('acordo.executivo_cobranca_id', $filtro_arr['filtros']['executivo']);
                    }
                    if(!empty($filtro_arr['filtros']['status'])){
                        $query->whereIn('acordo.status', $filtro_arr['filtros']['status']);
                    }
                    if(isset($filtro_arr['filtros']['data_inicial']) && (isset($filtro_arr['filtros']['data_final']))){
                        $query->whereBetween('acordo.created_at',[$filtro_arr['filtros']['data_inicial'],$filtro_arr['filtros']['data_final']]);
                    }
                })->whereNotIn('operacoes.status_operacao_id',[1])->distinct('acordo.numero_acordo')->get();

        $array = collect();
        foreach ($result as $value) {
            $array->add($value->parcelas);
            $value->operacoes_acordo;
        }
        return $result;
    }

    protected function criarBoleto(Array $boleto_arr, Parcela $parcela)
    {
        $boleto = null;
        if(!empty($boleto_arr)){
            foreach ($boleto_arr as $b) {
                $b['parcelas_id'] = $parcela->id;
                $b['usuario_empresas_id'] = auth()->user()->usuario_empresas[0]->id;
                $boleto = Boleto::create($b);
            }
        }

        return $boleto;
    }

    protected function criarParcelas(Array $parcelas, Acordo $acordo, Array $operacoesParcelas)
    {
        $parcs = [];

        foreach ($parcelas as $p) {

            $p['acordos_id'] = $acordo->id;
            $p['usuario_empresas_id'] = auth()->user()->usuario_empresas[0]->id;
            $parcela = Parcela::create($p);

            foreach($operacoesParcelas as $op){
                if ($parcela->numero_parcela == $op['indice']){
                    OperacoesParcelas::create(
                        ['operacao_id' => $op['operacao_id'],
                         'parcela_id' => $parcela->id,
                         'instituicao' => $op['instituicao'],
                         'cliente' => $op['cliente'],
                         'aluno' => $op['cliente'],
                         'documento' => $op['documento'],
                         'numero_operacao' => $op['numero_operacao'],
                         'vencimento_original' => $op['vencimento_original'],
                         'vencimento' => $op['vencimento'],
                         'parcela' => $op['parcela'],
                         'valor_negociado' => $op['valor_negociado'],
                         'valor_repasse' => $op['valor_repasse'],
                         'origem' => $op['origem'],
                         'principal' => $op['principal'],
                         'juros' => $op['juros'],
                         'multa' =>  $op['multa'],
                         'correcao' => $op['correcao'],
                         'desconto' => $op['desconto'],
                         'honorarios' => $op['honorarios'],
                         'realizado' => $op['realizado'],
                         'taxa_cartao' => $op['taxa_cartao'],
                         'comissao' => $op['comissao'],
                         'repasse' =>  $op['valor_repasse'],
                         'forma' => $op['forma'],
                         'referencia' => '',
                         'numero_documento' => '',
                         'desconto_acordo' => 0,
                         'credor_id' => $op['credor_id'],
                         'valor_juros_original' => $op['juros'],
                         'valor_multa_original' => $op['multa'],
                         'valor_honorario_original' => $op['honorarios'],
                         'valor_desconto_original' => $op['desconto'],
                         'valor_comissao_original' => $op['comissao'],
                      ]);
                    }
                }
            $parcs[] = $parcela;
        }

        return $parcs;
    }

    public function criar_acordo($data)
    {
        $acordo_arr = $data['acordo'];
        $op_arr = $data['operacoes'];
        $parcelas_arr = $data['parcelas'];
        $operacoes_par = $data['parcelasOperacoes'];

        try {
            DB::beginTransaction();
            $acordo['id'] = $this->generateNumeroAcordo();
            $acordo_arr['numero_acordo'] = str_pad($acordo['id'] + 1, 8, "0", STR_PAD_LEFT);
            $acordo = $this->model->create($acordo_arr);
            $email = Email::where('cliente_id',$acordo['cliente_id'])->pluck('email')->toArray();
            foreach ($op_arr as $op) {
                $op['status_operacao_id'] = 4;
                $operacoes[] = Operacao::where('id',$op['id'])->first();
                Operacao::where('id', $op['id'])->update($op);
                OperacaoAcordo::create(['acordos_id' => $acordo['id'], 'operacoes_id' => $op['id']]);
            }

            #cria parcelas e boleto da parcela
            $parcs = $this->criarParcelas($parcelas_arr, $acordo, $operacoes_par);
            $credor = Operacao::find($op_arr[0]['id'])->remessa->credor_id;
            $str = null;
            $valorNominal = 0;
            foreach ($operacoes as $value) {
                $str = $str . $value->numero_operacao .' - Venc.: '. $value->data_vencimento->format('d/m/Y') .'; ';
                $valorNominal = $valorNominal + $value->valor_nominal;
            }

            $texto = ' =========== NEGOCIAÇÃO Nº <strong> ACORDO - '.$acordo_arr['numero_acordo'].'</strong>===========<br>' .
                     ' Nr. Operações: '. $str . '<br>'.
                     '<strong> VALOR NOMINAL: R$ '. number_format($valorNominal, 2,'.',',') . '</strong><br>'.
                     '<strong>-JUROS: </strong> '.$acordo_arr['juros_porcento'].'% '.' R$ '.$acordo_arr['juros']. '<br>'.
                     '<strong>-MULTA: </strong>'.$acordo_arr['multas_porcento'].'% '.' R$ '.$acordo_arr['multas']. '<br>'.
                     '<strong>-HONORÁRIO: </strong>'.$acordo_arr['honorarios_porcento'].'% '.' R$ '.$acordo_arr['honorarios'].'<br>'.
                     '<strong>-DESCONTOS: </strong>'.$acordo_arr['desconto_porcento'].'% '.' R$ '.$acordo_arr['desconto']. '<br>'.
                     '<strong> VLR. PROPOSTA: R$ </strong>'.$acordo['valor_nominal'].' P/: '.Carbon::parse($parcelas_arr[0]['data_vencimento'])->format('d/m/Y').'<br>'.
                     '<strong> NEGOCIADO PARA PAGAMENTO EM </strong>'.$acordo['quantidade_parcelas'].' X '.'<br>'.
                     '<strong> PARCELA N.º 1 - VALOR: R$ </strong>'.$acordo['valor_parcela']. ' - VENCIMENTO: '.Carbon::parse($parcelas_arr[0]['data_vencimento'])->format('d/m/Y').' - '.$parcelas_arr[0]['forma_pagamento'].'<br>'.
                     ' ENVIADO PARA OS E-MAILS: '. implode(",", $email);

            $this->eventosRepository->createEventos(
                $acordo['cliente_id'],
                $texto,
                self::EVENTO_ACORDO_REALIZADO,
                $credor,
                $data['parametros_contato']
            );

            DB::commit();
            $acordo['parcelas'] = $parcs;
            return $acordo;

        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }

    public function cancelar_acordo($id)
    {
        $acordo = Acordo::find($id);
        $ops_ac = $acordo->operacoes_acordo()->get();
        $credor = Operacao::find($ops_ac[0]['id'])->remessa->credor_id;
        $parcs_collect = $acordo->parcelas();

        if(!$parcs_collect->where("data_baixa", "!=", null)->count()) {
            $parcs = $parcs_collect->get();

            try {
                DB::beginTransaction();

                foreach ($ops_ac as $op) {
                    $o = $op->operacao;
                    $o->status_operacao_id = self::STATUS_OPERACAO_CANCELAR_ACORDO;
                    $o->save();
                }

                $parcs = [];
                foreach ($parcs as $pc) {
                    $boletos = $pc->boleto()->get();
                    if(!empty($pc->boleto()->get())){
                        foreach ($boletos as $b) {
                            $b->delete();
                        }
                    }

                    $pc->delete();
                }

                $this->eventosRepository->createEventos(
                    $acordo['cliente_id'],
                    ' ACORDO NÚMERO: <strong>' . $acordo->numero_acordo .'</strong> cancelado',
                    self::EVENTO_ACORDO_CANCELADO,
                    $credor
                );

                $acordo->status = 'Cancelado';
                $acordo->save();
                DB::commit();
                return 'Arquivo cancelado com sucesso';
            } catch (\Throwable $th) {
                DB::rollback();
                throw $th;
            }
        } else {
            throw new \Exception("Este acordo já tem parcelas pagas");
        }
    }

    public function listarAcordosCredor(){
        return $this->model->AcordosCredor();
    }

    public function totalAcordosCredor()
    {
        return  $this->acordoCredor->count();
    }

    //TODO: RETIRAR INSTRUÇÃO SQL DO METODO ACORDOCREDOR
    public function acordoCredor()
    {
        $acordos =  Campanha::join('acordo', 'acordo.campanhas_id', '=', 'campanhas.id')
            ->join('operacao_acordo', 'operacao_acordo.acordos_id', '=', 'acordo.id')
            ->join('operacoes', 'operacoes.id', '=', 'operacao_acordo.operacoes_id')
            ->join('remessas', 'remessas.id', '=', 'operacoes.remessa_id')
            ->where('remessas.credor_id',auth()->user()->credor_id)
            ->select('acordo.*')
            ->distinct('acordo.id')
            ->get();

        return $this->model->whereHas('operacoes_acordo', function(Builder $query) use($acordos) {
                                        $query->whereIn('acordos_id', $acordos->pluck('id'));
                                    })->with('cliente','parcelas','operacoes_acordo.operacao')->get();

    }

    public function acordo_periodo_chart($data){
        try {
            $acordo_mes = $this->model->select(DB::raw('count(*) as total_acordos'))
            ->whereBetween('acordo.created_at',[$data->data_inicial,$data->data_final])->get();
        } catch (\Throwable $th) {
            return $th;
        }
        return $acordo_mes;
    }

    //TODO: DELEGAR RESPONSABILIDADE DO METODO ENVIAR_SMS PARA O REPOSITORY ADEQUADO
    public function enviar_sms($acordo){
        try {
            foreach ($acordo->parcelas as $value) {
                if ($value->forma_pagamento == 'Cartão de Crédito'){
                    $numero_acodo = md5($acordo->numero_acordo);
                    $telefone = Telefone::select('cliente_id','telefone')->where('cliente_id',$acordo->cliente_id)->first();
                    $result = $this->plataforma_padrao->enviar_sms([
                        'celular' => $telefone->telefone,
                        'mensagem' => 'Olá, segue o link de pagamento do cartão: https://fatura.grupovoz.combr/cartao/'.$numero_acodo
                    ]);
                    return $result;
                }
            }
        } catch (\Throwable $th) {
            //throw $th;
        }


    }

    //TODO: REMOVER METODO DECODEHAS DO ACORDOREPOSITORY, CRIAR UMA CLASSE NO HELPERS PRA ISSO
    public function decodeHash($hash){
        return $this->model->where(DB::raw('md5(numero_acordo)'),$hash)
        ->with('cliente.operacoes.remessa.credor','cliente.emails','cliente.telefones','cliente.operacoes.status_operacao')
        ->first();
    }

    public function decodificar_hash($hash){
        // $hash = '40b22e25d8682bfad0cd45a87e3a862a';
        try {
            $all = $this->decodeHash($hash);
            $result = [
                'id_acordo' => $all->numero_acordo,
                'id_cliente' => $all->cliente_id,
                'id_credor' => $all->cliente->operacoes[0]->remessa->credor->id,
                'status_acordo' => $all->cliente->operacoes[0]->status_operacao->nome,
                'valor' => $all->valor_nominal,
                'parcelas' => $all->quantidade_parcelas,
                'cliente' => $all->cliente->nome,
                'cpf' => $all->cliente->cpf_cnpj,
                'credor' => $all->cliente->operacoes[0]->remessa->credor->nome,
                'email' => $all['cliente']['emails'][0]['email'],
                'fone' => $all['cliente']['telefones'][0]['telefone']
            ];

            return $result;

        } catch (\Throwable $th) {
            // return $th;
        }

    }

    //TODO: REFATORAR METODO ACORDOS_VENCIDOS PARA REMOVER INSTRUÇÃO SQL, UTILIZAR O MODEL PARA ISSO
    public function acordos_vencidos(){
        try {
            $acordos = DB::table('parcelas')->select(DB::raw('distinct acordo.id'))
                    ->join('acordo', 'acordo.id', '=', 'parcelas.acordos_id')
                    ->leftJoin('operacao_acordo', 'operacao_acordo.id', '=', 'acordo.id')
                    ->leftjoin('operacoes', 'operacoes.id', '=', 'operacao_acordo.operacoes_id')
                    ->where(DB::raw('acordo.quantidade_parcelas'),'=',1)
                    ->where(DB::raw('(current_date - parcelas.data_vencimento)'),'>',5)
                    ->whereNull('parcelas.data_baixa')
                    ->orderBy('acordo.id')
                    ->get();
            foreach ($acordos as $value)
            {
                $parcelas[] = DB::table('parcelas')->select('id')->where('acordos_id',$value->id)->first();
                $operacoes[] = DB::table('operacao_acordo')->select('operacoes_id')->where('acordos_id',$value->id)->first();

                DB::update(
                    "update acordo set status = 'Cancelado' where id = ?",
                    [$value->id]
                );

                foreach ($parcelas as $val) {
                    DB::delete(
                    "delete from boleto where parcelas_id = ?",
                    [$val->id]
                    );
                }

                DB::delete(
                    "delete from parcelas where acordos_id = ?",
                    [$value->id]
                );

                foreach ($operacoes as $val) {
                    DB::update(
                        "update operacoes set status_operacao_id = 1 where id = ?",
                        [$val->operacoes_id]
                    );
                }

            }

            return($acordos);

        } catch (\Throwable $th) {
            //throw $th;
        }

    }

    //TODO: REFATORAR QUEBRA DE ACRODO ESTA COM EXCESSO DE RESPONSABILIDADES
    public function quebra_acordo($id)
    {
        try {
            $acordo = $this->model->find($id);
            $operacoes = OperacaoAcordo::where('acordos_id',$id)->with('operacao')->get();
            $parcelasAberta = DB::table('parcelas')->where('acordos_id',$id)
                        ->whereNull('parcelas.data_baixa')
                        ->get();
            $parcelasPaga = Parcela::where('acordos_id',$id)
                        ->whereNotNull('parcelas.data_baixa')
                        ->first();

            if (!$parcelasPaga)
            {
                //** Cancelamento de Acordo **/
                //** Alterando o status da operação **/
                foreach ($operacoes as $val)
                {
                    DB::update(
                        "update operacoes set status_operacao_id = 1 where id = ?",
                        [$val->operacao->id]
                    );

                    DB::update(
                        "update operacoes set data_atualizacao = null where id = ?",
                        [$val->operacao->id]
                    );

                    DB::update(
                        "update operacoes set valor_atualizado = 0 where id = ?",
                        [$val->operacao->id]
                    );
                }

                //** Transformar parcelas em operações **/
                foreach ($parcelasAberta as $value)
                {
                    //** Deletando os boletos das parcelas que estão em aberto **/
                    DB::delete(
                        "delete from boleto where parcelas_id = ?",
                        [$value->id]
                    );
                }

                DB::update(
                    "update parcelas set status = 'Quebrado' where id = ?",
                    [$value->acordos_id]
                );

                //** Alterando o status do acordo **/
                DB::update(
                    "update acordo set status = 'Cancelado' where id = ?",
                    [$acordo->id]
                );

                //** atualizando o user que quebrou o acordo **/
                DB::update(
                    "update acordo set usuario_cancelou_id = ? where id = ".$acordo->id,
                    [auth()->user()->id],
                );

                //** adicionando a data de canelamento **/
                DB::update(
                    "update acordo set data_cancelou = ? where id = ".$acordo->id,
                    [now()],
                );

                //** adicionando a descrição do cancelamento do acordo **/
                DB::update(
                    "update acordo set descricao_cancelamento = ? where id = ".$acordo->id,
                    ['ACORDO CANCELADO PELO USUARIO: '.auth()->user()->name],
                );

                $this->eventosRepository->createEventos($acordo->cliente_id, 'ACORDO <strong>NUMERO: ' . $acordo->numero_acordo.'</strong> CANCELADO PELO USUARIO: '.auth()->user()->name, self::EVENTO_ACORDO_CANCELADO);

                return true;

            }else{
                if ($parcelasAberta != '[]'){
                    //** Quebra de Acordo **/

                    //** Verificando a quantidade de vezes que o acordo foi quebrado **/
                    $acordoQuebrado = DB::table('operacao_acordo')->select(DB::raw('distinct substring(operacoes.numero_operacao from 1 for 2) as numero_operacao'))
                                                ->join('operacoes','operacoes.id','=','operacao_acordo.operacoes_id')
                                                ->where('operacao_acordo.acordos_id',$acordo->id)
                                                ->orderByDesc('numero_operacao')->get();

                    if (substr($acordoQuebrado[0]->numero_operacao, 0, 1) == 'R'){
                        $count = substr($acordoQuebrado[0]->numero_operacao, 1, 1)+1;
                    }else{
                        $count = 1;
                    }

                    //** Transformar parcelas em operações **/
                    foreach ($parcelasAberta as $value)
                    {
                        $dados = [
                            'numero_operacao'   => 'R'.$count.'-'.$acordo->numero_acordo.'/'.$value->numero_parcela,
                            'data_vencimento'   => $value->data_vencimento,
                            'valor_nominal'     => $value->valor_nominal,
                            'valor_atualizado'  => $value->valor_nominal,
                            'valor_juros'       => null,
                            'valor_multa'       => null,
                            'valor_honorario'   => null,
                            'cliente_id'        => $acordo->cliente_id,
                            'motivo_pendencia_id'=> null,
                            'status_operacao_id'=> 1,
                            'remessa_id'        => $operacoes[0]->operacao->remessa_id,
                            'aluno'             => $operacoes[0]->operacao->aluno,
                            'competencia'       => $operacoes[0]->operacao->competencia,
                            'descricao'         => $operacoes[0]->operacao->descricao,
                            'desconto'          => 0,
                            'origem_desconto'   => null,
                            'data_processamento' => Carbon::now(),
                            'usuario_empresas_id' => auth()->user()->usuario_empresas[0]->empresa_id,
                            'numero_acordo' => $acordo->id
                        ];

                        if(count($parcelasAberta) != $acordo->quantidade_parcelas)
                        {
                            $this->operacaoRepository->create($dados);
                        }

                    }

                    //** Alterando o status do acordo **/
                    DB::update(
                        "update acordo set status = 'Quebrado' where id = ?",
                        [$acordo->id]
                    );

                    //** atualizando o user que quebrou o acordo **/
                    DB::update(
                        "update acordo set usuario_cancelou_id = ? where id = ".$acordo->id,
                        [auth()->user()->id],
                    );

                    //** adicionando a data de canelamento **/
                    DB::update(
                        "update acordo set data_cancelou = ? where id = ".$acordo->id,
                        [now()],
                    );

                    //** adicionando a descrição do cancelamento do acordo **/
                    DB::update(
                        "update acordo set descricao_cancelamento = ? where id = ".$acordo->id,
                        ['ACORDO QUEBRADO PELO USUARIO: '.auth()->user()->name],
                    );

                    $this->eventosRepository->createEventos(
                                    $acordo->cliente_id,
                                    'ACORDO NUMERO: <strong>' . $acordo->numero_acordo .'</strong> QUEBRADO. ',
                                    self::EVENTO_ACORDO_CANCELADO);

                    return true;
                }else{
                    return false;
                }

            }

        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function cancelarAcordosAguardando(){
        $this->model->where('status', 'Aguardando')
                    ->whereHas('parcelas', function($query){
                        $query->where('data_vencimento', '<', Carbon::now())
                              ->whereNull('data_baixa');
                    })->orderBy('id','desc')->chunk(100, function($acordos){
            foreach($acordos as $acordo){
                $parcelas = $acordo->parcelas;
                if (count($parcelas)>0){
                    if($parcelas->where("data_baixa", "!=", null)->count() > 0){
                        continue;
                    }

                   $parcelas->filter(function($item) use($acordo){
                       $dataNow = Carbon::now();
                       $vencimento = Carbon::parse($item->data_vencimento);
                       if ( $dataNow->diff($vencimento)->d > self::PRAZO){
                            DB::beginTransaction();
                            $acordo->update([
                                'status' => 'CANCELADO',
                                'descricao_cancelamento' => 'ACORDO NUMERO: <strong> ' . $acordo->numero_acordo .' </strong> CANCELADO PELO SISTEMA. ',
                                'usuario_cancelou_id' => 4, //noniltton
                                'data_cancelou' => Carbon::now(),
                            ]);

                            foreach($acordo->parcelas as $p){
                                // foreach($p->boleto as $b){
                                //     $b->delete();
                                // }

                                foreach($p->operacoes_parcelas as $opp){
                                     $opp->delete();
                                }
                                 $p->update(['status' => 'CANCELADO']);
                            }

                            $credorId = 0;
                            foreach($acordo->operacoes_acordo as $op){
                                $credorId = $op->operacao->remessa->credor_id;
                                $op->operacao->update(['status_operacao_id' => self::STATUS_OPERACAO_CANCELAR_ACORDO]);
                            }

                            $this->eventosRepository->eventosAutomatico(
                                        $acordo->cliente_id,
                                        'ACORDO NUMERO: <strong> ' . $acordo->numero_acordo .' </strong> CANCELADO PELO SISTEMA. ',
                                        self::EVENTO_ACORDO_CANCELADO,
                                        $credorId,
                                        null,
                                        4
                            );

                            DB::commit();
                        }
                    });
                }
            }
        });

        return;
      }

      public function cancelarAcordosNegociados(){
          return $this->model->where('status','Negociado')->chunk(300, function($acordos){
              foreach($acordos as $acordo){
                  $parcelas = $acordo->parcelas->sortBy('data_vencimento');
                  foreach($parcelas as $parcela){
                      if ($parcela->valor_baixado <= 0){
                          if (Carbon::parse($parcela->data_vencimento)->diffInDays(Carbon::now()) > self::PRAZO){
                              DB::beginTransaction();
                              $acordo->update([
                                 'status' => 'Vencido',
                              ]);

                              foreach($acordo->parcelas as $p){
                                 $p->update(['status' => 'Vencido']);
                              }

                              DB::commit();
                          }
                      }
                  }
              }
          });
      }

}
