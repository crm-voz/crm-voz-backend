<?php

namespace App\Repository\Dashboards;

use App\Models\BackOffice\Campanha;
use App\Models\BackOffice\Credor;
use App\Models\Cobranca\Acordo;
use App\Repository\Backoffice\CampanhasRepository;
use App\Repository\Backoffice\CredorRepository;
use App\Repository\Backoffice\RemessaRepository;
use App\Services\AcordoServices;
use Exception;

class DashboardCredorRepository
{
    public function __construct(CredorRepository $credorRepository,
                                CampanhasRepository $campanhasRepository,
                                RemessaRepository $remessaRepository)
    {
        $this->credorRepository = $credorRepository;
        $this->campanhasRepository = $campanhasRepository;
        $this->remessaRepository = $remessaRepository;
    }

    public function ListarAcordosCredor(){
        $modelCredor = Credor::find(auth()->user()->credor_id);
        if (!$modelCredor)
            throw new Exception('Credor não localizado.');

        $operacoesId = $modelCredor->operacoes->unique()->pluck('id');
        $acordos = Acordo::whereHas('operacoes_acordo', function($q) use($operacoesId) {
                                      $q->whereIn('operacoes_id', $operacoesId);
                                   })->get()->unique();
        return AcordoServices::acordos($acordos);
    }

    public function quantidadeAcordosCredor(){
        return $this->ListarAcordosCredor()->count();
    }

    public function quantidadeCampanhasCredor(){
        return $this->campanhasCredor()->count();
    }

    public function quantidadeClientesCredor(){
        return $this->credorRepository->clientesCredor()->count();
    }

    public function quantidadeCredorOperacoes()
    {
        return $this->credorRepository->operacoesCredor()->count();
    }

    public function quantidadeRemessaCredor(){
        return $this->remessaRepository->remessasCredor()->count();
    }

    public function campanhasCredor(){
        return $this->campanhasRepository->campanhasCredor();
    }

    public function acordosCampanhas(Campanha $campanha){
        return $this->campanhasRepository->listarAcordosCampanhas($campanha);
    }

    public function faturamentoCampanha(Campanha $campanha){
        return $this->campanhasRepository->faturamentoCampanha($campanha);
    }
}
