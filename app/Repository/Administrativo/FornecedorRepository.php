<?php

namespace App\Repository\Administrativo;

use App\Models\Administrativo\Fornecedor;
use App\Repository\Backoffice\BaseRepository;

class FornecedorRepository extends BaseRepository {

    public function __construct(Fornecedor $model)
    {
       parent::__construct($model);
    }

    public function contratos($idfornecedor){
        return $this->find($idfornecedor)->with('contratos')->get();
    }

}
