<?php

namespace App\Repository\Administrativo;

use App\Models\Administrativo\AnexosContrato;
use App\Repository\Backoffice\BaseRepository;

class AnexosContratoRepository extends BaseRepository {

    public function __construct(AnexosContrato $model)
    {
       parent::__construct($model);
    }

}
