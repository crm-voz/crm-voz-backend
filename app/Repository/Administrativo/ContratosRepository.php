<?php

namespace App\Repository\Administrativo;

use App\Models\Administrativo\Contratos;
use App\Repository\Backoffice\BaseRepository;

class ContratosRepository extends BaseRepository {

    public function __construct(Contratos $model)
    {
       parent::__construct($model);
    }

}
