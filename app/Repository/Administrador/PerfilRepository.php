<?php

namespace App\Repository\Administrador;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PerfilRepository extends Controller
{
    public $users;
    public $index = true, $store = true , $update = true, $destroy = true, $permissoes_especiais = false;

    public function show(){
        /** Perfil por Grupo de Usuário;
         *  Em Funcionário tem que está setado o grupo que pertence;
         *  Em Permitir Acesso tem:
         *      Está setado o grupo de acesso;
         *      Regra de permissão;
         *      Situacao = true.
        **/
        // $this->users = DB::table('funcionarios')
        // ->join('grupo_acesso', 'grupo_acesso.id', '=', 'funcionarios.grupo_acesso_id')
        // ->join('permitir_acesso', 'permitir_acesso.grupo_acesso_id', '=', 'funcionarios.grupo_acesso_id')
        // ->join('acao_permissao', 'acao_permissao.id', '=', 'permitir_acesso.acao_permissao_id')
        // ->join('usuario_empresas', 'usuario_empresas.id', '=', 'funcionarios.usuario_empresas_id')
        // ->where('usuario_empresas.user_id','=', auth()->user()->id)
        // ->where('permitir_acesso.situacao','=', true)
        // ->select('acao_permissao.*')
        // ->get();

        /** Perfil por Usuário Específico;
         *  Em Funcionário tem que está em um grupo que não tenha regra;
         *  Em Permitir Acesso tem:
         *      Está setado o usuário;
         *      Regra de permissão;
         *      Situacao = true.
        **/
        // if($this->users == '[]'){
        //     $this->users = DB::table('funcionarios')
        //     ->join('permitir_acesso', 'permitir_acesso.usuario_empresas_id', '=', 'funcionarios.usuario_empresas_id')
        //     ->join('acao_permissao', 'acao_permissao.id', '=', 'permitir_acesso.acao_permissao_id')
        //     ->join('usuario_empresas', 'usuario_empresas.id', '=', 'funcionarios.usuario_empresas_id')
        //     ->where('usuario_empresas.user_id','=', auth()->user()->id)
        //     ->where('permitir_acesso.situacao','=', true)
        //     ->select('acao_permissao.*')
        //     ->get();
        // }

        // foreach($this->users as $acao){
        //     if ($acao->gravar == false){
        //         $this->store = false;
        //     }if ($acao->leitura == false){
        //         $this->index = false;
        //     }if ($acao->modificar == false){
        //         $this->update = false;
        //     }if ($acao->apagar == false){
        //         $this->destroy = false;
        //     }if ($acao->permissoes_especiais == true){
        //         $this->permissoes_especiais =true;
        //     }
        // }
    }

    public function msg(){
        return response()->json(['mensagem' => 'Você não tem permissão!'], 403);
    }

}
