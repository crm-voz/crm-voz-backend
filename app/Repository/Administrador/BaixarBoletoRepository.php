<?php


namespace App\Repository\Administrador;

use App\Models\Cobranca\Parcela;
use App\Models\Core\Boleto;
use App\Models\Financeiro\BaixaBoleto;
use App\Repository\Backoffice\BaseRepository;
use App\Repository\Cobranca\ParcelaRepository;
use Carbon\Carbon;
use Illuminate\Support\Str;

class BaixarBoletoRepository extends BaseRepository
{

    protected $model, $modelParcela, $modelBaixarBoleto, $parcelaRepository;
    public function __construct(Boleto $boleto,
                                Parcela $parcela,
                                BaixaBoleto $baixaBoleto,
                                ParcelaRepository $parc)
    {
        parent::__construct($boleto);
        $this->modelParcela = $parcela;
        $this->modelBaixarBoleto = $baixaBoleto;
        $this->parcelaRepository = $parc;
    }

    public function getBoleto($nossoNumero, $convenio){
        return $this->model->where('nosso_numero',$nossoNumero)->where('convenio', $convenio)->first();
    }

    public function store($nome_arquivo, $banco, $convenio, $cnab, $quantidade_baixada, $valor_total_baixado){
        $dados = [
            'descricao' => $nome_arquivo,
            'banco' => $banco,
            'convenio' => $convenio,
            'cnab' => $cnab,
            'quantidade_baixada' => $quantidade_baixada,
            'valor_total_baixado' => $valor_total_baixado
        ];

        try {
            $this->modelBaixarBoleto->create($dados);
            return $dados;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function baixar_boleto_itau_400($request){

        $file = $request->file('remessa');
        $arquivo = file($file);
        $nome_arquivo = $request->file('remessa')->getClientOriginalName();
        $tipo_retorno = substr($arquivo[0], 1, 1);
        $agencia_banco = substr($arquivo[0], 26, 4);
        $conta_banco = substr($arquivo[0], 32, 5);
        $numero_banco = substr($arquivo[0], 76, 3);
        $nome_banco = substr($arquivo[0], 79, 15);
        $nome_empresa = substr($arquivo[0], 46, 30);
        $convenio = substr($arquivo[0], 32, 9);
        $linha = count($arquivo);
        $lenght = $arquivo[0];
        $cnab = Str::length($lenght)-2;
        $quantidade_baixada = 0;
        $valor_total_baixado = 0;

        for ($i=1; $i < $linha-1; $i++) {

            $nome_cliente = substr($arquivo[$i], 324, 30);
            $nosso_numero = substr($arquivo[$i], 85, 8).substr($arquivo[$i], 93, 1);
            $valor_lancado = doubleval(substr($arquivo[$i], 253, 11).'.'.substr($arquivo[$i], 264, 2));
            $vencimento = substr($arquivo[$i], 146, 2).'-'.substr($arquivo[$i], 148, 2).'-'.'20'.substr($arquivo[$i], 150, 2);
            $cod_liquidacao = substr($arquivo[$i], 108, 2);

            try {
                if($boleto = $this->model->where('nosso_numero',$nosso_numero)->where('convenio',$convenio)->with('parcelas')->first()){
                    $parcelas = $this->modelParcela->find($boleto->parcelas->id);
                }

                if (isset($nosso_numero) == isset($boleto->nosso_numero)){
                    if($cod_liquidacao == '06'){
                        $request->numero_parcela = [$parcelas->numero_parcela];
                        $request->data_baixa = $request->input('data_baixa',Carbon::createFromFormat('d/m/Y', $request?->data_baixa)->format('Y-m-d H:i:s'));
                        $request->retirar_baixa = false;
                        $this->parcelaRepository->baixaManual($request, $parcelas->acordos_id);
                        $quantidade_baixada = $quantidade_baixada+1;
                        $valor_total_baixado = $valor_total_baixado + $valor_lancado;
                    }
                }
                $dados = collect([
                    'Banco' => $nome_banco,
                    'Cnab' => $cnab,
                    'nome do arquivo'=> $nome_arquivo,
                    'cod_liquidacao' => $cod_liquidacao,
                    'nosso_numero' => $nosso_numero,
                    'quantidade_baixada' => $quantidade_baixada,
                    'valor_total_baixado' => $valor_total_baixado
                ]);
            } catch (\Throwable $th) {
                //throw $th;
            }

        }
        $this->store($nome_arquivo,$nome_banco,$convenio,$cnab,$quantidade_baixada,$valor_total_baixado);

        return $dados;
    }

    public function baixar_boleto_bradesco_400($request){

        $file = $request->file('remessa');
        $arquivo = file($file);
        $nome_arquivo = $request->file('remessa')->getClientOriginalName();
        $tipo_retorno = substr($arquivo[0], 1, 1);
        $agencia_banco = substr($arquivo[1], 25, 4);
        $conta_banco = substr($arquivo[1], 30, 6);
        $digito_conta = substr($arquivo[1], 36, 1);
        $numero_banco = substr($arquivo[0], 76, 3);
        $nome_banco = substr($arquivo[0], 79, 15);
        $nome_empresa = substr($arquivo[0], 46, 30);
        $convenio = substr($arquivo[0], 39, 7);
        $linha = count($arquivo);
        $lenght = $arquivo[0];
        $cnab = Str::length($lenght)-2;
        $quantidade_baixada = 0;
        $valor_total_baixado = 0;

        for ($i=1; $i < $linha-1; $i++) {
            $nosso_numero = substr($arquivo[$i], 70, 11);
            $valor_lancado = doubleval(substr($arquivo[$i], 253, 11).'.'.substr($arquivo[$i], 264, 2));
            $vencimento = substr($arquivo[$i], 146, 2).'-'.substr($arquivo[$i], 148, 2).'-'.'20'.substr($arquivo[$i], 150, 2);
            $cod_liquidacao = substr($arquivo[$i], 108, 2);

            try {
                if($boleto = $this->model->where('nosso_numero',$nosso_numero)->where('convenio',$convenio)->with('parcelas')->first()){
                    $parcelas = $this->modelParcela->find($boleto->parcelas->id);
                }

                if (isset($nosso_numero) == isset($boleto->nosso_numero)){
                    if($cod_liquidacao == '06'){
                        $request->numero_parcela = [$parcelas->numero_parcela];
                        $request->data_baixa = $request->input('data_baixa',Carbon::createFromFormat('d/m/Y', $request?->data_baixa)->format('Y-m-d H:i:s'));
                        $request->retirar_baixa = false;
                        $this->parcelaRepository->baixaManual($request, $parcelas->acordos_id);
                        $quantidade_baixada = $quantidade_baixada+1;
                        $valor_total_baixado = $valor_total_baixado + $valor_lancado;
                    }
                }
                $dados = collect([
                    'Banco' => $nome_banco,
                    'Cnab' => $cnab,
                    'nome do arquivo'=> $nome_arquivo,
                    'cod_liquidacao' => $cod_liquidacao,
                    'nosso_numero' => $nosso_numero,
                    'quantidade_baixada' => $quantidade_baixada,
                    'valor_total_baixado' => $valor_total_baixado
                ]);
            } catch (\Throwable $th) {
                //throw $th;
            }

        }
        $this->store($nome_arquivo,$nome_banco,$convenio,$cnab,$quantidade_baixada,$valor_total_baixado);

        return $dados;
    }

    public function baixar_boleto_bbb_240($request){
        $file = $request->file('remessa');
        $arquivo = file($file);
        $nome_arquivo = $request->file('remessa')->getClientOriginalName();
        $linha = count($arquivo);
        $numero_banco = substr($arquivo[0], 0, 3);
        $nome_banco = substr($arquivo[0], 102, 30);
        $lenght = $arquivo[0];
        $cnab = Str::length($lenght)-2;
        $convenio = substr($arquivo[0], 34, 9);
        $quantidade_baixada = 0;
        $valor_total_baixado = 0;

        $dados = [0];
        for ($i=2; $i < $linha-1; $i++){

            $nosso_numero = substr($arquivo[$i], 37, 17);
            $valor_lancado = doubleval(substr($arquivo[$i], 81, 15))/100;
            $cod_liquidacao = substr($arquivo[$i], 15, 2);

            try {
                if ($boleto = $this->model->where('nosso_numero','000'.$nosso_numero)->where('convenio',$convenio)->with('parcelas')->first()){
                    $parcelas = $this->modelParcela->find($boleto->parcelas->id);
                }
                if (isset($nosso_numero) == isset($boleto->nosso_numero)){
                    if($cod_liquidacao == '06'){
                        $request->numero_parcela = [$parcelas->numero_parcela];
                        $request->data_baixa = $request->input('data_baixa',Carbon::createFromFormat('d/m/Y', $request?->data_baixa)->format('Y-m-d H:i:s'));
                        $request->retirar_baixa = false;
                        if ($this->parcelaRepository->baixaManual($request, $parcelas->acordos_id) == true){
                            $quantidade_baixada = $quantidade_baixada+1;
                            $valor_total_baixado = $valor_total_baixado + $valor_lancado;
                        }
                    }
                }

                $dados = collect([
                    'Banco' => $nome_banco,
                    'Cnab' => $cnab,
                    'nome do arquivo'=> $nome_arquivo,
                    'cod_liquidacao' => $cod_liquidacao,
                    'nosso_numero' => $nosso_numero,
                    'quantidade_baixada' => $quantidade_baixada,
                    'valor_total_baixado' => $valor_total_baixado
                ]);


            } catch (\Throwable $th) {
                // return $th;
            }
        }

        $this->store($nome_arquivo,$nome_banco,$convenio,$cnab,$quantidade_baixada,$valor_total_baixado);

        return $dados;
    }

    public function baixar_boleto_santander_240($request){
        $file = $request->file('remessa');
        $arquivo = file($file);
        $nome_arquivo = $request->file('remessa')->getClientOriginalName();
        $linha = count($arquivo);
        $numero_banco = substr($arquivo[0], 0, 3);
        $nome_banco = substr($arquivo[0], 102, 30);
        $lenght = $arquivo[0];
        $cnab = Str::length($lenght)-2;
        $convenio = substr($arquivo[0], 54, 7);
        $quantidade_baixada = 0;
        $valor_total_baixado = 0;

        $dados = [0];
        for ($i=2; $i < $linha-1; $i++){

            if (substr($arquivo[$i], 13, 1) == 'T'){
                $nosso_numero = substr($arquivo[$i], 40, 13);
            }
            if (substr($arquivo[$i], 13, 1) == 'U'){
                $valor_lancado = doubleval(substr($arquivo[$i], 92, 15))/100;
            }

            $cod_liquidacao = substr($arquivo[$i], 15, 2);

            try {
                if ($boleto = $this->model->where('nosso_numero',$nosso_numero)->where('convenio',$convenio)->with('parcelas')->first()){
                    $parcelas = $this->modelParcela->find($boleto->parcelas->id);
                }
                if (isset($nosso_numero) == isset($boleto->nosso_numero)){
                    if($cod_liquidacao == '06'){
                        $request->numero_parcela = [$parcelas->numero_parcela];
                        $request->data_baixa = $request->input('data_baixa',Carbon::createFromFormat('d/m/Y', $request?->data_baixa)->format('Y-m-d H:i:s'));
                        $request->retirar_baixa = false;
                        $this->parcelaRepository->baixaManual($request, $parcelas->acordos_id);
                        $quantidade_baixada = $quantidade_baixada+1;
                        $valor_total_baixado = $valor_total_baixado + $valor_lancado;
                    }
                }

                $dados = collect([
                    'Banco' => $nome_banco,
                    'Cnab' => $cnab,
                    'nome do arquivo'=> $nome_arquivo,
                    'cod_liquidacao' => $cod_liquidacao,
                    'nosso_numero' => $nosso_numero,
                    'quantidade_baixada' => $quantidade_baixada,
                    'valor_total_baixado' => $valor_total_baixado
                ]);


            } catch (\Throwable $th) {
                // return $th;
            }
        }

        $this->store($nome_arquivo,$nome_banco,$convenio,$cnab,$quantidade_baixada,$valor_total_baixado);

        return $dados;
    }

    public function baixar_boleto_caixa_240($request){
        $file = $request->file('remessa');
        $arquivo = file($file);
        $nome_arquivo = $request->file('remessa')->getClientOriginalName();
        $linha = count($arquivo);
        $numero_banco = substr($arquivo[0], 0, 3);
        $nome_banco = substr($arquivo[0], 102, 30);
        $lenght = $arquivo[0];
        $cnab = Str::length($lenght)-2;
        $convenio = substr($arquivo[0], 58, 6);
        $quantidade_baixada = 0;
        $valor_total_baixado = 0;

        $dados = [0];
        for ($i=2; $i < $linha-1; $i++){

            if (substr($arquivo[$i], 13, 1) == 'T'){
                $nosso_numero = substr($arquivo[$i], 39, 17);
            }
            if (substr($arquivo[$i], 13, 1) == 'U'){
                $valor_lancado = doubleval(substr($arquivo[$i], 77, 15))/100;
            }

            $cod_liquidacao = substr($arquivo[$i], 15, 2);
            try {
                if ($boleto = $this->model->where('nosso_numero',$nosso_numero)->where('convenio',$convenio)->with('parcelas')->first()){
                    $parcelas = $this->modelParcela->find($boleto->parcelas->id);
                }
                if (isset($nosso_numero) == isset($boleto->nosso_numero)){
                    if($cod_liquidacao == '06'){
                        $request->numero_parcela = [$parcelas->numero_parcela];
                        $request->data_baixa = $request->input('data_baixa',Carbon::createFromFormat('d/m/Y', $request?->data_baixa)->format('Y-m-d H:i:s'));
                        $request->retirar_baixa = false;
                        $this->parcelaRepository->baixaManual($request, $parcelas->acordos_id);
                        $quantidade_baixada = $quantidade_baixada+1;
                        $valor_total_baixado = $valor_total_baixado + $valor_lancado;
                    }
                }

                $dados = collect([
                    'Banco' => $nome_banco,
                    'Cnab' => $cnab,
                    'nome do arquivo'=> $nome_arquivo,
                    'cod_liquidacao' => $cod_liquidacao,
                    'nosso_numero' => $nosso_numero,
                    'quantidade_baixada' => $quantidade_baixada,
                    'valor_total_baixado' => $valor_total_baixado
                ]);

            } catch (\Throwable $th) {
                // return $th;
            }

        }

        $this->store($nome_arquivo,$nome_banco,$convenio,$cnab,$quantidade_baixada,$valor_total_baixado);

        return $dados;
    }

}
