<?php


namespace App\Repository\Administrador;

use App\Gateway\Sms\BestVoice;
use App\Gateway\Sms\iSms;
use App\Models\BackOffice\Cliente;
use App\Models\BackOffice\Credor;
use App\Models\Core\TemplateSms;
use App\Services\LogContatosServices;
use Illuminate\Support\Facades\DB;

class TemplateSMSRepository{

   protected iSms $plataforma_padrao;
   protected $templatesms_id, $sql, $logContatosService;
   public function __construct(TemplateSms $templateSms, Cliente $cliente, Credor $credor, BestVoice $plataforma, LogContatosServices $log)
   {
        $this->templateSms = $templateSms;
        $this->cliente = $cliente;
        $this->credor = $credor;
        $this->plataforma_padrao = new $plataforma;
        $this->logContatosService = $log;
   }

   public function preencherSms($filtro_arr){
                $sql = $this->cliente->query();
                    $sql->select('cliente.id','cliente.nome as cliente','telefones.id as telefone_id','telefones.telefone','credores.nome as credor');
                    $sql->join('operacoes', 'operacoes.cliente_id', '=', 'cliente.id')
                        ->join('remessas', 'remessas.id', '=', 'operacoes.remessa_id')
                        ->join('credores', 'credores.id', '=', 'remessas.credor_id')
                        ->leftJoin('eventos', 'eventos.cliente_id', '=', 'cliente.id')
                        ->leftJoin('cidades', 'cidades.id', '=', 'cliente.cidades_id')
                        ->leftJoin('status_localizacao', 'status_localizacao.id', '=', 'cliente.status_localizacao_id')
                        ->leftJoin('classificacao_negativacao', 'classificacao_negativacao.id', '=', 'operacoes.classificacao_negativacao_id')
                        ->leftJoin('acordo', 'acordo.cliente_id', '=', 'cliente.id')
                        ->leftJoin('parcelas', 'parcelas.acordos_id', '=', 'acordo.id')
                        ->leftJoin('telefones', 'telefones.cliente_id', '=', 'cliente.id')
                        ->where('telefones.tags','Móvel')
                        ->where('operacoes.status_operacao_id',1)
                        ->where(function($query) use($filtro_arr){
                            if(!empty($filtro_arr['filtros']['credores'])){
                                $query->whereIn('credores.id', $filtro_arr['filtros']['credores']);
                            }
                            switch(isset($filtro_arr['filtros']['tipo_base'])) {
                                case 'operacao':
                                    if(isset($filtro_arr['filtros']['data_inicial']) && (isset($filtro_arr['filtros']['data_final']))){
                                        $query->whereBetween('operacoes.data_vencimento',[$filtro_arr['filtros']['data_inicial'],$filtro_arr['filtros']['data_final']]);
                                    }
                                    break;
                                case 'parcela':
                                    if(isset($filtro_arr['filtros']['data_inicial']) && (isset($filtro_arr['filtros']['data_final']))){
                                        $query->whereBetween('parcelas.data_vencimento',[$filtro_arr['filtros']['data_inicial'],$filtro_arr['filtros']['data_final']]);
                                    }
                                    break;
                                case 'acordo':
                                    if(isset($filtro_arr['filtros']['data_inicial']) && (isset($filtro_arr['filtros']['data_final']))){
                                        $query->whereBetween('acordo.created_at',[$filtro_arr['filtros']['data_inicial'].' 00:00:00 ',$filtro_arr['filtros']['data_final'].' 23:59:59']);
                                    }
                                    break;
                            }
                            if(!empty($filtro_arr['filtros']['status_operacao']['valor'])){
                                if ($filtro_arr['filtros']['status_operacao']['exceto'] == 0){
                                    $query->whereIn('operacoes.status_operacao_id', $filtro_arr['filtros']['status_operacao']['valor']);
                                }else{
                                    $query->whereNotIn(DB::raw("coalesce(operacoes.status_operacao_id,0)"), [$filtro_arr['filtros']['status_operacao']['valor']]);
                                }
                            }
                            if(!empty($filtro_arr['filtros']['motivo_pendencia']['valor'])){
                                if ($filtro_arr['filtros']['motivo_pendencia']['exceto'] == 0){
                                    $query->whereIn('operacoes.motivo_pendencia_id', $filtro_arr['filtros']['motivo_pendencia']['valor']);
                                }else{
                                    $query->whereNotIn(DB::raw("coalesce(operacoes.motivo_pendencia_id,0)"), [$filtro_arr['filtros']['motivo_pendencia']['valor']]);
                                }
                            }
                            if(!empty($filtro_arr['filtros']['motivo_evento']['valor'])){
                                if ($filtro_arr['filtros']['motivo_evento']['exceto'] == 0){
                                    $query->whereIn('eventos.motivo_evento_id', $filtro_arr['filtros']['motivo_evento']['valor']);
                                }else{
                                    $query->whereNotIn(DB::raw("coalesce(eventos.motivo_evento_id,0)"), [$filtro_arr['filtros']['motivo_evento']['valor']]);
                                }
                            }
                            if(!empty($filtro_arr['filtros']['classificacao_negativacao']['valor'])){
                                if ($filtro_arr['filtros']['classificacao_negativacao']['exceto'] == 0){
                                    $query->whereIn('operacoes.classificacao_negativacao_id', $filtro_arr['filtros']['classificacao_negativacao']['valor']);
                                }else{
                                    $query->whereNotIn(DB::raw("coalesce(operacoes.classificacao_negativacao_id,0)"), [$filtro_arr['filtros']['classificacao_negativacao']['valor']]);
                                }
                            }
                            if(!empty($filtro_arr['filtros']['classificacao_cliente']['valor'])){
                                if ($filtro_arr['filtros']['classificacao_cliente']['exceto'] == 0){
                                    $query->whereIn('cliente.status_cliente_id->id', $filtro_arr['filtros']['classificacao_cliente']['valor']);
                                }else{
                                    $query->whereNotIn(DB::raw("coalesce(cliente.status_cliente_id->>'id','null')"), [$filtro_arr['filtros']['classificacao_cliente']['valor']]);
                                }
                            }
                            if(!empty($filtro_arr['filtros']['localizacao']['valor'])){
                                if ($filtro_arr['filtros']['localizacao']['exceto'] == 0){
                                    $query->whereIn('cliente.localizacao', $filtro_arr['filtros']['localizacao']['valor']);
                                }else{
                                    $query->whereNotIn(DB::raw("coalesce(cliente.localizacao,'null')"), [$filtro_arr['filtros']['localizacao']['valor']]);
                                }
                            }
                            if(isset($filtro_arr['filtros']['periodo_contato']['data_inicial']) && (isset($filtro_arr['filtros']['periodo_contato']['data_final']))){
                                if ($filtro_arr['filtros']['periodo_contato']['exceto'] == 0){
                                    $query->whereBetween('cliente.ultimo_contato', [$filtro_arr['filtros']['periodo_contato']['data_inicial'], $filtro_arr['filtros']['periodo_contato']['data_final']]);
                                }else{
                                    $query->whereNotBetween(DB::raw("coalesce(cliente.ultimo_contato,'1992-03-29 07:00:00')"), [$filtro_arr['filtros']['periodo_contato']['data_inicial'], $filtro_arr['filtros']['periodo_contato']['data_final']]);
                                }
                            }
                        })
                        ->distinct(DB::raw('telefones.telefone'));
                        if((!empty($filtro_arr['filtros']['valor_medio_cliente']['valor_inicial']) > 0) && ((!empty($filtro_arr['filtros']['valor_medio_cliente']['valor_final']))) > 0){
                            $sql->groupByRaw('cliente.id')
                                 ->havingBetween(DB::raw('SUM(operacoes.valor_nominal) / count(operacoes.id)'), [$filtro_arr['filtros']['valor_medio_cliente']['valor_inicial'], $filtro_arr['filtros']['valor_medio_cliente']['valor_final']]);
                        }
                    $result =    $sql->get();
    $i = 0;
    foreach ($result as $value) {
        $TemplateSms = $this->templateSms->find($filtro_arr['filtros']['templatesms_id']);
        $texto = $TemplateSms->mensagem;
        $nomes = explode(' ', $value->cliente);
        $primeiroNome = $nomes[0];
        $stringCorrigida1 = str_replace('{{PNOME}}', $primeiroNome, $texto);
        $stringCorrigida2 = str_replace('{{CREDOR}}', $value->credor, $stringCorrigida1);
        if ($this->plataforma_padrao->enviar_sms(['celular' => $value->telefone, 'mensagem' => $stringCorrigida2])){
            $this->logContatosService->salvarLogContatosSms($value->telefone_id, $stringCorrigida2);
            $i++;
        }
    }
    return('Foram enviados '.$i.' Sms');

    }


    public function preencher($templateId, Cliente $cliente, Credor $credor){
        $TemplateSms = $this->templateSms->find($templateId);
        $texto = $TemplateSms->mensagem;
        $stringCorrigida1 = str_replace('{{PNOME}}', $cliente->nome, $texto);
        $stringCorrigida2 = str_replace('{{CREDOR}}', $credor->nome, $stringCorrigida1);
        return $stringCorrigida2;
    }

    public function enviar(array $dados){
        return $this->plataforma_padrao->enviar_sms($dados);
    }


}
