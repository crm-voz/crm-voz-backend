<?php


namespace App\Repository\Administrador;

use App\Models\BackOffice\Cliente;
use App\Models\BackOffice\Credor;
use App\Models\Core\TemplateEmail;

class TemplateEmailRepository{

   public function __construct(TemplateEmail $template, Cliente $cliente, Credor $credor)
   {
        $this->template = $template;
        $this->cliente = $cliente;
        $this->credor = $credor;
   }

   public function preencher($templateId, $clienteId, $credorId){
        $modelCliente = $this->cliente->find($clienteId);
        $modelCredor = $this->credor->find($credorId);
        return $this->preencherEmail($templateId, $modelCliente, $modelCredor);
    }

   public function preencherEmail($templateId, Cliente $cliente, Credor $credor){
        $template = $this->template->find($templateId);
        $texto = $template->mensagem;
        $stringCorrigida1 = str_replace('$CLIENTE$', $cliente->nome, $texto);
        $stringCorrigida2 = str_replace('$CREDOR$', $credor->nome, $stringCorrigida1);
        return $stringCorrigida2;
   }

}
