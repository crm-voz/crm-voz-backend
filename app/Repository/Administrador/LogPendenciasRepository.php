<?php

namespace App\Repository\Administrador;

use App\Models\BackOffice\Operacao;
use App\Models\Core\LogPendencias;
use App\Repository\Backoffice\BaseRepository;

class LogPendenciasRepository extends BaseRepository
{
    public function __construct(LogPendencias $model)
    {
        parent::__construct($model);
    }

    public function filtrar($OperacaoId)
    {
        $dados = $this->model->where('operacao_id',$OperacaoId)->with('usuario_empresas')->orderBy('created_at','desc')->get();
        $result = collect();
        foreach ($dados as $value) {
             $result->add([
                'id' => $value->id,
                'operacao_id' => $value->operacao_id,
                'numero_operacao' => $value->operacao->numero_operacao,
                'usuario_empresa_id' => $value->usuario_empresa_id,
                'observacao' => $value->observacao,
                'status' => $value->status,
                'created_at' => $value->created_at,
                'updated_at' => $value->updated_at,
                'nome_usuario' => $value->usuario_empresas->user->name
             ]);
        }
        return $result;
    }

    public function criarEmLote($dados)
    {
        try {
            foreach($dados['operacoes_id'] as $op)
            {
                $this->model->create([
                    'operacao_id' => $op,
                    'usuario_empresa_id' => auth()->user()->usuario_empresas[0]->id,
                    'observacao' => $dados['observacao'],
                    'status' => $dados['status']
                ]);
            }
            return true;
        } catch (\Throwable $th) {
            logger($th->getMessage());
            return false;
        }
    }

    public function criarLogPadrao($op_id)
    {
        try {
            $operacao = Operacao::whereIn('id',$op_id)->whereNull('operacoes.motivo_pendencia_id')->get();
            foreach($operacao as $op)
            {
                $this->model->create([
                    'operacao_id' => $op->id,
                    'usuario_empresa_id' => auth()->user()->usuario_empresas[0]->id,
                    'observacao' => 'REGISTRO AUTOMÁTICO',
                    'status' => 'REGISTRO'
                ]);
            }
        } catch (\Throwable $th) {
            logger($th->getMessage());
            return false;
        }
    }


}
