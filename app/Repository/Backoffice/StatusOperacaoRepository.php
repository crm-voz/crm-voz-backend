<?php

namespace App\Repository\Backoffice;

use App\Models\Core\StatusOperacao;

class StatusOperacaoRepository extends BaseRepository
{
    public function __construct(StatusOperacao $statusOperacao)
    {
       parent::__construct($statusOperacao);
    }
}
