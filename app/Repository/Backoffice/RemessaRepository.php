<?php

namespace App\Repository\Backoffice;

use App\Http\Controllers\Api\v1\Services\RemessaAvulsaServices;
use App\Models\BackOffice\Remessa;
use App\Http\Controllers\Api\v1\Services\RemessaServices;
use Carbon\Carbon;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Illuminate\Support\Facades\DB;
use Exception;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class RemessaRepository extends BaseRepository {

    protected $remessaAvulsa;
    public function __construct(Remessa $model)
    {
       parent::__construct($model);
    }

    public function remessasCredor(){
        return $this->model->where('credor_id', auth()->user()->credor_id)->get();
    }

    public function indexList($filtros = []){
        $remessas = $this->model->query();
        if(array_key_exists('credor', $filtros)) {
            $remessas->where('credor_id', $filtros['credor']);
        }

        if(array_key_exists('data_remessa', $filtros)) {
            $remessas->whereDate('data_remessa', '<=', $filtros['data_remessa']);
        }

        $remessas->orderBy('created_at','desc');

        $lista = collect();
        foreach($remessas->limit(100)->get() as $remessa){
            $lista->add([
                "id" => $remessa->id,
                "numero_remessa" => $remessa->numero_remessa,
                "data_remessa" => $remessa->data_remessa,
                "data_entrada" => $remessa->data_entrada,
                "nome_arquivo" => $remessa->nome_arquivo,
                "credor_id" => $remessa->credor_id,
                "usuario_empresa_id" => $remessa->usuario_empresa_id,
                "created_at" => $remessa->created_at,
                "nome_credor" => $remessa->credor->nome,
            ]);
        }

        return $lista;
    }

    public function get($credor_id = null, $filtros = []){
        $rems = $this->model
        ->select('remessas.*', 'credores.nome as nome_credor')
        ->join('credores', 'remessas.credor_id', '=', 'credores.id')
        ->where(function ($query) use ($credor_id){
            if($credor_id){
                $query->where('credor_id', $credor_id);
            }
        });

        if(array_key_exists('credor', $filtros)) {
            $rems = $rems->where('credor_id', $filtros['credor']);
        }

        if(array_key_exists('data_entrada', $filtros)) {
            $rems = $rems->whereDate('data_entrada', '<=', $filtros['data_entrada']);
        }

        return $rems->get();
    }

    public function cancel($id){
        try {
            $remessa = $this->model->find($id);
            $res_operacoes = $remessa->operacoes()->delete();
            $remessa->delete();

            return ($res_operacoes && $remessa);
        } catch (\Throwable $th) {
            throw $th;
        }

    }

    public function deletar($id) {
        $remessa = $this->model->find($id);
        try {
            DB::beginTransaction();
            $remessa->operacoes()->delete();
            $remessa->delete();
            DB::commit();
        } catch (\Exception $err) {
            DB::rollback();
            throw $err;
        }
    }

    public function importar($remessa) {
        try {
            $spreadsheet = IOFactory::load(Storage::path("remessas/$remessa->nome_arquivo"));
            $tab_arr = $spreadsheet->getActiveSheet()->toArray();
            // Retirar o cabeçalho
            array_splice($tab_arr, 0, 1);
            // Abertura da Remessa
            $ops_count = 0;
            foreach($tab_arr as $reg){
                if($reg[0] != ''){
                    $cpf = $reg[7];
                    if (Str::length($reg[7]) == 10){
                        $cpf = '0'.$reg[7];
                    }
                    if (Str::length($reg[7]) == 9){
                        $cpf = '00'.$reg[7];
                    }
                    if (Str::length($reg[7]) == 8){
                        $cpf = '000'.$reg[7];
                    }
                    if (Str::length($reg[7]) == 7){
                        $cpf = '0000'.$reg[7];
                    }

                    $arr_remessa = [
                        'cliente' =>
                        [
                            'cpf_cnpj'      => trim(preg_replace('/[a-zA-Z\-\.]/', "", $cpf)),
                            'nome'          => trim(utf8_encode($reg[8])),
                            'endereco'      => trim(utf8_encode($reg[9])),
                            'bairro'        => trim(utf8_encode($reg[10])),
                            'cep'           => trim(preg_replace('/[a-zA-Z\-\.]/', "", $reg[11])),
                            'cidade'        => trim(utf8_encode($reg[12])),
                            'uf'            => trim(utf8_encode($reg[13])),

                            'telefones'     => [
                                [ 'telefone'     => trim(utf8_encode($reg[14])) ],
                                [ 'telefone'     => trim(utf8_encode($reg[15])) ],
                                [ 'telefone'     => trim(utf8_encode($reg[16])) ],
                                [ 'telefone'     => trim(utf8_encode($reg[17])) ],
                                [ 'telefone'     => trim(utf8_encode($reg[18])) ],
                                [ 'telefone'     => trim(utf8_encode($reg[19])) ]
                            ],
                            'emails'        => [
                                [ 'email'        => trim(utf8_encode($reg[20])) ],
                                [ 'email'        => trim(utf8_encode($reg[21])) ],
                                [ 'email'        => trim(utf8_encode($reg[22])) ],+
                                
                                [ 'email'        => trim(utf8_encode($reg[23])) ],
                                [ 'email'        => trim(utf8_encode($reg[24])) ],
                                [ 'email'        => trim(utf8_encode($reg[25])) ]
                            ],
                            'operacoes'     => [
                                [
                                    'numero_operacao'   => trim(preg_replace('/[a-zA-Z\-\.]/', "", $reg[0])),
                                    'data_vencimento'   => Carbon::createFromFormat('d/m/Y', $reg[2])->format('Y-m-d'),
                                    'valor_operacao'    => trim(preg_replace('/[a-zA-Z\-\$\,]/', "", $reg[3])),
                                    'aluno'             => trim(utf8_encode($reg[5])),
                                    'competencia'       => trim(utf8_encode($reg[6])),
                                    'descricao'         => trim(utf8_encode($reg[1])),
                                    'desconto'          => $reg[4] != '' ? trim(preg_replace('/[a-zA-Z\-\$\,]/', "", $reg[4])) : 0,
                                    'adicionais'        => ['data_inicio' => $reg[26]],
                                ]
                            ]
                        ]
                    ];

                    if (!$remessa){
                        throw new Exception("Erro ao gerar remessa", 1);
                    }

                    $remessaServices = new RemessaServices($arr_remessa, $remessa->credor);
                    $remessa_res = $remessaServices->salvar_com_remessa($remessa);
                    if($remessa_res) {
                        $ops_count++;
                    }
                }
            }

            if($ops_count === 0) {
                throw new \Exception('Todas as operações já foram cadastradas em uma remessa anterior', 1);
            }
            return ['result' => true, 'remessa' => $remessa];
        } catch (\Exception $err) {
            return ['result' => false, 'erro' => $err->getMessage()];
        }
    }

    public function importar_remessa_avulsa($remessa, $data_remessa=null, $credor_id=null) {
        try {
            $spreadsheet = IOFactory::load($remessa);
            $tab_arr = $spreadsheet->getActiveSheet()->toArray();
            // Retirar o cabeçalho
            array_splice($tab_arr, 0, 1);
            // Abertura da Remessa
            $ops_count = 0;

            if ($data_remessa){
                $data_remessa = $data_remessa;
            }else{
                $data_remessa = Carbon::now();
            }

            $remessaCreate = Remessa::create([
                "numero_remessa" => Carbon::now()->format('Ymdhs'),
                "data_remessa" => Carbon::now(),
                "nome_arquivo" => "Remessa Avulsa",
                "credor_id" => $credor_id,
                "usuario_empresa_id" => auth()->user()->usuario_empresas[0]->id,
                "data_entrada" => $data_remessa
            ]);
            foreach($tab_arr as $reg){
                if($reg[0] != ''){
                    $cpf = $reg[7];
                    if (Str::length($reg[7]) == 10){
                        $cpf = '0'.$reg[7];
                    }
                    if (Str::length($reg[7]) == 9){
                        $cpf = '00'.$reg[7];
                    }
                    if (Str::length($reg[7]) == 8){
                        $cpf = '000'.$reg[7];
                    }
                    if (Str::length($reg[7]) == 7){
                        $cpf = '0000'.$reg[7];
                    }

                    $arr_remessa = [
                        'cliente' =>
                        [
                            'cpf_cnpj'      => trim(preg_replace('/[a-zA-Z\-\.]/', "", $cpf)),
                            'nome'          => trim(utf8_encode($reg[8])),
                            'endereco'      => trim(utf8_encode($reg[9])),
                            'bairro'        => trim(utf8_encode($reg[10])),
                            'cep'           => trim(preg_replace('/[a-zA-Z\-\.]/', "", $reg[11])),
                            'cidade'        => trim(utf8_encode($reg[12])),
                            'uf'            => trim(utf8_encode($reg[13])),

                            'telefones'     => [
                                [ 'telefone'     => trim(utf8_encode($reg[14])) ],
                                [ 'telefone'     => trim(utf8_encode($reg[15])) ],
                                [ 'telefone'     => trim(utf8_encode($reg[16])) ],
                                [ 'telefone'     => trim(utf8_encode($reg[17])) ],
                                [ 'telefone'     => trim(utf8_encode($reg[18])) ],
                                [ 'telefone'     => trim(utf8_encode($reg[19])) ]
                            ],
                            'emails'        => [
                                [ 'email'        => trim(utf8_encode($reg[20])) ],
                                [ 'email'        => trim(utf8_encode($reg[21])) ],
                                [ 'email'        => trim(utf8_encode($reg[22])) ],
                                [ 'email'        => trim(utf8_encode($reg[23])) ],
                                [ 'email'        => trim(utf8_encode($reg[24])) ],
                                [ 'email'        => trim(utf8_encode($reg[25])) ]
                            ],
                            'operacoes'     => [
                                [
                                    'numero_operacao'   => trim(preg_replace('/[a-zA-Z\-\.]/', "", $reg[0])),
                                    'data_vencimento'   => Carbon::createFromFormat('m/d/Y', $reg[2])->format('Y-m-d'),
                                    'valor_operacao'    => trim(preg_replace('/[a-zA-Z\-\$\,]/', "", $reg[3])),
                                    'aluno'             => trim(utf8_encode($reg[5])),
                                    'competencia'       => trim(utf8_encode($reg[6])),
                                    'descricao'         => trim(utf8_encode($reg[1])),
                                    'desconto'          => $reg[4] != '' ? trim(preg_replace('/[a-zA-Z\-\$\,]/', "", $reg[4])) : 0,
                                    'adicionais'        => ['data_inicio' => $reg[26]],
                                ]
                            ]
                        ]
                    ];

                    if (!$remessa){
                        throw new Exception("Erro ao gerar remessa", 1);
                    }

                    $remessaAvulsaServices = new RemessaAvulsaServices($arr_remessa, $data_remessa, $credor_id);
                    $remessa_res = $remessaAvulsaServices->salvarRemessa($remessaCreate);
                    if($remessa_res) {
                        $ops_count++;
                    }
                }
            }

            if($ops_count === 0) {
                throw new \Exception('Todas as operações já foram cadastradas em uma remessa anterior', 1);
            }
            return ['result' => true, 'remessa' => $remessa];
        } catch (\Exception $err) {
            return ['result' => false, 'erro' => $err->getMessage()];
        }
    }

    public function total_remessas_periodo_chart($dados){
        try {
            $total_remessas = $this->model->select(DB::raw('count(*) as total_remessas'))
                ->whereBetween('remessas.data_entrada',[$dados->data_inicial,$dados->data_final])
                ->get();
        } catch (\Throwable $th) {
            return $th;
        }
        $total_remessas = ['total_remessas' => $total_remessas[0]->total_remessas];
        return [$total_remessas];
    }

    public function clientesRemessa($remessa){
        $clientes = collect();
        foreach($remessa->operacoes as $operacao){
            $clientes->add($operacao->cliente);
        }
        return $clientes->unique();
    }
}
