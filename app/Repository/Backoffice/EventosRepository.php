<?php

namespace App\Repository\Backoffice;

use App\Models\Core\Eventos;

class EventosRepository extends BaseRepository{
    protected const CREDOR_VOZ = 51;
    public function __construct(Eventos $model)
    {
       parent::__construct($model);
    }

    public function get($id = null){
        if ($id){
            return $this->model->where('cliente_id', $id)->get();
        }

        return $this->model->where('status', true)->get();
    }
        public function eventosAutomatico($id,$message,$motivo,$credorId = null, $parametro = null, $userEmpresa = null)
    {
        return Eventos::create([
            'descricao' => $message,
            'cliente_id' => $id,
            'usuario_empresas_id' => $userEmpresa ? $userEmpresa :auth()->user()->usuario_empresas[0]->id,
            'motivo_evento_id' => $motivo,
            'credores_id' => $credorId ? $credorId : self::CREDOR_VOZ,
            'parametros_contato' => $parametro ? json_encode($parametro) : json_decode($parametro)
        ]);

    }

    public function createEventos($id,$message,$motivo,$credorId = null, $parametro = null)
    {
        return Eventos::create([
            'descricao' => $message,
            'cliente_id' => $id,
            'usuario_empresas_id' => auth()->user()->usuario_empresas[0]->id,
            'motivo_evento_id' => $motivo,
            'credores_id' => $credorId ?? auth()->user()->usuario_empresas[0]->empresa->id,
            'parametros_contato' => $parametro ? json_encode($parametro) : json_decode($parametro)
        ]);

    }

}
