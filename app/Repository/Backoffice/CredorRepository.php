<?php

namespace App\Repository\Backoffice;

use App\Models\BackOffice\Credor;
use Exception;

class CredorRepository extends BaseRepository
{
    public function __construct(Credor $credor, AcordoRepository $acordoRepository)
    {
       parent::__construct($credor);
       $this->acordoRepository = $acordoRepository;
    }

    public function index($todos = false){
        $credores = $this->model::select('credores.*', 'cidades.nome as cidade_nome')
                    ->join('cidades', 'credores.cidade_id', '=', 'cidades.id')
                    ->where(function($q) use($todos){
                        if (!$todos){
                            $q->where('status',true);
                        }
                    })->get();
        foreach ($credores as $value) {
            $value->parametros_config = json_decode($value->parametros_config);
        }

        return $credores;
    }

    public function filtro($filtro, $items_por_pag, $item_pag){
        return $this->model->where('nome', 'like', '%' . $filtro . '%')
                            ->orWhere('cnpj', 'like', '%' . $filtro . '%')
                            ->orWhere('status', 'like', '%' . $filtro . '%')
                            ->offset($item_pag)
                            ->limit($items_por_pag)
                            ->get();
    }

    public function get($id = null)
    {
        return $this->model->where(function($query) use($id){
            if ($id){
                $query->find($id);
            }
        })->get();
    }

    public function ListarCredores($todos = false){
        $credores = $this->model->where(function($query) use($todos){
            if (!$todos){
                $query->where('status',true);
            }
        })->select('id','nome','razao_social')
          ->get();

        $lista = collect();
        foreach($credores as $credor){
            $lista->add((object)[
                'id' => $credor->id,
                'nome' => $credor->nome,
                'razao_social' => $credor->razao_social
            ]);
        }
        return $lista;
    }

    public function gets($idEmpresa = null)
    {
        return $this->model->where(function ($query) use($idEmpresa){
            if ($idEmpresa){
                $query->where('empresa_id', $idEmpresa);
            }
        })->get();
    }

    public function listarAcordosCredor($credorId){
        return $this->acordoRepository->acordosCredor($credorId);
    }

    public function stats_operacao($id = null) {
        $status = [];

        if($id) {
            $cf = $this->model->find($id);

            foreach($this->status_op->all() as $sops) {
                $status[$cf->nome]["operacoes_status"][$sops->nome] = $cf->operacoes()->where('status_operacao_id', $sops->id)->count();
            };
        } else {
            $cf = $this->model->all();

            foreach($cf as $c) {
                foreach($this->status_op->all() as $sops) {
                    $status[$c->nome]["operacoes_status"][$sops->nome] = $c->operacoes()->where('status_operacao_id', $sops->id)->count();
                };
            }
        }
        return $status;
    }

    public function clientesCredor(){
        $credor = $this->model->find(auth()->user()->credor_id);
        $clientes = collect();
        foreach($credor->operacoes as $operacao){
            $clientes->add($operacao->cliente);
        }
        return $clientes->unique();
    }

    public function operacoesCredor(){
        $credor = $this->model->find(auth()->user()->credor_id);
        if (!$credor){
            throw new Exception('Credor não encontrado.');
        }

        return $credor->operacoes;
    }

}
