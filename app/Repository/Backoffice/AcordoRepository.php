<?php

namespace App\Repository\Backoffice;

use App\Models\Cobranca\Acordo;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class AcordoRepository extends BaseRepository
{
    public function __construct(Acordo $acordo)
    {
       parent::__construct($acordo);
    }

    public function acordosCredor($credorId){
        $acordos = $this->model->join('operacao_acordo','acordo.id','=','operacao_acordo.acordos_id')
                           ->join('operacoes','operacoes.id','=','operacao_acordo.operacoes_id')
                           ->join('remessas','remessas.id', '=','operacoes.remessa_id')
                           ->where('remessas.credor_id',$credorId)->select('acordo.*')->with('parcelas')->get();
        return $acordos;
    }

}


