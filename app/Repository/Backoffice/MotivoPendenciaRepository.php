<?php

namespace App\Repository\Backoffice;

use App\Models\Core\MotivoPendencia;

class MotivoPendenciaRepository extends BaseRepository{
    public function __construct(MotivoPendencia $model)
    {
       parent::__construct($model);
    }

    public function get(){
        return $this->model->where('status', true)->get();
    }

}
