<?php

namespace App\Repository\Backoffice;

use App\Models\Core\MotivoDevolucao;

class MotivoDevolucaoRepository extends BaseRepository
{
    public function __construct(MotivoDevolucao $motivoDevolucao)
    {
       parent::__construct($motivoDevolucao);
    }
}
