<?php

namespace App\Repository\Backoffice;

use App\Models\BackOffice\Desconto;
use App\Models\BackOffice\Operacao;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use PhpOffice\PhpSpreadsheet\IOFactory;

use function chillerlan\QRCodePublic\send_response;

class DescontosRepository extends BaseRepository {

    public function __construct(Desconto $desconto, OperacaoRepository $operacaoRepository)
    {
       parent::__construct($desconto);
       $this->operacaoRepository = $operacaoRepository;
       $this->modelDesconto = $desconto;
    }

    /*
    * file : arquivo a ser importado
    * tipo: tipo da planilha (desconto ou operacao em lote)
    * valor: valor do tipo que sera persistido no banco
    */
    public function importar($file, $credor_id, $tipo){
        try {
            $spreadsheet = IOFactory::load($file);
            $tab_arr = $spreadsheet->getActiveSheet()->toArray();
            // Retirar o cabeçalho
            array_splice($tab_arr, 0, 1);
            DB::beginTransaction();
            if ($tipo == 1){
                foreach($tab_arr as $reg){
                    $operacao = $this->operacaoRepository->getOperacao(trim(utf8_encode($reg[6])), $credor_id);
                    if($operacao){

                        $desconto = [
                        'tipo' => trim(utf8_encode($reg[0])),
                        'desconto_padrao' => trim(utf8_encode($reg[1])),
                        'desconto_minimo' => trim(utf8_encode($reg[2])),
                        'desconto_maximo' => trim(utf8_encode($reg[3])),
                        'data_inicio' =>  trim(utf8_encode($reg[4])),
                        'data_final' =>  trim(utf8_encode($reg[5])),
                        'operacoes_id' => $operacao->id,
                        'status' => true,
                        ];

                        $this->modelDesconto->create($desconto);
                    }
                }
            }
            DB::commit();
            return true;

        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;

        }
    }

}
