<?php

namespace App\Repository\Backoffice;

use App\Models\Core\Snapshot;

class SnapshotRepository extends BaseRepository
{
    public function __construct(Snapshot $snpashot)
    {
       parent::__construct($snpashot);
    }

    public function createJson($dados){
        return $this->model->create([
            'banco' => $dados->toJson()
        ]);
    }
}
