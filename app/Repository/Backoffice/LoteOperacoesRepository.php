<?php

namespace App\Repository\Backoffice;

use Illuminate\Support\Facades\DB;
use PhpOffice\PhpSpreadsheet\IOFactory;

class LoteOperacoesRepository extends BaseRepository {

    protected $modelOperacoes, $modelEventos;
    public function __construct(OperacaoRepository $operacaoRepository,
                                EventosRepository $eventosRepository)
    {
       $this->operacaoRepository = $operacaoRepository;
       $this->eventosRepository = $eventosRepository;
    }

    /*
    * file : arquivo a ser importado
    * tipo: tipo da planilha (desconto ou operacao em lote)
    * valor: valor do tipo que sera persistido no banco
    */
    public function importar($file)
    {
        try {
            $spreadsheet = IOFactory::load($file);
            $tab_arr = $spreadsheet->getActiveSheet()->toArray();
            // Retirar o cabeçalho
            array_splice($tab_arr, 0, 1);
            DB::beginTransaction();
            foreach($tab_arr as $reg){
                $operacao = $this->operacaoRepository->getOperacao(trim(utf8_encode($reg[0])), intval($reg[3]));
                if($operacao){
                    $this->eventosRepository->eventosAutomatico(
                        $operacao->cliente_id,
                        $reg[8],
                        $reg[7],
                        intval($operacao->remessa->credor->id),
                        null,
                        null
                    );
                    if (intval($reg[4]) > 0)
                        $operacao->update(['motivo_pendencia_id' =>intval($reg[4])]);
                    if (intval($reg[5]) > 0)
                        $operacao->update(['status_operacao_id' =>intval($reg[5])]);
                    if (intval($reg[6]) > 0)
                        $operacao->update(['classificacao_negativacao_id' =>intval($reg[6])]);
                }
            }
            DB::commit();
            return true;

        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;

        }
    }

}
