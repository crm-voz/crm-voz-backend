<?php

namespace App\Repository\Backoffice;

use App\Gateway\Sms\BestVoice;
use App\Gateway\Sms\iSms;
use App\Models\BackOffice\Telefone;
use App\Models\Core\Boleto;
use App\Models\Core\LogTelefone;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class EnvioAutomaticoSmsRepository extends BaseRepository{

    protected iSms $plataforma_padrao;
    public function __construct(Boleto $boleto, BestVoice $plataforma)
    {
       parent::__construct($boleto);
       $this->plataforma_padrao = new $plataforma;
    }

    protected function saveLog($id, $mensagem){
        LogTelefone::create(['telefone_id' => $id, 'observacao' => json_encode(['lembrete' =>$mensagem])]);
    }

    protected function enviarSMS($telefones, $msg, $log){
        foreach($telefones as $telefone){
          // logger('Cliente ' . $telefone->cliente->nome . ' - ' . $telefone->telefone);
            $this->plataforma_padrao->enviar_sms(['celular' => $telefone->telefone,'mensagem' => $msg]);
           $this->saveLog($telefone->id, $log);
        }
    }

    public function envio_automatico_sms(){
        $diaSemana = Carbon::now()->isoformat('d'); //verificar se é final de semana 0->domingo 6->sabado
        if ($diaSemana != "0" && $diaSemana != "6"){
            $clientesaVencer = $this->model->select(DB::raw("cliente.id as cliente, (vencimento - current_date) as dia_vencer, boleto.valor,
                                                            boleto.vencimento, boleto.linha_digitavel, boleto.baixa, boleto.valor_pago, boleto.linha_digitavel"))
                                    ->join('parcelas','parcelas.id','=','boleto.parcelas_id')
                                    ->join('acordo','acordo.id','=','parcelas.acordos_id')
                                    ->join('cliente','cliente.id','=','acordo.cliente_id')
                                    ->whereNull('boleto.baixa')
                                    ->whereNull('boleto.valor_pago')
                                    ->where(DB::raw('(boleto.vencimento - current_date)'), '>', -1)
                                    ->where(DB::raw('(boleto.vencimento - current_date)'), '<', 4)
                                    ->orderBy('dia_vencer','asc')
                                    ->get();

           // logger('clientes ' . $clientesaVencer->count());
            foreach ($clientesaVencer as $value) {
                $telefones = Telefone::where('cliente_id', $value->cliente)->get();
                switch ($value->dia_vencer) {
                    case 0:
                        # code...
                        $message = "Olá, lembrando que seu boleto no valor ".$value['valor']." vence hoje, ".date('d-m-Y', strtotime($value['vencimento']))."! "."linha digitavel: ".$value['linha_digitavel'];
                        $log = "lembrete de sms 0 dia linha digitavel: ".$value['linha_digitavel'];
                        $this->enviarSMS($telefones, $message, $log);
                        break;
                    case 1:
                        # code...
                        $message = "Olá, lembrando que seu boleto no valor ".$value['valor']." vence amanhã, ".date('d-m-Y', strtotime($value['vencimento']))."! "."linha digitavel: ".$value['linha_digitavel'];
                        $log = "lembrete de sms 1 dia linha digitavel: ".$value['linha_digitavel'];
                        $this->enviarSMS($telefones, $message, $log);
                        break;

                    case 2:
                        # code...
                        $message = "Olá, lembrando que seu boleto no valor ".$value['valor']." vence daqui a 2 dias, ".date('d-m-Y', strtotime($value['vencimento']))."! "."linha digitavel: ".$value['linha_digitavel'];
                        $log = "lembrete de sms 2 dias linha digitavel: ".$value['linha_digitavel'];
                        $this->enviarSMS($telefones, $message, $log);
                        break;

                    case 3:
                        # code...
                        $message = "Olá, lembrando que seu boleto no valor ".$value['valor']." vence daqui a 3 dias, ".date('d-m-Y', strtotime($value['vencimento']))."! "."linha digitavel: ".$value['linha_digitavel'];
                        $log = "lembrete de sms 3 dias linha digitavel: ".$value['linha_digitavel'];
                        $this->enviarSMS($telefones, $message, $log);
                        break;
                }
            }

            return ;
        }
    }
}
