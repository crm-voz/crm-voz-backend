<?php

namespace App\Repository\Backoffice;

use App\Models\BackOffice\Cliente;
use App\Models\BackOffice\Operacao;
use App\Models\Cobranca\ExecutivoCobranca;
use App\Models\Cobranca\Parcela;
use App\Services\ClienteServices;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Gateway\Dados\TargetApi;

class ClientesRepository extends BaseRepository {


    public function __construct(Cliente $cliente,
                                EventosRepository $eventosRepository,
                                OperacaoRepository $operacaoRepository)
    {
       parent::__construct($cliente);
       $this->eventosRepository = $eventosRepository;
       $this->operacaoRepository = $operacaoRepository;
    }

    public function enriquecerClientes($tipo, $value){
        $target = new TargetApi();
        $response = match($tipo){
            'cpf' => $target->pesquisarPorCpf($value),
            'nome' => $target->pesquisarPorNome($value),
            'telefone' => $target->pesquisarPorTelefone($value),
            'email' => $target->pesquisarPorEmail($value),
        };

        foreach($response['result'] as $pessoa){
            foreach($pessoa as $p){
                $cliente = $this->model->where('cpf_cnpj',$p['cadastral']['CPF'])->first();
                if($cliente)
                    app(ClienteServices::class)->updateCliente($cliente, $p);
            }
        }
        return true;
    }

    public function updateClientesSemContato(int $dias, $status_localizacao)
    {
        $datafinal = Carbon::now();
         $this->model->whereNull('localizacao')->orWhere('localizacao','Localizado')
                     ->chunk('300', function($clientes)  use($status_localizacao, $datafinal, $dias){
                        foreach($clientes as $cliente){
                            if($cliente){
                                if ($datafinal->diffInDays($cliente->ultimo_contato) >= $dias)
                                $cliente->update(['localizacao' => $status_localizacao]);
                            }
                        }
                    });
    }

    public function updateClientesSemContatoParcelas(int $dias, $status_localizacao)
    {
        $datafinal = Carbon::now();
        $this->model->whereNull('localizacao')->orWhere('localizacao','Localizado')
                    ->chunk('300', function($clientes)  use($status_localizacao, $datafinal, $dias){
                        foreach($clientes as $cliente){
                            if ($cliente){
                                $parcelas = Parcela::whereHas('acordos', function($query) use($cliente) {
                                    $query->where('cliente_id', $cliente->id);
                                })->whereRaw('current_timestamp > data_vencimento')
                                  ->whereIn('status',['Aguardando','A Vencer'])
                                  ->get();
                                if ($parcelas){
                                    foreach($parcelas as $parcela){
                                        if ($datafinal->diffInDays($parcela->data_vencimento) >= $dias)
                                            $cliente->update(['localizacao' => $status_localizacao]);
                                    }
                                }
                            }
                        }
                    });

    }

    public function get($id = null) {
        if ($id){
            return $this->model->with('status_localizacao')->find($id);
        }

        return $this->model->with('status_localizacao')->paginate(self::PER_PAGE);
    }

    public function getCreate(){
        $clientes = $this->model->with('status_localizacao')->get();
        return $clientes->map(function($item){
            return [
                'id' => $item->id,
                'nome' => $item->nome,
                'cpf_cnpj' => $item->cpf_cnpj,
                'status_localiacao' => $item->status_localizacao,
                'status_cliente_id' => json_decode($item->status_cliente_id)
            ];
        });
    }

    public function created(){
        return Cliente::limit(1000)->orderBy('created_at','desc')->get();
    }

    public function operacoesPorCpf($cpf, $credorId)
    {
        return Cliente::select('cliente.nome as nome_cliente','operacoes.*','credores.nome as nome_credor')
                    ->Join('operacoes', 'operacoes.cliente_id', '=','cliente.id' )
                    ->Join('remessas', 'operacoes.remessa_id', '=', 'remessas.id')
                    ->Join('credores', 'remessas.credor_id', '=', 'credores.id')
                    ->where('cliente.cpf_cnpj',$cpf)
                    ->where('credores.id',$credorId)
                    ->where('operacoes.status_operacao_id',1)
                    ->get();
    }

    public function pesquisaAvancada(array $dados){
        $campo = $dados['campo'];
        $value = $dados['valor'];
        switch($campo){
            case 'cpf' : {
              $result = $this->model->where('cpf_cnpj', 'ilike', '%'. $value . '%')->get();
              break;
            }
            case 'nome' : {
                $result = $this->model->where('nome', 'ilike', '%'. $value . '%')->get();
                break;
              }
              case 'telefone' : {
                $result = $this->model->with('telefones')->whereHas('telefones', function ($query) use($value) {
                    $query->where('telefone','ilike','%'. $value . '%');
                })->get();
                break;
              }
              case 'email' : {
                $result = $this->model->with('emails')->whereHas('emails', function ($query) use($value) {
                    $query->where('email','ilike','%'. $value . '%');
                })->get();
                break;
              }
              case 'operacao' : {
                //$result = $this->operacaoRepository->getClientePorNumeroOperacao($value);
                $result = $this->model->whereHas('operacoes', function($query) use ($value){
                    $query->where('numero_operacao','ilike',$value);
                })->get();
                break;
              }
              case 'aluno' : {
                $result = $this->model->where('nome','ilike','%'.$value.'%')->get();
                break;
              }
          }
        return $result;
    }

    public function search($value){
       return  $this->model->with('status_localizacao')->where('nome', 'like', $value . '%')
                                ->orWhere('cpf_cnpj', 'like', '%' . $value . '%')
                                ->orWhere('id', 'like', '%' . $value . '%')
                                ->orderBy('id', 'desc')
                                ->get();
    }

    public function filtrar($filtro_arr){
        $clientes = $this->model->query();
        $clientes->whereHas('operacoes', function ($q) use ($filtro_arr){
            if(array_key_exists('data_inicial', $filtro_arr['filtros']) && array_key_exists('data_final', $filtro_arr['filtros'])) {
                $q->whereBetween('data_vencimento',[$filtro_arr['filtros']['data_inicial'],$filtro_arr['filtros']['data_final']]);
            }

            if(array_key_exists('credores', $filtro_arr['filtros'])) {
                $q->whereHas('remessa', function ($query) use ($filtro_arr) {
                    $query->whereIn('credor_id', $filtro_arr['filtros']['credores']);
                });
            }

            if(array_key_exists('status_operacao', $filtro_arr['filtros'])) {
                $op  =  $filtro_arr['filtros']['status_operacao']['exceto'] == 0 ? '=' : '!=';
                $q->whereHas('status_operacao', function ($query) use($filtro_arr, $op) {
                    $query->where('id', $op, $filtro_arr['filtros']['status_operacao']['valor']);
                });
            }

            if(array_key_exists('motivo_pendencia', $filtro_arr['filtros'])) {
                $op  =  $filtro_arr['filtros']['motivo_pendencia']['exceto'] == 0 ? '=' : '!=';
                $q->whereHas('motivo_pendencia', function ($query) use($filtro_arr, $op) {
                    $query->where('id', $op, $filtro_arr['filtros']['motivo_pendencia']['valor']);
                });
            }

            if(array_key_exists('classificacao_negativacao', $filtro_arr['filtros'])){
                $op  =  $filtro_arr['filtros']['classificacao_negativacao']['exceto'] == 0 ? '=' : '!=';
                $q->whereHas('classificacao_negativacao', function ($query) use($filtro_arr, $op) {
                    $query->where('id', $op, $filtro_arr['filtros']['classificacao_negativacao']['valor']);
                });
            }

            if(array_key_exists('valor', $filtro_arr['filtros'])) {
                $tipo           = $filtro_arr['filtros']['valor']['tipo'];
                $valor_inicial  = array_key_exists('valor_inicial', $filtro_arr['filtros']['valor']) ? $filtro_arr['filtros']['valor']['valor_inicial'] : null;
                $valor_final    = array_key_exists('valor_final', $filtro_arr['filtros']['valor']) ? $filtro_arr['filtros']['valor']['valor_final'] : null;
                switch ($tipo) {
                    // Valor da operação
                    case 1:
                        $q->where(function($query) use($valor_inicial, $valor_final) {
                            $query->whereBetween('valor_nominal', [$valor_inicial, $valor_final]);
                        });
                        break;

                    // Valor ticket médio do cliente
                    case 2:
                        # code...
                        break;

                    // Valor ticket médio da operação
                    case 3:
                        # code...
                        break;
                }
            }

        });
        if(array_key_exists('periodo_contato', $filtro_arr['filtros'])) {
            if ($filtro_arr['filtros']['periodo_contato']['exceto'] == 0) {
                $clientes->whereBetween('ultimo_contato', [$filtro_arr['filtros']['periodo_contato']['data_inicial'], $filtro_arr['filtros']['periodo_contato']['data_final']]);
            }else{
                $clientes->whereNotBetween('ultimo_contato', [$filtro_arr['filtros']['periodo_contato']['data_inicial'], $filtro_arr['filtros']['periodo_contato']['data_final']]);
            }
        }
        if(array_key_exists('classificacao_cliente',  $filtro_arr['filtros'])) {
            if($filtro_arr['filtros']['classificacao_cliente']['exceto'] == 0) {
                $clientes->whereIn('status_cliente_id->title', $filtro_arr['filtros']['classificacao_cliente']['valor']);
            } else {
                $clientes->whereNotIn('status_cliente_id->title', $filtro_arr['filtros']['classificacao_cliente']['valor']);
            }
        }
        return  $clientes->with('status_localizacao')->limit(1000)->get();
    }

    public function localizacaoChart(){
        try {
            $cliente = $this->model->select(DB::raw('count(*) as quantidade, cliente.localizacao'))
                ->whereIn('cliente.localizacao',['Localizado','Nao Localizado','Em Processo de Localizacao'])
                ->groupBy('cliente.localizacao')
                ->get();

            $total = 0;
            for ($i=0; $i < $cliente->count(); $i++) {
                $total = $total + $cliente[$i]->quantidade;
            }

            $cliente[0] = ['quantidade' => $cliente[0]->quantidade,'localizacao' => $cliente[0]->localizacao, 'indice' => $cliente[0]->quantidade*100/$total];
            isset($cliente[0]) ? ($cliente[0]) : 0;
            $cliente[1] = ['quantidade' => $cliente[1]->quantidade,'localizacao' => $cliente[1]->localizacao, 'indice' => $cliente[1]->quantidade*100/$total];
            isset($cliente[1]) ? ($cliente[1]) : 0;
            $cliente[2] = ['quantidade' => $cliente[2]->quantidade,'localizacao' => $cliente[2]->localizacao, 'indice' => $cliente[2]->quantidade*100/$total];
            isset($cliente[2]) ? ($cliente[2]) : 0;

            $tot = ['quantidade' => $total,'localizacao' => 'total'];
            isset($tot) ? ($tot) : 0;
        } catch (\Throwable $th) {
            return $th;
        }

        return ['dados' => $cliente, 'total'=> $tot, 'mensagem' => 'Operação executada com sucesso.'];
    }

    public function statusClienteChart(){
        try {
            $status_localizacao = $this->model->select(DB::raw('count(cliente.*) as quantidade, status_localizacao.nome'))
                ->join('status_localizacao','status_localizacao.id','=','cliente.status_localizacao_id')
                ->groupBy('status_localizacao.nome')
                ->get();
        } catch (\Throwable $th) {
            return $th;
        }
        return $status_localizacao;
    }

    public function totalClientePeriodoChart($dados){
        try {
            $total_cliente = $this->model->select(DB::raw('count(*) as total_cliente'))
                ->whereBetween('cliente.created_at',[$dados->data_inicial,$dados->data_final])
                ->get();
        } catch (\Throwable $th) {
            return $th;
        }

        return $total_cliente;
    }

    public function operacoesAcordo($id){
       return Operacao::select('operacoes.*', 'status_operacao.nome as status_nome',
                                'status_operacao.sigla as status_sigla','acordo.id as acordo_id','acordo.numero_acordo')
                        ->where('operacoes.cliente_id', $id)
                        ->join('status_operacao', 'operacoes.status_operacao_id', '=', 'status_operacao.id')
                        ->join('operacao_acordo', 'operacao_acordo.operacoes_id', '=', 'operacoes.id')
                        ->join('acordo', 'acordo.id', '=', 'operacao_acordo.acordos_id')
                        ->get();
    }

    public function updatestatusLocalizacao($ids, $localizacao){
      return  $this->model->whereIn('id', $ids)
                          ->update(['localizacao' => $localizacao]);
    }

    public function updateLote($ids, $statusIds){
        //dd($ids, $statusIds);
        return $this->model->whereIn('id', $ids)
                           ->update(['status_localizacao_id' => $statusIds]);
    }

    public function updateClassificacaoMultiplosClientes($ids, $statusIds){
        return $this->model->whereIn('id', $ids)
                           ->update(['status_cliente_id' => $statusIds[0]]);
    }


    public function criarEvento($id,$message, $motivoId){
        return $this->eventosRepository->createEventos($id, $message, $motivoId);
    }

    public function criar_evento($id,$tipo,$execAntigo,$executivo){
        $executivoAntigo = ExecutivoCobranca::where('id',$execAntigo)->with('funcionarios')->first();
        return $this->eventosRepository->createEventos($id, 'ALTERAÇAO NA '.$tipo.' FEITA PELO USUÁRIO '.auth()->user()->name.' DE '.$executivoAntigo->funcionarios->nome.' PARA '.$executivo, 41);
    }

    public function pesquisar($filtro_arr){
            $sql = $this->model->query();
                    $sql->select('cliente.*');
                    $sql->join('operacoes', 'operacoes.cliente_id', '=', 'cliente.id')
                        ->join('remessas', 'remessas.id', '=', 'operacoes.remessa_id')
                        ->join('credores', 'credores.id', '=', 'remessas.credor_id')
                        ->leftJoin('eventos', 'eventos.cliente_id', '=', 'cliente.id')
                        ->leftJoin('cidades', 'cidades.id', '=', 'cliente.cidades_id')
                        ->leftJoin('status_localizacao', 'status_localizacao.id', '=', 'cliente.status_localizacao_id')
                        ->leftJoin('classificacao_negativacao', 'classificacao_negativacao.id', '=', 'operacoes.classificacao_negativacao_id')
                        ->leftJoin('acordo', 'acordo.cliente_id', '=', 'cliente.id')
                        ->leftJoin('parcelas', 'parcelas.acordos_id', '=', 'acordo.id')
                        ->where(function($query) use($filtro_arr){
                            if(!empty($filtro_arr['filtros']['credores'])){
                                $query->whereIn('credores.id', $filtro_arr['filtros']['credores']);
                            }
                            switch(isset($filtro_arr['filtros']['tipo_base'])) {
                                case 'operacao':
                                    if(isset($filtro_arr['filtros']['data_inicial']) && (isset($filtro_arr['filtros']['data_final']))){
                                        $query->whereBetween('operacoes.data_vencimento',[$filtro_arr['filtros']['data_inicial'],$filtro_arr['filtros']['data_final']]);
                                    }
                                    break;
                                case 'parcela':
                                    if(isset($filtro_arr['filtros']['data_inicial']) && (isset($filtro_arr['filtros']['data_final']))){
                                        $query->whereBetween('parcelas.data_vencimento',[$filtro_arr['filtros']['data_inicial'],$filtro_arr['filtros']['data_final']]);
                                    }
                                    break;
                                case 'acordo':
                                    if(isset($filtro_arr['filtros']['data_inicial']) && (isset($filtro_arr['filtros']['data_final']))){
                                        $query->whereBetween('acordo.created_at',[$filtro_arr['filtros']['data_inicial'].' 00:00:00 ',$filtro_arr['filtros']['data_final'].' 23:59:59']);
                                    }
                                    break;
                            }
                            if(!empty($filtro_arr['filtros']['status_operacao']['valor'])){
                                if ($filtro_arr['filtros']['status_operacao']['exceto'] == 0){
                                    $query->whereIn('operacoes.status_operacao_id', $filtro_arr['filtros']['status_operacao']['valor']);
                                }else{
                                    $query->whereNotIn(DB::raw("coalesce(operacoes.status_operacao_id,0)"), [$filtro_arr['filtros']['status_operacao']['valor']]);
                                }
                            }
                            if(!empty($filtro_arr['filtros']['motivo_pendencia']['valor'])){
                                if ($filtro_arr['filtros']['motivo_pendencia']['exceto'] == 0){
                                    $query->whereIn('operacoes.motivo_pendencia_id', $filtro_arr['filtros']['motivo_pendencia']['valor']);
                                }else{
                                    $query->whereNotIn(DB::raw("coalesce(operacoes.motivo_pendencia_id,0)"), [$filtro_arr['filtros']['motivo_pendencia']['valor']]);
                                }
                            }
                            if(!empty($filtro_arr['filtros']['motivo_evento']['valor'])){
                                if ($filtro_arr['filtros']['motivo_evento']['exceto'] == 0){
                                    $query->whereIn('eventos.motivo_evento_id', $filtro_arr['filtros']['motivo_evento']['valor']);
                                }else{
                                    $query->whereNotIn(DB::raw("coalesce(eventos.motivo_evento_id,0)"), [$filtro_arr['filtros']['motivo_evento']['valor']]);
                                }
                            }
                            if(!empty($filtro_arr['filtros']['classificacao_negativacao']['valor'])){
                                if ($filtro_arr['filtros']['classificacao_negativacao']['exceto'] == 0){
                                    $query->whereIn('operacoes.classificacao_negativacao_id', $filtro_arr['filtros']['classificacao_negativacao']['valor']);
                                }else{
                                    $query->whereNotIn(DB::raw("coalesce(operacoes.classificacao_negativacao_id,0)"), [$filtro_arr['filtros']['classificacao_negativacao']['valor']]);
                                }
                            }
                            if(!empty($filtro_arr['filtros']['classificacao_cliente']['valor'])){
                                if ($filtro_arr['filtros']['classificacao_cliente']['exceto'] == 0){
                                    $query->whereIn('cliente.status_cliente_id->id', $filtro_arr['filtros']['classificacao_cliente']['valor']);
                                }else{
                                    $query->whereNotIn(DB::raw("coalesce(cliente.status_cliente_id->>'id','null')"), [$filtro_arr['filtros']['classificacao_cliente']['valor']]);
                                }
                            }
                            if(!empty($filtro_arr['filtros']['localizacao']['valor'])){
                                if ($filtro_arr['filtros']['localizacao']['exceto'] == 0){
                                    $query->whereIn('cliente.localizacao', $filtro_arr['filtros']['localizacao']['valor']);
                                }else{
                                    $query->whereNotIn(DB::raw("coalesce(cliente.localizacao,'null')"), [$filtro_arr['filtros']['localizacao']['valor']]);
                                }
                            }
                            if(isset($filtro_arr['filtros']['periodo_contato']['data_inicial']) && (isset($filtro_arr['filtros']['periodo_contato']['data_final']))){
                                if ($filtro_arr['filtros']['periodo_contato']['exceto'] == 0){
                                    $query->whereBetween('cliente.ultimo_contato', [$filtro_arr['filtros']['periodo_contato']['data_inicial'], $filtro_arr['filtros']['periodo_contato']['data_final']]);
                                }else{
                                    $query->whereNotBetween(DB::raw("coalesce(cliente.ultimo_contato,'1992-03-29 07:00:00')"), [$filtro_arr['filtros']['periodo_contato']['data_inicial'], $filtro_arr['filtros']['periodo_contato']['data_final']]);
                                }
                            }
                        });
                        if(empty($filtro_arr['filtros']['operacao_por_cliente']['valor'])){
                            $sql->distinct(DB::raw('cliente.nome'));
                        }
                        if((!empty($filtro_arr['filtros']['valor_medio_cliente']['valor_inicial']) >= 0) && ((!empty($filtro_arr['filtros']['valor_medio_cliente']['valor_final']))) > 0){
                            $sql->groupByRaw('cliente.id')
                                 ->havingBetween(DB::raw('SUM(operacoes.valor_nominal) / count(operacoes.id)'), [$filtro_arr['filtros']['valor_medio_cliente']['valor_inicial'], $filtro_arr['filtros']['valor_medio_cliente']['valor_final']]);
                        }
                        if(!empty($filtro_arr['filtros']['operacao_por_cliente']['valor']) > 0){
                            $sql->groupByRaw('cliente.id')
                                 ->havingRaw('count(DISTINCT operacoes.id) = ?', [$filtro_arr['filtros']['operacao_por_cliente']['valor']]);
                        }
                    return  $sql->orderBy('cliente.nome')->get();

    }

    public function buscarClientesPeloId($ids)
    {
        return $this->model->whereIn('id', $ids)->with('telefones')->get();
    }

    public function enderecoAnterior($endAnterior, $endAtual){
        $result = [];
        if ($endAnterior->cep <> $endAtual['cep']){
            $result[] = 'Cep Anterior: '.$endAnterior->cep;
        }
        if ($endAnterior->endereco <> $endAtual['endereco']){
            $result[] = ' Endereço Anterior: '.$endAnterior->endereco;
        }
        if ($endAnterior->bairro <> $endAtual['bairro']){
            $result[] = ' Bairro Anterior: '.$endAnterior->bairro;
        }
        if ($endAnterior->numero <> $endAtual['numero']){
            $result[] = ' Numero Anterior: '.$endAnterior->numero;
        }
        if ($endAnterior->cidades_id <> $endAtual['cidades_id']){
            $result[] = ' Cidade Anterior: '.$endAnterior->cidades->nome;
        }
        if($result){
            $this->eventosRepository->createEventos($endAnterior->id, implode(";",$result), 107);
        }
        return;
    }
}
