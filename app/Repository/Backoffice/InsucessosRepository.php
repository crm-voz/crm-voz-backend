<?php

namespace App\Repository\Backoffice;

use App\Models\BackOffice\Telefone;
use App\Models\Core\LogTelefone;
use Illuminate\Support\Facades\DB;
use PhpOffice\PhpSpreadsheet\IOFactory;

class InsucessosRepository extends BaseRepository{

    protected $modelTelefone;
    public function __construct(LogTelefone $model, Telefone $tel)
    {
       parent::__construct($model);
       $this->modelTelefone = $tel;
    }

    public function importar($file){
        try {
            $spreadsheet = IOFactory::load($file);
            $tab_arr = $spreadsheet->getActiveSheet()->toArray();
            // Retirar o cabeçalho
            array_splice($tab_arr, 0, 1);
            foreach($tab_arr as $reg){
                $telefone = $this->modelTelefone->where('telefone',intval($reg[6]))->get();
                try {
                    if($telefone <> '[]')
                    {
                        $this->model->create([
                            'telefone_id' => $telefone[0]->id,
                            'observacao'  => json_encode([
                                'call_id' => $reg[0],
                                'data' => $reg[1],
                                'hora' => $reg[2],
                                'chave' => $reg[3],
                                'nome' => $reg[4],
                                'cpf' => $reg[5],
                                'fone' => intval($reg[6]),
                                'resultado' => $reg[7],
                                'status' => $reg[8],
                                'status_complemento' => $reg[9],
                                'nome_operador' =>  $reg[10],
                                'duracao' =>  $reg[11],
                            ])
                        ]);
                    }

                } catch (\Exception $e) {
                    throw $e;
                }
            }
            return true;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function listagem($id){
        $result = $this->model->select('log_telefone.*')
                    ->join('telefones', 'telefones.id', '=', 'log_telefone.telefone_id')
                    ->join('cliente', 'cliente.id', '=', 'telefones.cliente_id')
                    ->where('cliente.id',$id)->get();

        $final =collect();
        foreach ($result as $value) {
            $final->add([
                'codigo' => 1,
                'descricao' => json_decode($value->observacao),
                'datahora' => $value->created_at,
                'executivo' => 'CRM VOZ',
                'motivo' => 'ações'
            ]);
        }
        return $final;
    }

}
