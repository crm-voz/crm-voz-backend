<?php

namespace App\Repository\Backoffice;

use App\Models\Cobranca\Acordo;
use App\Models\Cobranca\ExecutivoCobranca;
use App\Models\Core\Funcionario;
use Carbon\Carbon;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class ExecutivoCobrancaRepository extends BaseRepository{

    public function __construct(ExecutivoCobranca $model,
                                Acordo $acordo)
    {
        parent::__construct($model);
        $this->acordo = $acordo;
    }

    public function listarSupervisores(){
        $supervisores = collect();
        $funcionarios = Funcionario::where('funcao_id', env('SUPERVISOR_COBRANCA',15))->get();
        foreach($funcionarios as $funcionario){
            $supervisores->add((object)[
                 'id' => $funcionario->id,
                 'nome' => $funcionario->nome
            ]);
        }
        return $supervisores;

    }

    public function ListarExecutivos(){
        $executivos = collect();
        foreach($this->model->all() as $executivo){
            $executivos->add((object)[
                'id' => $executivo->id,
                'nome' => $executivo->funcionarios->nome,
                'meta' => $executivo->meta
            ]);
        }
        return $executivos;
    }

    public function acordos($executivoId)
    {
        return $this->acordo->where('executivo_cobranca_id', $executivoId)->get();
    }


    public function acordosDoMes($executivoId)
    {
        $mes = Carbon::now()->format('m');
        return $this->acordo->select('acordo.*','cliente.nome as nome_cliente','credores.nome as nome_credor')
                    ->join('cliente','cliente.id','=','acordo.cliente_id')
                    ->join('operacoes','operacoes.cliente_id','=','acordo.cliente_id')
                    ->join('remessas','remessas.id','=','operacoes.remessa_id')
                    ->join('credores','credores.id','=','remessas.credor_id')
                    ->where('executivo_cobranca_id',$executivoId)
                    ->whereMonth('acordo.created_at',$mes)
                    ->get();
    }

    public function parcelasAcordoPorExecutivo(int $executivoId, String $status){
        $mes = Carbon::now()->format('m');
        $parcelas = $this->acordo->where('executivo_cobranca_id', $executivoId)
                                ->whereMonth('created_at', $mes)
                                ->whereHas('parcelas', function($query)use($status){
                                    $query->where('status',$status);
                                })->get();
        $lista = collect();
        foreach($parcelas as $parcela){
            $lista->add([
                'id' => $parcela->id,
                'valor_nominal' => $parcela->valor_nominal,
                'valor_baixado' => $parcela->valor_baixado,
                'status' => $parcela->status
            ]);
        }

        return $lista;
    }

    public function rankExecutivo($data)
    {
        try {
            $rank = ExecutivoCobranca::select('executivo_cobranca.id','funcionarios.nome',DB::raw('sum(acordo.honorarios)as valor_total'),'users.url_perfil')
                                       ->join('funcionarios','funcionarios.id','=','executivo_cobranca.funcionarios_id')
                                       ->join('users','users.cpf','=','funcionarios.cpf')
                                       ->join('acordo','acordo.executivo_cobranca_id','=','executivo_cobranca.id')
                                       ->whereBetween('acordo.created_at',[$data->data_inicial.' 00:00:00',$data->data_final.' 23:59:59'])
                                       ->groupBy('funcionarios.nome','executivo_cobranca.id','users.url_perfil')
                                       ->orderByDesc('valor_total')
                                       ->get();
            $i = 0;
            foreach ($rank as $value) {
                $rank[$i++]->valor_total = floatval($value->valor_total);
            }
            return $rank;
        } catch (\Throwable $th) {
            throw $th;
        }
    }
}
