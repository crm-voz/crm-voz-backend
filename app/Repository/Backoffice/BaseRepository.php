<?php

namespace App\Repository\Backoffice;
use Illuminate\Database\Eloquent\Model;

class BaseRepository {
    protected const PER_PAGE = 50;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    public function where($field,$op,$value){
        return $this->model->where($field,$op,$value)->paginate(self::PER_PAGE);
    }

    public function find($id){
        return $this->model->find($id);
    }

    public function create(array $dados){
        return $this->model->create($dados);
    }

    public function update(array $dados, int $id){
        if(!$obj = $this->find($id)){
            return false;
        }
        return $obj->update($dados);
    }

    public function delete($id){
        if(!$obj = $this->find($id)){
            return false;
        }
        return $obj->delete();
    }

    public function all(){
        return $this->model->all();
    }

    public function whereIn($campo, array $ids){
        return $this->model->whereIn($campo, $ids)->get();
    }

}
