<?php

namespace App\Repository\Backoffice;
use App\Models\BackOffice\Operacao;
use App\Models\BackOffice\OperacaoDevolucao;
use App\Models\Core\LogPendencias;
use Illuminate\Support\Facades\DB;

class OperacaoRepository extends BaseRepository{

    public function __construct(Operacao $operacao,
                                EventosRepository $eventosRepository,
                                OperacaoDevolucao $opDevolucao,
                                MotivoPendenciaRepository $motivoPendenciaRepository)
    {
       parent::__construct($operacao);
        $this->eventosRepository = $eventosRepository;
        $this->modelOpDevolucao = $opDevolucao;
        $this->motivoPendenciaRepository = $motivoPendenciaRepository;
    }

    public function getIndex(){
        return $this->model->with('motivo_pendencia','status_operacao','cliente','remessa.credor','usuario_empresas')
                           ->limit(100)->paginate(100);
    }

    public function get($idOperacao = null)
    {
        return $this->model->where(function($query) use($idOperacao){
            if ($idOperacao){
                $query->where('id', $idOperacao);
            }
        })->get();
    }

    //todo: Refatorar este metodo.
    //Nonilton Alves
    public function getOperacoes($idCliente = null)
    {
        return $this->model->where(function($query) use($idCliente){
                                        if ($idCliente){
                                            $query->where('cliente_id', $idCliente);
                                        }
                                    })
                            ->join('remessas', 'operacoes.remessa_id' ,'=', 'remessas.id')
                            ->select('operacoes.*', 'remessas.numero_remessa', 'remessas.data_remessa')
                            ->get();
    }

    public function devolverOperacoes($dados)
    {
        foreach($dados as $id) {
           $operacao = $this->model->find($id);
            if ($operacao->status_operacao_id == 1){
                $operacao->status_operacao_id = 3;
                $operacao->save(); //status devolvido
            }
        }
    }

    public function cancelarDevolucaoOperacao($dados){
        try {
            foreach($dados as $id) {
                $operacao = $this->model->find($id);
                $operacao->status_operacao_id = 1;
                $operacao->save(); //status cobranca
            }
            return true;
        } catch (\Throwable $th) {
            return false;
        }
    }

    /*
    * função: getoperacao -> retorna um objeto do tipo Operacao
    * parametro:  numero_operacao -> numero da operação que vem na planilha enviada pelo credor.
    * parametro: idCredor -> id do credor no sistema CRMVoz
    */
    public function getOperacao($numero_operacao, $idCredor)
    {
        return $this->model->where('numero_operacao', $numero_operacao)
                           ->whereHas('remessa', function ($query) use($idCredor) {
                                $query->where('credor_id', $idCredor);
                           })->first();
    }

    public function filtrar($filtro_arr)
    {
        $operacoes = $this->model->query();
        if(array_key_exists('vencimento', $filtro_arr)) {
            $operacoes->whereBetween('data_vencimento', [$filtro_arr['vencimento']['data_inicial'],$filtro_arr['vencimento']['data_final']]);
        }

        if(array_key_exists('credores', $filtro_arr)) {
            $operacoes->whereHas('remessa', function($query) use($filtro_arr){
                $query->whereIn('credor_id', $filtro_arr['credores']);
            });
        }

        if(array_key_exists('status_operacao', $filtro_arr)) {
            $op  =  $filtro_arr['status_operacao']['exceto'] == 0 ? '=' : '!=';
            $operacoes->whereHas('status_operacao', function ($query) use($filtro_arr, $op) {
                $query->where('id', $op, $filtro_arr['status_operacao']['valor']);
            });
        }

        if(array_key_exists('motivo_pendencia', $filtro_arr)) {
            $op  =  $filtro_arr['motivo_pendencia']['exceto'] == 0 ? '=' : '!=';
            $operacoes->whereHas('motivo_pendencia', function ($query) use($filtro_arr, $op) {
                $query->whereIn('id', $filtro_arr['motivo_pendencia']['valor']);
            });
        }

        if(array_key_exists('periodo_contato', $filtro_arr)) {
            $operacoes->whereHas('cliente', function($query) use ($filtro_arr) {
                if (array_key_exists('exceto',$filtro_arr['periodo_contato'])) {
                    if ($filtro_arr['periodo_contato']['exceto'] == 0) {
                        $query->whereBetween('ultimo_contato', [$filtro_arr['periodo_contato']['data_inicial'],$filtro_arr['periodo_contato']['data_final']]);
                    }else{
                        $query->whereNotBetween(DB::raw("coalesce(cliente.ultimo_contato,'1992-03-29 07:00:00')"), [$filtro_arr['periodo_contato']['data_inicial'],$filtro_arr['periodo_contato']['data_final']]);
                    }
                }

                if(array_key_exists('classificacao_cliente', $filtro_arr)) {
                    if($filtro_arr['classificacao_cliente']['exceto'] == 0) {
                        $query->whereIn('status_cliente_id->title', $filtro_arr['classificacao_cliente']['valor']);
                    } else {
                        $query->whereNotIn('status_cliente_id->title', $filtro_arr['classificacao_cliente']['valor']);
                    }
                }
            });
        }

        if(array_key_exists('classificacao_negativacao', $filtro_arr)){
            $op  =  $filtro_arr['classificacao_negativacao']['exceto'] == 0 ? '=' : '!=';
            $operacoes->whereHas('classificacao_negativacao', function($query) use($filtro_arr, $op) {
                $query->where('id', $op, $filtro_arr['classificacao_negativacao']['valor']);
            });
        }

        if(array_key_exists('valor', $filtro_arr)) {
            $tipo           = $filtro_arr['valor']['tipo'];
            $valor_inicial  = array_key_exists('valor_inicial', $filtro_arr['valor']) ? $filtro_arr['valor']['valor_inicial'] : null;
            $valor_final    = array_key_exists('valor_final', $filtro_arr['valor']) ? $filtro_arr['valor']['valor_final'] : null;

            switch ($tipo) {
                // Valor da operação
                case 1:
                    $operacoes->where(function($query) use($valor_inicial, $valor_final) {
                        if($valor_inicial) {
                            $query->where('valor_nominal', '>=', $valor_inicial);
                        }
                        if($valor_final) {
                            $query->where('valor_nominal', '<=', $valor_final);
                        }
                    });
                    break;

                // Valor ticket médio do cliente
                case 2:
                    # code...
                    break;

                // Valor ticket médio da operação
                case 3:
                    # code...
                    break;
            }
        }
        return  $operacoes->with('motivo_pendencia');
    }

    public function motivo_pendencia_chart()
    {
        try {
            $ids = array(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,37,38,39);
            $operacao = $this->model->select(DB::raw('count(operacoes.*) as quantidade, motivo_pendencia.nome'))
                                    ->join('motivo_pendencia', 'motivo_pendencia.id', '=', 'operacoes.motivo_pendencia_id')
                                    ->whereIn('operacoes.status_operacao_id',$ids)
                                    ->groupBy('motivo_pendencia.nome')
                                    ->get();

            $total = 0;
            for ($i=0; $i < $operacao->count(); $i++) {
                $total = $total + $operacao[$i]->quantidade;
            }
            for ($i=0; $i < $operacao->count(); $i++) {
                $operacao[$i] = ['quantidade' => $operacao[$i]->quantidade,'nome' => $operacao[$i]->nome, 'indice' => $operacao[$i]->quantidade*100/$total];
            }
        } catch (\Throwable $th) {
            return $th;
        }

        return $operacao;
    }

    public function motivo_devolucao_chart()
    {
        try {
            $devolucao = $this->modelOpDevolucao->select(DB::raw('count(motivo_devolucao.*) as quantidade, motivo_devolucao.nome'))
            ->join('devolucao', 'devolucao.id', '=', 'operacao_devolucao.devolucao_id')
            ->join('motivo_devolucao', 'motivo_devolucao.id', '=', 'devolucao.motivo_devolucao_id')
            ->groupBy('motivo_devolucao.nome')
            ->get();

            $total = 0;
            for ($i=0; $i < $devolucao->count(); $i++) {
                $total = $total + $devolucao[$i]->quantidade;
            }
            for ($i=0; $i < $devolucao->count(); $i++) {
                $devolucao[$i] = ['quantidade' => $devolucao[$i]->quantidade,'nome' => $devolucao[$i]->nome, 'indice' => $devolucao[$i]->quantidade*100/$total];
            }
        } catch (\Throwable $th) {
            return $th;
        }

        return $devolucao;
    }

    public function status_operacao_chart()
    {
        try {
            $operacao = $this->model->select(DB::raw('count(operacoes.*) as quantidade, status_operacao.nome'))
            ->join('status_operacao', 'status_operacao.id', '=', 'operacoes.status_operacao_id')
            ->whereIn('operacoes.status_operacao_id',[1,2,3,4,5])
            ->groupBy('status_operacao.nome')
            ->get();

            $total = 0;
            for ($i=0; $i < $operacao->count(); $i++) {
                $total = $total + $operacao[$i]->quantidade;
            }

            $operacao[0] = ['quantidade' => $operacao[0]->quantidade,'nome' => $operacao[0]->nome, 'indice' => $operacao[0]->quantidade*100/$total];
            $operacao[1] = ['quantidade' => $operacao[1]->quantidade,'nome' => $operacao[1]->nome, 'indice' => $operacao[1]->quantidade*100/$total];
            $operacao[2] = ['quantidade' => $operacao[2]->quantidade,'nome' => $operacao[2]->nome, 'indice' => $operacao[2]->quantidade*100/$total];
            $operacao[3] = ['quantidade' => $operacao[3]->quantidade,'nome' => $operacao[3]->nome, 'indice' => $operacao[3]->quantidade*100/$total];
            $operacao[4] = ['quantidade' => $operacao[4]->quantidade,'nome' => $operacao[4]->nome, 'indice' => $operacao[4]->quantidade*100/$total];
            // $operacao[5] = ['quantidade' => $total,'nome' => 'total'];
        } catch (\Throwable $th) {
            return $th;
        }

        return $operacao;
    }

    public function pendencia_chart()
    {
        try {

            $comPendencia = $this->model->select(DB::raw('count(operacoes.*) as quantidade'))
            ->join('motivo_pendencia', 'motivo_pendencia.id', '=', 'operacoes.motivo_pendencia_id')
            ->whereIn('operacoes.motivo_pendencia_id',[1,2,3,5,6,7,8])
            ->get();

            $semPendencia = $this->model->select(DB::raw('count(operacoes.*) as quantidade'))
            ->join('motivo_pendencia', 'motivo_pendencia.id', '=', 'operacoes.motivo_pendencia_id')
            ->whereIn('operacoes.motivo_pendencia_id',[4,9,10])
            ->get();


            $total = $comPendencia[0]->quantidade + $semPendencia[0]->quantidade;
            $comPendencia[0] = ['nome' => 'Com Pendencia' ,'quantidade' => $comPendencia[0]->quantidade,  'indice' => $comPendencia[0]->quantidade*100/$total];
            $semPendencia[0] = ['nome' => 'Sem Pendencia' ,'quantidade' => $semPendencia[0]->quantidade,  'indice' => $semPendencia[0]->quantidade*100/$total];
            $geral[0] = $comPendencia[0];
            $geral[1] = $semPendencia[0];

            $subtotal = ['nome' => 'total', 'quantidade' => $total, 'indice' =>  $total/$total*100];
            $result = ['dados' => $geral]+['total' => $subtotal]+['mensagem' => 'Operação executada com sucesso.'];

        } catch (\Throwable $th) {
            return $th;
        }

        return $result;
    }

    public function valor_nominal_ano_vencimento_chart()
    {
        try {
            $result = $this->model->select(DB::raw("to_char(operacoes.data_vencimento, 'YYYY') AS ano_vencimento,
                                                count(distinct operacoes.cliente_id) as total_cliente,
                                                count(operacoes.id) as total_operacao,
                                                sum(operacoes.valor_nominal) as valor_nominal"))
            ->groupBy('ano_vencimento')
            ->orderBy('ano_vencimento')
            ->get();
        } catch (\Throwable $th) {
            return $th;
        }

        return $result;
    }

    public function titulo_aberto_periodo_chart($dados)
    {
        try {
            $titulos = $this->model->select(DB::raw('count(*) as total_titulos_abertos'))
                ->whereBetween('operacoes.data_processamento',[$dados->data_inicial,$dados->data_final])
                ->where('operacoes.status_operacao_id','=',1)->get();
        } catch (\Throwable $th) {
            return $th;
        }
        return $titulos;
    }

    public function negativados()
    {
        try {
            $negativados = $this->model
                ->select(DB::raw('distinct cliente.id, cliente.nome, cliente.cpf_cnpj,
                    cliente.endereco, cliente.endereco, classificacao_negativacao.nome as negativacao,
                    credores.nome as nome_credor'))
                ->join('cliente', 'cliente.id', '=', 'operacoes.cliente_id')
                ->join('classificacao_negativacao', 'classificacao_negativacao.id', '=', 'operacoes.classificacao_negativacao_id')
                ->join('remessas', 'remessas.id', '=', 'operacoes.remessa_id')
                ->join('credores', 'credores.id', '=', 'remessas.credor_id')
                ->whereNotNull('operacoes.classificacao_negativacao_id')
                ->get();
        } catch (\Throwable $th) {
            // return $th;
        }
        return $negativados;
    }

    public function operacoes_negativados_cliente($id)
    {
        try {
            $op = $this->model
                ->select('operacoes.*','classificacao_negativacao.nome as negativacao')
                ->join('cliente', 'cliente.id', '=', 'operacoes.cliente_id')
                ->join('classificacao_negativacao', 'classificacao_negativacao.id', '=', 'operacoes.classificacao_negativacao_id')
                ->whereNotNull('operacoes.classificacao_negativacao_id')
                ->where('operacoes.cliente_id',$id)
                ->orderBy('operacoes.id')
                ->get();
        } catch (\Throwable $th) {
            return $th;
        }
        return $op;
    }

    protected function criarEventoNosClientes($clientes, $description, $motivoId){
        foreach($clientes->unique() as $cliente){
            $this->eventosRepository->createEventos($cliente->id,$description, $motivoId);
         }
    }

    public function updateLoteStatusOperacao($ids, $statusOperacaoId, $description, $motivoId)
    {
        $operacoes = $this->model->whereIn('id', $ids)->get();
        DB::beginTransaction();
        try {
            $clientes = collect();
            foreach($operacoes as $operacao){
                $clientes->add($operacao->cliente);
            }
            $count = $this->model->whereIn('id', $ids)->update(['status_operacao_id' => $statusOperacaoId]);
            $this->criarEventoNosClientes($clientes, $description, $motivoId);
            DB::commit();
            return $count;
        } catch (\Exception $e) {
            DB::rollBack();
            return 0;
        }
    }

    public function updateLoteMotivoPendencia($ids, $motivoPendenciaId, $description, $motivoId){
        $operacoes = $this->model->whereIn('id', $ids)->get();
        DB::beginTransaction();
        try {
            $clientes = collect();
            foreach($operacoes as $operacao){
                $clientes->add($operacao->cliente);
            }
            $str = '';
            foreach($operacoes as $operacao){
                $str = $str . $operacao->numero_operacao .' - ' . $operacao->data_vencimento->format('d/m/Y') . '; ';
            }

            if($motivoPendenciaId){
                $motivoPendencia = $this->motivoPendenciaRepository->find($motivoPendenciaId);
                $message = $motivoPendencia->nome . '<br>' . $description . '<br> <strong>OPERAÇÕES: ' . $str . '</strong>';
            }else{
                $message =  'REMOÇÃO DE PENDÊNCIA <br>' . $description . '<br> <strong>OPERAÇÕES: ' . $str . '</strong>';
            }

            $count = $this->model->whereIn('id', $ids)->update(['motivo_pendencia_id' => $motivoPendenciaId]);
            $this->criarEventoNosClientes($clientes, $message, $motivoId);
            DB::commit();
            return $count;
        } catch (\Exception $e) {
            dd($e->getMessage());
            DB::rollBack();
            return 0;
        }
    }

    public function updateLoteNegativadoOperacao($ids, $motivoPendenciaId, $description, $motivoId){
        $operacoes = $this->model->whereIn('id', $ids)->get();
        DB::beginTransaction();
        try {
            $clientes = collect();
            foreach($operacoes as $operacao){
                $clientes->add($operacao->cliente);
            }
            $count = $this->model->whereIn('id', $ids)->update(['classificacao_negativacao_id' => $motivoPendenciaId]);
            $this->criarEventoNosClientes($clientes, $description, $motivoId);
            DB::commit();
            return $count;
        } catch (\Exception $e) {
            DB::rollBack();
            return 0;
        }
    }

    public function listarOperacoesPendente(){
        try {
            // $logPendencia = LogPendencias::
            // select(DB::raw('cliente.nome as cliente, cliente.cpf_cnpj, operacoes.id as operacoes_id,
            //                 operacoes.numero_operacao, operacoes.data_vencimento,
            //                 operacoes.valor_nominal, operacoes.aluno, motivo_pendencia.nome as motivo_pendencia, credores.nome as credor,
            //                 log_pendencias.status, MAX(log_pendencias.created_at) created_at, users.name as usuario'))
            // ->join('operacoes', 'operacoes.id', '=', 'log_pendencias.operacao_id')
            // ->join('cliente', 'cliente.id', '=', 'operacoes.cliente_id')
            // ->join('remessas', 'remessas.id', '=', 'operacoes.remessa_id')
            // ->join('credores', 'credores.id', '=', 'remessas.credor_id')
            // ->join('motivo_pendencia','motivo_pendencia.id','=','operacoes.motivo_pendencia_id')
            // ->leftJoin('usuario_empresas','usuario_empresas.id','=','log_pendencias.usuario_empresa_id')
            // ->leftJoin('users','users.id','=','usuario_empresas.user_id')
            // ->whereNotNull('operacoes.motivo_pendencia_id')
            // ->whereIn('credores.id',[177,178,180])
            // ->distinct('log_pendencias.operacao_id')
            // ->groupBy(DB::raw('log_pendencias.operacao_id, log_pendencias.status, cliente.nome,
            //                    cliente.cpf_cnpj, operacoes_id, operacoes.numero_operacao, operacoes.data_vencimento,
            //                    operacoes.valor_nominal, motivo_pendencia.nome, credor, usuario'))
            // ->get();

            $logPendencia = DB::select('with dados as (select cliente.nome as cliente, cliente.cpf_cnpj,
                operacoes.id as operacoes_id, operacoes.numero_operacao, operacoes.data_vencimento,
                operacoes.valor_nominal, operacoes.aluno, motivo_pendencia.nome as motivo_pendencia, credores.nome as credor,
                log_pendencias.status, max(log_pendencias.created_at) as created_at, users.name as usuario
                FROM log_pendencias
                inner join operacoes on operacoes.id = log_pendencias.operacao_id
                inner join cliente on cliente.id = operacoes.cliente_id
                inner join remessas on remessas.id=operacoes.remessa_id
                inner join credores on credores.id=remessas.credor_id
                left join motivo_pendencia on motivo_pendencia.id = operacoes.motivo_pendencia_id
                left join usuario_empresas on usuario_empresas.id = log_pendencias.usuario_empresa_id
                left join users on users.id = usuario_empresas.user_id
                where credores.id in (177,178,180)
                GROUP BY operacoes.id, log_pendencias.status, cliente.nome,
                cliente.cpf_cnpj, operacoes_id, operacoes.numero_operacao, operacoes.data_vencimento,
                operacoes.valor_nominal, motivo_pendencia.nome, credores.nome, usuario, operacoes.numero_operacao, log_pendencias.created_at
                order by operacoes.numero_operacao,log_pendencias.created_at  desc
            ) select distinct on (dados.numero_operacao) * from dados');

            return $logPendencia;

        } catch (\Throwable $th) {
            throw $th;
        }
    }

}
