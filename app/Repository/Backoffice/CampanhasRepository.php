<?php

namespace App\Repository\Backoffice;

use App\Models\BackOffice\Campanha;
use App\Models\BackOffice\Cliente;
use App\Models\BackOffice\ClienteCampanha;
use App\Models\Cobranca\Acordo;
use App\Models\Cobranca\Parcela;
use App\Repository\Cobranca\AcordoRepository;
use App\Repository\Cobranca\ParcelaRepository;
use App\Services\CampanhasServices;
use App\Services\GerarListaServices;
use Illuminate\Support\Facades\DB;

class CampanhasRepository extends BaseRepository
{
    public function __construct(Campanha $campanha,
                                OperacaoRepository $operacaoRepository,
                                AcordoRepository $acordoRepository,
                                ParcelaRepository $parcelaRepository)
    {
        parent::__construct($campanha);
        $this->operacaoRepository = $operacaoRepository;
        $this->acordoRepository = $acordoRepository;
        $this->parcelaRepository = $parcelaRepository;
    }

    //TODO: verificar metodo campanhasAtivas, acredito estar errado
    public function campanhasAtivas(){
        return $this->model->whereDate('data_inicio_campanha', '<=', now())
                           ->whereDate('data_final_campanha', '>=', now())
                           ->get();
        //return $this->model->where('id',4)->get();
    }

    public function detalharCampanha($campanha){
        $service = app(CampanhasServices::class);
        return $service->campanha($campanha);
    }

    public function listarCampanhas(){
        $campanhas = $this->model->all();
        $lista = collect();
        $service = app(CampanhasServices::class);
        foreach($campanhas as $campanha){
            $lista->add($service->campanha($campanha));
        }
        return $lista;
    }

    public function get($id) {
        return $this->model->where(function($query) use($id){
            if ($id){
                $query->find($id);
            }
        })->get();
    }

    //** Estou esperando o ID da campanhaCliente gerado pela CRON */
    public function gerar_lista($id, $extensao) {
        $service = app(GerarListaServices::class);
        //executa novamente os filtros
        //remove operacoes com pendencias
        //cadastra clientes que não fazem parte da campanha
        $campanha = Campanha::find($id);
        $this->criarClientesCampanha($campanha);

        //gera lista apenas de clientes que não tem acordo fechado na campanha.
        return $service->gerarLista($id, $extensao);
    }

    public function totalCampanhaCredor(){
        return $this->campanhasCredor()->count();
    }

    public function campanhasCredor(){
        return  $this->model->join('acordo', 'acordo.campanhas_id', '=', 'campanhas.id')
                            ->join('operacao_acordo', 'operacao_acordo.acordos_id', '=', 'acordo.id')
                            ->join('operacoes', 'operacoes.id', '=', 'operacao_acordo.operacoes_id')
                            ->join('remessas', 'remessas.id', '=', 'operacoes.remessa_id')
                            ->where('remessas.credor_id',auth()->user()->credor_id)
                            ->select('campanhas.*')
                            ->distinct('campanhas.id')
                            ->get();
    }

    public function operacoesCampanha(Campanha $campanha){
        $operacaoRepository = app(OperacaoRepository::class);
        $filtro = json_decode($campanha->filtro, 'true');
        return $operacaoRepository->filtrar(  $filtro  )->get();
    }

    public function quantidadeOperacoesCampanha(Campanha $campanha){
        return $this->operacoesCampanha($campanha)->count();
    }

    public function quantidadeAcordosCampanha(Campanha $campanha){
        return $this->listarAcordosCampanhas($campanha)->count();
    }


    public function listarAcordosCampanhas(Campanha $campanha){
        $acordo = Acordo::where('campanhas_id', $campanha->id)->get();
        foreach ($acordo as $value) {
            $value->cliente->status_cliente_id = json_decode($value->cliente->status_cliente_id);
            $value->executivo_cobranca->funcionarios_id = DB::table('funcionarios')->where('id',$value->executivo_cobranca->funcionarios_id)->first('nome');
        }
        return $acordo;
    }

    public function clientesCampanha(Campanha $campanha){
        return $campanha->clientes;
    }

    public function faturamentoCampanha(Campanha $campanha){
        $acordosId = Acordo::where('campanhas_id', $campanha->id)
                            ->where('status','Negociado')->orWhere('status','Liquidado')
                            ->get()->pluck('id');

       $faturamento = 0;
       Parcela::where(function ($q) use($acordosId) {
                        $q->whereIn('acordos_id', $acordosId);
                })
                ->whereNotNull('valor_baixado')
                ->whereNotNull('data_baixa')
                ->chunk(300, function($parcelas) use($faturamento){
                        foreach($parcelas as $parcela){
                            $faturamento += $parcela->valor_baixado;
                        }
                });
        return $faturamento;
    }

    public function criarClientesCampanha($campanha)
    {
        $filtro = json_decode($campanha->filtro, 'true');
        if ($campanha->tipo_filtro == "operacao"){
            $operacoes = $this->operacaoRepository->filtrar(  $filtro  )->get();
            foreach($operacoes as $operacao){
                if ($operacao->status_operacao_id == 1){
                    if (!$operacao->motivo_pendencia_id){
                        $clienteCampanha = app(ClienteCampanha::class);
                        $clienteCampanha->criarCampanha($campanha->id,$operacao->cliente->id);
                    }
                }
            }
        }

        if ($campanha->tipo_filtro == "acordo"){
            $acordos = $this->acordoRepository->filtrar(  $filtro  );
            foreach($acordos as $acordo){
                $acordo->operacoes_acordo->each(function($item) use($campanha){
                    $clienteCampanha = app(ClienteCampanha::class);
                    $clienteCampanha->criarCampanha($campanha->id, $item->operacao->cliente->id);
                });
            }
        }

        if ($campanha->tipo_filtro == "parcela"){
          $parcelas = $this->parcelaRepository->filtrar(  $filtro  );
          $acordos = collect();
          $parcelas->each(function($item)use($acordos){
             $acordos->add($item->acordos);
          });
          $acordos = $acordos->unique();
          $acordos->each(function($item) use($campanha){
            $operacoesAcordo = $item->operacoes_acordo;
            $operacoesAcordo->each(function($item) use($campanha){
                $clienteCampanha = app(ClienteCampanha::class);
                $clienteCampanha->criarCampanha($campanha->id, $item->operacao->cliente->id);
             });
          });
        }
        return;
    }

    public function totalizadores($filtro_arr){
        return Cliente::select(DB::raw('count(DISTINCT cliente.nome) as clientes'),
                               DB::raw('count(DISTINCT operacoes.numero_operacao) as operacoes'),
                               DB::raw('sum(DISTINCT operacoes.valor_nominal) as valor_nominal'),
                               DB::raw('count(DISTINCT telefones.id) as telefones'),
                               DB::raw('count(DISTINCT emails.id) as emails')
                              )
                        ->join('operacoes', 'operacoes.cliente_id', '=', 'cliente.id')
                        ->join('remessas', 'remessas.id', '=', 'operacoes.remessa_id')
                        ->join('credores', 'credores.id', '=', 'remessas.credor_id')
                        ->leftJoin('eventos', 'eventos.cliente_id', '=', 'cliente.id')
                        ->leftJoin('cidades', 'cidades.id', '=', 'cliente.cidades_id')
                        ->leftJoin('status_localizacao', 'status_localizacao.id', '=', 'cliente.status_localizacao_id')
                        ->leftJoin('classificacao_negativacao', 'classificacao_negativacao.id', '=', 'operacoes.classificacao_negativacao_id')
                        ->leftJoin('acordo', 'acordo.cliente_id', '=', 'cliente.id')
                        ->leftJoin('parcelas', 'parcelas.acordos_id', '=', 'acordo.id')
                        ->leftJoin('telefones', 'telefones.cliente_id', '=', 'cliente.id')
                        ->leftJoin('emails', 'emails.cliente_id', '=', 'cliente.id')
                        ->where('operacoes.status_operacao_id',1)
                        ->whereNull('operacoes.motivo_pendencia_id')
                        ->where(function($query) use($filtro_arr){
                            if(!empty($filtro_arr['filtros']['credores'])){
                                $query->whereIn('credores.id', $filtro_arr['filtros']['credores']);
                            }
                            switch(isset($filtro_arr['filtros']['tipo_base'])) {
                                case 'operacao':
                                    if(isset($filtro_arr['filtros']['data_inicial']) && (isset($filtro_arr['filtros']['data_final']))){
                                        $query->whereBetween('operacoes.data_vencimento',[$filtro_arr['filtros']['data_inicial'],$filtro_arr['filtros']['data_final']]);
                                    }
                                    break;
                                case 'parcela':
                                    if(isset($filtro_arr['filtros']['data_inicial']) && (isset($filtro_arr['filtros']['data_final']))){
                                        $query->whereBetween('parcelas.data_vencimento',[$filtro_arr['filtros']['data_inicial'],$filtro_arr['filtros']['data_final']]);
                                    }
                                    break;
                                case 'acordo':
                                    if(isset($filtro_arr['filtros']['data_inicial']) && (isset($filtro_arr['filtros']['data_final']))){
                                        $query->whereBetween('acordo.created_at',[$filtro_arr['filtros']['data_inicial'].' 00:00:00 ',$filtro_arr['filtros']['data_final'].' 23:59:59']);
                                    }
                                    break;
                            }
                            if(!empty($filtro_arr['filtros']['status_operacao']['valor'])){
                                if ($filtro_arr['filtros']['status_operacao']['exceto'] == 0){
                                    $query->whereIn('operacoes.status_operacao_id', $filtro_arr['filtros']['status_operacao']['valor']);
                                }else{
                                    $query->whereNotIn(DB::raw("coalesce(operacoes.status_operacao_id,0)"), [$filtro_arr['filtros']['status_operacao']['valor']]);
                                }
                            }
                            if(!empty($filtro_arr['filtros']['motivo_pendencia']['valor'])){
                                if ($filtro_arr['filtros']['motivo_pendencia']['exceto'] == 0){
                                    $query->whereIn('operacoes.motivo_pendencia_id', $filtro_arr['filtros']['motivo_pendencia']['valor']);
                                }else{
                                    $query->whereNotIn(DB::raw("coalesce(operacoes.motivo_pendencia_id,0)"), [$filtro_arr['filtros']['motivo_pendencia']['valor']]);
                                }
                            }
                            if(!empty($filtro_arr['filtros']['motivo_evento']['valor'])){
                                if ($filtro_arr['filtros']['motivo_evento']['exceto'] == 0){
                                    $query->whereIn('eventos.motivo_evento_id', $filtro_arr['filtros']['motivo_evento']['valor']);
                                }else{
                                    $query->whereNotIn(DB::raw("coalesce(eventos.motivo_evento_id,0)"), [$filtro_arr['filtros']['motivo_evento']['valor']]);
                                }
                            }
                            if(!empty($filtro_arr['filtros']['classificacao_negativacao']['valor'])){
                                if ($filtro_arr['filtros']['classificacao_negativacao']['exceto'] == 0){
                                    $query->whereIn('operacoes.classificacao_negativacao_id', $filtro_arr['filtros']['classificacao_negativacao']['valor']);
                                }else{
                                    $query->whereNotIn(DB::raw("coalesce(operacoes.classificacao_negativacao_id,0)"), [$filtro_arr['filtros']['classificacao_negativacao']['valor']]);
                                }
                            }
                            if(!empty($filtro_arr['filtros']['classificacao_cliente']['valor'])){
                                if ($filtro_arr['filtros']['classificacao_cliente']['exceto'] == 0){
                                    $query->whereIn('cliente.status_cliente_id->id', $filtro_arr['filtros']['classificacao_cliente']['valor']);
                                }else{
                                    $query->whereNotIn(DB::raw("coalesce(cliente.status_cliente_id->>'id','null')"), [$filtro_arr['filtros']['classificacao_cliente']['valor']]);
                                }
                            }
                            if(!empty($filtro_arr['filtros']['localizacao']['valor'])){
                                if ($filtro_arr['filtros']['localizacao']['exceto'] == 0){
                                    $query->whereIn('cliente.localizacao', $filtro_arr['filtros']['localizacao']['valor']);
                                }else{
                                    $query->whereNotIn(DB::raw("coalesce(cliente.localizacao,'null')"), [$filtro_arr['filtros']['localizacao']['valor']]);
                                }
                            }
                            if(isset($filtro_arr['filtros']['periodo_contato']['data_inicial']) && (isset($filtro_arr['filtros']['periodo_contato']['data_final']))){
                                if ($filtro_arr['filtros']['periodo_contato']['exceto'] == 0){
                                    $query->whereBetween('cliente.ultimo_contato', [$filtro_arr['filtros']['periodo_contato']['data_inicial'], $filtro_arr['filtros']['periodo_contato']['data_final']]);
                                }else{
                                    $query->whereNotBetween(DB::raw("coalesce(cliente.ultimo_contato,'1992-03-29 07:00:00')"), [$filtro_arr['filtros']['periodo_contato']['data_inicial'], $filtro_arr['filtros']['periodo_contato']['data_final']]);
                                }
                            }
                        })
                        //->groupBy('credores.nome')
                        ->get();

    }

}
