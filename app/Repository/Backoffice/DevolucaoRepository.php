<?php

namespace App\Repository\Backoffice;

use App\Models\BackOffice\Devolucao;
use App\Models\BackOffice\Operacao;
use App\Models\BackOffice\OperacaoDevolucao;

class DevolucaoRepository extends BaseRepository{
    public function __construct(Devolucao $devolucao)
    {
       parent::__construct($devolucao);
    }

    public function get($id=null){
        return $this->model->where(function($query) use($id){
                                        if ($id){
                                            $query->find($id);
                                        }
                                    })
                            ->join('motivo_devolucao', 'motivo_devolucao.id', '=', 'devolucao.motivo_devolucao_id')
                            ->select('devolucao.id', 'devolucao.descricao', 'devolucao.created_at', 'devolucao.updated_at', 'motivo_devolucao.nome as motivo_devolucao')
                            ->get();
    }



    public function create(array $dados){
        $devolucoes = $dados['operacoes'];
        $dados['usuario_empresas_id'] = auth()->user()->usuario_empresas[0]->id;
        $devolucao = parent::create($dados);
        $ops = [];
        foreach($devolucoes as $id_d){
            $ops[] = OperacaoDevolucao::create([
                'operacao_id' => $id_d,
                'devolucao_id' => $devolucao->id,
            ]);
         }

        return ['devolucao' => $devolucao, 'operacoes' => $ops];
    }

    //TODO::REMOVER INSTRUCOES SELECT DESNECESSÁRIAS
    public function detalhe($id){
        $d = $this->model->find($id);

        $devolucao = $d ->join('motivo_devolucao', 'motivo_devolucao.id', '=', 'devolucao.motivo_devolucao_id')
                        ->select('devolucao.id', 'devolucao.descricao', 'devolucao.created_at', 'devolucao.updated_at', 'motivo_devolucao.nome as motivo_devolucao')
                        ->first();

        $operacoes = $d ->operacoes()
                        ->join('remessas', 'operacoes.remessa_id' ,'=', 'remessas.id')
                        ->select('operacoes.*', 'remessas.id as remessa_id', 'remessas.numero_remessa', 'remessas.data_remessa')
                        ->get();

        $usuario = $d   ->usuario_empresas()
                        ->join('users', 'usuario_empresas.user_id', '=', 'users.id')
                        ->select('users.id', 'users.name', 'users.email')
                        ->first();

        foreach($operacoes as $op){
            $op->cliente;
        }

        return [ 'devolucao' => $devolucao, 'operacoes' => ($operacoes) ? $operacoes : [], 'usuario' => $usuario ];

        //return $this->model->with(['operacoes','motivo_devolucao','usuario_empresas'])->find($id);
    }

    public function devolucoesDoCredor($credorId){
        return $this->model->with('operacoes')->whereHas('operacoes', function($query) use($credorId){
            $query->with('remessa')->whereHas('remessa', function($q) use($credorId){
                $q->where('credor_id',$credorId);
            });
        })->get();
    }

}
