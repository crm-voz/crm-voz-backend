<?php

namespace App\Services;

class ParcelasServices
 {
     public static function listarParcelas($parcelas){
        $listaParcelas = collect();
        foreach($parcelas as $parcela){
            $listaParcelas->add([
                'id' => $parcela->id,
                'valor_nominal' => $parcela->valor_nominal,
                'valor_previsto' => $parcela->valor_previsto,
                'data_vencimento' => $parcela->data_vencimento,
                'numero_parcela' => $parcela->numero_parcela,
                'data_baixa' => $parcela->data_baixa,
                'valor_baixado' => $parcela->valor_baixado,
                'valor_juros' => $parcela->valor_juros,
                'valor_honorario' => $parcela->valor_honorario,
                'forma_pagamento' => $parcela->forma_pagamento,
                'valor_multa' => $parcela->valor_multa
            ]);
        }
        return $listaParcelas;
     }


 }
