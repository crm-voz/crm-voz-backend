<?php

namespace App\Services;

use App\Models\BackOffice\Email;
use App\Models\BackOffice\Telefone;
use App\Repository\Backoffice\EventosRepository;

class ClienteServices {

    protected $eventosRepository;
    public function __construct(EventosRepository $e)
    {
        $this->eventosRepository = $e;
    }
    public function updateCliente($cliente, $pessoa)
    {


        $this->createTelefones($cliente, $pessoa);
        $this->createEmail($cliente, $pessoa);
        return;
    }

    protected function createTelefones($cliente, $pessoa)
    {
        $fones = [];
        foreach($pessoa['contato']['telefone'] as $telefone){
            $tel = Telefone::where('telefone', $telefone['ddd'] . $telefone['numero'])->first();
            $fones[] = 'Telefone: '.'('.$telefone['ddd'].') '. $telefone['numero'];
            if(!$tel){
                Telefone::create([
                    'telefone' =>$telefone['ddd'] . $telefone['numero'],
                    'cliente_id' => $cliente->id,
                    'status' => true,
                    'origem' => 'Enriquecimento',
                    'tags' => match($telefone['tipoTelefone']){
                        'Fixo' => 'Fixo',
                        'Celular' => 'Móvel',
                        default => 'Móvel'
                    }
                ]);
            }
        }
        $this->eventosRepository->createEventos($cliente->id, implode(" ",$fones), 116);
        return ;
    }

    protected function createEmail($cliente, $pessoa)
    {
        foreach($pessoa['contato']['email'] as $email){
            $mail = Email::where('email', $email['email'])->first();
            if(!$mail){
                Email::create([
                    'email' => $email['email'],
                    'cliente_id' => $cliente->id,
                ]);
            }
        }
        return ;
    }
}
