<?php

namespace App\Services;

use App\Repository\Backoffice\CampanhasRepository;

class CampanhasServices
{
    public function __construct(CampanhasRepository $campanhasRepository)
    {
        $this->campanhasRepository = $campanhasRepository;
    }

    public function campanha($campanha)
    {
        return [
            'id' => $campanha->id,
            'tipo_campanha' => $campanha->tipo_campanha,
            'data_inicio_campanha' => $campanha->data_inicio_campanha,
            'data_final_campanha' => $campanha->data_final_campanha,
            'respiro' => $campanha->respiro,
            'filtro' => json_decode( $campanha->filtro ),
            'usuario_empresas_id' => $campanha->usuario_empresas_id,
            'total_clientes' => $campanha->clientes->count(),
            'faturamento' => 0, //$this->campanhasRepository->faturamentoCampanha($campanha),
            'acordos' =>  0, //$this->campanhasRepository->quantidadeAcordosCampanha($campanha),
            'operacoes' => 0,//$this->campanhasRepository->quantidadeOperacoesCampanha($campanha),
        ];
    }
}
