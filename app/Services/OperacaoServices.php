<?php

namespace App\Services;

class OperacaoServices
{
    public function __construct($operacoes)
    {
        $this->operacoes = $operacoes;
    }

    public function getLista()
    {
        $lista = collect();
        foreach($this->operacoes as $operacao){
            $lista->add([
                'id' => $operacao->id,
                'status_operacao' => ['id' => $operacao->status_operacao->id, 'nome' => $operacao->status_operacao->nome, 'sigla' => $operacao->status_operacao->sigla],
                'operacao' => $operacao->numero_operacao,
                'motivo_pendencia' => $operacao->motivo_pendencia,
                'cliente' => $operacao->cliente->nome,
                'credor' => $operacao->remessa->credor->nome,
                'data_vencimento' => $operacao->data_vencimento,
                'data_processamento' => $operacao->data_processamento,
                'valor_nominal' => $operacao->valor_nominal,
                'valor_atualizado' => $operacao->valor_atualizado,
                'desconto' => $operacao->desconto,
                'descricao' => $operacao->descricao,
            ]);
        }

        return $lista;
    }
}
