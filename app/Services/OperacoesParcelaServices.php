<?php

namespace App\Services;

class OperacoesParcelaServices
 {
    public static function listar($operacoesParcelas)
    {
        $lista = collect();
        foreach($operacoesParcelas as $op){

            $lista->add([
                'operacao_id' => $op->operacao_id,
                'parcela_id' => $op->parcela_id,
                'instituicao' => $op->instituicao,
                'cliente' => $op->cliente,
                'aluno' => $op->aluno,
                'documento' => $op->documento,
                'numero_operacao' => $op->numero_operacao,
                'vencimento_original' => $op->vencimento_original,
                'vencimento' => $op->vencimento,
                'recebimento' => $op->recebimento,
                'parcela' => $op->parcela,
                'valor_negociado' => $op->valor_negociado,
                'valor_repasse' => $op->valor_repasse,
                'origem' => $op->origem,
                'principal' => $op->principal,
                'juros' => $op->juros,
                'multa' => $op->multa,
                'correcao' => $op->correcao,
                'desconto' => $op->desconto,
                'honorarios' => $op->honorarios,
                'realizado' => $op->realizado,
                'taxa_cartao' => $op->taxa_cartao,
                'comissao' => $op->comissao,
                'repasse' => $op->repasse,
                'forma' => $op->forma,
                'referencia' => $op->referencia,
                'numero_documento' => $op->numero_documento,
                'desconto_acordo' => $op->desconto_acordo,
                'credor_id' => $op->credor_id,
                'valor_juros_original' => $op->valor_juros_original,
                'valor_multa_original' => $op->valor_multa_original,
                'valor_honorario_original' => $op->valor_honorario_original,
                'valor_desconto_original' => $op->valor_desconto_original,
                'valor_comissao_original' => $op->valor_comissao_original,
                'situacao' => $op->parcelas->acordos->situacao,
                'numero_acordo' => $op->parcelas->acordos->id,
                'quantidade_parcelas' => $op->parcelas->acordos->quantidade_parcelas
            ]);
        }
        return $lista;
    }

 }
