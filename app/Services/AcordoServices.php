<?php

namespace App\Services;
use App\Services\ParcelasServices;
use App\Services\OperacoesAcordoServices;

class AcordoServices
{
    public static function acordos($acordos){
        $lista = collect();
        foreach($acordos as $acordo){
            $lista->add([
                'id' => $acordo->id,
                'numero_acordo' => $acordo->numero_acordo,
                'status' => $acordo->status,
                'valor_nominal' => $acordo->valor_nominal,
                'juros' => $acordo->juros,
                'multas' => $acordo->multas,
                'honorarios' => $acordo->honorarios,
                'desconto' => $acordo->desconto,
                'valor_entrada' => $acordo->valor_entrada,
                'quantidade_parcelas' => $acordo->quantidade_parcelas,
                'valor_parcela' => $acordo->valor_parcela,
                'data_cancelou' => $acordo->data_cancelou,
                'descricao_cancelamento' => $acordo->descricao_cancelamento,
                'transacao_cartao'  => $acordo->transacao_cartao,
                'cliente_id' => $acordo->cliente_id,
                'nome_cliente' => $acordo->cliente->nome,
                'cpf_cnpj' => $acordo->cliente->cpf_cnpj,
                'executivo_cobranca'  => [
                    'id' => $acordo->executivo_cobranca->id,
                    'nome' => $acordo->executivo_cobranca->funcionarios->nome,
                    'cpf' => $acordo->executivo_cobranca->funcionarios->cpf,
                ],
                'usuario_cancelou_id'  => $acordo->usuario_cancelou_id,
                'campanhas_id' => $acordo->campanhas_id,
                'juros_porcento'  => $acordo->juros_porcento,
                'multas_porcento'  => $acordo->multas_porcento,
                'honorarios_porcento' => $acordo->honorarios_porcento,
                'desconto_porcento' => $acordo->desconto_porcento,
                'valor_entrada_porcento'  => $acordo->valor_entrada_porcento,
                'parcelas' => ParcelasServices::listarParcelas($acordo->parcelas),
                'operacoes' => OperacoesAcordoServices::listarOperacoes($acordo->operacoes_acordo),
                'created_at' => $acordo->created_at,
                'updated_at' => $acordo->updated_at,
            ]);
        }

        return $lista;
    }
}
