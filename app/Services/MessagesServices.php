<?php

namespace App\Services;


class MessagesServices
{
    public static function generateMessagesProposta($proposta, $parcelas, $operacoes){
      return  ' =========== NEGOCIAÇÃO <strong> Nº PROPOSTA - '.$proposta['numero_proposta'].'</strong>=========== <br>'.
              ' Nr. Operação: '.implode(",",$operacoes). '<br>'.
              ' <strong>VALOR NOMINAL: R$ '.$proposta['valor_nominal']. '</strong><br>'.
              ' <strong>-JUROS: '. $proposta['juros_porcento']. '% '. ' R$ '. $proposta['juros']. '</strongg><br>'.
              ' <strong>-MULTA: '. $proposta['multas_porcento']. '% '. ' R$ ' . $proposta['multas']. '</strong><br>'.
              ' <strong>-HONORÁRIO: '.$proposta['honorarios_porcento']. '% ' . ' R$ '.$proposta['honorarios']. '</strong><br>'.
              ' <strong>-DESCONTOS.: '.$proposta['honorarios_porcento'] . ' R$ '.$proposta['desconto']. '</strong><br>'.
              ' VLR. PROPOSTA: R$ '.$parcelas[0]['valor_nominal'].' P/: '.$parcelas[0]['data_vencimento']. '<br>'.
              ' NEGOCIADO PARA PAGAMENTO EM <strong>'.$proposta['quantidade_parcelas'].' X '. '</strong><br>'.
              ' PARCELA N.º 1 - VALOR: R$ <strong>'.$parcelas[0]['valor_nominal']. '</strong><br>'.
              ' <strong>- VENCIMENTO: '.$parcelas[0]['data_vencimento'].' - '.$parcelas[0]['forma_pagamento'].'</strong>';
    }
}
