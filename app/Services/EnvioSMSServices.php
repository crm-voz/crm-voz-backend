<?php

namespace App\Services;

use App\Repository\Administrador\TemplateSMSRepository;
use Illuminate\Support\Facades\Log;

class EnvioSMSServices
{
    public function __construct(TemplateSMSRepository $templateSMSRepository, LogContatosServices $log)
    {
        $this->templateSMSRepository = $templateSMSRepository;
        $this->logContatoServices = $log;
    }

    public function enviar($clientes, $templateId){
        try{

            foreach($clientes as $cliente){
                $telefones = $cliente->telefones;
                foreach($telefones as $telefone){
                    if (!$cliente->operacoes[0]->remessa->credor){
                        continue;
                    }
                    $stringSMs = $this->templateSMSRepository->preencher($templateId, $cliente, $cliente->operacoes[0]->remessa->credor);
                    if ($this->templateSMSRepository->enviar(['celular' => $telefone->telefone, 'mensagem' => $stringSMs])){
                        $this->logContatoServices->salvarLogContatosSms($telefone->id,$stringSMs);
                    }
                }
                Log::info('Sucesso: ' . $cliente->cpf_cnpj);
            }

        }catch (\Exception $e) {
            Log::error('Erro: '.$e->getMessage());
        }
    }
}
