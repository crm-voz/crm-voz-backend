<?php

namespace App\Services;

use App\Models\BackOffice\Credor;
use App\Models\Cobranca\Acordo;
use App\Models\Cobranca\ExecutivoCobranca;
use App\Models\Core\Carteira;
use App\Models\Core\ExecutivosCarteira;
use App\Models\Core\Funcionario;
use App\Repository\Backoffice\CredorRepository;
use App\Repository\Backoffice\ExecutivoCobrancaRepository;

class CarteiraServices
{
    public function __construct(Acordo $acordo,
                                ExecutivoCobrancaRepository $executivoCobrancaRepository,
                                CredorRepository $credorRepository)
    {
        $this->acordo = $acordo;
        $this->executivoCobrancaRepository = $executivoCobrancaRepository;
        $this->credorRepository = $credorRepository;
    }

    private function calularProgressao($valor, $meta){
        if ($valor <= 0 || $meta<=0)
          return 0;

        return ($valor*100)/$meta;
    }

    private function calcularTotalAcordos($acordos){
        return $acordos->reduce(fn($carry, $item) => $carry + $item->valor_nominal);
    }

    private function calcularProgressMeta($carteira){
        $credoresId = $carteira->credoresCarteira->pluck('credor_id');
        $acordos = $this->acordo->AcordosInCredor($credoresId);
        $totalAcordos = $this->calcularTotalAcordos($acordos);
        return ['liquidado' => $totalAcordos ?? 0,
                'progress' => $this->calularProgressao($totalAcordos, $carteira->meta)];
    }

    public function calculaProgressExecutivo($executivo){
        $acordos = $this->executivoCobrancaRepository->acordos($executivo->id);
        $totalAcordos = $this->calcularTotalAcordos($acordos);
        return ['liquidado' => $totalAcordos ?? 0,
                'progress' => $this->calularProgressao($totalAcordos, intval($executivo->meta))];

    }

    public function calcularProgressCredor($credor, $meta){
        $acordos = $this->acordo->AcordosCredor($credor->id);
        $totalAcordos = $this->calcularTotalAcordos($acordos);
        return ['liquidado' => $totalAcordos ?? 0,
                'progress' => $this->calularProgressao($totalAcordos, $meta)];
    }

    public function credores($carteira){
        $lista = collect();
        foreach($carteira->credoresCarteira as $credor){
            $retorno = $this->calcularProgressCredor($credor, intval($credor->meta));
            $nomeCredor = Credor::where('id',$credor->credor_id)->first();
            $lista->add([
                'id' => $credor->credor_id,
                'name' => $nomeCredor->nome,
                'meta' => intval($credor->meta),
                'liquidado' => $retorno['liquidado'],
                'progress' => $retorno['progress'],
            ]);
        }

        return $lista->sortByDesc('progress');
    }

    public function executivos($carteira){
        $executivos = collect();
        foreach($carteira as $executivo){
            $retorno = $this->calculaProgressExecutivo($executivo->executivo);
            $nomeExecutivo = ExecutivoCobranca::where('id',$executivo->executivo_cobranca_id)->with('funcionarios')->first();
            $executivos->add([
                'id' => $executivo->executivo_cobranca_id,
                'name' => $nomeExecutivo?->funcionarios->nome,
                'meta' => intval($executivo->meta),
                'liquidado' => $retorno['liquidado'],
                'progress' => $retorno['progress'],
            ]);
        }

        return $executivos->sortByDesc('progress');
    }

    public function calcularDesempenho(Carteira $carteira){
       $nomeSupervisor = Funcionario::where('id',$carteira->executivo_cobranca_id)->first();
       $executivos = ExecutivosCarteira::where('carteira_id',$carteira->id)->with('executivo')->get();
       $retorno = $this->calcularProgressMeta($carteira);
        return [
           'id' => $carteira->id,
           'supervisor' => $nomeSupervisor->nome,
           'meta' => $carteira->meta,
           'description' => $carteira->descricao,
           'liquidado' => $retorno['liquidado'],
           'progress' => $retorno['progress'],
           'executivos' => $this->executivos($executivos),
           'credores' => $this->credores($carteira)
       ];
    }
}
