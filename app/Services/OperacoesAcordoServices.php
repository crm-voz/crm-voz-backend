<?php

namespace App\Services;

class OperacoesAcordoServices
 {
    public static function listarOperacoes($operacoesAcordo)
    {
        $lista = collect();
        foreach($operacoesAcordo as $op){
            $lista->add([
            "numero_operacao" => $op->operacao->numero_operacao,
            "data_atualizacao" => $op->operacao->data_atualizacao,
            "data_vencimento" => $op->operacao->data_vencimento,
            "data_processamento"=> $op->operacao->data_processamento,
            "valor_nominal"=> $op->operacao->valor_nominal,
            "valor_atualizado"=> $op->operacao->valor_atualizado,
            "descricao"=> $op->operacao->descricao,
            "cliente_id"=> $op->operacao->cliente_id,
            "classificacao_operacao_id"=> $op->operacao->classificacao_operacao_id,
            "usuario_empresas_id"=> $op->operacao->usuario_empresas_id,
            "status_operacao"=> [
                'id' => $op->operacao->status_operacao_id,
                'nome' => $op->operacao->status_operacao->nome,
                'sigla' => $op->operacao->status_operacao->sigla,
            ],
            "classificacao_negativacao_id"=> $op->operacao->classificacao_negativacao_id,
            "divida_reconhecida"=> $op->operacao->divida_reconhecida,
            "motivo_pendencia_id"=> $op->operacao->motivo_pendencia_id,
            "remessa_id"=> $op->operacao->remessa_id,
            "desconto"=> $op->operacao->desconto,
            "origem_desconto"=> $op->operacao->origem_desconto,
            "aluno"=> $op->operacao->aluno,
            "competencia"=> $op->operacao->competencia,
            "numero_acordo"=> $op->operacao->numero_acordo,
             "numero_acordo_pai"=> $op->operacao->numero_acordo_pai,
            ]);
        }
        return $lista;
    }

 }
