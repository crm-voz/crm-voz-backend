<?php

namespace App\Services;

use App\Models\Core\LogTelefone;

class LogContatosServices{

    public function salvarLogContatosSms($telefone_id, $templtae_id){
        try {
            $logTelefone = new LogTelefone();
            $logTelefone->telefone_id = $telefone_id;
            $logTelefone->observacao = json_encode(["descricao" => "Envio de sms", "template_sms_id" => $templtae_id]);
            $logTelefone->save();
        } catch (\Throwable $th) {
            throw $th;
        }
    }
    public function salvarLogContatosManualSms($telefone_id, $msg, $templtae_id=null){
        try {
            $logTelefone = new LogTelefone();
            $logTelefone->telefone_id = $telefone_id;
            $logTelefone->observacao = json_encode(["descricao" => $msg, "template_sms_id" => $templtae_id]);
            $logTelefone->save();
        } catch (\Throwable $th) {
            throw $th;
        }
    }
}
