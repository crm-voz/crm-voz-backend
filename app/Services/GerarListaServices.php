<?php

namespace App\Services;

use App\Models\BackOffice\Campanha;
use App\Models\BackOffice\ClienteCampanha;
use App\Models\Cobranca\Acordo;
use Carbon\Carbon;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\StreamedResponse;

class GerarListaServices
{
    public function __construct(ClienteCampanha $clienteCampanha)
    {
        $this->clienteCampanha = $clienteCampanha;
    }

    public function gerarLista($campanhaId, $extensao)
    {
        $campanha = Campanha::find($campanhaId);
        $clientes = $campanha->clientes;
        if ($extensao == 'csv'){
            $fileName = public_path($campanha->id.'.csv');
            $headers = array(
                "Content-type"        => "text/csv",
                "Content-Disposition" => "attachment; filename=$fileName",
                "Pragma"              => "no-cache",
                "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
                "Expires"             => "0"
            );

            $columns = array('chave', 'nome', 'CPF', 'endereco','cep','municipio','uf','sexo',
            'telefone1','telefone2','telefone3','telefone4','telefone5','telefone6','telefone7','telefone8',
            'telefone9','telefone10','telefone11','telefone12','telefone13','telefone14','telefone15');
            $callback = function() use($columns,$fileName) {
                $file = fopen($fileName, 'w');
                fputcsv($file, $columns,";");
                $row = [];

                foreach ($this->clienteCampanha->unique() as $value) {
                    $row['chave']  = $value->cliente_id;
                    $row['nome']   = $value->cliente->nome;
                    $row['CPF']    = '="'.str_replace('"', '\"', $value->cliente->cpf_cnpj).'"';
                    $row['endereco']  = $value->cliente->endereco;
                    $row['cep']  = $value->cliente->cep;
                    $row['municipio']  = $value->cliente->cidades->nome;
                    $row['uf']  = $value->cliente->cidades->uf;
                    $row['sexo']  = '';
                    for($i = 1; $i <= 15; $i++){
                        $row['telefone'.$i]  = '';
                    }
                    $i = 1;
                    foreach ($value->cliente->telefones as $v) {
                        $row['telefone'.$i] = ($v->telefone);
                        $i++;
                    }

                    fputcsv($file, array($row['chave'], $row['nome'], $row['CPF'],$row['endereco'],
                    $row['cep'], $row['municipio'],$row['uf'], $row['sexo'],
                    $row['telefone1'], $row['telefone2'], $row['telefone3'], $row['telefone4'],
                    $row['telefone5'], $row['telefone6'], $row['telefone7'], $row['telefone8'],
                    $row['telefone9'], $row['telefone10'], $row['telefone11'], $row['telefone12'],
                    $row['telefone13'], $row['telefone14'], $row['telefone15']),";");
                }

                fclose($file);
            };

            $response = Http::post('http://192.168.254.253/api/mailingup/',[
                                        'login' => 'jairo',
                                        'senha' => '12345',
                                        'CampanhaId' => $campanha->servico,
                                        'uploading' => 42,
                                        'arquivo' => $fileName,
                                        'operacaoId' => 1]);
            //dd($response);
            return response()->json(['dados' => true,'url' => $fileName,(new StreamedResponse($callback, 200, $headers))->sendContent()]);
        }

        if ($extensao == 'txt'){
            $line = [];
            $line[] = "telefone1;telefone2;telefone3;telefone4;telefone5;telefone6;campo1;campo2;campo3;campo4;campo5;idcontato;Switch_Result;Agent_Result;Result_TS; Retry_TS;Retry_Index;Retry_Count;Retry_Username;Complete_TS;campo6;telefone_ref1;telefone_ref2;telefone_ref3;telefone_ref4;telefone_ref5;telefone_ref6";
//          $campanha->clientes->chunk(100, function($clientes) use($campanha, $line){
                foreach($clientes as $value) {
                    $acordo = Acordo::where('cliente_id', $value->id)->where('campanhas_id', $campanha->id)->first();
                    if (!$acordo){
                        $l = '';
                        $tels = $value->telefones;
                        for($it=0; $it < 6; $it++) {
                            $tel = isset($tels[$it]['telefone']) ? $tels[$it]['telefone'] : '';
                            $l .= $tel . ';';
                        }
                        $l .= $value->nome . ';';
                        $l .= 0 . ';';
                        $l .= $value->cidades->uf . ';;';
                        $l .= $value->id . ';' .$value->id . ';;;;;;;;;';
                        $l .= $value->cpf_cnpj . ';;;;;;';
                        $line[] = $l."\r\n";
                    }
                }
           // });
           $arquivo = 'campanhas/listas/'. $campanha->id.'.txt';
           Storage::disk('public')->put($arquivo, $line);
           return response()->json([$clientes]);
        }
    }
}
