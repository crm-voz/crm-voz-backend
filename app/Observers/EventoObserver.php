<?php

namespace App\Observers;

use App\Models\BackOffice\Cliente;
use App\Models\Core\Eventos;
use App\Models\Core\StatusLocalizacao;
use Carbon\Carbon;

class EventoObserver
{
    protected const COBRANCA_TELEFONE = 1;
    protected const ATENDIMENTO_ONLINE = 16;
    protected const ATENDIMENTO_WHATSAPP = 19;
    protected const PROPOSTA_ENVIADA_CLIENTE = 9;
    protected const AGUARDA_RETORNO_INSTITUICAO = 13;
    protected const PENDENCIA = 12;
    protected const ANALISE_URGENTE = 15;
    protected const RETORNO_INSTITUICAO = 14;

    protected function getStatusLocalizacao($statusId)
    {
        $status = StatusLocalizacao::find($statusId);
        return json_encode(['title' => $status->nome, 'id' => $status->id]);
    }

    protected function updateStatusCliente(Eventos $evento)
    {
        $cliente = Cliente::find($evento->cliente_id);
        $status = null;
        switch ($evento->motivo_evento_id) {
            case 9:
                $status = $this->getStatusLocalizacao(6);
                break;
            case 13:
                $status = $this->getStatusLocalizacao(7);
                break;
            case 15:
                $status = $this->getStatusLocalizacao(24);
                break;
            case [1,5,14,21,14] :
               $status = '';
               break;
            case 33 :
              $status = $this->getStatusLocalizacao(3);
        }

        $cliente->update(['status_cliente_id' => $status,
                          'localizacao' => 'Localizado',
                          'ultimo_contato' => Carbon::now()]);
    }

    public function created(Eventos $evento)
    {
        if ($evento->motivo_evento_id == self::COBRANCA_TELEFONE
                || $evento->motivo_evento_id == self::ATENDIMENTO_ONLINE
                || $evento->motivo_evento_id == self::ATENDIMENTO_WHATSAPP
                || $evento->motivo_evento_id == self::PENDENCIA){
                $cliente = Cliente::find($evento->cliente_id);
                $cliente->update(['localizacao' => 'Localizado',
                                  'ultimo_contato' => Carbon::now()]);
        }
        if ($evento->motivo_evento_id == self::PROPOSTA_ENVIADA_CLIENTE){
            $this->updateStatusCliente($evento);
        }
        if ($evento->motivo_evento_id == self::AGUARDA_RETORNO_INSTITUICAO){
            $this->updateStatusCliente($evento);
        }
        if ($evento->motivo_evento_id == self::ANALISE_URGENTE){
            $this->updateStatusCliente($evento);
        }
    }
}
