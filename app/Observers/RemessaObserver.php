<?php

namespace App\Observers;

use App\Models\BackOffice\Remessa;
use App\Repository\Backoffice\RemessaRepository;
use App\Services\EnvioSMSServices;
use Illuminate\Support\Facades\Log;

class RemessaObserver
{
    protected const TEMPLATE_ID = 1;

    public function __construct(RemessaRepository $remessaRepository)
    {
        $this->enviarSms = app(EnvioSMSServices::class);
        $this->remessaRepository = $remessaRepository;
    }

    public function created(Remessa $remessa)
    {
        ini_set('memory_limit','3024M');

        $remessas = $this->remessaRepository->where('status','like','NAO PROCESSADO');
        foreach($remessas as $remessa){
           $remessa->update(['status' => 'EM PROCESSAMENTO']);
            $this->remessaRepository->importar($remessa);
            $remessa->update(['status' => 'ENVIAR SMS']);
            Log::info('Arquivo Remessa processado ' . $remessa->nome_arquivo);
        }
    }

    public function updated(Remessa $remessa)
    {
        $remessas = $this->remessaRepository->where('status','like','ENVIAR SMS');
        foreach($remessas as $remessa){
             $remessa->update(['status' => 'ENVIANDO SMS']);
             $this->enviarSms->enviar($this->remessaRepository->clientesRemessa($remessa), self::TEMPLATE_ID);
             $remessa->update(['status' => 'PROCESSADO']);
         }
    }
}
