<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class GenericMail extends Mailable
{
    use Queueable, SerializesModels;
    protected $dados;
    protected $anexo;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($dados, $anexo)
    {
        $this->dados = $dados;
        $this->anexo = $anexo;
    }

    /**
     * Build the message.
     *
     * [
     *  "assunto" => "Assunto do email",
     *  "conteudo" => "Conteudo do email"
     * ]
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('atendimento@grupovoz.com.br')
                    ->markdown('admin.emails.generic')
                    ->subject($this->dados['assunto'])
                    ->with(['dados' => $this->dados])
                    ->attachData($this->anexo, 'anexo.pdf', [
                        'mime' => 'application/pdf'
                    ]);
    }
}
