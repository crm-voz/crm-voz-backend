<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendMailCliente extends Mailable
{
    use Queueable, SerializesModels;
    protected $cliente;
    protected $dados;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($cliente, $dados)
    {
        $this->cliente = $cliente[0];
        $this->dados = $dados;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        
        foreach ($this->cliente->emails as $email) {
                $this->from($email)
                        ->markdown('admin.emails.cobranca')
                        ->subject($this->dados['assunto'])
                        ->with(['operacoes'=>$this->cliente->operacoes, 'dados' => $this->dados]);
        }
    }
}
