<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendMailSinteticoNegociacao extends Mailable
{
    use Queueable, SerializesModels;

    public $data;
    public $datad;

    public function __construct($data=null, $parcela=null)
    {
        $this->data = $data;
        $this->datad = $parcela;
    }

    public function build()
    {
        $address = 'atendimento@grupovoz.com.br';
        $subject = 'Demonstrativo Sintético Negociação';
        $name = $this->data['cliente'];

        return $this->markdown('admin.emails.RelatorioSinteticoAcordo')
                    ->from($address, $name)
                    ->cc($address, $name)
                    ->bcc($address, $name)
                    ->replyTo($address, $name)
                    ->subject($subject)
                    ->with([
                        'test_message' => $this->data['obrservacao'],
                    ]);
    }
}
