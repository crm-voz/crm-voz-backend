<?php

namespace App\Console\Services;

interface ITipoCampanha {
    public function execute($campanha);
}
