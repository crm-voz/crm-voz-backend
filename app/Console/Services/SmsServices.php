<?php

namespace App\Console\Services;

use App\Services\EnvioSMSServices;
use Illuminate\Support\Facades\Log;

class SmsServices implements ITipoCampanha {

    public function __construct(EnvioSMSServices $envioSMSServices)
    {
        $this->envioSMSServices = $envioSMSServices;
    }

    public function execute($campanha){
        try{
            $clientes = $campanha->clientes;
            $retorno = $this->envioSMSServices->enviar($clientes, $campanha->template_id);
            Log::info('ENVIO SMS CAMPANHA: ' . $campanha->id);
            return $retorno;
        }catch (\Exception $e) {
            Log::error('Erro: '.$e->getMessage());
        }
    }
}
