<?php

namespace App\Console\Services;

use App\Repository\Backoffice\CampanhasRepository;

class AtivoServices implements ITipoCampanha {

    public function __construct(CampanhasRepository $campanhaRepository)
    {
        $this->campanhaRepository = $campanhaRepository;
    }

    public function execute($campanha){
      return $this->campanhaRepository->gerar_lista($campanha->id, 'txt');
    }

}
