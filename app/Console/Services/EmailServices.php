<?php

namespace App\Console\Services;

use App\Gateway\Email\Avulso;
use App\Models\Core\LogEmail;
use App\Repository\Administrador\TemplateEmailRepository;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;


class EmailServices implements ITipoCampanha {

    public function __construct(Avulso $plataforma)
    {
        $this->plataforma_padrao = $plataforma;
    }

    public function execute($campanha){
        try{
            $clientes = $campanha->clientes;
            foreach($clientes as $cliente){
               foreach($cliente->emails as $email){
                    if (!$cliente->operacoes[0]->remessa->credor){
                        continue;
                    }
                   $link = 'http://192.168.254.205/email/send/'.$cliente->nome.'/'.$email->email.'/'.$cliente->operacoes[0]->remessa->credor->nome.'/'.$campanha->tempate_email_id.'/'.$cliente->id;
                   $response = Http::get($link);
                    LogEmail::create([
                        'email_id' => $email->id,
                        'observacao' => $response,
                    ]);
               }
            }

        }catch (\Exception $e) {
           Log::error('Erro: '.$e->getMessage());
        }
    }
}
