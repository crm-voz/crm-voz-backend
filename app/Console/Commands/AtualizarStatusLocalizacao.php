<?php

namespace App\Console\Commands;

use App\Http\Requests\ClientesRequest;
use App\Repository\Backoffice\ClientesRepository;
use Illuminate\Console\Command;

class AtualizarStatusLocalizacao extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Statuslocalizacao:atualizar';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Atualiza o status de localização de clientes de acordo com o tempo de contato, clientes com mais de 60
                              dias são atualizados para Em processo de Localiação e com 90 dias são atualizados para Não Localizados';

    protected const STATUS_NAO_LOCALIZADO = 'Nao Localizado';
    protected const STATUS_PROCESSO_LOCALIZACAO = 'Em Processo de Localizacao';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(ClientesRepository $clientesRepository)
    {
        parent::__construct();
        $this->clientesRepository = $clientesRepository;
    }

    //TODO: INTEGRAR COM O PARAMETRO QUE SERÁ CRIADO NO CREDOR.
    public function atualizacaoClientesOperacoes()
    {
        //atualiza clientes para Em processo de localização
        $this->clientesRepository->updateClientesSemContato(60,self::STATUS_PROCESSO_LOCALIZACAO);
        //atualiza clientes para Não Localizado
        $this->clientesRepository->updateClientesSemContato(90,self::STATUS_NAO_LOCALIZADO);
    }

    public function autalizaClientesParcelas()
    {
        //atualiza clientes com para Em processo de localização
        $this->clientesRepository->updateClientesSemContatoParcelas(60,self::STATUS_PROCESSO_LOCALIZACAO);
        //atualiza clientes para Não Localizado
        $this->clientesRepository->updateClientesSemContatoParcelas(90,self::STATUS_NAO_LOCALIZADO);
    }
    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        //atualiza baseado no ultimo contato
        $this->atualizacaoClientesOperacoes();
        //atualiza baseado no vencimento da parcela
        $this->autalizaClientesParcelas();
        logger('atualização de status de localização');

        return 0;
    }
}
