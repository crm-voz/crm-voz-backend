<?php

namespace App\Console\Commands;

use App\Console\Commands\AcordoServices\AcordoServices;
use App\Repository\Cobranca\AcordoRepository;
use Illuminate\Console\Command;

class AcordosVencidosCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'acordos:cancelar';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Verificar acordos com parcelas vencidas e não baixadas e classifica-los de acordo com as regras de negócio.';
    protected $acordoRepository;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(AcordoRepository $acordoRepository)
    {
        parent::__construct();
        $this->acordoRepository = $acordoRepository;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->acordoRepository->cancelarAcordosAguardando();
        //$this->acordoRepository->cancelarAcordosNegociados();
    }
}
