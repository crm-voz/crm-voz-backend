<?php

namespace App\Console\Commands;

use App\Console\Commands\SnapshotServices\Snapshot;
use Illuminate\Console\Command;

class SnapshotCreate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'snapshot:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Criar snpashot do banco de dados';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Snapshot $snapshot)
    {
        parent::__construct();
        $this->snapshot = $snapshot;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        ini_set('memory_limit','3024M');
        $this->snapshot->generate();
        //ini_set('memory_limit','128M');
        return Command::SUCCESS;
    }
}
