<?php

namespace App\Console\Commands\SnapshotServices;

use App\Repository\Backoffice\DevolucaoRepository;
use App\Repository\Backoffice\MotivoDevolucaoRepository;
use App\Repository\Backoffice\MotivoPendenciaRepository;
use App\Repository\Backoffice\OperacaoRepository;
use App\Repository\Backoffice\StatusOperacaoRepository;

class OperacaoServices {
    public function __construct(StatusOperacaoRepository $statusOperacaoRepository,
                                MotivoPendenciaRepository $motivoPendenciaRepository,
                                OperacaoRepository $operacaoRepository,
                                DevolucaoRepository $devolucaoRepository,
                                MotivoDevolucaoRepository $motivoDevolucaoRepository)
    {
        $this->statusOperacaoRepository = $statusOperacaoRepository;
        $this->motivoPendenciaRepository = $motivoPendenciaRepository;
        $this->operacaoRepository = $operacaoRepository;
        $this->devolucaoRepository = $devolucaoRepository;
        $this->motivoDevolucaoRepository = $motivoDevolucaoRepository;
    }

    protected function operacoesPorStatus($operacoes)
    {
        $registros = collect();
        $status = $this->statusOperacaoRepository->all();

        $status->each(function($item) use($operacoes, $registros){
            $filtered = $operacoes->filter(function($op,$key) use($item){
                return $op->status_operacao_id == $item->id;
            });
           $registros->add([$item->nome => $filtered->count()]);
        });

        return $registros;

    }

    protected function operacoesPorPendencia($operacoes)
    {
        $registros = collect();
        $pendencias = $this->motivoPendenciaRepository->all();
        $pendencias->each(function($item) use($operacoes, $registros){
            $filtered = $operacoes->filter(function($op,$key) use($item){
                return $op->motivo_pendencia_id == $item->id;
            });
           $registros->add([$item->nome => $filtered->count()]);
        });
        return $registros;
    }

    protected function operacoesDevolvidas($operacoes)
    {
        $registros = collect();
        if ($operacoes->count() == 0) {
            return $registros;
        }

        $credorId = $operacoes[0]->remessa->credor_id;
        $devolucoes = $this->devolucaoRepository->devolucoesDoCredor($credorId);
        $motivo = $this->motivoDevolucaoRepository->all();
        $motivo->each(function($item) use($devolucoes, $registros){
            $filtered = $devolucoes->filter(function($op,$key) use($item){
                return $op->motivo_devolucao_id == $item->id;
            });
           $registros->add([$item->nome => $filtered->count()]);
        });
        return $registros;
    }

    public function generateInformacoes($operacoes)
    {
        return [
            'operacoes_por_status' => $this->operacoesPorStatus($operacoes),
            'operacoes_por_pendencia' => $this->operacoesPorPendencia($operacoes),
            'operacoes_devolvidas' => $this->operacoesDevolvidas($operacoes)
        ];
    }
}
