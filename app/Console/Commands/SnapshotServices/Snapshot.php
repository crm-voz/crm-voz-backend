<?php

namespace App\Console\Commands\SnapshotServices;

use App\Repository\Backoffice\CredorRepository;
use App\Repository\Backoffice\SnapshotRepository;
use Carbon\Carbon;

class Snapshot {

    public function __construct(SnapshotRepository $snapshotRepository,
                                CredorRepository $credorRepository,
                                OperacaoServices $operacaoServices,
                                ClienteServices $clienteServices)
    {
        $this->credorRepository = $credorRepository;
        $this->operacaoServices = $operacaoServices;
        $this->clienteServices = $clienteServices;
        $this->snapshotRepository = $snapshotRepository;
    }

    protected function generateCredores($id = null){
        $credores =  $this->credorRepository->get($id);

        $registros = collect();
        foreach($credores as $credor){
            //logger($credor->operacoes);
            $registros->add([
                'id' => $credor->id,
                'credor'=> $credor->nome,
                'operacoes' => $this->operacaoServices->generateInformacoes($credor->operacoes),
                'clientes' => $this->clienteServices->generateInformacoes($credor->operacoes)
            ]);
        }
        return $registros;
    }

    public function generate($id = null){
        $registros = collect();
        $registros->add([Carbon::now()->format('m-d-Y H:s:i') => $this->generateCredores($id)]);
        try {
           $this->snapshotRepository->createJson($registros);
           logger('Snapshot automático  as ' . Carbon::now()->format('m-d-Y H:s:i'));
        } catch (\Exception $e) {
            Logger('Erro snpashot :' . $e->getMessage());
        }
    }
}
