<?php

namespace App\Console\Commands\SnapshotServices;

class ClienteServices {

    protected function totalClientesStatusLocalizacao($clientes, $statusLocalizacao){
        return $clientes->filter(function($cliente, $key) use($statusLocalizacao){
                                    return $cliente->localizacao == $statusLocalizacao;
                                })->count();
    }

    public function generateInformacoes($operacoes){
        $listaClientes = collect();
        foreach($operacoes as $operacao){
            $listaClientes->add($operacao->cliente);
        }
        $clientes = $listaClientes->unique();
        $totalClientes = $clientes->count();
        $clientesLocalizados = $this->totalClientesStatusLocalizacao($clientes,'Localizado');
        $clientesNaoLocalizados = $this->totalClientesStatusLocalizacao($clientes,'Nao Localizado');
        $emProcessoLocalizacao = $this->totalClientesStatusLocalizacao($clientes,'Em Processo de Localizacao');

        return [
            'total' => $totalClientes,
            'localizados' => $clientesLocalizados,
            'nao_localizados' => $clientesNaoLocalizados,
            'em_processo_localizacao' => $emProcessoLocalizacao,
            'sem_status_localizacao' => $totalClientes - ($clientesLocalizados + $clientesNaoLocalizados + $emProcessoLocalizacao)
        ];
    }
}
