<?php

namespace App\Console\Commands;

use App\Repository\Backoffice\RemessaRepository;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class Remessas extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'remessas:processar';
    protected const TEMPLATE_ID = 1;
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Consiste em transportar de fato os dado da planilhas das remessas para dentro do sistema';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(RemessaRepository $remessaRepository)
    {
        parent::__construct();
        $this->remessaRepository = $remessaRepository;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $remessas = $this->remessaRepository->where('status','like','NAO PROCESSADO');
        foreach($remessas as $remessa){
            //$remessa->update(['status' => 'EM PROCESSAMENTO']);
            $result = $this->remessaRepository->importar($remessa);
            if ($result['result']){
                $remessa->update(['status' => 'ENVIAR SMS']);
                Log::info('Arquivo Remessa processado ' . $remessa->nome_arquivo);
            }else{
                $remessa->update(['status' => 'NAO PROCESSADO']);
                Log::info('Erro ao processar remessa ' . $remessa->numero_remessa);
                Log::error($result['erro']);
            }
        }

        $remessas = $this->remessaRepository->where('status','like','ENVIAR SMS');
        foreach($remessas as $remessa){
             $remessa->update(['status' => 'ENVIANDO SMS']);
             //$this->enviarSms->enviar($this->remessaRepository->clientesRemessa($remessa), self::TEMPLATE_ID);
             $remessa->update(['status' => 'PROCESSADO']);
         }
    }
}
