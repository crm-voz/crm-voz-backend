<?php

namespace App\Console\Commands;

use App\Repository\Backoffice\EnvioAutomaticoSmsRepository;
use Illuminate\Console\Command;

class EnvioAutomaticoSmsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lembretesms:enviar';
    protected $envioAutomaticoSms;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Envio automático de sms de lembrete de pagamento para clientes com parcelas vencendo a partir de
                                tres dias para vencer.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(EnvioAutomaticoSmsRepository $env)
    {
        parent::__construct();
        $this->envioAutomaticoSms = $env;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->envioAutomaticoSms->envio_automatico_sms();
        return ;
    }
}
