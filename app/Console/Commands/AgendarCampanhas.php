<?php

namespace App\Console\Commands;

use App\Console\Services\EmailServices;
use App\Console\Services\SmsServices;
use App\Console\Services\AtivoServices;
use App\Models\BackOffice\ClienteCampanha;
use App\Repository\Backoffice\CampanhasRepository;
use App\Repository\Backoffice\OperacaoRepository;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class AgendarCampanhas extends Command
{
    /**
     * The name and signature of the console command.
     * @author Welliton Cunha
     * @since 29.06.2021 às 15:47
     * @var string
     */
    protected $signature = 'agendar:campanhas';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Comando para agendar campanhas, cuja a data atual esteja entre o periodo de inicio e final configurados na campanha.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(CampanhasRepository $campanhasRepository, OperacaoRepository $operacaoRepository)
    {
        parent::__construct();
        $this->campanhaRepository = $campanhasRepository;
        $this->operacaoRepository = $operacaoRepository;
    }

    public function executar($campanha)
    {
        $this->campanhaRepository->criarClientesCampanha($campanha);
        if($campanha->tipo_campanha == 'sms'){
            $sms = app(SmsServices::class);
            $sms->execute($campanha) ;
        }

        if($campanha->tipo_campanha == 'email'){
            $email = app(EmailServices::class);
            $email->execute($campanha);
        }

        if($campanha->tipo_campanha == 'ativo'){
            /** Atualizando o updated da tebela campanha na situação de email**/
            $ativo = app(AtivoServices::class);
            $ativo->execute($campanha);
        }

        $campanha->primeira_execucao = false;
        $campanha->updated_at = now();
        $campanha->save();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        ini_set('memory_limit','3024M');
        $dataAtual = Carbon::now();
        $campanhas = $this->campanhaRepository->campanhasAtivas();
        if (!$campanhas){
            return ;
        }
        foreach($campanhas as $campanha){
            if($campanha->primeira_execucao){
                if($dataAtual >= $campanha->data_inicio_campanha)
                    $this->executar($campanha);
            }else{

                $diferenca = $dataAtual->diffInMinutes($campanha->updated_at);
                /** Diferença entre data e hora atual seja maior ou igual ao o tempo de respiro **/
                if ((intval($diferenca)) >= ($campanha->respiro)){
                    $this->executar($campanha);
                }
            }
            Log::info('Campanha ' . $campanha->id . ' executada as ' . Carbon::now()->format('d/m/Y h:s') );
        }
    }
}
