<?php

use Illuminate\Http\Request;
use Illuminate\Support\Str;
/*
    $request -> request da requisição;
    $field -> nome do campo no form;
    $slug-> prefixo do nome do arquivo no servidor;
    $path-> pasta onde sera gravado o arquivo.
*/

function upload(Request $request, string $field, string $slug, string $path): string {
    if($request->has($field)){
        $image = $request->file($field);
        $name = Str::slug($slug).'_'.time() .'.'. $image->getClientOriginalExtension();
        $path = $request->file($field)->storeAs("/public/$path",$name);
        return $name;
    }else{
        return 'user.jpg';
    }
}

