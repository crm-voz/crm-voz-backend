<?php
namespace App\Helpers\Repasses;

use PhpOffice\PhpSpreadsheet\Spreadsheet;

abstract class Repasse{
    /**
     * @author Welliton Cunha
     * @since 21.02.2022 às 12:29
     *
     * @see Repasses
     */

    protected $tabela;
    public function __construct(Spreadsheet $tabela)
    {
        $this->tabela = $tabela;
    }

}
