<?php

namespace App\Helpers\Repasses;

use PhpOffice\PhpSpreadsheet\Style\Border as StyleBorder;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class RepasseDiadia extends Repasse{
    /**
     * @author Welliton Cunha
     *
     * @see Demo
     */

    public function diaDia($dados){
        $sheet = $this->tabela->getActiveSheet();
        $i = 2;

        //** Formando os títulos de cada coluna  **/
        $sheet->setCellValue('A1', 'Colégio/Faculdade');
        $sheet->setCellValue('B1', 'Cliente');
        $sheet->setCellValue('C1', 'Aluno');
        $sheet->setCellValue('D1', 'Documento');
        $sheet->setCellValue('E1', 'Nr. Operação');
        $sheet->setCellValue('F1', 'Vencimento Original');
        $sheet->setCellValue('G1', 'Vencimento');
        $sheet->setCellValue('H1', 'Recebimento');
        $sheet->setCellValue('I1', 'Parcela');
        $sheet->setCellValue('J1', 'Valor Negociado');
        $sheet->setCellValue('K1', 'Valor Repasse');
        $sheet->setCellValue('L1', 'Origem');
        $sheet->setCellValue('M1', 'Principal');
        $sheet->setCellValue('N1', 'Juros');
        $sheet->setCellValue('O1', 'Multa');
        $sheet->setCellValue('P1', 'Correção');
        $sheet->setCellValue('Q1', 'Desconto');
        $sheet->setCellValue('R1', 'Honorários');
        $sheet->setCellValue('S1', 'Realizado');
        $sheet->setCellValue('T1', 'Taxa Cartão');
        $sheet->setCellValue('U1', 'Comissão');
        $sheet->setCellValue('V1', 'Repasse');
        $sheet->setCellValue('W1', 'Forma');

        $cellRange = 'A1:W1';
        //** Cabeçalho em Negrito **/
        $sheet->getStyle($cellRange)->getFont()->setBold(true);

        //** Adicionando o título na aba da planilha **/
        $sheet->setTitle('diadia');

        //** Formatando as colunas para ajustar a largura dos textos **/
        for ($row=1; $row <= 1; $row++){
            foreach (range('A', 'W') as $column){
                $sheet->getStyle($cellRange)->getActiveSheet()->getColumnDimension($column)->setAutoSize(true);
            }
        }

        $totalNegociado = 0;
        $totalPrincipal = 0;
        $totalJuros = 0;
        $totalMultas = 0;
        $totalDescontos = 0;
        $totalHonorarios = 0;
        $totalRealizado = 0;
        $totalRepasse = 0;
        foreach ($dados as $value) {
            $count = $i++;
            $sheet->setCellValue('A'.$count, $value->colegio_faculdade);
            $sheet->setCellValue('B'.$count, $value->cliente);
            $sheet->setCellValue('C'.$count, $value->aluno);
            $sheet->setCellValue('D'.$count, $value->documento);
            $sheet->setCellValue('E'.$count, $value->numero_operacao);
            $sheet->setCellValue('F'.$count, date( 'd/m/Y' , strtotime($value->vencimento_original)));
            $sheet->setCellValue('G'.$count, date( 'd/m/Y' , strtotime($value->vencimento)));
            $sheet->setCellValue('H'.$count, date( 'd/m/Y' , strtotime($value->recebimento)));
            $sheet->setCellValue('I'.$count, $value->numero_parcela.'/'.$value->quantidade_parcelas);
            $sheet->setCellValue('J'.$count, $value->valor_negociado);
            $sheet->setCellValue('k'.$count, '');
            $sheet->setCellValue('L'.$count, $value->origem);
            $sheet->setCellValue('M'.$count, $principal = $value->valor_nominal);//($principal = $value->valor_baixado - $value->valor_honorario - $value->juros - $value->valor_multa - $value->desconto));
            $sheet->setCellValue('N'.$count, $value->valor_juros);
            $sheet->setCellValue('O'.$count, $value->valor_multa);
            $sheet->setCellValue('P'.$count, '');
            $sheet->setCellValue('Q'.$count, $value->desconto);
            $sheet->setCellValue('R'.$count, $value->valor_honorario);
            $sheet->setCellValue('S'.$count, $value->valor_baixado);
            $sheet->setCellValue('T'.$count, '');
            $sheet->setCellValue('U'.$count, $value->valor_comissao);
            $sheet->setCellValue('V'.$count, ($repasse = $value->valor_baixado - $value->valor_comissao - $value->valor_honorario));
            $sheet->setCellValue('W'.$count, $value->forma_pagamento);

            //** Soma dos valores calculáveis das colunas **/
            $totalNegociado = $totalNegociado + $value->valor_negociado;
            $totalPrincipal = $totalPrincipal + $principal;
            $totalJuros = $totalJuros + $value->valor_juros;
            $totalMultas = $totalMultas + $value->valor_multa;
            $totalDescontos = $totalDescontos + $value->desconto;
            $totalHonorarios = $totalHonorarios + $value->valor_honorario;
            $totalRealizado = $totalRealizado + $value->valor_baixado;
            $totalRepasse = $totalRepasse + $repasse;
        }

        /** Resultado da soma dos valores calculáveis das colunas **/
        $sheet->setCellValue('J'.$count+1, $totalNegociado)->getStyle('J'.$count+1)->getFont()->setBold(true);
        $sheet->setCellValue('M'.$count+1, $totalPrincipal)->getStyle('M'.$count+1)->getFont()->setBold(true);
        $sheet->setCellValue('N'.$count+1, $totalJuros)->getStyle('N'.$count+1)->getFont()->setBold(true);
        $sheet->setCellValue('O'.$count+1, $totalMultas)->getStyle('O'.$count+1)->getFont()->setBold(true);
        $sheet->setCellValue('Q'.$count+1, $totalDescontos)->getStyle('Q'.$count+1)->getFont()->setBold(true);
        $sheet->setCellValue('R'.$count+1, $totalHonorarios)->getStyle('R'.$count+1)->getFont()->setBold(true);
        $sheet->setCellValue('S'.$count+1, $totalRealizado)->getStyle('S'.$count+1)->getFont()->setBold(true);
        $sheet->setCellValue('V'.$count+1, $totalRepasse)->getStyle('V'.$count+1)->getFont()->setBold(true);

        //** Aplicando as bordas em todas as linhas de texto **/
        $sheet->getStyle('A1:W'.$count+1)->getBorders()->applyFromArray(['allBorders' => ['borderStyle' => StyleBorder::BORDER_THIN]]);

        $writer = new Xlsx($this->tabela);
        $filename = 'Recebimento_diadia_'.rand().'.xlsx';
        $writer->save($filename);

        //** Criando o buffer do excel **/
        $content = file_get_contents($filename);
        header("Content-Disposition: attachment; filename=".$filename);
        header("Access-Control-Allow-Origin: *");
        unlink($filename);
        exit($content);
    }

}
