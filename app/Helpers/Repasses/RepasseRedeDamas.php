<?php

namespace App\Helpers\Repasses;

use PhpOffice\PhpSpreadsheet\Style\Border as StyleBorder;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class RepasseRedeDamas extends Repasse{
    /**
     * @author Welliton Cunha
     *
     * @see Demo
     */

    public function redeDamas($dados){
        $sheet = $this->tabela->getActiveSheet();
        $i = 2;

        //** Formando os títulos de cada coluna  **/
        $sheet->setCellValue('A1', 'Credor');
        $sheet->setCellValue('B1', 'Empresa');
        $sheet->setCellValue('C1', 'Cliente');
        $sheet->setCellValue('D1', 'Documento');
        $sheet->setCellValue('E1', 'Referência');
        $sheet->setCellValue('F1', 'Num Documento');
        $sheet->setCellValue('G1', 'contrato');
        $sheet->setCellValue('H1', 'Origem');
        $sheet->setCellValue('I1', 'Vencimento');
        $sheet->setCellValue('J1', 'Pagamento');
        $sheet->setCellValue('K1', 'Parcela');
        $sheet->setCellValue('L1', 'Principal');
        $sheet->setCellValue('M1', 'Juros');
        $sheet->setCellValue('N1', 'Multa');
        $sheet->setCellValue('O1', 'Correção');
        $sheet->setCellValue('P1', 'Descontos');
        $sheet->setCellValue('Q1', 'Honorários');
        $sheet->setCellValue('R1', 'Realizado');
        $sheet->setCellValue('S1', 'Remuneração');
        $sheet->setCellValue('T1', 'Repasse');
        $sheet->setCellValue('U1', 'Forma');
        $sheet->setCellValue('V1', 'Banco');

        $cellRange = 'A1:V1';
        //** Cabeçalho em Negrito **/
        $sheet->getStyle($cellRange)->getFont()->setBold(true);

        //** Adicionando o título na aba da planilha **/
        $sheet->setTitle('rede_damas');

        //** Formatando as colunas para ajustar a largura dos textos **/
        for ($row=1; $row <= 1; $row++){
            foreach (range('A', 'V') as $column){
                $sheet->getStyle($cellRange)->getActiveSheet()->getColumnDimension($column)->setAutoSize(true);
            }
        }

        $totalPrincipal = 0;
        $totalJuros = 0;
        $totalMultas = 0;
        $totalDescontos = 0;
        $totalHonorarios = 0;
        $totalRealizado = 0;
        $totalRepasse = 0;
        foreach ($dados as $value) {
            $count = $i++;
            $sheet->setCellValue('A'.$count, 'REDE DAMAS');
            $sheet->setCellValue('B'.$count, $value->colegio_faculdade);
            $sheet->setCellValue('C'.$count, $value->cliente);
            $sheet->setCellValue('D'.$count, $value->documento);
            $sheet->setCellValue('E'.$count, '');
            $sheet->setCellValue('F'.$count, '');
            $sheet->setCellValue('G'.$count, '');
            $sheet->setCellValue('H'.$count, $value->origem);
            $sheet->setCellValue('I'.$count, date( 'd/m/Y' , strtotime($value->vencimento)));
            $sheet->setCellValue('J'.$count, date( 'd/m/Y' , strtotime($value->recebimento)));
            $sheet->setCellValue('k'.$count, $value->numero_parcela.'/'.$value->quantidade_parcelas);
            $sheet->setCellValue('L'.$count, ($principal = $value->valor_baixado - $value->valor_honorario - $value->juros - $value->valor_multa - $value->desconto));
            $sheet->setCellValue('M'.$count, $value->valor_juros);
            $sheet->setCellValue('N'.$count, $value->valor_multa);
            $sheet->setCellValue('O'.$count, '');
            $sheet->setCellValue('P'.$count, $value->desconto);
            $sheet->setCellValue('Q'.$count, $value->valor_honorario);
            $sheet->setCellValue('R'.$count, $value->valor_baixado);
            $sheet->setCellValue('S'.$count, '');
            $sheet->setCellValue('T'.$count, ($repasse = $value->valor_baixado - $value->valor_comissao - $value->valor_honorario));
            $sheet->setCellValue('U'.$count, $value->forma_pagamento);
            $sheet->setCellValue('V'.$count, '');

            //** Soma dos valores calculáveis das colunas **/
            $totalPrincipal = $totalPrincipal + $principal;
            $totalJuros = $totalJuros + $value->valor_juros;
            $totalMultas = $totalMultas + $value->valor_multa;
            $totalDescontos = $totalDescontos + $value->desconto;
            $totalHonorarios = $totalHonorarios + $value->valor_honorario;
            $totalRealizado = $totalRealizado + $value->valor_baixado;
            $totalRepasse = $totalRepasse + $repasse;
        }

        /** Resultado da soma dos valores calculáveis das colunas **/
        $sheet->setCellValue('L'.$count+1, $totalPrincipal)->getStyle('L'.$count+1)->getFont()->setBold(true);
        $sheet->setCellValue('M'.$count+1, $totalJuros)->getStyle('M'.$count+1)->getFont()->setBold(true);
        $sheet->setCellValue('N'.$count+1, $totalMultas)->getStyle('N'.$count+1)->getFont()->setBold(true);
        $sheet->setCellValue('P'.$count+1, $totalDescontos)->getStyle('P'.$count+1)->getFont()->setBold(true);
        $sheet->setCellValue('Q'.$count+1, $totalHonorarios)->getStyle('Q'.$count+1)->getFont()->setBold(true);
        $sheet->setCellValue('R'.$count+1, $totalRealizado)->getStyle('R'.$count+1)->getFont()->setBold(true);
        $sheet->setCellValue('T'.$count+1, $totalRepasse)->getStyle('T'.$count+1)->getFont()->setBold(true);

        //** Aplicando as bordas em todas as linhas de texto **/
        $sheet->getStyle('A1:V'.$count+1)->getBorders()->applyFromArray(['allBorders' => ['borderStyle' => StyleBorder::BORDER_THIN]]);

        $writer = new Xlsx($this->tabela);
        $filename = 'Recebimento_rededamas_'.rand().'.xlsx';
        $writer->save($filename);

        //** Criando o buffer do excel **/
        $content = file_get_contents($filename);
        header("Content-Disposition: attachment; filename=".$filename);
        header("Access-Control-Allow-Origin: *");
        unlink($filename);
        exit($content);
    }

}
