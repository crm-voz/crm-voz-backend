<?php
namespace App\Helpers\Relatorios;

use Codedge\Fpdf\Fpdf\Fpdf;
use Illuminate\Support\Facades\Storage;

abstract class Demo{
    /**
     * @author Welliton Cunha
     *
     * @see Demo
     */

    protected $pdf;
    protected $largura;
    protected $altura;
    public $baseOp, $dataAcordo;

    abstract public function escrever_dados_cliente($nome);
    abstract public function escrever_dados_credor($nome);
    abstract public function escrever_dados_acordo($acordo);
    abstract public function escrever_dados_operacao($operacoes);
    abstract public function escrever_dados_parcelas($parcelas);


    public function gerar($output='S'){
        header("Access-Control-Allow-Origin: *");
        return $this->pdf->Output($output);
    }

    public function __construct($nomeimg,$orientacao) {
        $this->pdf = new Fpdf($orientacao);
        $this->pdf->AddPage();

        $this->largura = $this->pdf->GetPageWidth();
        $this->altura = $this->pdf->GetPageHeight();
        $this->pdf->Image(Storage::path("public/images/$nomeimg"), 0, 0, $this->largura, $this->altura);
    }

}
