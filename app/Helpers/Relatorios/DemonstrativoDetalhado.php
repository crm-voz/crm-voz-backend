<?php

namespace App\Helpers\Relatorios;

use App\Helpers\Relatorios\Demo;
use Codedge\Fpdf\Fpdf\Fpdf;
use Illuminate\Support\Facades\Storage;

class DemonstrativoDetalhado extends Demo{
    /**
     * @author Welliton Cunha
     *
     * @see Demo
     */
    protected $img, $imglocal, $op;
    public function __construct($img) {
        parent::__construct($img,'L');
        $this->imglocal = $img;
    }

    public function escrever_dados_cliente($nome) {
        $this->pdf->SetFont('Arial', 'B', 8);
        $this->pdf->SetXY(47, 1.5);
        $this->pdf->Write(10, utf8_decode($nome));
        return $this;
    }

    public function escrever_dados_credor($nome) {
        $this->pdf->SetFont('Arial', 'B', 8);
        $this->pdf->SetXY(47, 5);
        $this->pdf->Write(10, utf8_decode($nome));
        return $this;
    }

    public function escrever_dados_acordo($acordo,$operacoes=null) {

        if ($this->imglocal == 'simulacao-demostrativo-detalhado.png'){
            $this->pdf->SetFont('Arial', 'B', 8);
            $this->pdf->SetXY(211, 5.3);
            $this->pdf->Write(10, utf8_decode(date('d/m/Y', strtotime(now()))));
        }

        if ($this->imglocal == 'demostrativo-detalhado.png'){
            $operacoes = $acordo->operacoes_acordo()->get()->map(function($item, $key) {
                return $item->operacao;
            });
            $this->pdf->SetFont('Arial', 'B', 8);
            $this->pdf->SetXY(188, 5.3);
            $this->pdf->Write(10, utf8_decode(date('d/m/Y', strtotime($acordo->created_at))));
            $this->dataAcordo = $acordo->created_at;

            $this->pdf->SetXY(212, 5.3);
            $this->pdf->Write(10, utf8_decode($acordo->numero_acordo));
        }

        $this->pdf->SetFont('Arial', 'B', 10);
        $this->pdf->SetXY(238, 5.3);
        $this->pdf->Write(10, utf8_decode('R$ '.round($acordo->valor_nominal, 2)));

        $this->pdf->SetFont('Arial', '', 8);
        $this->pdf->SetXY(205,59+(count($operacoes)*5));
        $this->pdf->Write(10, utf8_decode(round($acordo->multas_porcento, 2).'% de Multa'));

        $this->pdf->SetFont('Arial', '', 8);
        $this->pdf->SetXY(265,59+(count($operacoes)*5));
        $this->pdf->Write(10, utf8_decode('R$ '.round($acordo->multas,2)));

        $this->pdf->SetFont('Arial', '', 8);
        $this->pdf->SetXY(205,63+(count($operacoes)*5));
        $this->pdf->Write(10, utf8_decode(round($acordo->juros_porcento,2).'% de Juros'));

        $this->pdf->SetFont('Arial', '', 8);
        $this->pdf->SetXY(265,63+(count($operacoes)*5));
        $this->pdf->Write(10, utf8_decode('R$ '.round($acordo->juros,2)));

        $this->pdf->SetFont('Arial', '', 8);
        $this->pdf->SetXY(205,67+(count($operacoes)*5));
        $this->pdf->Write(10, utf8_decode(round($acordo->desconto_porcento,2).'% de Desconto'));

        $this->pdf->SetFont('Arial', '', 8);
        $this->pdf->SetXY(265,67+(count($operacoes)*5));
        $this->pdf->Write(10, utf8_decode('R$ '.round($acordo->desconto,2)));

        $this->pdf->SetFont('Arial', '', 8);
        $this->pdf->SetXY(205,71+(count($operacoes)*5));
        $this->pdf->Write(10, utf8_decode(round($acordo->honorarios_porcento, 2).'% de Encargos Administrativo'));

        $this->pdf->SetFont('Arial', '', 8);
        $this->pdf->SetXY(265,71+(count($operacoes)*5));
        $this->pdf->Write(10, utf8_decode('R$ '.round($acordo->honorarios, 2)));

        return $this;
    }

    public function escrever_dados_operacao($operacoes) {
        $baseY = 0; //valor base
        $baseAdd = 5; //quanto em pixels será adicionado à cada loop.
        $this->baseOp = count($operacoes)*$baseAdd;

        foreach ($operacoes as $key) {

            $this->pdf->SetY(51+$baseY);
            $this->pdf->Cell(4, 5, iconv('UTF-8', 'windows-1252', ""), 0, 0, "L", 0);
            $this->pdf->SetFont('Arial', '', 8);
            $this->pdf->Cell(90, 5, iconv('UTF-8', 'windows-1252',  $key["numero_operacao"]), 0, 0, "L", 0);
            $this->pdf->Ln(4);

            $this->pdf->SetY(51+$baseY);
            $this->pdf->Cell(28, 5, iconv('UTF-8', 'windows-1252', ""), 0, 0, "L", 0);
            $this->pdf->SetFont('Arial', '', 8);
            $this->pdf->Cell(90, 5, iconv('UTF-8', 'windows-1252',  date('d/m/Y', strtotime($key["data_vencimento"]))), 0, 0, "L", 0);
            $this->pdf->Ln(4);

            $this->pdf->SetY(51+$baseY);
            $this->pdf->Cell(54, 5, iconv('UTF-8', 'windows-1252', ""), 0, 0, "L", 0);
            $this->pdf->SetFont('Arial', '', 8);
            $this->pdf->Cell(90, 5, iconv('UTF-8', 'windows-1252', $key["aluno"]), 0, 0, "L", 0);
            $this->pdf->Ln(4);

            $this->pdf->SetY(51+$baseY);
            $this->pdf->Cell(119, 5, iconv('UTF-8', 'windows-1252', ""), 0, 0, "L", 0);
            $this->pdf->SetFont('Arial', '', 8);
            $this->pdf->Cell(90, 5, iconv('UTF-8', 'windows-1252',  "R$ ".round($key["valor_nominal"],2)), 0, 0, "L", 0);
            $this->pdf->Ln(4);

            $this->pdf->SetY(51+$baseY);
            $this->pdf->Cell(148, 5, iconv('UTF-8', 'windows-1252', ""), 0, 0, "L", 0);
            $this->pdf->SetFont('Arial', '', 8);
            $this->pdf->Cell(90, 5, iconv('UTF-8', 'windows-1252',  "R$ ".round($key["valor_juros"],2)), 0, 0, "L", 0);
            $this->pdf->Ln(4);

            $this->pdf->SetY(51+$baseY);
            $this->pdf->Cell(171, 5, iconv('UTF-8', 'windows-1252', ""), 0, 0, "L", 0);
            $this->pdf->SetFont('Arial', '', 8);
            $this->pdf->Cell(90, 5, iconv('UTF-8', 'windows-1252',  "R$ ".round($key["valor_multa"],2)), 0, 0, "L", 0);
            $this->pdf->Ln(4);

            $this->pdf->SetY(51+$baseY);
            $this->pdf->Cell(194, 5, iconv('UTF-8', 'windows-1252', ""), 0, 0, "L", 0);
            $this->pdf->SetFont('Arial', '', 8);
            $this->pdf->Cell(90, 5, iconv('UTF-8', 'windows-1252',  "R$ ".round($key["valor_desconto"],2)), 0, 0, "L", 0);
            $this->pdf->Ln(4);

            $this->pdf->SetY(51+$baseY);
            $this->pdf->Cell(221.5, 5, iconv('UTF-8', 'windows-1252', ""), 0, 0, "L", 0);
            $this->pdf->SetFont('Arial', '', 8);
            $this->pdf->Cell(90, 5, iconv('UTF-8', 'windows-1252',  "R$ ".round($key["valor_honorario"], 2)), 0, 0, "L", 0);
            $this->pdf->Ln(4);

            $this->pdf->SetY(51+$baseY);
            $this->pdf->Cell(260, 5, iconv('UTF-8', 'windows-1252', ""), 0, 0, "L", 0);
            $this->pdf->SetFont('Arial', '', 8);
            $this->pdf->Cell(90, 5, iconv('UTF-8', 'windows-1252',  "R$ ".round($key["valor_nominal"],2)), 0, 0, "L", 0);
            $this->pdf->Ln(4);

            $this->pdf->Ln();

            $baseY = $baseY+$baseAdd;

        }

        return $this;
    }

    public function escrever_dados_parcelas($parcelas) {

        $this->pdf->Image(Storage::path("public/images/parcelas-encargos-paisagem.png"),0.5,54+$this->baseOp,297);
        $baseY = 0; //valor base
        $baseAdd = 5; //quanto em pixels será adicionado à cada loop.

        foreach ($parcelas as $key) {
            $this->pdf->SetY(71+$this->baseOp+$baseY);
            $this->pdf->Cell(5, 5, iconv('UTF-8', 'windows-1252', ""), 0, 0, "L", 0);
            $this->pdf->SetFont('Arial', '', 8);
            $this->pdf->Cell(90, 5, iconv('UTF-8', 'windows-1252',  $key["numero_parcela"]), 0, 0, "L", 0);
            $this->pdf->Ln(4);

            $this->pdf->SetY(71+$this->baseOp+$baseY);
            $this->pdf->Cell(16, 5, iconv('UTF-8', 'windows-1252', ""), 0, 0, "L", 0);
            $this->pdf->SetFont('Arial', '', 8);
            $this->pdf->Cell(90, 5, iconv('UTF-8', 'windows-1252',  $key["forma_pagamento"]), 0, 0, "L", 0);
            $this->pdf->Ln(4);

            $this->pdf->SetY(71+$this->baseOp+$baseY);
            $this->pdf->Cell(46.5, 5, iconv('UTF-8', 'windows-1252', ""), 0, 0, "L", 0);
            $this->pdf->SetFont('Arial', '', 8);
            $this->pdf->Cell(90, 5, iconv('UTF-8', 'windows-1252', date('d/m/Y', strtotime($key["data_vencimento"]))), 0, 0, "L", 0);
            $this->pdf->Ln(4);

            $this->pdf->SetY(71+$this->baseOp+$baseY);
            $this->pdf->Cell(80, 5, iconv('UTF-8', 'windows-1252', ""), 0, 0, "L", 0);
            $this->pdf->SetFont('Arial', '', 8);
            $this->pdf->Cell(90, 5, iconv('UTF-8', 'windows-1252',  "R$ ".round($key["valor_nominal"], 2)), 0, 0, "L", 0);
            $this->pdf->Ln(4);

            $this->pdf->SetY(71+$this->baseOp+$baseY);
            $this->pdf->Cell(104.5, 5, iconv('UTF-8', 'windows-1252', ""), 0, 0, "L", 0);
            $this->pdf->SetFont('Arial', '', 8);
            if (!empty($key->data_baixa))
            $this->pdf->Cell(90, 5, iconv('UTF-8', 'windows-1252',  date('d/m/Y', strtotime($key["data_baixa"]))), 0, 0, "L", 0);
            $this->pdf->Ln(4);

            $this->pdf->SetY(71+$this->baseOp+$baseY);
            $this->pdf->Cell(137, 5, iconv('UTF-8', 'windows-1252', ""), 0, 0, "L", 0);
            $this->pdf->SetFont('Arial', '', 8);
            if (!empty($key->valor_baixado))
            $this->pdf->Cell(90, 5, iconv('UTF-8', 'windows-1252',  "R$ ".round($key["valor_baixado"], 2)), 0, 0, "L", 0);
            $this->pdf->Ln(4);

            $this->pdf->Ln();

            $baseY = $baseY+$baseAdd;

            // condição para ter muitas parcelas, gera outra folha com o restante das parcelas.
            if ($baseY+$this->baseOp == 100){
                $baseY = -80;
                $this->pdf->AddPage();
                $this->pdf->Image(Storage::path("public/images/rodape-demostrativo-detalhado.png"), 0,170);
            }

        }

        $this->pdf->Image(Storage::path("public/images/line.png"),4,76+$this->baseOp+$baseY-5,176.5);
        $this->pdf->SetFont('Arial', '', 5);
        $this->pdf->SetXY(143.5, 75+$this->baseOp+$baseY-5);
        $this->pdf->Write(10, utf8_decode('Emitido em '.date('d/m/Y H:i:s', strtotime($this->dataAcordo))));

        return $this;
    }
}
