<?php

namespace App\Helpers\Relatorios;

use App\Helpers\Relatorios\Demo;
use Illuminate\Support\Facades\Storage;

class DemonstrativoSimples extends Demo{
    /**
     * @author Welliton Cunha
     *
     * @see Demo
     */
    protected $img, $imglocal;
    public function __construct($img) {
        parent::__construct($img,'P');
        $this->imglocal = $img;
    }

    public function escrever_dados_cliente($nome) {
        $this->pdf->SetFont('Arial', 'B', 8);
        $this->pdf->SetXY(47, 1.5);
        $this->pdf->Write(10, utf8_decode($nome));
        return $this;
    }

    public function escrever_dados_credor($nome) {
        $this->pdf->SetFont('Arial', 'B', 8);
        $this->pdf->SetXY(47, 5);
        $this->pdf->Write(10, utf8_decode($nome));
        return $this;
    }

    public function escrever_dados_acordo($acordo) {

        if ($this->imglocal == 'demo-Simples.png'){
            $this->pdf->SetFont('Arial', 'B', 7);
            $this->pdf->SetXY(110, 5.5);
            $this->pdf->Write(10, utf8_decode(date('d/m/Y', strtotime($acordo->created_at))));

            $this->pdf->SetXY(131, 5.5);
            $this->pdf->Write(10, utf8_decode($acordo->numero_acordo));
        }
        if ($this->imglocal == 'simulacao-demo-Simples.png'){
            $this->pdf->SetFont('Arial', 'B', 7);
            $this->pdf->SetXY(126, 5.9);
            $this->pdf->Write(10, utf8_decode(date('d/m/Y', strtotime(now()))));
        }

        $this->pdf->SetFont('Arial', 'B', 10);
        $this->pdf->SetXY(152, 5.5);
        $this->pdf->Write(10, utf8_decode('R$ '.round($acordo->valor_nominal, 2)));

        return $this;
    }

    public function escrever_dados_operacao($operacoes) {

        $baseY = 0; //valor base
        $baseAdd = 5; //quanto em pixels será adicionado à cada loop.
        $this->baseOp = count($operacoes)*$baseAdd;

        foreach ($operacoes as $key) {

            $this->pdf->SetY(54+$baseY);
            // $this->pdf->SetFont('Arial', 'B', 8);
            $this->pdf->Cell(3, 5, iconv('UTF-8', 'windows-1252', ""), 0, 0, "L", 0);
            $this->pdf->SetFont('Arial', '', 8);
            $this->pdf->Cell(90, 5, iconv('UTF-8', 'windows-1252',  $key["numero_operacao"]), 0, 0, "L", 0);
            $this->pdf->Ln(4);

            $this->pdf->SetY(54+$baseY);
            // $this->pdf->SetFont('Arial', 'B', 8);
            $this->pdf->Cell(33, 5, iconv('UTF-8', 'windows-1252', ""), 0, 0, "L", 0);
            $this->pdf->SetFont('Arial', '', 8);
            $this->pdf->Cell(90, 5, iconv('UTF-8', 'windows-1252',  date('d/m/Y', strtotime($key["data_vencimento"]))), 0, 0, "L", 0);
            $this->pdf->Ln(4);

            $this->pdf->SetY(54+$baseY);
            // $this->pdf->SetFont('Arial', 'B', 8);
            $this->pdf->Cell(67, 5, iconv('UTF-8', 'windows-1252', ""), 0, 0, "L", 0);
            $this->pdf->SetFont('Arial', '', 8);
            $this->pdf->Cell(90, 5, iconv('UTF-8', 'windows-1252', $key["aluno"]), 0, 0, "L", 0);
            $this->pdf->Ln(4);

            $this->pdf->SetY(54+$baseY);
            // $this->pdf->SetFont('Arial', 'B', 8);
            $this->pdf->Cell(174, 5, iconv('UTF-8', 'windows-1252', ""), 0, 0, "L", 0);
            $this->pdf->SetFont('Arial', '', 8);
            $this->pdf->Cell(90, 5, iconv('UTF-8', 'windows-1252',  "R$ ".round($key["valor_nominal"],2)), 0, 0, "L", 0);
            $this->pdf->Ln(4);

            $this->pdf->Ln();

            $baseY = $baseY+$baseAdd;

        }

        return $this;
    }

    public function escrever_dados_parcelas($parcelas) {

        $this->pdf->Image(Storage::path("public/images/parcelas.png"),0,54+$this->baseOp,209);

        $baseY = 0; //valor base
        $baseAdd = 5; //quanto em pixels será adicionado à cada loop.

        foreach ($parcelas as $key) {
            $this->pdf->SetY(71+$this->baseOp+$baseY);
            // $this->pdf->SetFont('Arial', 'B', 8);
            $this->pdf->Cell(3, 5, iconv('UTF-8', 'windows-1252', ""), 0, 0, "L", 0);
            $this->pdf->SetFont('Arial', '', 8);
            $this->pdf->Cell(90, 5, iconv('UTF-8', 'windows-1252',  $key["numero_parcela"]), 0, 0, "L", 0);
            $this->pdf->Ln(4);

            $this->pdf->SetY(71+$this->baseOp+$baseY);
            // $this->pdf->SetFont('Arial', 'B', 8);
            $this->pdf->Cell(14, 5, iconv('UTF-8', 'windows-1252', ""), 0, 0, "L", 0);
            $this->pdf->SetFont('Arial', '', 8);
            $this->pdf->Cell(90, 5, iconv('UTF-8', 'windows-1252',  $key["forma_pagamento"]), 0, 0, "L", 0);
            $this->pdf->Ln(4);

            $this->pdf->SetY(71+$this->baseOp+$baseY);
            // $this->pdf->SetFont('Arial', 'B', 8);
            $this->pdf->Cell(50, 5, iconv('UTF-8', 'windows-1252', ""), 0, 0, "L", 0);
            $this->pdf->SetFont('Arial', '', 8);
            $this->pdf->Cell(90, 5, iconv('UTF-8', 'windows-1252', date('d/m/Y', strtotime($key["data_vencimento"]))), 0, 0, "L", 0);
            $this->pdf->Ln(4);

            $this->pdf->SetY(71+$this->baseOp+$baseY);
            // $this->pdf->SetFont('Arial', 'B', 8);
            $this->pdf->Cell(89, 5, iconv('UTF-8', 'windows-1252', ""), 0, 0, "L", 0);
            $this->pdf->SetFont('Arial', '', 8);
            $this->pdf->Cell(90, 5, iconv('UTF-8', 'windows-1252',  "R$ ".round($key["valor_nominal"], 2)), 0, 0, "L", 0);
            $this->pdf->Ln(4);

            $this->pdf->SetY(71+$this->baseOp+$baseY);
            // $this->pdf->SetFont('Arial', 'B', 8);
            $this->pdf->Cell(123, 5, iconv('UTF-8', 'windows-1252', ""), 0, 0, "L", 0);
            $this->pdf->SetFont('Arial', '', 8);
            if (!empty($key->data_baixa))
            $this->pdf->Cell(90, 5, iconv('UTF-8', 'windows-1252',  date('d/m/Y', strtotime($key["data_baixa"]))), 0, 0, "L", 0);
            $this->pdf->Ln(4);

            $this->pdf->SetY(71+$this->baseOp+$baseY);
            // $this->pdf->SetFont('Arial', 'B', 8);
            $this->pdf->Cell(167, 5, iconv('UTF-8', 'windows-1252', ""), 0, 0, "L", 0);
            $this->pdf->SetFont('Arial', '', 8);
            if (!empty($key->valor_baixado))
            $this->pdf->Cell(90, 5, iconv('UTF-8', 'windows-1252',  "R$ ".round($key["valor_baixado"], 2)), 0, 0, "L", 0);
            $this->pdf->Ln(4);

            $this->pdf->Ln();

            $baseY = $baseY+$baseAdd;

            //condição para ter muitas parcelas, gera outra folha com o restante das parcelas.
            if ($baseY+$this->baseOp == 180){
                $baseY = -88;
                $this->pdf->AddPage();
                $this->pdf->Image(Storage::path("public/images/rodape-demo-Simples.png"), 0,260);
            }
        }

        $this->pdf->Image(Storage::path("public/images/line.png"),0,76+$this->baseOp+$baseY-5,209);
        $this->pdf->SetFont('Arial', '', 5);
        $this->pdf->SetXY(171, 75+$this->baseOp+$baseY-5);
        $this->pdf->Write(10, utf8_decode('Emitido em '.date('d/m/Y H:i:s', strtotime($this->dataAcordo))));

        return $this;
    }
}
