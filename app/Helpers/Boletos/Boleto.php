<?php
namespace App\Helpers\Boletos;

use Codedge\Fpdf\Fpdf\Fpdf;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;

abstract class Boleto {
    protected $pdf;
    protected $largura;
    protected $altura;

    abstract public function escrever_dados_beneficiario($ben, $cnpj, $end);
    abstract public function escrever_vencimento($data);
    abstract public function escrever_valor($valor, $qnt);
    abstract public function escrever_nosso_numero($num);
    abstract public function escrever_carteira($cart);
    abstract public function escrever_agencia($ag);
    abstract public function escrever_data_doc($data);
    abstract public function escrever_num_doc($num);
    abstract public function escrever_desconto($d);
    abstract public function escrever_deducoes($d);
    abstract public function escrever_juros($j);
    abstract public function escrever_acrescimos($j);
    abstract public function escrever_cobrado($c);
    abstract public function escrever_pagador($nome, $cpf, $avalista=null);
    abstract public function escrever_cod_credor($ib);
    abstract public function escrever_linha_digitavel($linha);
    abstract public function escrever_cod_barras($linha);
    abstract public function escrever_qr_code($linha);

    public function criar_array(array $dados) {
        try {
            $linha = $dados['boleto']['linha_digitavel'];
            $this->escrever_dados_beneficiario($dados['credor']['nome'], $dados['credor']['cnpj'], $dados['credor']['endereco'])
                ->escrever_vencimento($dados['boleto']['nome'])
                ->escrever_valor($dados['boleto']['valor'], 1)
                ->escrever_nosso_numero($dados['boleto']['nosso_numero'])
                ->escrever_carteira()
                ->escrever_agencia()
                ->escrever_convenio()
                ->escrever_data_doc(Carbon::now()->format('d/m/Y'))
                ->escrever_num_doc('00000')
                ->escrever_desconto($dados['boleto']['desconto'])
                ->escrever_deducoes(0)
                ->escrever_juros($dados['boleto']['multa'])
                ->escrever_acrescimos(0)
                ->escrever_cobrado($dados['boleto']['valor'])
                ->escrever_pagador($dados['cliente']['nome'], $dados['cliente']['cpf_cnpj'], '')
                ->escrever_cod_credor('0000-0/000000')
                ->escrever_linha_digitavel($linha)
                ->escrever_cod_barras($linha)
                ->escrever_qr_code($linha)
                ->gerar();

            return $this;
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function gerar($output='S'){
        header("Access-Control-Allow-Origin: *");
        return $this->pdf->Output($output);
    }

    public function __construct($nomeimg) {
        $this->pdf = new Fpdf();
        $this->pdf->AddPage();

        $this->largura = $this->pdf->GetPageWidth();
        $this->altura = $this->pdf->GetPageHeight();
        $this->pdf->Image(Storage::path("public/images/$nomeimg"), 0, 0, $this->largura, $this->altura);
    }

    protected function linha_para_bol($linha) {
        return preg_replace('/(\d{4})(\d{5})\d{1}(\d{10})\d{1}(\d{10})\d{1}(\d{15})/', '$1$5$2$3$4', preg_replace("/\D/", "", $linha));
    }
}
