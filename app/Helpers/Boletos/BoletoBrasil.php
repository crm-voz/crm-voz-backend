<?php

namespace App\Helpers\Boletos;

use Picqer\Barcode\BarcodeGeneratorPNG;
use chillerlan\QRCode\{QRCode, QROptions};
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class BoletoBrasil extends Boleto{

    public function __construct() {
        parent::__construct('boleto-brasil.png');
    }

    public function escrever_dados_beneficiario($ben, $cnpj, $end) {
        // Escrever o nome do beneficiário
        $this->pdf->SetFont('Arial', 'B', 9);
        $this->pdf->SetXY(10, 19.6);
        $this->pdf->Write(10, utf8_decode($ben));

        $this->pdf->SetXY(70, 26);
        $this->pdf->Write(10, utf8_decode($cnpj));

        $this->pdf->SetXY(10.5, 92.5);
        $this->pdf->Write(10, utf8_decode("$ben CNPJ: $cnpj"));

        return $this;
    }

    public function escrever_vencimento($data) {
        $this->pdf->SetFont('Arial', 'B', 9);
        $this->pdf->SetXY(103, 26);
        $this->pdf->Write(10, utf8_decode($data));

        $this->pdf->SetXY(148, 85);
        $this->pdf->Write(10, utf8_decode($data));

        return $this;
    }

    public function escrever_valor($valor, $qnt) {
        $this->pdf->SetFont('Arial', 'B', 9);
        $this->pdf->SetXY(148, 26);
        $this->pdf->Write(10, utf8_decode('R$ ' . number_format($valor, 2, ',', '.')));

        $this->pdf->SetXY(148, 107);
        $this->pdf->Write(10, utf8_decode('R$ ' . number_format($valor, 2, ',', '.')));

        return $this;
    }

    public function escrever_nosso_numero($num) {
        $this->pdf->SetFont('Arial', 'B', 7);
        $this->pdf->SetXY(168, 19.5);
        $this->pdf->Write(10, utf8_decode($num));

        $this->pdf->SetXY(148, 100);
        $this->pdf->Write(10, utf8_decode($num));

        return $this;
    }

    public function escrever_carteira($cart='100') {
        $this->pdf->SetFont('Arial', 'B', 9);
        $this->pdf->SetXY(38, 107);
        $this->pdf->Write(10, utf8_decode($cart));

        return $this;
    }

    public function escrever_agencia($ag='00000-0') {
        $this->pdf->SetFont('Arial', 'B', 9);
        $this->pdf->SetXY(100, 19.5);
        $this->pdf->Write(10, utf8_decode($ag));

        $this->pdf->SetXY(148, 92.5);
        $this->pdf->Write(10, utf8_decode($ag));

        return $this;
    }

    public function escrever_convenio($conv) {
        // $this->pdf->SetFont('Arial', 'B', 8);
        // $this->pdf->SetXY(179, 24);
        // $this->pdf->Write(10, utf8_decode($conv));

        // $this->pdf->SetXY(179, 150);
        // $this->pdf->Write(10, utf8_decode($conv));

        return $this;
    }

    public function escrever_data_doc($data) {
        $this->pdf->SetFont('Arial', 'B', 9);
        $this->pdf->SetXY(11, 100);
        $this->pdf->Write(10, utf8_decode($data));

        $this->pdf->SetXY(110, 100);
        $this->pdf->Write(10, utf8_decode($data));

        return $this;
    }

    public function escrever_num_doc($num) {
        $this->pdf->SetFont('Arial', 'B', 9);
        $this->pdf->SetXY(11, 26);
        $this->pdf->Write(10, utf8_decode($num));

        $this->pdf->SetXY(38, 100);
        $this->pdf->Write(10, utf8_decode($num));

        return $this;
    }

    public function escrever_desconto($d) {
        $this->pdf->SetFont('Arial', 'B', 9);
        $this->pdf->SetXY(11, 33);
        $this->pdf->Write(10, utf8_decode('R$ ' . number_format($d, 2, ',', '.')));

        $this->pdf->SetXY(148, 115);
        $this->pdf->Write(10, utf8_decode('R$ ' . number_format($d, 2, ',', '.')));

        return $this;
    }

    public function escrever_deducoes($d) {
        $this->pdf->SetFont('Arial', 'B', 9);
        $this->pdf->SetXY(46.5, 33);
        $this->pdf->Write(10, utf8_decode('R$ ' . number_format($d, 2, ',', '.')));

        $this->pdf->SetXY(148, 122.5);
        $this->pdf->Write(10, utf8_decode('R$ ' . number_format($d, 2, ',', '.')));

        return $this;
    }

    public function escrever_juros($j) {
        $this->pdf->SetFont('Arial', 'B', 9);
        $this->pdf->SetXY(70, 33);
        $this->pdf->Write(10, utf8_decode('R$ ' . number_format($j, 2, ',', '.')));

        $this->pdf->SetXY(148, 130);
        $this->pdf->Write(10, utf8_decode('R$ ' . number_format($j, 2, ',', '.')));

        return $this;
    }

    public function escrever_acrescimos($j) {
        $this->pdf->SetFont('Arial', 'B', 9);
        $this->pdf->SetXY(108, 33);
        $this->pdf->Write(10, utf8_decode('R$ ' . number_format($j, 2, ',', '.')));

        $this->pdf->SetXY(148, 137);
        $this->pdf->Write(10, utf8_decode('R$ ' . number_format($j, 2, ',', '.')));

        return $this;
    }

    public function escrever_cobrado($c) {
        $this->pdf->SetFont('Arial', 'B', 9);
        $this->pdf->SetXY(148, 33);
        $this->pdf->Write(10, utf8_decode('R$ ' . number_format($c, 2, ',', '.')));

        $this->pdf->SetXY(148, 145);
        $this->pdf->Write(10, utf8_decode('R$ ' . number_format($c, 2, ',', '.')));

        return $this;
    }

    public function escrever_pagador($nome, $cpf, $avalista=null) {
        $this->pdf->SetFont('Arial', 'B', 10);
        $this->pdf->SetXY(10, 40);
        $this->pdf->Write(10, utf8_decode("$nome CPF/CNPJ: $cpf"));

        $this->pdf->SetFont('Arial', 'B', 9);
        $this->pdf->SetXY(10, 152);
        $this->pdf->Write(10, utf8_decode("$nome - CPF/CNPJ: $cpf"));

        if($avalista) {
            $this->pdf->SetXY(10, 166);
            $this->pdf->Write(10, utf8_decode($avalista));
        }

        return $this;
    }

    public function escrever_cod_credor($ib) {
        // $this->pdf->SetFont('Arial', 'B', 7);
        // $this->pdf->SetXY(143.5, 64.8);
        // $this->pdf->Write(10, utf8_decode($ib));

        return $this;
    }

    public function escrever_linha_digitavel($linha)
    {
        $this->pdf->SetFont('Arial', 'B', 11);
        $this->pdf->SetXY(78, 9.5);
        $this->pdf->Write(10, utf8_decode($linha));

        $this->pdf->SetXY(78, 74.5);
        $this->pdf->Write(10, utf8_decode($linha));

        return $this;
    }

    public function escrever_cod_barras($linha)
    {
        $gen = new BarcodeGeneratorPNG();
        $img = $gen->getBarcode($this->linha_para_bol($linha), $gen::TYPE_INTERLEAVED_2_5);
        $path = "public/barcodes/" . Str::random(9) . ".png";
        Storage::put($path, $img);
        $this->pdf->Image(Storage::path($path), 10, 180, 90, 13);
        Storage::delete($path);

        return $this;
    }

    public function escrever_qr_code($linha)
    {
        $this->pdf->SetXY(133, 187);
        $this->pdf->Write(10, utf8_decode('Pagamento via Pix'));

        $options = new QROptions([
            'version'      => 10,
            'outputType'   => QRCode::OUTPUT_IMAGE_PNG,
            'eccLevel'     => QRCode::ECC_L,
            'scale'        => 5,
            'imageBase64'  => false,
        ]);

        $qr = (new QRCode($options))->render($linha);
        $path = "public/qrcodes/" . Str::random(9) . ".png";
        Storage::put($path, $qr);
        // $this->pdf->Image(Storage::path($path), 170, 50, 15, 15);
        $this->pdf->Image(Storage::path($path), 168, 176, 33, 33);
        Storage::delete($path);

        return $this;
    }
}
