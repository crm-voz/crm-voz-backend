<?php

namespace App\Helpers\Boletos;

use Picqer\Barcode\BarcodeGeneratorPNG;
use chillerlan\QRCode\{QRCode, QROptions};
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class BoletoCaixa extends Boleto {

    public function __construct() {
        parent::__construct('boleto-caixa.png');
    }

    public function escrever_dados_beneficiario($ben, $cnpj, $end) {
        // Escrever o nome do beneficiário
        $this->pdf->SetFont('Arial', 'B', 8);
        $this->pdf->SetXY(12, 20);
        $this->pdf->Write(10, utf8_decode($ben));

        $this->pdf->SetXY(95, 20);
        $this->pdf->Write(10, utf8_decode($cnpj));

        $this->pdf->SetXY(12, 25);
        $this->pdf->Write(10, utf8_decode($end));

        $this->pdf->SetXY(12, 146);
        $this->pdf->Write(10, utf8_decode($ben));

        $this->pdf->SetXY(95, 146);
        $this->pdf->Write(10, utf8_decode($cnpj));

        $this->pdf->SetXY(12, 151);
        $this->pdf->Write(10, utf8_decode($end));

        return $this;
    }

    public function escrever_vencimento($data) {
        $this->pdf->SetFont('Arial', 'B', 8);
        $this->pdf->SetXY(175, 12);
        $this->pdf->Write(10, utf8_decode($data));

        $this->pdf->SetXY(175, 139);
        $this->pdf->Write(10, utf8_decode($data));

        return $this;
    }

    public function escrever_valor($valor, $qnt) {
        $this->pdf->SetFont('Arial', 'B', 8);
        $this->pdf->SetXY(175, 41);
        $this->pdf->Write(10, utf8_decode('R$ ' . number_format($valor, 2, ',', '.')));

        $this->pdf->SetXY(175, 168);
        $this->pdf->Write(10, utf8_decode('R$ ' . number_format($valor, 2, ',', '.')));

        return $this;
    }

    public function escrever_nosso_numero($num) {
        $this->pdf->SetFont('Arial', 'B', 8);
        $this->pdf->SetXY(160, 34);
        $this->pdf->Write(10, utf8_decode($num));

        $this->pdf->SetXY(160, 160);
        $this->pdf->Write(10, utf8_decode($num));

        return $this;
    }

    public function escrever_carteira($cart) {
        $this->pdf->SetFont('Arial', 'B', 8);
        $this->pdf->SetXY(40, 41);
        $this->pdf->Write(10, utf8_decode($cart));

        $this->pdf->SetXY(40, 167.5);
        $this->pdf->Write(10, utf8_decode($cart));

        return $this;
    }

    public function escrever_agencia($ag) {
        $this->pdf->SetFont('Arial', 'B', 8);
        $this->pdf->SetXY(170, 24);
        $this->pdf->Write(10, utf8_decode($ag).' / ');

        $this->pdf->SetXY(170, 150);
        $this->pdf->Write(10, utf8_decode($ag).' / ');

        return $this;
    }

    public function escrever_convenio($conv) {
        $this->pdf->SetFont('Arial', 'B', 8);
        $this->pdf->SetXY(179, 24);
        $this->pdf->Write(10, utf8_decode($conv));

        $this->pdf->SetXY(179, 150);
        $this->pdf->Write(10, utf8_decode($conv));

        return $this;
    }

    public function escrever_data_doc($data) {
        $this->pdf->SetFont('Arial', 'B', 8);
        $this->pdf->SetXY(12, 34);
        $this->pdf->Write(10, utf8_decode($data));

        $this->pdf->SetXY(100, 34);
        $this->pdf->Write(10, utf8_decode($data));

        $this->pdf->SetXY(12, 160);
        $this->pdf->Write(10, utf8_decode($data));

        $this->pdf->SetXY(100, 160);
        $this->pdf->Write(10, utf8_decode($data));

        return $this;
    }

    public function escrever_num_doc($num) {
        $this->pdf->SetFont('Arial', 'B', 8);
        $this->pdf->SetXY(35.8, 34);
        $this->pdf->Write(10, utf8_decode($num));

        $this->pdf->SetXY(35.8, 160);
        $this->pdf->Write(10, utf8_decode($num));

        return $this;
    }

    public function escrever_desconto($d) {
        $this->pdf->SetFont('Arial', 'B', 8);
        $this->pdf->SetXY(170, 50);
        $this->pdf->Write(10, utf8_decode('R$ ' . number_format($d, 2, ',', '.')));

        $this->pdf->SetXY(170, 176);
        $this->pdf->Write(10, utf8_decode('R$ ' . number_format($d, 2, ',', '.')));

        return $this;
    }

    public function escrever_deducoes($d) {
        $this->pdf->SetFont('Arial', 'B', 8);
        $this->pdf->SetXY(170, 58);
        $this->pdf->Write(10, utf8_decode('R$ ' . number_format($d, 2, ',', '.')));

        $this->pdf->SetXY(170, 184);
        $this->pdf->Write(10, utf8_decode('R$ ' . number_format($d, 2, ',', '.')));

        return $this;
    }

    public function escrever_juros($j) {
        $this->pdf->SetFont('Arial', 'B', 8);
        $this->pdf->SetXY(170, 66);
        $this->pdf->Write(10, utf8_decode('R$ ' . number_format($j, 2, ',', '.')));

        $this->pdf->SetXY(170, 192);
        $this->pdf->Write(10, utf8_decode('R$ ' . number_format($j, 2, ',', '.')));

        return $this;
    }

    public function escrever_acrescimos($j) {
        $this->pdf->SetFont('Arial', 'B', 8);
        $this->pdf->SetXY(170, 74);
        $this->pdf->Write(10, utf8_decode('R$ ' . number_format($j, 2, ',', '.')));

        $this->pdf->SetXY(170, 200);
        $this->pdf->Write(10, utf8_decode('R$ ' . number_format($j, 2, ',', '.')));

        return $this;
    }

    public function escrever_cobrado($c) {
        $this->pdf->SetFont('Arial', 'B', 8);
        $this->pdf->SetXY(170, 81);
        $this->pdf->Write(10, utf8_decode('R$ ' . number_format($c, 2, ',', '.')));

        $this->pdf->SetXY(170, 208);
        $this->pdf->Write(10, utf8_decode('R$ ' . number_format($c, 2, ',', '.')));

        return $this;
    }

    public function escrever_pagador($nome, $cpf, $avalista=null) {
        $this->pdf->SetFont('Arial', 'B', 8);
        $this->pdf->SetXY(30, 86);
        $this->pdf->Write(10, utf8_decode($nome).'- CPF:'.utf8_decode($cpf));

        $this->pdf->SetXY(30, 99.3);
        $this->pdf->Write(10, utf8_decode($avalista));

        $this->pdf->SetXY(30, 213);
        $this->pdf->Write(10, utf8_decode($nome).'- CPF:'.utf8_decode($cpf));

        $this->pdf->SetXY(30, 226);
        $this->pdf->Write(10, utf8_decode($avalista));

        return $this;
    }

    public function escrever_cod_credor($ib) {
        // aqui vai ser feito a consulta do credor e seu texto de instruções
        // $this->pdf->SetFont('Arial', 'B', 8);
        // $this->pdf->SetXY(143.5, 64.8);
        // $this->pdf->Write(10, utf8_decode($ib));

        return $this;
    }

    public function escrever_linha_digitavel($linha)
    {
        $this->pdf->SetFont('Arial', 'B', 10);
        $this->pdf->SetXY(65, 3);
        $this->pdf->Write(10, utf8_decode($linha));

        $this->pdf->SetXY(65, 130);
        $this->pdf->Write(10, utf8_decode($linha));

        return $this;
    }

    public function escrever_cod_barras($linha)
    {
        $gen = new BarcodeGeneratorPNG();
        $img = $gen->getBarcode($this->linha_para_bol($linha), $gen::TYPE_INTERLEAVED_2_5);
        $path = "public/barcodes/" . Str::random(9) . ".png";
        Storage::put($path, $img);
        $this->pdf->Image(Storage::path($path), 11, 238, 100, 15);
        Storage::delete($path);

        return $this;
    }

    public function escrever_qr_code($linha)
    {
        // $options = new QROptions([
        //     'version'      => 10,
        //     'outputType'   => QRCode::OUTPUT_IMAGE_PNG,
        //     'eccLevel'     => QRCode::ECC_L,
        //     'scale'        => 5,
        //     'imageBase64'  => false,
        // ]);

        // $qr = (new QRCode($options))->render($linha);
        // $path = "public/qrcodes/" . Str::random(9) . ".png";
        // Storage::put($path, $qr);
        // $this->pdf->Image(Storage::path($path), 170, 238, 28, 28);
        // Storage::delete($path);

        // $this->pdf->SetXY(131, 238);
        // $this->pdf->Write(10, utf8_decode('Pagamento via Pix'));

        return $this;
    }
}
