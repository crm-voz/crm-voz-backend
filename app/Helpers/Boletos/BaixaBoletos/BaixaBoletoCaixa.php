<?php

namespace App\Helpers\Boletos\BaixaBoletos;

use App\Repository\Administrador\BaixarBoletoRepository;
use App\Repository\Cobranca\ParcelaRepository;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class BaixaBoletoCaixa implements IBaixaBoleto
{
    public function __construct(ParcelaRepository $parc, BaixarBoletoRepository $baixarBoletoRepository)
    {
        $this->parcelaRepository = $parc;
        $this->baixarBoletoRepository = $baixarBoletoRepository;
    }

    public function baixar(Request $request)
    {
        $file = $request->file('remessa');
        $arquivo = file($file);
        $nome_arquivo = $request->file('remessa')->getClientOriginalName();
        $linha = count($arquivo);
        $numero_banco = substr($arquivo[0], 0, 3);
        $nome_banco = substr($arquivo[0], 102, 30);
        $lenght = $arquivo[0];
        $cnab = Str::length($lenght)-2;
        $convenio = substr($arquivo[0], 58, 6);
        $quantidade_baixada = 0;
        $valor_total_baixado = 0;

        try {
            DB::beginTransaction();
            for ($i=2; $i < $linha-1; $i++){

                if (substr($arquivo[$i], 13, 1) == 'T'){
                    $nosso_numero = substr($arquivo[$i], 39, 17);
                }
                if (substr($arquivo[$i], 13, 1) == 'U'){
                    $valor_lancado = doubleval(substr($arquivo[$i], 77, 15))/100;
                }

                $cod_liquidacao = substr($arquivo[$i], 15, 2);

                $boleto = $this->baixarBoletoRepository->getBoleto($nosso_numero, $convenio);
                if (!$boleto)
                    continue;

                $parcelas = $this->parcelaRepository->where('id','=',$boleto->parcelas->id);
                if (!$parcelas)
                    throw new Exception("Parcela referente ao boleto $boleto->id não foi encontrada.");

                if (isset($nosso_numero) == isset($boleto->nosso_numero)){
                    if($cod_liquidacao == '06'){
                        $data_baixa = $request->input('data_baixa',Carbon::createFromFormat('d/m/Y', $request?->data_baixa)->format('Y-m-d H:i:s'));
                        $valor_pago = $valor_lancado;
                        $valor_comissao = 0; // existe no arquivo de retorno?
                        $this->parcelaRepository->incluirBaixa($parcelas, $data_baixa, $valor_comissao, $valor_pago);

                        $quantidade_baixada = $quantidade_baixada+1;
                        $valor_total_baixado = $valor_total_baixado + $valor_lancado;
                    }
                }

            }

            $this->baixarBoletoRepository->store($nome_arquivo,$nome_banco,$convenio,$cnab,$quantidade_baixada,$valor_total_baixado);
            DB::commit();
            return ['result' => true];
        } catch (\Throwable $th) {
            logger($th->getMessage());
            DB::rollBack();
            return ['result' => false, 'erro' => $th->getMessage()];
        }

    }


}
