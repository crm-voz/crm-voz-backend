<?php

namespace App\Helpers\Boletos\BaixaBoletos;

use Illuminate\Http\Request;

interface IBaixaBoleto{

    //baixar
    public function baixar(Request $request);

}
