<?php

namespace App\Helpers\Boletos\BaixaBoletos;

use App\Repository\Administrador\BaixarBoletoRepository;
use App\Repository\Cobranca\ParcelaRepository;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class BaixaBoletoBradesco implements IBaixaBoleto
{
    public function __construct(ParcelaRepository $parc, BaixarBoletoRepository $baixarBoletoRepository)
    {
        $this->parcelaRepository = $parc;
        $this->baixarBoletoRepository = $baixarBoletoRepository;
    }

    public function baixar(Request $request)
    {
        $file = $request->file('remessa');
        $arquivo = file($file);
        $nome_arquivo = $request->file('remessa')->getClientOriginalName();
        $tipo_retorno = substr($arquivo[0], 1, 1);
        $agencia_banco = substr($arquivo[1], 25, 4);
        $conta_banco = substr($arquivo[1], 30, 6);
        $digito_conta = substr($arquivo[1], 36, 1);
        $numero_banco = substr($arquivo[0], 76, 3);
        $nome_banco = substr($arquivo[0], 79, 15);
        $nome_empresa = substr($arquivo[0], 46, 30);
        $convenio = substr($arquivo[0], 39, 7);
        $linha = count($arquivo);
        $lenght = $arquivo[0];
        $cnab = Str::length($lenght)-2;
        $quantidade_baixada = 0;
        $valor_total_baixado = 0;
        try {
            DB::beginTransaction();
            for ($i=1; $i < $linha-1; $i++) {
                $nosso_numero = substr($arquivo[$i], 70, 11);
                $valor_lancado = doubleval(substr($arquivo[$i], 253, 11).'.'.substr($arquivo[$i], 264, 2));
                $vencimento = substr($arquivo[$i], 146, 2).'-'.substr($arquivo[$i], 148, 2).'-'.'20'.substr($arquivo[$i], 150, 2);
                $cod_liquidacao = substr($arquivo[$i], 108, 2);

                $boleto = $this->baixarBoletoRepository->getBoleto($nosso_numero, $convenio);
                if (!$boleto)
                    continue;

                $parcelas = $this->parcelaRepository->where('id','=',$boleto->parcelas->id);
                if (!$parcelas)
                    throw new Exception("Parcela referente ao boleto $boleto->id não foi encontrada.");

                if (isset($nosso_numero) == isset($boleto->nosso_numero)){
                    if($cod_liquidacao == '06'){
                        $data_baixa = $request->input('data_baixa',Carbon::createFromFormat('d/m/Y', $request?->data_baixa)->format('Y-m-d H:i:s'));
                        $valor_pago = $valor_lancado;
                        $valor_comissao = 0; // existe no arquivo de retorno?
                        $this->parcelaRepository->incluirBaixa($parcelas, $data_baixa, $valor_comissao, $valor_pago);

                        $quantidade_baixada = $quantidade_baixada+1;
                        $valor_total_baixado = $valor_total_baixado + $valor_lancado;
                    }
                }
            }

            $this->baixarBoletoRepository->store($nome_arquivo,$nome_banco,$convenio,$cnab,$quantidade_baixada,$valor_total_baixado);
            DB::commit();
            return ['result' => true];
        } catch (\Throwable $th) {
            logger($th->getMessage());
            DB::rollBack();
            return ['result' => false, 'erro' => $th->getMessage()];
        }

    }


}
