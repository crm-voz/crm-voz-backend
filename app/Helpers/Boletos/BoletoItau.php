<?php

namespace App\Helpers\Boletos;

use Picqer\Barcode\BarcodeGeneratorPNG;
use chillerlan\QRCode\{QRCode, QROptions};
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class BoletoItau extends Boleto{

    public function __construct() {
        parent::__construct('boleto-itau.png');
    }

    public function escrever_dados_beneficiario($ben, $cnpj, $end) {
        // Escrever o nome do beneficiário
        $this->pdf->SetFont('Arial', 'B', 10);
        $this->pdf->SetXY(13.5, 19.2);
        $this->pdf->Write(10, utf8_decode($ben));

        $this->pdf->SetXY(13.5, 64.5);
        $this->pdf->Write(10, utf8_decode($ben));

        // Escrever o CNPJ do beneficiário
        $this->pdf->SetFont('Arial', 'B', 8);
        $this->pdf->SetXY(77.3, 19.2);
        $this->pdf->Write(10, utf8_decode($cnpj));

        // Escrever o endereço do beneficiário
        $this->pdf->SetFont('Arial', 'B', 7);
        $this->pdf->SetXY(13.5, 24.9);
        $this->pdf->Write(10, utf8_decode($end));

        return $this;
    }

    public function escrever_vencimento($data) {
        $this->pdf->SetFont('Arial', 'B', 7);
        $this->pdf->SetXY(162, 19.2);
        $this->pdf->Write(10, utf8_decode($data));

        $this->pdf->SetXY(143.2, 59.2);
        $this->pdf->Write(10, utf8_decode($data));

        return $this;
    }

    public function escrever_valor($valor, $qnt) {
        $this->pdf->SetFont('Arial', 'B', 7);
        $this->pdf->SetXY(102.7, 30.3);
        $this->pdf->Write(10, utf8_decode('R$ ' . number_format($valor, 2, ',', '.')));

        $this->pdf->SetXY(90, 75.5);
        $this->pdf->Write(10, utf8_decode('R$ ' . number_format($valor, 2, ',', '.')));

        $this->pdf->SetXY(162, 36);
        $this->pdf->Write(10, utf8_decode('R$ ' . number_format($valor, 2, ',', '.')));

        $this->pdf->SetXY(143, 75.8);
        $this->pdf->Write(10, utf8_decode('R$ ' . number_format($valor, 2, ',', '.')));

        $this->pdf->SetFont('Arial', 'B', 7);
        $this->pdf->SetXY(75, 30.5);
        $this->pdf->Write(10, utf8_decode(strval($qnt)));

        $this->pdf->SetXY(67.7, 75.7);
        $this->pdf->Write(10, utf8_decode(strval($qnt)));

        return $this;
    }

    public function escrever_nosso_numero($num) {
        $this->pdf->SetFont('Arial', 'B', 7);
        $this->pdf->SetXY(13.5, 30);
        $this->pdf->Write(10, utf8_decode($num));

        $this->pdf->SetXY(143.5, 70.2);
        $this->pdf->Write(10, utf8_decode($num));

        return $this;
    }

    public function escrever_carteira($cart='100') {
        $this->pdf->SetFont('Arial', 'B', 7);
        $this->pdf->SetXY(36, 30);
        $this->pdf->Write(10, utf8_decode($cart));

        $this->pdf->SetXY(38, 75.8);
        $this->pdf->Write(10, utf8_decode($cart));

        return $this;
    }

    public function escrever_agencia($ag='00000-0') {
        $this->pdf->SetFont('Arial', 'B', 7);
        $this->pdf->SetXY(162, 30.3);
        $this->pdf->Write(10, utf8_decode($ag));

        return $this;
    }

    public function escrever_convenio($conv) {
        // $this->pdf->SetFont('Arial', 'B', 8);
        // $this->pdf->SetXY(179, 24);
        // $this->pdf->Write(10, utf8_decode($conv));

        // $this->pdf->SetXY(179, 150);
        // $this->pdf->Write(10, utf8_decode($conv));

        return $this;
    }

    public function escrever_data_doc($data) {
        $this->pdf->SetFont('Arial', 'B', 7);
        $this->pdf->SetXY(13.8, 36);
        $this->pdf->Write(10, utf8_decode($data));

        $this->pdf->SetXY(95, 36);
        $this->pdf->Write(10, utf8_decode($data));

        $this->pdf->SetXY(13.5, 70.5);
        $this->pdf->Write(10, utf8_decode($data));

        $this->pdf->SetXY(88.5, 70.5);
        $this->pdf->Write(10, utf8_decode($data));

        return $this;
    }

    public function escrever_num_doc($num) {
        $this->pdf->SetFont('Arial', 'B', 7);
        $this->pdf->SetXY(35.8, 36);
        $this->pdf->Write(10, utf8_decode($num));

        $this->pdf->SetXY(37, 70.5);
        $this->pdf->Write(10, utf8_decode($num));

        return $this;
    }

    public function escrever_desconto($d) {
        $this->pdf->SetFont('Arial', 'B', 7);
        $this->pdf->SetXY(143, 81.5);
        $this->pdf->Write(10, utf8_decode('R$ ' . number_format($d, 2, ',', '.')));

        return $this;
    }

    public function escrever_deducoes($d) {
        $this->pdf->SetFont('Arial', 'B', 7);
        $this->pdf->SetXY(143, 87);
        $this->pdf->Write(10, utf8_decode('R$ ' . number_format($d, 2, ',', '.')));

        return $this;
    }

    public function escrever_juros($j) {
        $this->pdf->SetFont('Arial', 'B', 7);
        $this->pdf->SetXY(143, 93);
        $this->pdf->Write(10, utf8_decode('R$ ' . number_format($j, 2, ',', '.')));

        return $this;
    }

    public function escrever_acrescimos($j) {
        $this->pdf->SetFont('Arial', 'B', 7);
        $this->pdf->SetXY(143, 98.5);
        $this->pdf->Write(10, utf8_decode('R$ ' . number_format($j, 2, ',', '.')));

        return $this;
    }

    public function escrever_cobrado($c) {
        $this->pdf->SetFont('Arial', 'B', 7);
        $this->pdf->SetXY(143, 103.5);
        $this->pdf->Write(10, utf8_decode('R$ ' . number_format($c, 2, ',', '.')));

        return $this;
    }

    public function escrever_pagador($nome, $cpf, $avalista=null) {
        $this->pdf->SetFont('Arial', 'B', 7);
        $this->pdf->SetXY(103, 19.5);
        $this->pdf->Write(10, utf8_decode($nome));

        $this->pdf->SetXY(13.8, 109);
        $this->pdf->Write(10, utf8_decode($nome));

        $this->pdf->SetXY(156, 109.7);
        $this->pdf->Write(10, utf8_decode($cpf));

        if($avalista) {
            $this->pdf->SetXY(31, 116);
            $this->pdf->Write(10, utf8_decode($avalista));
        }

        return $this;
    }

    public function escrever_cod_credor($ib) {
        $this->pdf->SetFont('Arial', 'B', 7);
        $this->pdf->SetXY(143.5, 64.8);
        $this->pdf->Write(10, utf8_decode($ib));

        return $this;
    }

    public function escrever_linha_digitavel($linha)
    {
        $this->pdf->SetFont('Arial', 'B', 12);
        $this->pdf->SetXY(62, 12.5);
        $this->pdf->Write(10, utf8_decode($linha));

        $this->pdf->SetXY(62, 52.5);
        $this->pdf->Write(10, utf8_decode($linha));

        return $this;
    }

    public function escrever_cod_barras($linha)
    {
        $gen = new BarcodeGeneratorPNG();
        $img = $gen->getBarcode($this->linha_para_bol($linha), $gen::TYPE_INTERLEAVED_2_5);
        $path = "public/barcodes/" . Str::random(9) . ".png";
        Storage::put($path, $img);
        $this->pdf->Image(Storage::path($path), 14, 127, 120, 15);
        Storage::delete($path);

        return $this;
    }

    public function escrever_qr_code($linha)
    {
        // $this->pdf->SetXY(131, 145);
        // $this->pdf->Write(10, utf8_decode('Pagamento via Pix'));

        // $options = new QROptions([
        //     'version'      => 10,
        //     'outputType'   => QRCode::OUTPUT_IMAGE_PNG,
        //     'eccLevel'     => QRCode::ECC_L,
        //     'scale'        => 5,
        //     'imageBase64'  => false,
        // ]);

        // $qr = (new QRCode($options))->render($linha);
        // $path = "public/qrcodes/" . Str::random(9) . ".png";
        // Storage::put($path, $qr);
        // $this->pdf->Image(Storage::path($path), 170, 125, 33, 33);
        // Storage::delete($path);

        return $this;
    }
}
