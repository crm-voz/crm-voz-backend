<?php

namespace App\Helpers\Boletos;

use App\Models\BackOffice\Cliente;
use Picqer\Barcode\BarcodeGeneratorPNG;
use chillerlan\QRCode\{QRCode, QROptions};
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class BoletoSicoob extends Boleto {

    public function __construct() {
        parent::__construct('boleto-sicoob.png');
    }

    public function escrever_dados_beneficiario($ben, $cnpj, $end) {
        // Escrever o nome do beneficiário
        $this->pdf->SetFont('Arial', '', 9);
        $this->pdf->SetXY(11, 18);
        $this->pdf->Write(10, utf8_decode($ben));

        $this->pdf->SetXY(11, 22);
        $this->pdf->Write(10, utf8_decode($end));

        $this->pdf->SetXY(90, 22);
        $this->pdf->Write(10, utf8_decode($cnpj));

        $this->pdf->SetXY(11, 162);
        $this->pdf->Write(10, utf8_decode($ben));

        $this->pdf->SetXY(90, 162);
        $this->pdf->Write(10, utf8_decode($cnpj));

        return $this;
    }

    public function escrever_vencimento($data) {
        $this->pdf->SetFont('Arial', '', 9);
        $this->pdf->SetXY(130, 26);
        $this->pdf->Write(10, utf8_decode($data));

        $this->pdf->SetXY(174, 148);
        $this->pdf->Write(10, utf8_decode($data));

        return $this;
    }

    public function escrever_valor($valor, $qnt) {
        $preco = utf8_decode(number_format($valor, 2, ',', '.'));
        $str_len = $this->pdf->GetStringWidth($preco);

        $this->pdf->SetFont('Arial', '', 9);
        $this->pdf->SetXY(168, 26);
        $this->pdf->Write(10, $preco);

        $this->pdf->SetXY(190 - $str_len, 182);
        $this->pdf->Write(10, $preco);

        return $this;
    }

    public function escrever_nosso_numero($num) {
        $this->pdf->SetFont('Arial', '', 9);
        $this->pdf->SetXY(145, 56);
        $this->pdf->Write(10, utf8_decode($num));

        $this->pdf->SetXY(190 - ($this->pdf->GetStringWidth(utf8_decode($num))), 173);
        $this->pdf->Write(10, utf8_decode($num));

        return $this;
    }

    public function escrever_carteira($cart='100') {
        $this->pdf->SetFont('Arial', '', 9);
        $this->pdf->SetXY(45, 184);
        $this->pdf->Write(10, utf8_decode($cart));

        return $this;
    }

    public function escrever_agencia($ag='00000-0') {
        // $this->pdf->SetFont('Arial', 'B', 8);
        // $this->pdf->SetXY(95, 55);
        // $this->pdf->Write(10, utf8_decode($ag));

        // $this->pdf->SetXY(153, 135);
        // $this->pdf->Write(10, utf8_decode($ag));

        return $this;
    }

    public function escrever_convenio($conv) {
        // $this->pdf->SetFont('Arial', 'B', 8);
        // $this->pdf->SetXY(103, 55);
        // $this->pdf->Write(10, utf8_decode('/ '.$conv));

        // $this->pdf->SetXY(160, 135);
        // $this->pdf->Write(10, utf8_decode('/ '.$conv));

        return $this;
    }

    public function escrever_data_doc($data) {
        $this->pdf->SetFont('Arial', '', 9);
        $this->pdf->SetXY(17, 173);
        $this->pdf->Write(10, utf8_decode($data));

        return $this;
    }

    public function escrever_num_doc($num) {
        $this->pdf->SetFont('Arial', '', 9);
        $this->pdf->SetXY(50, 173);
        $this->pdf->Write(10, utf8_decode($num));

        $this->pdf->SetXY(167, 73);
        $this->pdf->Write(10, utf8_decode('100'));

        return $this;
    }

    public function escrever_desconto($d) {
        $this->pdf->SetFont('Arial', '', 9);
        $this->pdf->SetXY(23, 47);
        $this->pdf->Write(10, utf8_decode(number_format($d, 2, ',', '.')));

        $this->pdf->SetXY(182, 194);
        $this->pdf->Write(10, utf8_decode(number_format($d, 2, ',', '.')));

        return $this;
    }

    public function escrever_deducoes($d) {
        $this->pdf->SetFont('Arial', '', 9);
        $this->pdf->SetXY(60, 47);
        $this->pdf->Write(10, utf8_decode(number_format($d, 2, ',', '.')));

        $this->pdf->SetXY(182, 205);
        $this->pdf->Write(10, utf8_decode(number_format($d, 2, ',', '.')));

        return $this;
    }

    public function escrever_juros($j) {
        $this->pdf->SetFont('Arial', '', 9);
        $this->pdf->SetXY(97, 47);
        $this->pdf->Write(10, utf8_decode(number_format($j, 2, ',', '.')));

        $this->pdf->SetXY(182, 216);
        $this->pdf->Write(10, utf8_decode(number_format($j, 2, ',', '.')));

        return $this;
    }

    public function escrever_acrescimos($j) {
        $this->pdf->SetFont('Arial', '', 9);
        $this->pdf->SetXY(134, 47);
        $this->pdf->Write(10, utf8_decode(number_format($j, 2, ',', '.')));

        $this->pdf->SetXY(182, 227);
        $this->pdf->Write(10, utf8_decode(number_format($j, 2, ',', '.')));

        return $this;
    }

    public function escrever_cobrado($c) {
        $this->pdf->SetFont('Arial', 'B', 8);
        $this->pdf->SetXY(169, 47);
        $this->pdf->Write(10, utf8_decode(number_format($c, 2, ',', '.')));

        $this->pdf->SetXY(182, 238);
        $this->pdf->Write(10, utf8_decode(number_format($c, 2, ',', '.')));

        return $this;
    }

    public function escrever_pagador($nome, $cpf, $avalista=null) {
        $cliente = Cliente::where('cpf_cnpj',  $cpf)->first();
        //dd($cliente->cidades->nome, $cliente->cidades->uf);
        $this->pdf->SetFont('Arial', '', 9);
        $this->pdf->SetXY(11, 72);
        $this->pdf->Write(10, utf8_decode($cliente->nome));

        $this->pdf->SetXY(11, 84);
        $this->pdf->Write(10, utf8_decode($cliente->endereco));

        $this->pdf->SetXY(11, 95);
        $this->pdf->Write(10, utf8_decode($cliente->bairro));

        $this->pdf->SetXY(11, 106);
        $this->pdf->Write(10, utf8_decode($cliente->cidades->nome));

        $this->pdf->SetXY(140, 106);
        $this->pdf->Write(10, utf8_decode($cliente->cidades->uf));

        $this->pdf->SetXY(160, 106);
        $this->pdf->Write(10, utf8_decode($cliente->cep));

        // SECUNDARIO
        $this->pdf->SetXY(20, 227);
        $this->pdf->Write(10, utf8_decode($nome));

        $this->pdf->SetXY(100, 227);
        $this->pdf->Write(10, utf8_decode($cpf));

        $this->pdf->SetXY(20, 230);
        $this->pdf->Write(10, utf8_decode($cliente->endereco));

        $this->pdf->SetXY(20, 233);
        $this->pdf->Write(10, utf8_decode($cliente->bairro));

        $this->pdf->SetXY(20, 236);
        $this->pdf->Write(10, utf8_decode($cliente->cidades->nome . ' - ' .$cliente->cidades->uf));

        $this->pdf->SetXY(100, 236);
        $this->pdf->Write(10, utf8_decode($cliente->cep));

        // if($avalista) {
        //     $this->pdf->SetXY(28, 222.6);
        //     $this->pdf->Write(8, utf8_decode($avalista));
        // }

        return $this;
    }

    public function escrever_cod_credor($ib) {
        // $this->pdf->SetFont('Arial', 'B', 7);
        // $this->pdf->SetXY(143.5, 64.8);
        // $this->pdf->Write(10, utf8_decode($ib));

        return $this;
    }

    public function escrever_linha_digitavel($linha)
    {
        $this->pdf->SetFont('Arial', 'B', 9);
        $this->pdf->SetXY(98, 141);
        $this->pdf->Write(10, utf8_decode($linha));

        $this->pdf->SetFont('Arial', '', 9);
        $this->pdf->SetXY(11, 148);
        $this->pdf->Write(15, utf8_decode('PAGÁVEL PREFERENCIALMENTE NO SICOOB'));

        $this->pdf->SetXY(70, 171);
        $this->pdf->Write(15, utf8_decode('DM'));

        $this->pdf->SetXY(86, 171);
        $this->pdf->Write(15, utf8_decode('N'));

        $this->pdf->SetXY(54, 182);
        $this->pdf->Write(15, utf8_decode('REAL'));

        return $this;
    }

    public function escrever_cod_barras($linha)
    {
        $gen = new BarcodeGeneratorPNG();
        $img = $gen->getBarcode($this->linha_para_bol($linha), $gen::TYPE_INTERLEAVED_2_5);
        $path = "public/barcodes/" . Str::random(9) . ".png";
        Storage::put($path, $img);
        $this->pdf->Image(Storage::path($path), 11, 252, 109, 14);
        Storage::delete($path);

        return $this;
    }

    public function escrever_qr_code($linha)
    {
        // $options = new QROptions([
        //     'version'      => 10,
        //     'outputType'   => QRCode::OUTPUT_IMAGE_PNG,
        //     'eccLevel'     => QRCode::ECC_L,
        //     'scale'        => 5,
        //     'imageBase64'  => false,
        // ]);

        // $qr = (new QRCode($options))->render($linha);
        // $path = "public/qrcodes/" . Str::random(9) . ".png";
        // Storage::put($path, $qr);
        // $this->pdf->Image(Storage::path($path), 170, 270, 28, 28);
        // Storage::delete($path);

        // $this->pdf->SetXY(131, 266);
        // $this->pdf->Write(10, utf8_decode('Pagamento via Pix'));

        return $this;
    }
}
