<?php

namespace App\Helpers\Boletos;

use Picqer\Barcode\BarcodeGeneratorPNG;
use chillerlan\QRCode\{QRCode, QROptions};
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class BoletoBradesco extends Boleto {

    public function __construct() {
        parent::__construct('boleto-bradesco.png');
    }

    public function escrever_dados_beneficiario($ben, $cnpj, $end) {
        // Escrever o nome do beneficiário
        $this->pdf->SetFont('Arial', 'B', 8);
        $this->pdf->SetXY(8, 55);
        $this->pdf->Write(10, utf8_decode($ben));

        $this->pdf->SetXY(70, 72);
        $this->pdf->Write(10, utf8_decode($cnpj));

        $this->pdf->SetXY(8, 135);
        $this->pdf->Write(10, utf8_decode($ben));

        $this->pdf->SetXY(8, 139);
        $this->pdf->Write(10, utf8_decode('CNPJ: ' . $cnpj));

        return $this;
    }

    public function escrever_vencimento($data) {
        $this->pdf->SetFont('Arial', 'B', 8);
        $this->pdf->SetXY(113, 72);
        $this->pdf->Write(10, utf8_decode($data));

        $this->pdf->SetXY(151, 124);
        $this->pdf->Write(10, utf8_decode($data));

        return $this;
    }

    public function escrever_valor($valor, $qnt) {
        $this->pdf->SetFont('Arial', 'B', 8);
        $this->pdf->SetXY(150, 72);
        $this->pdf->Write(10, utf8_decode('R$ ' . number_format($valor, 2, ',', '.')));

        $this->pdf->SetXY(150, 159);
        $this->pdf->Write(10, utf8_decode('R$ ' . number_format($valor, 2, ',', '.')));

        return $this;
    }

    public function escrever_nosso_numero($num) {
        $this->pdf->SetFont('Arial', 'B', 8);
        $this->pdf->SetXY(170, 56);
        $this->pdf->Write(10, utf8_decode($num));

        $this->pdf->SetXY(150, 151);
        $this->pdf->Write(10, utf8_decode($num));

        return $this;
    }

    public function escrever_carteira($cart='100') {
        $this->pdf->SetFont('Arial', 'B', 8);
        $this->pdf->SetXY(50, 159);
        $this->pdf->Write(10, utf8_decode($cart));

        return $this;
    }

    public function escrever_agencia($ag='00000-0') {
        $this->pdf->SetFont('Arial', 'B', 8);
        $this->pdf->SetXY(95, 55);
        $this->pdf->Write(10, utf8_decode($ag));

        $this->pdf->SetXY(153, 135);
        $this->pdf->Write(10, utf8_decode($ag));

        // $this->pdf->SetXY(149, 197.8);
        // $this->pdf->Write(10, utf8_decode($ag));

        return $this;
    }

    public function escrever_convenio($conv) {
        $this->pdf->SetFont('Arial', 'B', 8);
        $this->pdf->SetXY(103, 55);
        $this->pdf->Write(10, utf8_decode('/ '.$conv));

        $this->pdf->SetXY(160, 135);
        $this->pdf->Write(10, utf8_decode('/ '.$conv));

        return $this;
    }

    public function escrever_data_doc($data) {
        $this->pdf->SetFont('Arial', 'B', 8);
        $this->pdf->SetXY(8, 151);
        $this->pdf->Write(10, utf8_decode($data));

        return $this;
    }

    public function escrever_num_doc($num) {
        $this->pdf->SetFont('Arial', 'B', 8);
        $this->pdf->SetXY(8, 72);
        $this->pdf->Write(10, utf8_decode($num));

        $this->pdf->SetXY(50, 151);
        $this->pdf->Write(10, utf8_decode($num));

        return $this;
    }

    public function escrever_desconto($d) {
        $this->pdf->SetFont('Arial', 'B', 8);
        $this->pdf->SetXY(8, 80);
        $this->pdf->Write(10, utf8_decode('R$ ' . number_format($d, 2, ',', '.')));

        $this->pdf->SetXY(150, 166);
        $this->pdf->Write(10, utf8_decode('R$ ' . number_format($d, 2, ',', '.')));

        return $this;
    }

    public function escrever_deducoes($d) {
        $this->pdf->SetFont('Arial', 'B', 8);
        $this->pdf->SetXY(47, 80);
        $this->pdf->Write(10, utf8_decode('R$ ' . number_format($d, 2, ',', '.')));

        $this->pdf->SetXY(150, 174);
        $this->pdf->Write(10, utf8_decode('R$ ' . number_format($d, 2, ',', '.')));

        return $this;
    }

    public function escrever_juros($j) {
        $this->pdf->SetFont('Arial', 'B', 8);
        $this->pdf->SetXY(80, 80);
        $this->pdf->Write(10, utf8_decode('R$ ' . number_format($j, 2, ',', '.')));

        $this->pdf->SetXY(150, 182);
        $this->pdf->Write(10, utf8_decode('R$ ' . number_format($j, 2, ',', '.')));

        return $this;
    }

    public function escrever_acrescimos($j) {
        $this->pdf->SetFont('Arial', 'B', 8);
        $this->pdf->SetXY(116, 80);
        $this->pdf->Write(10, utf8_decode('R$ ' . number_format($j, 2, ',', '.')));

        $this->pdf->SetXY(150, 190);
        $this->pdf->Write(10, utf8_decode('R$ ' . number_format($j, 2, ',', '.')));

        return $this;
    }

    public function escrever_cobrado($c) {
        $this->pdf->SetFont('Arial', 'B', 8);
        $this->pdf->SetXY(150, 80);
        $this->pdf->Write(10, utf8_decode('R$ ' . number_format($c, 2, ',', '.')));

        $this->pdf->SetXY(150, 198);
        $this->pdf->Write(10, utf8_decode('R$ ' . number_format($c, 2, ',', '.')));

        return $this;
    }

    public function escrever_pagador($nome, $cpf, $avalista=null) {
        $this->pdf->SetFont('Arial', 'B', 8);
        $this->pdf->SetXY(8, 87.5);
        $this->pdf->Write(10, utf8_decode($nome));

        $this->pdf->SetXY(8, 208);
        $this->pdf->Write(10, utf8_decode($nome));
        $this->pdf->SetXY(8, 212);
        $this->pdf->Write(10, utf8_decode('CPF: '.$cpf));

        if($avalista) {
            $this->pdf->SetXY(28, 222.6);
            $this->pdf->Write(8, utf8_decode($avalista));
        }

        return $this;
    }

    public function escrever_cod_credor($ib) {
        // $this->pdf->SetFont('Arial', 'B', 7);
        // $this->pdf->SetXY(143.5, 64.8);
        // $this->pdf->Write(10, utf8_decode($ib));

        return $this;
    }

    public function escrever_linha_digitavel($linha)
    {
        $this->pdf->SetFont('Arial', 'B', 12);
        $this->pdf->SetXY(73, 43);
        $this->pdf->Write(10, utf8_decode($linha));

        $this->pdf->SetXY(73, 114.5);
        $this->pdf->Write(10, utf8_decode($linha));


        return $this;
    }

    public function escrever_cod_barras($linha)
    {
        $gen = new BarcodeGeneratorPNG();
        $img = $gen->getBarcode($this->linha_para_bol($linha), $gen::TYPE_INTERLEAVED_2_5);
        $path = "public/barcodes/" . Str::random(9) . ".png";
        Storage::put($path, $img);
        $this->pdf->Image(Storage::path($path), 8, 230, 140, 15);
        Storage::delete($path);

        return $this;
    }

    public function escrever_qr_code($linha)
    {
        // $options = new QROptions([
        //     'version'      => 10,
        //     'outputType'   => QRCode::OUTPUT_IMAGE_PNG,
        //     'eccLevel'     => QRCode::ECC_L,
        //     'scale'        => 5,
        //     'imageBase64'  => false,
        // ]);

        // $qr = (new QRCode($options))->render($linha);
        // $path = "public/qrcodes/" . Str::random(9) . ".png";
        // Storage::put($path, $qr);
        // $this->pdf->Image(Storage::path($path), 170, 270, 28, 28);
        // Storage::delete($path);

        // $this->pdf->SetXY(131, 266);
        // $this->pdf->Write(10, utf8_decode('Pagamento via Pix'));

        return $this;
    }
}
