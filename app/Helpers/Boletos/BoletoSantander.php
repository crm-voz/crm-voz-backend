<?php

namespace App\Helpers\Boletos;
use Picqer\Barcode\BarcodeGeneratorPNG;
use chillerlan\QRCode\{QRCode, QROptions};
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class BoletoSantander extends Boleto{

    public function __construct() {
        parent::__construct('boleto-santander.png');
    }

    public function escrever_dados_beneficiario($ben, $cnpj, $end) {
        // Escrever o nome do beneficiário
        $this->pdf->SetFont('Arial', 'B', 10);
        $this->pdf->SetXY(70, 10);
        $this->pdf->Write(10, utf8_decode($ben));

        // 2 nome beneficiário
        $this->pdf->SetXY(14, 76);
        $this->pdf->Write(10, utf8_decode($ben));

        // Escrever o CNPJ do beneficiário
        $this->pdf->SetFont('Arial', 'B', 8);
        $this->pdf->SetXY(70, 13);
        $this->pdf->Write(10, utf8_decode($cnpj));

        // Escrever o endereço do beneficiário
        $this->pdf->SetFont('Arial', 'B', 7);
        $this->pdf->SetXY(95, 13);
        $this->pdf->Write(10, utf8_decode($end));

        return $this;
    }

    public function escrever_vencimento($data) {
        // 1 vencimento
        $this->pdf->SetFont('Arial', 'B', 7);
        $this->pdf->SetXY(162, 19.2);
        $this->pdf->Write(10, utf8_decode($data));

        // 2 vencimento
        $this->pdf->SetXY(143.5, 70.2);
        $this->pdf->Write(10, utf8_decode($data));

        return $this;
    }

    public function escrever_valor($valor, $qnt) {
        // 1 valor do documento
        $this->pdf->SetFont('Arial', 'B', 7);
        $this->pdf->SetXY(162, 41.5);
        $this->pdf->Write(10, utf8_decode('R$ ' . number_format($valor, 2, ',', '.')));

        // 2 valor do documento
        $this->pdf->SetXY(143.5, 87.5);
        $this->pdf->Write(10, utf8_decode('R$ ' . number_format($valor, 2, ',', '.')));

        // valor cobrado
        $this->pdf->SetXY(143.5, 115.5);
        $this->pdf->Write(10, utf8_decode('R$ ' . number_format($valor, 2, ',', '.')));

        // $this->pdf->SetFont('Arial', 'B', 7);
        // $this->pdf->SetXY(75, 30.5);
        // $this->pdf->Write(10, utf8_decode(strval($qnt)));

        // $this->pdf->SetXY(67.7, 75.7);
        // $this->pdf->Write(10, utf8_decode(strval($qnt)));

        return $this;
    }

    public function escrever_nosso_numero($num) {
        // 1 nosso numero
        $this->pdf->SetFont('Arial', 'B', 7);
        $this->pdf->SetXY(162, 24.75);
        $this->pdf->Write(10, utf8_decode($num));

        // 2 nosso numero
        $this->pdf->SetXY(143.5, 81.5);
        $this->pdf->Write(10, utf8_decode($num));

        return $this;
    }

    public function escrever_carteira($cart='100') {
        $this->pdf->SetFont('Arial', 'B', 7);
        $this->pdf->SetXY(14, 87.5);
        $this->pdf->Write(10, utf8_decode($cart));

        // $this->pdf->SetXY(38, 75.8);
        // $this->pdf->Write(10, utf8_decode($cart));

        return $this;
    }

    public function escrever_agencia($ag='00000-0') {
        // 1 agencia
        $this->pdf->SetFont('Arial', 'B', 7);
        $this->pdf->SetXY(162, 35.5);
        $this->pdf->Write(10, utf8_decode($ag));

        // 2 agencia
        $this->pdf->SetXY(143.5, 76);
        $this->pdf->Write(10, utf8_decode($ag));

        return $this;
    }

    public function escrever_convenio($conv) {
        // // 1 convenio
        // $this->pdf->SetFont('Arial', 'B', 7);
        // $this->pdf->SetXY(162, 35.5);
        // $this->pdf->Write(10, utf8_decode($ag));

        // // 2 convenio
        // $this->pdf->SetXY(143.5, 76);
        // $this->pdf->Write(10, utf8_decode($ag));

        return $this;
    }

    public function escrever_data_doc($data) {
        $this->pdf->SetFont('Arial', 'B', 7);
        $this->pdf->SetXY(14, 82);
        $this->pdf->Write(10, utf8_decode($data));

        $this->pdf->SetXY(89, 82);
        $this->pdf->Write(10, utf8_decode($data));

        // $this->pdf->SetXY(13.5, 70.5);
        // $this->pdf->Write(10, utf8_decode($data));

        // $this->pdf->SetXY(88.5, 70.5);
        // $this->pdf->Write(10, utf8_decode($data));

        return $this;
    }

    public function escrever_num_doc($num) {
        // 1 numero do documento
        $this->pdf->SetFont('Arial', 'B', 7);
        $this->pdf->SetXY(162, 30.4);
        $this->pdf->Write(10, utf8_decode($num));

        // 2 numero do documento
        $this->pdf->SetXY(38, 82);
        $this->pdf->Write(10, utf8_decode($num));

        return $this;
    }

    public function escrever_desconto($d) {
        $this->pdf->SetFont('Arial', 'B', 7);
        $this->pdf->SetXY(143.5, 93);
        $this->pdf->Write(10, utf8_decode('R$ ' . number_format($d, 2, ',', '.')));

        return $this;
    }

    public function escrever_deducoes($d) {
        $this->pdf->SetFont('Arial', 'B', 7);
        $this->pdf->SetXY(143.5, 98.5);
        $this->pdf->Write(10, utf8_decode('R$ ' . number_format($d, 2, ',', '.')));

        return $this;
    }

    public function escrever_juros($j) {
        $this->pdf->SetFont('Arial', 'B', 7);
        $this->pdf->SetXY(143.5, 104);
        $this->pdf->Write(10, utf8_decode('R$ ' . number_format($j, 2, ',', '.')));

        return $this;
    }

    public function escrever_acrescimos($j) {
        $this->pdf->SetFont('Arial', 'B', 7);
        $this->pdf->SetXY(143.5, 110);
        $this->pdf->Write(10, utf8_decode('R$ ' . number_format($j, 2, ',', '.')));

        return $this;
    }

    public function escrever_cobrado($c) {
        // $this->pdf->SetFont('Arial', 'B', 7);
        // $this->pdf->SetXY(143, 103.5);
        // $this->pdf->Write(10, utf8_decode('R$ ' . number_format($c, 2, ',', '.')));

        return $this;
    }

    public function escrever_pagador($nome, $cpf, $avalista=null) {
        $this->pdf->SetFont('Arial', 'B', 7);
        $this->pdf->SetXY(21, 45);
        $this->pdf->Write(10, utf8_decode($nome));

        $this->pdf->SetXY(67, 45);
        $this->pdf->Write(10, utf8_decode($cpf));

        $this->pdf->SetXY(21, 119);
        $this->pdf->Write(10, utf8_decode($nome));

        $this->pdf->SetXY(67, 119);
        $this->pdf->Write(10, utf8_decode($cpf));

        if($avalista) {
            $this->pdf->SetXY(30, 126.5);
            $this->pdf->Write(10, utf8_decode($avalista));
        }

        return $this;
    }

    public function escrever_cod_credor($ib) {
        // $this->pdf->SetFont('Arial', 'B', 7);
        // $this->pdf->SetXY(143.5, 64.8);
        // $this->pdf->Write(10, utf8_decode($ib));

        return $this;
    }

    public function escrever_linha_digitavel($linha)
    {
        $this->pdf->SetFont('Arial', 'B', 12);
        $this->pdf->SetXY(62, 64);
        $this->pdf->Write(10, utf8_decode($linha));

        return $this;
    }

    public function escrever_cod_barras($linha)
    {
        $gen = new BarcodeGeneratorPNG();
        $img = $gen->getBarcode($this->linha_para_bol($linha), $gen::TYPE_INTERLEAVED_2_5);
        $path = "public/barcodes/" . Str::random(9) . ".png";
        Storage::put($path, $img);
        $this->pdf->Image(Storage::path($path), 14, 134, 90, 15);
        Storage::delete($path);

        return $this;
    }

    public function escrever_qr_code($linha)
    {
        // $this->pdf->SetXY(131, 147);
        // $this->pdf->Write(10, utf8_decode('Pagamento via Pix'));

        // $options = new QROptions([
        //     'version'      => 10,
        //     'outputType'   => QRCode::OUTPUT_IMAGE_PNG,
        //     'eccLevel'     => QRCode::ECC_L,
        //     'scale'        => 5,
        //     'imageBase64'  => false,
        // ]);

        // $qr = (new QRCode($options))->render($linha);
        // $path = "public/qrcodes/" . Str::random(9) . ".png";
        // Storage::put($path, $qr);
        // // $this->pdf->Image(Storage::path($path), 176, 47.2, 16, 16);
        // $this->pdf->Image(Storage::path($path), 170, 135, 33, 33);
        // Storage::delete($path);

        return $this;
    }
}
