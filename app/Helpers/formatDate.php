<?php

function formatDateTime($value, $format){
    return Carbon\Carbon::parse($value)->format($format);
}
