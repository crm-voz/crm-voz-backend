<?php

namespace App\Models\BackOffice;

use App\Models\Core\MotivoDevolucao;
use App\Models\Core\UsuarioEmpresa;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Devolucao extends Model
{
    use HasFactory;
    protected $table = 'devolucao';
    protected $fillable = ['descricao', 'motivo_devolucao_id','usuario_empresas_id'];
    protected $dates = ['created_at'];

    public function motivo_devolucao(){
        return $this->belongsTo(MotivoDevolucao::class);
    }

    public function usuario_empresas(){
        return $this->belongsTo(UsuarioEmpresa::class);
    }

    public function operacoes(){
        return $this->belongsToMany(Operacao::class, 'operacao_devolucao','devolucao_id','operacao_id');
    }
}
