<?php

namespace App\Models\BackOffice;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClienteCampanha extends Model
{
    use HasFactory;
    protected $table = 'cliente_campanha';
    protected $fillable = [
        'campanhas_id',
        'cliente_id'
    ];

    public function cliente(){
        return $this->belongsTo(Cliente::class,'cliente_id', 'id');
    }

    public function criarCampanha($idCampanha, $idCliente){
        $clienteJaParticipa = $this->where('cliente_id', $idCliente)
                                   ->where('campanhas_id', $idCampanha)->first();
        if (!$clienteJaParticipa){
            $this->create([
                'cliente_id' => $idCliente,
                'campanhas_id' => $idCampanha,
            ]);
        }
    }
}
