<?php

namespace App\Models\BackOffice;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Desconto extends Model
{
    use HasFactory;
    protected $table = 'descontos';
    protected $fillable = [
        'tipo',
        'desconto_padrao',
        'desconto_maximo',
        'desconto_minimo',
        'data_inicio',
        'data_final',
        'status',
        'operacoes_id'
    ];

    public function operacoes(){
        return $this->belongsTo(Operacao::class,'operacoes_id','id');
    }
}

