<?php

namespace App\Models\BackOffice;

use App\Models\Core\Cidade;
use App\Models\Core\Eventos;
use App\Models\BackOffice\Email;
use App\Models\BackOffice\Telefone;
use App\Models\Cobranca\Acordo;
use App\Models\Core\StatusLocalizacao;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use OwenIt\Auditing\Contracts\Auditable;

class Cliente extends Model implements Auditable
{
    use  HasFactory, \OwenIt\Auditing\Auditable;
    protected $table = 'cliente';
    protected $fillable = [
        'id',
        'nome',
        'cpf_cnpj',
        'endereco',
        'bairro',
        'numero',
        'cep',
        'cidades_id',
        'observacoes',
        'status_cliente_id',
        'status_localizacao_id',
        'localizacao',
        'ultimo_contato',
        'data_cadastro_confirmado'
    ];

    public function cidades(){
        return $this->belongsTo(Cidade::class, 'cidades_id','id');
    }

    public function emails(): HasMany{
        return $this->hasMany(Email::class);
    }

    public function acordos(){
        return $this->hasMany(Acordo::class,'cliente_id','id');
    }


    public function telefones(): HasMany{
        return $this->hasMany(Telefone::class);
    }

    public function operacoes(){
        return $this->hasMany(Operacao::class);
    }

    public function campanhas(){
        return $this->belongsToMany(Campanha::class,'cliente_campanha','cliente_id','campanhas_id');
    }

    public function status_localizacao() {
        return $this->belongsTo(StatusLocalizacao::class);
    }

    public function eventos(){
        return $this->hasMany(Eventos::class)->orderBy('created_at','desc');
    }

}
