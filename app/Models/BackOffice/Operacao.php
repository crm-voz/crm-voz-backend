<?php

namespace App\Models\BackOffice;

use App\Models\BackOffice\Cliente;
use App\Models\Cobranca\Acordo;
use App\Models\Cobranca\OperacaoAcordo;
use App\Models\Core\ClassificacaoNegativacao;
use App\Models\Core\LogPendencias;
use App\Models\Core\MotivoPendencia;
use App\Models\Core\StatusOperacao;
use App\Models\Core\UsuarioEmpresa;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use OwenIt\Auditing\Contracts\Auditable;

class Operacao extends Model implements Auditable
{
    use HasFactory, SoftDeletes, \OwenIt\Auditing\Auditable;
    protected $table = 'operacoes';
    protected $fillable = [
        'numero_operacao',
        'data_atualizacao',
        'data_vencimento',
        'data_processamento',
        'valor_nominal',
        'valor_atualizado',
        'valor_desconto',
        'descricao',
        'cliente_id',
        'classificacao_operacao_id',
        'usuario_empresas_id',
        'status_operacao_id',
        'classificacao_negativacao_id',
        'divida_reconhecida',
        'motivo_pendencia_id',
        'remessa_id',
        'desconto',
        'origem_desconto',
        'aluno',
        'competencia',
        'numero_acordo',
        'numero_acordo_pai',
        'adicionais'
    ];

    protected $dates = ['data_atualizacao','data_vencimento','data_processamento','created_at','update_at'];

    protected $casts = [
        'valor_nominal' => 'float',
        'valor_atualizado' => 'float',
        'desconto' => 'float'
    ];

    public function cliente(){
        return $this->belongsTo(Cliente::class,'cliente_id', 'id');
    }

    public function motivo_pendencia(){
        return $this->belongsTo(MotivoPendencia::class);
    }

    public function usuario_empresas(){
        return $this->belongsTo(UsuarioEmpresa::class);
    }

    public function status_operacao(){
        return $this->belongsTo(StatusOperacao::class);
    }

    public function classificacao_negativacao(){
        return $this->belongsTo(ClassificacaoNegativacao::class);
    }

    public function remessa(){
        return $this->belongsTo(Remessa::class);
    }

    public function devolucao(){
        return $this->belongsToMany(Devolucao::class,'operacao_devolucao','devolucao_id','operacao_id');
    }

    public function desconto(){
        return $this->hasMany(Desconto::class,'operacoes_id','id');
    }

    public function operacoes_acordo(){
        return $this->hasMany(OperacaoAcordo::class, 'operacoes_id', 'id');
    }

    public function logPendencias(){
        return $this->hasMany(LogPendencias::class,'operacao_id','id');
    }

}
