<?php

namespace App\Models\BackOffice;

use App\Models\BackOffice\Devolucao;
use App\Models\BackOffice\Operacao;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OperacaoDevolucao extends Model
{
    use HasFactory;
    protected $table = 'operacao_devolucao';
    protected $fillable = ['devolucao_id','operacao_id'];

}
