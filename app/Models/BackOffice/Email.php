<?php

namespace App\Models\BackOffice;
use App\models\BackOffice\Cliente;
use App\Models\Core\LogEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Email extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'emails';
    protected $dates = ['created_at'];
    protected $fillable = [
        'email','cliente_id','deleted_at'
    ];

    public function cliente(){
        return $this->belongsTo(Cliente::class);
    }

    public function logs(){
        return $this->hasMany(LogEmail::class);
    }
}
