<?php

namespace App\Models\BackOffice;

use App\Models\BackOffice\Cliente;
use App\Models\Core\LogTelefone;
use App\Models\Core\StatusLogTelefone;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Telefone extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'telefones';
    protected $fillable = [
        'telefone','cliente_id','origem','tags','status','deleted_at'
    ];

    public function cliente(){
        return $this->belongsTo(Cliente::class);
    }

    public function log(){
        return $this->hasMany(LogTelefone::class);
    }

    public function log_telefone(){
        return $this->belongsToMany(StatusLogTelefone::class, 'log_telefone', 'telefone_id', 'status_log_telefone_id');
    }
}
