<?php

namespace App\Models\BackOffice;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CredorContato extends Model
{
    use HasFactory;
    protected $table = 'credor_contatos';
    protected $fillable = [
        'nome','telefone','email','observacoes','credor_id','departamento','relatorios'
    ];

    public function credor(){
        return $this->belongsTo(Credor::class, 'credor_id', 'id');
    }
}
