<?php

namespace App\Models\BackOffice;

use App\Models\Core\UsuarioEmpresa;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Campanha extends Model
{
    use HasFactory;
    protected $table = 'campanhas';
    protected $fillable = [
        'id',
        'tipo_campanha',
        'data_inicio_campanha',
        'data_final_campanha',
        'respiro',
        'filtro',
        'usuario_empresas_id',
        'tipo_filtro',
        'template_id',
        'tempate_email_id',
        'servico',
        'primeira_execucao'
    ];

    public function usuario_empresas(){
        return $this->belongsTo(UsuarioEmpresa::class);
    }

    public function clientes(){
        return $this->belongsToMany(Cliente::class,'cliente_campanha','campanhas_id', 'cliente_id');
    }

}
