<?php

namespace App\Models\BackOffice;

use App\Models\Core\Cidade;
use App\Models\Core\Empresa;
use App\Models\Core\UsuarioEmpresa;
use App\Models\BackOffice\Operacao;
use App\Models\BackOffice\Remessa;
use App\Models\core\Eventos;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Credor extends Model implements Auditable
{
    use HasFactory, \OwenIt\Auditing\Auditable;
    protected $table = 'credores';
    protected $fillable = ['nome', 'razao_social','cnpj','inscricao_estadual','endereco','telefone','celular','email','status',
                           'empresa_id','cep','bairro','numero','cidade_id', 'parametros_config','status_em_processo','status_nao_localizado'];
    // protected $with = ['cidade'];

    public function empresa(){
        return $this->belongsTo(Empresa::class, 'empresa_id', 'id');
    }

    public function usuario_empresas($id){
        return $this->hasMany(UsuarioEmpresa::class, 'empresa_id', 'id');
    }

    public function cidade(){
        return $this->belongsTo(Cidade::class,'cidade_id','id');
    }

    public function operacoes(){
        return $this->hasManyThrough(Operacao::class, Remessa::class, 'credor_id', 'remessa_id', 'id', 'id')->withTrashedParents();
    }

    public function remessas() {
        return $this->hasMany(Remessa::class);
    }

    public function eventos() {
        return $this->hasMany(Eventos::class);
    }

    public function credor_contatos() {
        return $this->hasMany(CredorContato::class);
    }
}
