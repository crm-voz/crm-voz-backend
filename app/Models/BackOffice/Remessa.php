<?php

namespace App\Models\BackOffice;

use App\Models\BackOffice\Credor;
use App\Models\Core\UsuarioEmpresa;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class Remessa extends Model implements Auditable
{
    use HasFactory, SoftDeletes, \OwenIt\Auditing\Auditable;
    protected $table = 'remessas';

    protected $fillable = [
        'numero_remessa','data_remessa','data_entrada',
        'nome_arquivo','credor_id','usuario_empresa_id', 'status'
    ];
    protected $with = ['credor'];

    protected $dates = ['data_remessa','data_entrada'];


    public function credor(){
        return $this->belongsTo(Credor::class);
    }
    public function usuario_empresa(){
        return $this->belongsTo(UsuarioEmpresa::class);
    }

    public function operacoes(){
        return $this->hasMany(Operacao::class);
    }

}
