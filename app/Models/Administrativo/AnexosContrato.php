<?php

namespace App\Models\Administrativo;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AnexosContrato extends Model
{
    use HasFactory;
    protected $table = 'anexos_contratos';
    protected $fillable = ['path','nome','contratos_id'];

    public function fornecedor(){
        return $this->belongsTo(Fornecedor::class,'fornecedor_id','id');
    }

    public function contrato(){
        return $this->belongsTo(Contratos::class,'contrato_id','id');
    }

}
