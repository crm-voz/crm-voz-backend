<?php

namespace App\Models\Administrativo;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Contratos extends Model
{
    use HasFactory;
    protected $table = 'contratos';
    protected $fillable = ['data_inicio',
                           'data_final',
                           'descricao',
                           'objeto',
                           'responsavel',
                           'telefone_responsavel',
                           'email_responsavel',
                           'fornecedor_id',
                           'status'];

    public function fornecedor(){
        return $this->belongsTo(Fornecedor::class,'fornecedor_id','id');
    }

    public function anexosContrato(){
        return $this->hasMany(AnexosContrato::class,'contratos_id','id');
    }

}
