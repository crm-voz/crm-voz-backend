<?php

namespace App\Models\Administrativo;

use App\Models\Core\Cidade;
use App\Models\Core\Empresa;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Fornecedor extends Model
{
    use HasFactory;

    protected $table = 'fornecedor';
    protected $fillable = ['nome_fantasia',
                           'razao_social',
                           'cnpj',
                           'inscricao_estadual',
                           'endereco',
                           'cep',
                           'representante',
                           'contato_representante',
                           'chave_pix',
                           'telefone',
                           'celular',
                           'email',
                           'status',
                           'site',
                           'empresa_id',
                           'observacao',
                           'cidade_id'];

    public function empresa(){
        return $this->belongsTo(Empresa::class, 'empresa_id', 'id');
    }

    public function cidade(){
        return $this->belongsTo(Cidade::class,'cidade_id','id');
    }

    public function contratos() {
        return $this->hasMany(Contratos::class,'fornecedor_id','id');
    }

}
