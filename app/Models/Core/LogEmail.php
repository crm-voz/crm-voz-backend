<?php

namespace App\Models\Core;

use App\Models\BackOffice\Email;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LogEmail extends Model
{
    use HasFactory;
    protected $table = 'log_email';
    protected $fillable = ['email_id','observacao'];

    public function email(){
        return $this->belongsTo(Email::class);
    }

}
