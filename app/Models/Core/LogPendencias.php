<?php

namespace App\Models\Core;

use App\Models\BackOffice\Operacao;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LogPendencias extends Model
{
    use HasFactory;
    protected $table = 'log_pendencias';
    protected $fillable = ['operacao_id',
                           'usuario_empresa_id',
                           'observacao',
                           'status'];

    public function operacao(){
        return $this->belongsTo(Operacao::class, 'operacao_id','id');
    }

    public function usuario_empresas(){
        return $this->belongsTo(UsuarioEmpresa::class,'usuario_empresa_id','id');
    }
}
