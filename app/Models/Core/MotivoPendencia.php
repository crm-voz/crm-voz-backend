<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MotivoPendencia extends Model
{
    use HasFactory;
    protected $table = 'motivo_pendencia';
    protected $fillable = [
        'nome','status','cor'
    ];

    public function operacoes(){
        return $this->hasMany(Operacoes::class);
    }
}
