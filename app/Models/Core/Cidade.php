<?php

namespace App\Models\Core;

use App\Models\BackOffice\Cliente;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cidade extends Model
{
    use HasFactory;

    protected $table = 'cidades';
    protected $fillable = [
        'nome', 'uf', 'codigo_ibge'
    ];

    public function funcionarios(){
        return $this->hasMany(funcionarios::class);
    }

    public function clientes(){
        return $this->hasMany(Cliente::class, 'cidades_id','id');
    }
}
