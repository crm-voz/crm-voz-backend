<?php

namespace App\Models\Core;

use App\Models\BackOffice\Telefone;
use App\Models\Cobranca\Parcela;
use App\Models\Core\UsuarioEmpresa;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Boleto extends Model implements Auditable
{
    use HasFactory, \OwenIt\Auditing\Auditable;
    protected $table = 'boleto';
    protected $fillable = [
        'linha_digitavel',
        'nosso_numero',
        'vencimento',
        'baixa',
        'valor',
        'valor_pago',
        'multa',
        'desconto',
        'parcelas_id',
        'usuario_empresas_id',
        'id_pix',
        'chave_pix',
        'nome_beneficiario',
        'cnpj_beneficiario',
        'endereco_beneficiario',
        'carteira',
        'agencia',
        'numero_documento',
        'deducoes',
        'juros',
        'acrescimos',
        'nome_pagador',
        'cpf_pagador',
        'nome_avaliador',
        'codigo_credor',
        'codigo_barra',
        'convenio',
        'codigo_banco'
    ];

    public function usuario_empresas(){
        return $this->belongsTo(UsuarioEmpresa::class);
    }

    public function parcelas(){
        return $this->belongsTo(Parcela::class);
    }

}
