<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PermissoesPerfil extends Model
{
    use HasFactory;
    protected $table = 'permissoes_perfil';
    protected $fillable = [
        'modificar',
        'perfil_usuario_id',
        'endpoints_id'
    ];

    public function perfil_usuario(){
        return $this->belongsTo(PerfilUsuario::class,'perfil_usuario_id','id');
    }

    public function endpoints(){
        return $this->belongsTo(Endpoints::class,'endpoints_id','id');
    }
}
