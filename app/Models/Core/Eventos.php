<?php

namespace App\Models\Core;

use App\Models\BackOffice\Cliente;
use App\Models\BackOffice\Credor;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Eventos extends Model implements Auditable
{
    use HasFactory,\OwenIt\Auditing\Auditable;
    protected $table = 'eventos';
    protected $fillable = [
        'descricao','cliente_id',
        'usuario_empresas_id','motivo_evento_id','credores_id', 'parametros_contato'
    ];

    protected $with = ['usuario_empresas.user:users.id,users.name','motivo_evento'];

    public function cliente(){
        return $this->belongsTo(Cliente::class);
    }

    public function usuario_empresas(){
        return $this->belongsTo(UsuarioEmpresa::class);
    }

    public function motivo_evento(){
        return $this->belongsTo(MotivoEvento::class);
    }

    public function credores(){
        return $this->belongsTo(Credor::class);
    }

}
