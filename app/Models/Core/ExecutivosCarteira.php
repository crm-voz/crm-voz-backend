<?php

namespace App\Models\Core;

use App\Models\Cobranca\ExecutivoCobranca;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ExecutivosCarteira extends Model
{
    use HasFactory;
    protected $table = 'executivos_carteira';
    protected $fillable = ['executivo_cobranca_id','carteira_id', 'meta'];

    public function carteira(){
        return $this->belongsTo(Carteira::class);
    }

    public function executivo(){
        return $this->belongsTo(ExecutivoCobranca::class,'executivo_cobranca_id','id');
    }

}
