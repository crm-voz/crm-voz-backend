<?php

namespace App\Models\Core;

use App\Models\BackOffice\Credor;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CredoresCarteira extends Model
{
    use HasFactory;
    protected $table = 'credor_carteira';
    protected $fillable = ['carteira_id','credor_id','meta'];

    public function carteira(){
        return $this->belongsTo(Carteira::class);
    }

    public function credor(){
        return $this->belongsTo(Credor::class);
    }

}
