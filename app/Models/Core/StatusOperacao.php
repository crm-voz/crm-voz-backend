<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StatusOperacao extends Model
{
    use HasFactory;
    protected $table = 'status_operacao';
    protected $fillable = [
        'nome'
    ];
}
