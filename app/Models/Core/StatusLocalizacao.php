<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StatusLocalizacao extends Model
{
    use HasFactory;
    protected $table = 'status_localizacao';
    protected $fillable = [
        'nome','cor','status'
    ];
}
