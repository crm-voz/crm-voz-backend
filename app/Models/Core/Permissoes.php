<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Permissoes extends Model
{
    use HasFactory;
    protected $table = 'permissoes';
    protected $fillable = [
        'usuario_empresas_id',
        'perfil_usuario_id'
    ];

    public function usuario_empresas(){
        return $this->belongsTo(UsuarioEmpresa::class,'usuario_empresas_id','id');
    }

    public function perfil_usuario(){
        return $this->belongsTo(PerfilUsuario::class,'perfil_usuario_id','id');
    }
}
