<?php

namespace App\Models\Core;

use App\Models\Cobranca\ExecutivoCobranca;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Carteira extends Model
{
    use HasFactory;
    protected $table = 'carteira';
    protected $fillable = ['executivo_cobranca_id','meta','descricao',];

    public function supervisor(){
        return $this->belongsTo(ExecutivoCobranca::class,'executivo_cobranca_id','id');
    }

    public function credoresCarteira(){
        return $this->hasMany(CredoresCarteira::class);
    }

    public function executivosCarteira(){
        return $this->hasMany(ExecutivosCarteira::class);
    }
}
