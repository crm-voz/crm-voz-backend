<?php

namespace App\Models\Core;

use App\Models\BackOffice\Credor;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TemplateSms extends Model
{
    use HasFactory;
    protected $table = 'template_sms';
    protected $fillable = [
        'titulo',
        'mensagem',
        'status',
        'credores_id'
    ];

    public function credores(){
        return $this->belongsTo(Credor::class, 'credores_id', 'id');
    }

}
