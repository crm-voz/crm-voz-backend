<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Endpoints extends Model
{
    use HasFactory;
    protected $table = 'endpoints';
    protected $fillable = [
        'nome',
        'url',
        'grupo'
    ];
}
