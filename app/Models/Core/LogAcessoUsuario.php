<?php

namespace App\Models\Core;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LogAcessoUsuario extends Model
{
    use HasFactory;
    protected $table = 'log_acesso_usuario';
    protected $fillable = [
        'usuario_id',
        'login',
        'logout'
    ];

    public function usuario(){
        return $this->belongsTo(User::class,'usuario_id','id');
    }

}
