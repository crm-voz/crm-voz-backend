<?php

namespace App\Models\Core;

use App\Models\BackOffice\Credor;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
    use HasFactory;
    protected $table = 'empresas';

    protected $fillable = [
        'nome_fantasia','razao_social','cnpj', 'inscricao_estadual',
        'cidade_id','endereco','telefone','celular','email','status'
    ];

    public function usuario_empresas(){
        return $this->belongsTo(UsuarioEmpresa::class, 'empresa_id', 'id');
    }

    public function credores(){
        return $this->hasMany(Credor::class, 'empresa_id', 'id');
    }

    public function cidade(){
        return $this->belongsTo(Cidade::class);
    }
}


