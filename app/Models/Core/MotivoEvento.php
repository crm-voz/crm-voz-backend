<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MotivoEvento extends Model
{
    use HasFactory;
    protected $table = 'motivo_evento';
    protected $fillable = [
        'id','nome','cor','status','ocultar','grupo'
    ];

    public function eventos(){
        return $this->hasMany(Eventos::class);
    }
}
