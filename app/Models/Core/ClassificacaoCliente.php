<?php

namespace App\Models\Core;

use App\Models\BackOffice\Cliente;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClassificacaoCliente extends Model
{
    use HasFactory;
    protected $table = 'classificacao_cliente';
    protected $fillable = ['nome','cor','status'];

    public function clientes(){
        return $this->hasMany(Cliente::class);
    }
}
