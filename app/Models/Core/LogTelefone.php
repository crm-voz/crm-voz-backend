<?php

namespace App\Models\Core;

use App\Models\BackOffice\Telefone;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LogTelefone extends Model
{
    use HasFactory;
    protected $table = 'log_telefone';
    protected $fillable = ['telefone_id','observacao'];

    public function telefone(){
        return $this->belongsTo(Telefone::class);
    }


}
