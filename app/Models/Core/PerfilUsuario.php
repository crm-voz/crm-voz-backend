<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PerfilUsuario extends Model
{
    use HasFactory;
    protected $table = 'perfil_usuario';
    protected $fillable = [
        'descricao'
    ];

    public function permissoesPerfil(){
        return $this->hasMany(PermissoesPerfil::class,'perfil_usuario_id','id');
    }

}
