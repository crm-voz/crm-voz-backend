<?php

namespace App\Models\Core;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UsuarioEmpresa extends Model
{
    use HasFactory;
    protected $table = 'usuario_empresas';
    protected $with = ['user','empresa'];
    protected $fillable = ['empresa_id','user_id','status','perfil_usuario_id'];
    protected $dates = ['created_at'];

    public function user(){
        return $this->belongsTo(User::class, 'user_id','id');
    }

    public function empresa(){
        return $this->belongsTo(Empresa::class,'empresa_id','id');
    }

    public function funcionarios(){
        return $this->hasMany(Funcionario::class, 'usuario_empresas_id', 'id');
    }

    public function perfilUsuario(){
        return $this->hasOne(PerfilUsuario::class, 'id','perfil_usuario_id');
    }
}
