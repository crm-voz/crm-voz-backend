<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Snapshot extends Model
{
    use HasFactory;
    protected $table = 'snapshot';
    protected $fillable = ['banco'];
    protected $dates = ['created_at'];
}
