<?php

namespace App\Models\Core;

use App\Models\BackOffice\Credor;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ParametroBoleto extends Model
{
    use HasFactory;
    protected $table = 'parametro_boleto';
    protected $fillable = [
        'nome_beneficiario',
        'cnpj_beneficiario',
        'endereco_beneficiario',
        'codigo_banco',
        'nome_banco',
        'convenio',
        'carteira',
        'agencia',
        'conta',
        'parametro'
    ];
}
