<?php

namespace App\Models\Core;

use App\Models\Core\UsuarioEmpresa;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClassificacaoNegativacao extends Model
{
    use HasFactory;
    protected $table = 'classificacao_negativacao';
    protected $fillable = [
        'nome'
    ];


}
