<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StatusLogTelefone extends Model
{
    use HasFactory;
    protected $table = 'status_log_telefone';
    protected $fillable = ['nome'];

    public function telefone(){
        return $this->belongsToMany(Telefone::class,'log_telefone', 'telefone_id','status_telefone_id');
    }

}
