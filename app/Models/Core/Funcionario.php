<?php

namespace App\Models\Core;

use App\Models\Core\Cidade;
use App\Models\Core\Funcao;
use App\Models\Core\UsuarioEmpresa;
use App\Models\Cobranca\ExecutivoCobranca;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Funcionario extends Model
{
    use HasFactory;
    protected $table = 'funcionarios';
    protected $fillable =
        [
            'nome','cpf','rg','endereco','cep','funcao_id',
            'cidade_id','usuario_empresas_id','status'
        ];
    protected $with = ['cidade', 'funcao','usuario_empresas'];

    public function funcao(){
        return $this->belongsTo(Funcao::class);
    }

    public function cidade(){
        return $this->belongsTo(Cidade::class);
    }

    public function usuario_empresas(){
        return $this->belongsTo(UsuarioEmpresa::class);
    }

    public function executivo_cobranca(){
        return $this->hasOne(ExecutivoCobranca::class, 'funcionarios_id', 'id');
    }

}
