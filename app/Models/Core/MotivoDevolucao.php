<?php

namespace App\Models\Core;

use App\Models\BackOffice\Devolucao;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MotivoDevolucao extends Model
{
    use HasFactory;
    protected $table = 'motivo_devolucao';
    protected $fillable = [
        'nome','cor','status'
    ];

    public function devolucoes(){
        return $this->hasMany(Devolucao::class);
    }
}
