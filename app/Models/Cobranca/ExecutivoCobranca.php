<?php

namespace App\Models\Cobranca;

use App\Models\Core\Carteira;
use App\Models\Core\Funcionario;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ExecutivoCobranca extends Model
{
    use HasFactory;
    protected $table = 'executivo_cobranca';
    protected $fillable = [
        'funcionarios_id',
        'usuario',
        'senha',
        'ramal',
        'meta'
    ];

    public function acordo(){
        return $this->hasMany(Acordo::class);
    }

    public function funcionarios(){
        return $this->belongsTo(Funcionario::class,'funcionarios_id','id');
    }

    public function carteira(){
        return $this->hasMany(Carteira::class);
    }

    public function propostas(){
        return $this->hasMany(Proposta::class, 'supervisor_cobranca_id', 'id');
    }

}
