<?php

namespace App\Models\Cobranca;

use App\Models\BackOffice\Operacao;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OperacaoAcordo extends Model
{
    use HasFactory;
    protected $table = 'operacao_acordo';
    protected $fillable = ['acordos_id','operacoes_id'];
    protected $with = ['operacao'];

    public function operacao() {
        return $this->belongsTo(Operacao::class, 'operacoes_id', 'id');
    }

    public function acordo() {
        return $this->belongsTo(Acordo::class, 'acordos_id', 'id');
    }

}
