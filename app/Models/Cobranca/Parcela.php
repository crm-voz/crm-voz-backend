<?php

namespace App\Models\Cobranca;

use App\Models\Core\Boleto;
use App\Models\Core\UsuarioEmpresa;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Parcela extends Model implements Auditable
{
    use HasFactory, \OwenIt\Auditing\Auditable;
    protected $table = 'parcelas';
    protected $fillable =[
        'valor_desconto',
        'desconto_percentual',
        'vencimento_original',
        'status',
        'valor_nominal',
        'valor_previsto',
        'data_vencimento',
        'numero_parcela',
        'data_baixa',
        'valor_baixado',
        'valor_juros',
        'valor_multa',
        'valor_honorario',
        'forma_pagamento',
        'acordos_id',
        'usuario_empresas_id',
        'transacao_parcela',
        'valor_comissao',
        'valor_nominal_original',
    ];

    protected $with = ['usuario_empresas.user:users.id,users.name','acordos.cliente'];

    public function acordos(){
        return $this->belongsTo(Acordo::class,'acordos_id','id');
    }

    public function usuario_empresas(){
        return $this->belongsTo(UsuarioEmpresa::class,'usuario_empresas_id','id');
    }

    public function boleto() {
        return $this->hasMany(Boleto::class,'parcelas_id','id');
    }

    public function operacoes_parcelas(){
        return $this->hasMany(OperacoesParcelas::class, 'parcela_id','id');
    }

}
