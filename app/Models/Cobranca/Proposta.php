<?php

namespace App\Models\Cobranca;

use App\Models\BackOffice\Cliente;
use App\Models\Core\Funcionario;
use Illuminate\Database\Eloquent\Model;

class Proposta extends Model
{
    protected $table = 'proposta';
    protected $fillable = [
        'numero_proposta',
        'valor_nominal',
        'juros',
        'multas',
        'honorarios',
        'desconto',
        'valor_entrada',
        'quantidade_parcelas',
        'valor_parcela',
        'cliente_id',
        'executivo_cobranca_id',
        'supervisor_cobranca_id',
        'juros_porcento',
        'multas_porcento',
        'honorarios_porcento',
        'desconto_porcento',
        'valor_entrada_porcento',
        'status',
    ];

    public function cliente(){
        return $this->belongsTo(Cliente::class,'cliente_id','id');
    }

    public function executivo_cobranca(){
        return $this->belongsTo(ExecutivoCobranca::class,'executivo_cobranca_id','id');
    }


    public function parcelas(){
        return $this->hasMany(ParcelasProposta::class,'proposta_id','id')->orderBy('numero_parcela');
    }

    public function operacoes_proposta(){
        return $this->hasMany(OperacaoProposta::class, 'proposta_id', 'id');
    }

    public function supervisor_cobranca(){
        return $this->belongsTo(Funcionario::class,'supervisor_cobranca_id','id');
    }

}
