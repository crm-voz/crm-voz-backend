<?php

namespace App\Models\Cobranca;

use App\Models\BackOffice\Operacao;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OperacaoProposta extends Model
{
    use HasFactory;
    protected $table = 'operacao_proposta';
    protected $fillable = ['proposta_id','operacoes_id'];
    protected $with = ['operacao'];

    public function operacao() {
        return $this->belongsTo(Operacao::class, 'operacoes_id', 'id');
    }

    public function Proposta() {
        return $this->belongsTo(Proposta::class, 'proposta_id', 'id');
    }

}
