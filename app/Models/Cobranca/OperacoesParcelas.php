<?php

namespace App\Models\Cobranca;

use App\Models\BackOffice\Operacao;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OperacoesParcelas extends Model
{
    use HasFactory;
    protected $table = 'operacoes_parcelas';
    protected $fillable = [
            'operacao_id',
            'parcela_id',
            'instituicao',
            'cliente',
            'aluno',
            'documento',
            'numero_operacao',
            'vencimento_original',
            'vencimento',
            'recebimento',
            'parcela',
            'valor_negociado',
            'valor_repasse',
            'origem',
            'principal',
            'juros',
            'multa',
            'correcao',
            'desconto',
            'honorarios',
            'realizado',
            'taxa_cartao',
            'comissao',
            'repasse',
            'forma',
            'referencia',
            'numero_documento',
            'desconto_acordo',
            'credor_id',
            'valor_juros_original',
            'valor_multa_original',
            'valor_honorario_original',
            'valor_desconto_original',
            'valor_comissao_original',
    ];

    public function operacao(){
        return $this->belongsTo(Operacao::class, 'operacao_id','id');
    }

    public function parcelas(){
        return $this->belongsTo(Parcela::class, 'parcela_id','id');
    }
}
