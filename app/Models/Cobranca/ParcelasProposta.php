<?php

namespace App\Models\Cobranca;

use App\Models\Core\Boleto;
use App\Models\Core\UsuarioEmpresa;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ParcelasProposta extends Model
{
    use HasFactory;
    protected $table = 'parcelas_proposta';
    protected $fillable =[
        'valor_nominal',
        'valor_previsto',
        'data_vencimento',
        'numero_parcela',
        'valor_juros',
        'valor_multa',
        'valor_honorario',
        'forma_pagamento',
        'proposta_id',
        'usuario_empresas_id',
        'valor_comissao'
    ];

    public function proposta(){
        return $this->belongsTo(Proposta::class,'proposta_id','id');
    }

    public function usuario_empresas(){
        return $this->belongsTo(UsuarioEmpresa::class,'usuario_empresas_id','id');
    }

}
