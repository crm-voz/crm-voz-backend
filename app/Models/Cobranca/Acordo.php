<?php

namespace App\Models\Cobranca;

use App\Models\BackOffice\Campanha;
use App\Models\BackOffice\Cliente;
use App\Models\Core\UsuarioEmpresa;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class Acordo extends Model implements Auditable
{
    use HasFactory, SoftDeletes, \OwenIt\Auditing\Auditable;
    protected $table = 'acordo';
    protected $fillable = [
        'id',
        'numero_acordo',
        'status',
        'valor_nominal',
        'juros',
        'multas',
        'honorarios',
        'desconto',
        'valor_entrada',
        'quantidade_parcelas',
        'valor_parcela',
        'data_cancelou',
        'descricao_cancelamento',
        'transacao_cartao',
        'cliente_id',
        'executivo_cobranca_id',
        'usuario_cancelou_id',
        'campanhas_id',
        'juros_porcento',
        'multas_porcento',
        'honorarios_porcento',
        'desconto_porcento',
        'valor_entrada_porcento',
        'situacao',
    ];

    public function cliente(){
        return $this->belongsTo(Cliente::class,'cliente_id','id');
    }

    public function executivo_cobranca(){
        return $this->belongsTo(ExecutivoCobranca::class,'executivo_cobranca_id','id');
    }

    public function usuario_cancelou(){
        return $this->belongsTo(UsuarioEmpresa::class,'usuario_cancelou_id','id');
    }

    public function campanhas(){
        return $this->belongsTo(Campanha::class,'campanhas_id','id');
    }

    public function parcelas(){
        return $this->hasMany(Parcela::class,'acordos_id','id')->orderBy('numero_parcela');
    }

    public function operacoes_acordo(){
        return $this->hasMany(OperacaoAcordo::class, 'acordos_id', 'id');
    }

    public function scopeAcordosCredor($query, $param){
        return $query->join('operacao_acordo','operacao_acordo.acordos_id','=','acordo.id')
                     ->join('operacoes','operacao_acordo.operacoes_id','=','operacoes.id')
                     ->join('remessas', function($join) use($param) {
                        $join->on('operacoes.remessa_id','=','remessas.id')
                             ->where('remessas.credor_id', $param);
                     })->where('acordo.status','Negociado')
                       ->select('acordo.*')
                       ->distinct('acordo.id')
                       ->get();
    }

    public function scopeAcordosInCredor($query, $args){
        return $query->join('operacao_acordo','operacao_acordo.acordos_id','=','acordo.id')
                     ->join('operacoes','operacao_acordo.operacoes_id','=','operacoes.id')
                     ->join('remessas', function($join) use($args) {
                        $join->on('operacoes.remessa_id','=','remessas.id')
                             ->whereIn('remessas.credor_id', $args);
                     })->where('acordo.status','Negociado')
                       ->select('acordo.*')
                       ->distinct('acordo.id')
                       ->get();
    }
}
