<?php

namespace App\Models\Financeiro;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BaixaBoleto extends Model
{
    use HasFactory;
    protected $table = 'baixa_boleto';
    protected $fillable = [
        'descricao',
        'banco',
        'convenio',
        'cnab',
        'quantidade_baixada',
        'valor_total_baixado'
    ];
}
