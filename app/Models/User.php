<?php

namespace App\Models;

use App\Models\Core\Empresa;
use App\Models\Core\LogAcessoUsuario;
use App\Models\Core\UsuarioEmpresa;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasFactory, Notifiable, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'id',
        'name',
        'email',
        'password',
        'url_perfil',
        'cpf',
        'credor_id',
        'remember_token',
        'url_perfil'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

   public function usuario_empresas(){
       return $this->hasMany(UsuarioEmpresa::class);
   }

    public function empresa(){
        return $this->hasMany(Empresa::class);
    }

    public function acessos(){
        return $this->hasMany(LogAcessoUsuario::class, 'usuario_id','id');
    }

    public function registerAccess(){
        return $this->acessos()->create([
            'usuario_id'   => $this->id,
            'login'  => date('Y-m-d H:i:s'),
        ]);
    }

    public function getUser($cpf){
        return $this->where('cpf', $cpf)->first();
    }

}
