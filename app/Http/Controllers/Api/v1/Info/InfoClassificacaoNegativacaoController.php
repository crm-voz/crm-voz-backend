<?php

namespace App\Http\Controllers\Api\v1\Info;

use App\Http\Controllers\Controller;
use App\Models\Core\ClassificacaoNegativacao;
use Illuminate\Http\Request;

class InfoClassificacaoNegativacaoController extends Controller
{
    public function get(Request $request) {
        $id = $request->input('id', null);

        return $this->sendResponse(ClassificacaoNegativacao::where(function ($query) use ($id){
                                                                    if($id) $query->where('id', $id);
                                                                })
                                                        ->get());
    }
}
