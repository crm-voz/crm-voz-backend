<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Models\BackOffice\Cliente;
use App\Models\BackOffice\Credor;
use App\Models\BackOffice\Remessa;
use App\Models\BackOffice\Devolucao;
use App\Models\BackOffice\Operacao;
use App\Models\Cobranca\Parcela;

class DashboardController extends Controller
{
    public function index() {
        $total = Operacao::sum('valor_nominal');
        $liquidado = Parcela::sum('valor_baixado');
        $devolvido = Operacao::where('operacoes.status_operacao_id',3)->sum('valor_nominal');

        $totais = [
            'Clientes'      => Cliente::count(),
            'Credores'      => Credor::count(),
            'Remessas'      => Remessa::count(),
            'Devolucoes'    => Devolucao::count(),
            'Operacoes'    => Operacao::count(),
            'Valor_Nominal' => floatval(Operacao::sum('valor_nominal')),//number_format(Operacao::sum('valor_nominal'), 2),
            'Conversao' => $liquidado / ($total - $devolvido)
        ];

        return $this->sendResponse($totais);
    }
}

