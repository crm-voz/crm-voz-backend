<?php

namespace App\Http\Controllers\Api\v1\Dashboard;

use App\Http\Controllers\Controller;
use App\Repository\Dashboards\DashboardCredorRepository;

class CredorController extends Controller
{
    public function __construct(DashboardCredorRepository $dashboardRepository)
    {
        $this->dashboardRepository = $dashboardRepository;
    }

    public function totalizar(){
        return $this->sendResponse([
            'quantidade_acordos' => $this->dashboardRepository->quantidadeAcordosCredor(),
            'quantidade_campanhas' => $this->dashboardRepository->quantidadeCampanhasCredor(),
            'quantidade_remessas' => $this->dashboardRepository->quantidadeRemessaCredor(),
            'quantidade_clientes' => $this->dashboardRepository->quantidadeClientesCredor(),
            'quantidade_operacoes' => $this->dashboardRepository->quantidadeCredorOperacoes(),
        ]);
    }

    public function listarAcordos(){
        return $this->sendResponse($this->dashboardRepository->ListarAcordosCredor());
    }

    public function campanhasCredor(){
        $campanhas = $this->dashboardRepository->campanhasCredor();
        $lista = collect();
        foreach($campanhas as $campanha){
            $lista->add([
                'id' => $campanha->id,
                'tipo_campanha' => $campanha->tipo_campanha,
                'data_inicio_campanha' => $campanha->data_inicio_campanha,
                'data_final_campanha' => $campanha->data_final_campanha,
                'usuario_empresas' => $campanha->usuario_empresas?->user->name,
                'total_clientes' => $campanha->clientes->count(),
                'clientes' => $campanha->clientes,
                'operacoes' => $campanha->operacoes,
                'quantidade_acordos' => $this->dashboardRepository->acordosCampanhas($campanha)->count(),
                'faturamento_campanha' => $this->dashboardRepository->faturamentoCampanha($campanha),
            ]);
        }
        return $this->sendResponse($lista);
    }


}
