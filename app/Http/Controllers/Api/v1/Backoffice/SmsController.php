<?php

namespace App\Http\Controllers\Api\v1\Backoffice;

use App\Http\Controllers\Controller;
use App\Gateway\Sms\BestVoice;
use App\Gateway\Sms\iSms;
use App\Models\BackOffice\Telefone;
use App\Repository\Backoffice\ClientesRepository;
use App\Services\EnvioSMSServices;
use App\Services\LogContatosServices;
use Illuminate\Http\Request;

class SmsController extends Controller
{
    protected iSms $plataforma_padrao;
    protected $logContatoService;

    public function __construct(BestVoice $plataforma,
                                ClientesRepository $clientesRepository,
                                EnvioSMSServices $envioSmsServices,
                                LogContatosServices $logContato) {
        $this->plataforma_padrao = new $plataforma;
        $this->clientesRepository = $clientesRepository;
        $this->envioSmsServices = $envioSmsServices;
        $this->logContatoService = $logContato;

    }

    public function enviar_sms_unico(Request $request) {

        $contato = Telefone::where('telefone',$request->celular)->get();
        foreach ($contato as $value) {
            $this->logContatoService->salvarLogContatosManualSms($value->id,$request->mensagem);
        }
        return $this->sendResponse($this->plataforma_padrao->enviar_sms($request->all()));
    }

    public function enviar_sms_cliente(Request $request)
    {
        $clientes = $this->clientesRepository->buscarClientesPeloId($request->clientes);
        return $this->sendResponse($this->envioSmsServices->enviar($clientes, $request->templateId));
    }
}
