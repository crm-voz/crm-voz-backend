<?php

namespace App\Http\Controllers\Api\v1\Backoffice;

use App\Http\Controllers\Controller;
use App\Repository\Backoffice\InsucessosRepository;
use Exception;
use Illuminate\Http\Request;

class InsucessosController extends Controller
{
    protected $repositoryInsucessos;
    public function __construct(InsucessosRepository $repository)
    {
        $this->repositoryInsucessos = $repository;
    }

    public function importar(Request $request){
        if( $request->file() ) {
            try {
                $file_arr = $request->file();
                $file = array_pop($file_arr);
                if (!$this->repositoryInsucessos->importar($file->getPathName())) {
                    throw new Exception("Falha ao processar planilha de Insucessos.", 1);
                }
                return $this->sendResponse([true]);
            } catch (\Exception $err) {
                return $this->sendError('Houve um erro... ' . $err->getMessage());
            }
        } else {
            return $this->sendError('O arquivo não foi recebido');
        }
    }

    public function listagem(Request $request, $id){
        try {
            return $this->repositoryInsucessos->listagem($id);
        } catch (\Exception $err) {
            return $this->sendError('Houve um erro... ' . $err->getMessage());
        }
    }
}
