<?php

namespace App\Http\Controllers\Api\v1\Backoffice;

use App\Repository\Backoffice\DevolucaoRepository;
use App\Repository\Backoffice\OperacaoRepository;
use App\Http\Controllers\Controller;
use App\Models\BackOffice\Devolucao;
use App\Models\BackOffice\Operacao;
use App\Models\BackOffice\OperacaoDevolucao;
use App\Models\Core\MotivoDevolucao;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DevolucaoController extends Controller
{
    protected DevolucaoRepository $devolucaoRepository;
    protected OperacaoRepository $operacaoRepository;

    public function __construct(DevolucaoRepository $devolucaoRepository,
                                OperacaoRepository $operacaoRepository)
    {
        $this->devolucaoRepository = $devolucaoRepository;
        $this->operacaoRepository = $operacaoRepository;
    }

    public function index(){
        $devolucao = $this->devolucaoRepository->get();
        return $this->sendResponse(['devolucoes' => $devolucao]);
    }

    public function create(){
        $motivoPendencia = MotivoDevolucao::all();
        $operacoes = Operacao::where('status_operacao_id', '!=', 3)->get();
        return $this->sendResponse(['motivos_devolucao' => $motivoPendencia, 'operacoes' => $operacoes]);
    }

    public function detalhar($id){
        try {
            $devolucao = $this->devolucaoRepository->detalhe($id);
        } catch (\Throwable $th) {
            return $this->sendError($th->getMessage());
        }

        return $this->sendResponse(['devolucao' => $devolucao]);
    }

    public function store(Request $request){
        $dados = $request->all();
        try {
          DB::beginTransaction();
          $response = $this->devolucaoRepository->create($dados);
          $this->operacaoRepository->devolverOperacoes($dados['operacoes']);
          DB::commit();
          return $this->sendResponse($response);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->sendError($e->getMessage());
        }
    }

    public function cancelarDevolucaoOperacoes(Request $request){
        $dados = $request->all();
        $devolucao = $this->devolucaoRepository->find($dados['id']);
        return  $this->sendResponse($this->operacaoRepository->cancelarDevolucaoOperacao($devolucao->operacoes));
    }

}
