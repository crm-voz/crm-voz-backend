<?php

namespace App\Http\Controllers\Api\v1\Backoffice;

use App\Repository\Backoffice\ClientesRepository;
use App\Http\Controllers\Controller;
use App\Models\BackOffice\Cliente;
use App\Models\BackOffice\Telefone;
use App\Models\Core\LogTelefone;
use App\Models\Core\StatusLogTelefone;
use App\Repository\Backoffice\EventosRepository;
use Illuminate\Http\Request;

class TelefonesClienteController extends Controller
{
    protected $model;
    protected $clientesRepository;
    public function __construct(Telefone $telefone, ClientesRepository $cliente, EventosRepository $eventos)
    {
        $this->model = $telefone;
        $this->clientesRepository = $cliente;
        $this->eventosRepository = $eventos;
    }

    public function index($id){
        $cliente = Cliente::find($id);
        $status = StatusLogTelefone::all();
        $telefones = $cliente?->telefones()->get();
        return $this->sendResponse(['cliente'=>$cliente,'status'=>$status, 'telefones' => $telefones]);
    }

    public function create($id){
        $cliente = Cliente::find($id);
        return $this->sendResponse(['cliente' => $cliente]);
    }

    public function edit($id){
        $telefone = Telefone::find($id);
        if(!$telefone){
            return $this->sendError($this->NAO_LOCALIZADO);

        }
        $telefone->cliente;
        return $this->sendResponse(['telefone' => $telefone]);
    }

    public function update(Request $request, $id){
        $dados = $request->all();
        try {
            $telefone = Telefone::find($id);
            if (!$telefone){
                return $this->sendError($this->NAO_LOCALIZADO);
            }
            $this->clientesRepository->criarEvento($telefone->cliente_id, 'ATUALIZADO TELEFONE ' . $telefone->telefone,102);
            return $this->sendResponse($this->salvar_telefone($dados, $id));
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }
    }

    public function statusUpdate(Request $request, $telefone_id){
        $dados = $request->all();
        $dados['telefone_id'] = $telefone_id;
        try {
            if(!$telefone = $this->model->find($telefone_id)){
                return $this->sendError($this->NAO_LOCALIZADO);
            }
            $log = (new LogTelefone())->create($dados);
            return $this->sendResponse($this->SUCESSO);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }
    }

    public function store(Request $request){
        $dados = $request->all();

        // Regex para validar somente celular
        if (($dados['tags'] == 'Móvel')|($dados['tags'] == 'Móvel/Whatsapp')){
            $regex = '/[0-9]{2}[9][6789][0-9]{3}[0-9]{4}/';
        }
        // Regex para validar somente telefone
        if ($dados['tags'] == 'Fixo'){
            $regex = '/[0-9]{10,11}/';
        }

        if (mb_strlen($dados['telefone']) >= 10){
            if(preg_match($regex, $dados['telefone'])){
                try{
                    $dados['status'] = isset($dados['status']) ? true : false;

                    $tel_cad = $this->model->withTrashed()
                                ->where('telefone', $dados['telefone'])
                                            ->where('cliente_id', $dados['cliente_id'])
                                            ->get();

                    if(count($tel_cad) == 0) {
                        return $this->sendResponse($this->model->create($dados));
                    } else {
                        if ($tel_cad[0]?->deleted_at){
                            return $this->sendError('Este telefone já está na blacklist.');
                        }else{
                            return $this->sendError('Este telefone já está cadastrado para este cliente.');
                        }

                    }
                }catch(\Exception $e){
                    return $this->sendError($e->getMessage());
                }
            }else{
                return $this->sendError("Caracteres inválido!");
            }
        }else{
            return $this->sendError("Telefone Inválido!");
        }
    }

    public function delete($id){
        $telefone = $this->model->find($id);
        if(!$telefone){
            return $this->sendError($this->NAO_LOCALIZADO);
        }
        try {
            $telefone->forceDelete();
            return $this->sendResponse($this->SUCESSO);
        } catch (\Exception $e) {
            return $this->sendError($this->ERRO);
        }
    }

    public function adicionar_blacklist($id){
        $telefone = $this->model->withTrashed()
        ->where('id', $id)->first();

        if(!$telefone){
            return $this->sendError($this->NAO_LOCALIZADO);
        }
        try {
            if ($telefone?->deleted_at){
                return $this->sendError('Esse contato jÁ esta na blacklist.');
            }else{
                $telefone->delete();
                $this->eventosRepository->createEventos($telefone->cliente_id, 'Adicionado telefone na blacklist: '.$telefone->telefone.', FEITA PELO USUARIO: '.auth()->user()->name, 102);
                return $this->sendResponse($this->SUCESSO);
            }
        } catch (\Exception $e) {
            return $this->sendError($this->ERRO);
        }

    }

    public function retirar_blacklist($id){

        $telefone = $this->model->withTrashed()
        ->where('id', $id)->first();

        if(!$telefone?->deleted_at){
            return $this->sendError($this->NAO_LOCALIZADO);
        }
        try {
            $telefone->restore();
            $this->eventosRepository->createEventos($telefone->cliente_id, 'Retirado telefone da blacklist: '.$telefone->telefone.', FEITA PELO USUARIO: '.auth()->user()->name, 104);
            return $this->sendResponse($this->SUCESSO);
        } catch (\Exception $e) {
            return $this->sendError($this->ERRO);
        }

    }

    private function salvar_telefone($dados, $id = null){
        try {
            $dados['status'] = isset($dados['status']) ? $dados['status'] : false;

            $tel_cad = $this->model->where('telefone', $dados['telefone'])
                                    ->where('cliente_id', $dados['cliente_id'])
                                    ->where(function($query) use($id) {
                                        if($id){
                                            $query->where('id', '!=', $id);
                                        }
                                    })
                                    ->get();

            if(count($tel_cad) == 0) {

                if($id) {
                    $tel = Telefone::find($id);
                    $tel->update($dados);
                } else {
                    $tel = $this->model->create($dados);
                }
                return $tel;

            } else {
                throw new \Exception('Este telefone já está cadastrado para este cliente');
            }
        } catch (\Exception $e) {
            throw $e;
        }
    }
}
