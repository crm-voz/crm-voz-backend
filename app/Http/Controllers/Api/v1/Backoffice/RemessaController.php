<?php

namespace App\Http\Controllers\Api\v1\Backoffice;

use App\Repository\Backoffice\RemessaRepository;
use App\Http\Controllers\Controller;
use App\Http\Requests\RemessaRequest;
use App\Models\BackOffice\Remessa;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;

class RemessaController extends Controller
{
    protected $remessaRepository;
    public function __construct(RemessaRepository $remessa)
    {
        $this->remessaRepository = $remessa;
    }

    public function index(Request $request){
        //header("Access-Control-Allow-Origin: *");
        $filtros = [];
        if($request->hasAny('credor')) {
            $filtros['credor'] = $request->input('credor');
        }

        if($request->hasAny('data_remessa')) {
            $filtros['data_remessa'] = $request->input('data_remessa');
        }

        $remessas = $this->remessaRepository->indexList(filtros: $filtros);
        return $this->sendResponse($remessas);
    }

    public function create(){
        $remessas = Remessa::all();
        return $this->sendResponse($remessas);
    }

    public function edit($id){
        $remessa = $this->remessaRepository->find($id);
        $credores = $remessa->credor()->get();
        $operacoes = $remessa->operacoes()->with('cliente')->get();
        $cad_user = $remessa->usuario_empresa()->get()[0]->funcionarios()->get()[0]->nome;
        if(!$remessa){
            return $this->sendResponse($this->NAO_LOCALIZADO);
        }
        return $this->sendResponse(['remessa' => $remessa, 'credor' => $credores, 'operacoes' => $operacoes, 'usuario_cadastrou' => $cad_user]);
    }

    public function store(RemessaRequest $request){
        //header("Access-Control-Allow-Origin: *");
        ini_set('upload_max_filesize','10M');
        $dados = $request->all();
        try {
            $dados['numero_remessa'] = Carbon::now()->format('Ymdhms');
            $dados['usuario_empresa_id'] = auth()->user()->usuario_empresas[0]->id;
            $dados['status'] = 'NAO PROCESSADO';
            $dados['data_entrada'] = Carbon::now();

            $nameFile = null;
            // Verifica se informou o arquivo e se é válido
            if ($request->hasFile('remessa') && $request->file('remessa')->isValid()) {

                // Define um aleatório para o arquivo baseado no timestamps atual
                $name = uniqid(date('HisYmd'));

                // Recupera a extensão do arquivo
                $extension = $request->remessa->extension();

                // Define finalmente o nome
                $nameFile = "{$name}.{$extension}";

                // Faz o upload:
                $upload = $request->remessa->storeAs('remessas', $nameFile);
                // Se tiver funcionado o arquivo foi armazenado em storage/app/public/remessas/nomedinamicoarquivo.extensao

                if ( !$upload )
                   $this->sendResponse(false,'Erro ao fazer upload do arquivo.',500);

                $dados['nome_arquivo'] = $nameFile;

            }

            $response = $this->remessaRepository->create($dados);
            //Artisan::call('remessas:processar');

            return $this->sendResponse($response);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }
   }



    public function update(RemessaRequest $request, $id){
        $dados = $request->all();
        $remessas = $this->remessaRepository->find($id);

        if(!$remessas){
            return $this->sendError($this->NAO_LOCALIZADO);
        }
        try {
            $remessas->update($dados);
            return $this->sendResponse($this->SUCESSO);
        } catch (\Exception $e) {
            return $this->sendError($this->ERRO);
        }
   }

    public function destroy($id){
        try {
            $this->remessaRepository->deletar($id);
            return $this->sendResponse($this->SUCESSO);
        } catch (\Throwable $th) {
            return $this->sendError($th->getMessage());
        }

    }

    public function cancelar($id){
        try {
            return ($this->remessaRepository->cancel($id)) ? $this->sendResponse('Remessa cancelada com sucesso') : $this->sendError('Erro ao cancelar remessa');
        } catch (\Throwable $th) {
            return $this->sendError('Houve um erro ao cancelar a remessa: ' . $th->getMessage());
        }

    }

    // public function importar(Request $request){
    //     if( $request->file() ) {
    //         $file_arr = $request->file();
    //         $file = array_pop($file_arr);

    //         $retorno = $this->remessaRepository->importar($file, $request->input('credor_id'), $request->input('data_remessa'));
    //         if ($retorno['result']){
    //             return $this->sendResponse(['msg' => 'Remessa salva com sucesso', 'remessa' => $retorno['remessa']]);
    //         }else{
    //             return $this->sendError($retorno['erro']);
    //         }

    //     } else {
    //         return $this->sendError('O arquivo não foi recebido');
    //     }
    // }

    public function total_remessas_periodo(Request $request){
        return $this->sendResponse($this->remessaRepository->total_remessas_periodo_chart($request));
    }

    public function totalRemessasCredor(){
        $remessas = Remessa::where('credor_id',auth()->user()->credor_id)->count();

        if (!$remessas){
            return $this->sendError($this->NAO_LOCALIZADO);
        }

        return $this->sendResponse(['total_remessas' => $remessas]);
    }

    public function remessa_avulsa(Request $request){
        if( $request->file() ) {
                    $file_arr = $request->file();
                    $file = array_pop($file_arr);

                    $retorno = $this->remessaRepository->importar_remessa_avulsa($file, $request->input('data_remessa'), $request->input('credor_id'));
                    if ($retorno['result']){
                        return $this->sendResponse(['msg' => 'Remessa salva com sucesso', 'remessa' => $retorno['remessa']]);
                    }else{
                        return $this->sendError($retorno['erro']);
                    }

                } else {
                    return $this->sendError('O arquivo não foi recebido');
                }
    }

}
