<?php

namespace App\Http\Controllers\Api\v1\Backoffice;

use App\Http\Controllers\Controller;
use App\Models\BackOffice\Desconto;
use App\Repository\Backoffice\DescontosRepository;
use Exception;
use Illuminate\Http\Request;

use function chillerlan\QRCodePublic\send_response;

class DescontosController extends Controller
{
    protected $repository;

    /**
     * @author Welliton Cunha
     * @since 11.08.2021 às 10:17
     * @see Descontos
     */

    public function __construct(DescontosRepository $desconto)
    {
        $this->repository = $desconto;
    }

    public function index(){
        $desconto = $this->repository->all();

        if(!$desconto){
            return $this->sendError($this->NAO_LOCALIZADO);
        }

        return $this->sendResponse(['descontos' => $desconto]);
    }

    public function store(Request $request){
        try {

            $desconto = $request->all();
            $this->repository->create($desconto);

            return $this->sendResponse($this->SUCESSO);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }
    }

    public function update(Request $request, $id){
        $dados = $request->all();
        $desconto = $this->repository->find($id);

        if (!$desconto){
            return $this->sendError($this->NAO_LOCALIZADO);
        }

        try {
            $desconto->update($dados);
            return $this->sendResponse($this->SUCESSO);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }
    }

    public function destroy($id){
        $desconto = $this->repository->find($id);

        if(!$desconto){
            return $this->sendError($this->NAO_LOCALIZADO);
        }
        try {
            $desconto->delete();
            return $this->sendResponse($this->SUCESSO);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }
    }

    public function edit($id){
        $desconto = $this->repository->find($id);

        if(!$desconto){
            return $this->sendError($this->NAO_LOCALIZADO);
        }

        return $this->sendResponse(['descontos' => $desconto]);
    }

    public function importar(Request $request){
        if( $request->file() ) {
            try {
                $file_arr = $request->file();
                $file = array_pop($file_arr);
                if (!$this->repository->importar($file, $request->input('credor_id'), $request->input('tipo'))) {
                    throw new Exception("Falha ao processar planilha de desconto.", 1);
                }
                return $this->sendResponse([true]);
            } catch (\Exception $err) {
                return $this->sendError('Houve um erro... ' . $err->getMessage());
            }
        } else {
            return $this->sendError('O arquivo não foi recebido');
        }
    }

}
