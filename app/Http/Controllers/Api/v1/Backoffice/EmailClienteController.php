<?php

namespace App\Http\Controllers\Api\v1\Backoffice;

use App\Repository\Backoffice\ClientesRepository;
use App\Http\Controllers\Controller;
use App\Mail\SendMailCliente;
use App\Models\BackOffice\Cliente;
use App\Models\BackOffice\Email;
use App\Repository\Backoffice\EventosRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use InvalidArgumentException;

class EmailClienteController extends Controller
{
    protected $model;
    protected ClientesRepository $clienteRepository;

    public function __construct(Email $email, ClientesRepository $clienteRepository, EventosRepository $eventos)
    {
        $this->model = $email;
        $this->clienteRepository = $clienteRepository;
        $this->eventosRepository = $eventos;
    }

    public function index($id){
        $cliente = Cliente::find($id);
        $emails = $cliente->emails()->get();
        return $this->sendResponse(['cliente' => $cliente, 'emails' => $emails]);
    }

    public function store(Request $request){
        $dados = $request->all();
        try {
            return $this->sendResponse($this->salvar_email($dados));
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }
    }

    public function edit($id){
        $email = Email::find($id);
        if(!$email){
            return $this->sendError($this->NAO_LOCALIZADO);

        }
        $email->cliente;
        return $this->sendResponse(['email' => $email]);
    }

    public function update(Request $request, $id){
        $dados = $request->all();
        try {
            $email = Email::find($id);
            if (!$email){
                return $this->sendError($this->NAO_LOCALIZADO);
            }
            $this->clienteRepository->criarEvento($email->cliente_id,'ATUALIZADO EMAIL',103);
            return $this->sendResponse($this->salvar_email($dados, $id));

        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }
    }

    public function send(Request $request, $id){
        $cliente = $this->clienteRepository->get($id);
        $dados = $request->all();
        if(!$cliente){
            return $this->sendResponse($this->NAO_LOCALIZADO);
        }
        try {
            Mail::to(env('MAIL_FROM_ADDRESS'))
                ->queue(
                        new SendMailCliente($cliente, $dados)
                    );
            return $this->sendResponse($this->SUCESSO);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }
    }

    public function delete($id){
        $email = $this->model->find($id);
        if(!$email){
            return $this->sendError($this->NAO_LOCALIZADO);
        }
        try {
            $email->forceDelete();
            return $this->sendResponse($this->SUCESSO);
        } catch (\Exception $e) {
            return $this->sendError($this->ERRO);
        }

    }

    public function adicionar_blacklist($id){
        $email = $this->model->withTrashed()
        ->where('id', $id)->first();

        if(!$email){
            return $this->sendError($this->NAO_LOCALIZADO);
        }
        try {
            if ($email?->deleted_at){
                return $this->sendError('Esse email já esta na blacklist.');
            }else{
                $email->delete();
                $this->eventosRepository->createEventos($email?->cliente_id, 'Adicionado email na blacklist: '.$email?->email.', FEITA PELO USUARIO: '.auth()->user()->name, 103);
                return $this->sendResponse($this->SUCESSO);
            }
        } catch (\Exception $e) {
            return $this->sendError($this->ERRO);
        }

    }

    public function retirar_blacklist($id){

        $email = $this->model->withTrashed()
        ->where('id', $id)->first();

        if(!$email?->deleted_at){
            return $this->sendError($this->NAO_LOCALIZADO);
        }
        try {
            $email->restore();
            $this->eventosRepository->createEventos($email?->cliente_id, 'Retirado email da blacklist: '.$email?->email.', FEITA PELO USUARIO: '.auth()->user()->name, 105);
            return $this->sendResponse($this->SUCESSO);
        } catch (\Exception $e) {
            return $this->sendError($this->ERRO);
        }

    }


    private function salvar_email($dados, $id = null){
        try {
            if (filter_var($dados['email'], FILTER_VALIDATE_EMAIL) === false){
                throw new InvalidArgumentException('Endereço de email inválido.');
            }
            $email_cad = $this->model->withTrashed()
                                    ->where('email', $dados['email'])
                                    ->where('cliente_id', $dados['cliente_id'])
                                    ->get();

            if(count($email_cad) == 0) {

                if($id) {
                    $em = Email::find($id);
                    $em->update($dados);
                } else {
                    $em = $this->model->create($dados);
                }

                return $em;
            } else {
                if ($email_cad[0]?->deleted_at){
                    return $this->sendError('Este email já está na blacklist.');
                }else{
                    throw new \Exception('Este email já está cadastrado para este cliente');
                }
            }
        } catch (\Exception $e) {
            throw $e;
        }
    }

}
