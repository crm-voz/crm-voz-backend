<?php

namespace App\Http\Controllers\Api\v1\Backoffice;

use App\Http\Controllers\Controller;
use App\Gateway\Email\iEmail;
use App\Gateway\Email\Avulso;
use App\Mail\SendMailSinteticoNegociacao;
use App\Models\Cobranca\Acordo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class EmailController extends Controller
{
    protected iEmail $plataforma_padrao;
    protected $modelAcordo;

    public function __construct(Avulso $plat, Acordo $acordo)
    {
        $this->plataforma_padrao = $plat;
        $this->modelAcordo = $acordo;
    }

    public function enviar_email_avulso(Request $request) {
        try {
            return $this->sendResponse($this->plataforma_padrao->enviar_email($request->all()));
        } catch (\Throwable $err) {
            return $this->sendError($err->getMessage());
        }
    }

    public function teste(){

        $data = [
            'conteudo' => 'Email teste - grupo voz!',
            'cliente' => 'Welliton Cunha'
        ];

        Mail::to('welliton@grupovoz.com.br')->send(new SendMailSinteticoNegociacao($data));
    }

    public function enviar_acordo_sintetico(Request $request){
        // $request->id_acordo = 3;

            $acordo = Acordo::find($request->id_acordo);

           /* $acordo = DB::table('acordo')
            ->join('operacao_acordo', 'operacao_acordo.acordos_id', '=', 'acordo.id')
            ->join('operacoes', 'operacoes.id', '=', 'operacao_acordo.operacoes_id')
            ->join('remessas', 'remessas.id', '=', 'operacoes.remessa_id')
            ->join('credores', 'credores.id', '=', 'remessas.credor_id')
            ->join('cliente', 'cliente.id', '=', 'operacoes.cliente_id')
            ->join('cidades', 'cidades.id', '=', 'cliente.cidades_id')
            ->where('acordo.id',$request->id_acordo)
            ->select('credores.nome as credor', 'cliente.nome as cliente', 'cliente.cpf_cnpj','cliente.endereco','cliente.bairro',
            'cliente.cep','cidades.nome as cidade','cidades.uf','acordo.numero_acordo','acordo.created_at','acordo.valor_nominal','acordo.desconto',
            'acordo.valor_parcela')
            ->get();

            $parcela = DB::table('acordo')
            ->join('operacao_acordo', 'operacao_acordo.acordos_id', '=', 'acordo.id')
            ->join('operacoes', 'operacoes.id', '=', 'operacao_acordo.operacoes_id')
            ->join('remessas', 'remessas.id', '=', 'operacoes.remessa_id')
            ->join('credores', 'credores.id', '=', 'remessas.credor_id')
            ->join('cliente', 'cliente.id', '=', 'operacoes.cliente_id')
            ->join('cidades', 'cidades.id', '=', 'cliente.cidades_id')
            ->join('parcelas', 'parcelas.acordos_id', '=', 'acordo.id')
            ->join('boleto', 'boleto.parcelas_id', '=', 'parcelas.id')
            ->where('acordo.id',$request->id_acordo)
            ->select('parcelas.*','boleto.linha_digitavel')
            ->orderBy('numero_parcela', 'asc')
            ->get(); */

        if (!$acordo){
            return $this->sendError($this->NAO_LOCALIZADO);
        }

        $data = [
            'credor' => $acordo->operacoes_acordo[0]->operacao[0]->remessa[0]->credor_id,
            'cliente' => $acordo->operacoes_acordo[0]->operacao[0]->cliente->cliente_id,
            'cpf_cnpj' => $acordo->operacoes_acordo[0]->operacao[0]->cliente->cpf_cnpj,
            'endereco' => $acordo->operacoes_acordo[0]->operacao[0]->cliente->endereco.' - '.$acordo->operacoes_acordo[0]->cliente->bairro,
            'cidade' => $acordo->operacoes_acordo[0]->operacao[0]->cliente->cidade->nome,
            'uf' => $acordo->operacoes_acordo[0]->operacao[0]->cliente->cidade->uf,
            'numero_acordo' => $acordo->numero_acordo,
            'data_acordo' => $acordo->created_at,
            'valor_acordo' => $acordo->valor_nominal,
            'desconto_acordo' => $acordo->desconto,
            'valor_parcela_acordo' => $acordo->valor_parcela,
            'obrservacao' => 'Email teste - grupo voz!'
        ];

        if ($request->email){
            Mail::to($request->email)->send(new SendMailSinteticoNegociacao($data, $acordo->parcela_acordo));
        }else{
            return new SendMailSinteticoNegociacao($data,$acordo->parcela_acordo);
            return $this->sendResponse($data, $acordo->parcela_acordo);
        }
    }
}
