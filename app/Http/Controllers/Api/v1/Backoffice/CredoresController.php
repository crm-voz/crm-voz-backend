<?php

namespace App\Http\Controllers\Api\v1\Backoffice;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\BackOffice\Credor;
use App\Models\BackOffice\Cliente;
use App\Models\Core\Cidade;
use App\Models\Core\Empresa;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Repository\Backoffice\CredorRepository;

class CredoresController extends Controller
{
    protected $credorRepository;

    public function __construct(CredorRepository $credorRepository)
    {
        $this->credorRepository = $credorRepository;
    }

    public function index(){
        $credores = $this->credorRepository->index();
        return $this->sendResponse($credores);
    }

    public function filtro(Request $request){
        $filtro = $request->input('filtro');
        $items_por_pag = intval($request->input('items_pag', 15));
        $item_pag = ( $request->hasAny(['pag']) ) ? intval($request->input('pag') - 1) * $items_por_pag : 0;
        return $this->sendResponse($this->credorRepository->filtro($filtro, $items_por_pag, $item_pag));
    }

    public function update(Request $request, $id){
        $dados = $request->all();
        $empresas = $this->credorRepository->find($id);

        if (!$empresas){
            return $this->sendResponse($this->NAO_LOCALIZADO);
        }

        try {
            $empresas->update($dados);
            return $this->sendResponse($this->SUCESSO);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }
    }

    public function store(Request $request){
        $dados = $request->all();

        if( $request->hasAny('empresa_id') )
        {
            try {
                $created_model = $this->credorRepository->create($dados);
            } catch (\Exception $e) {
                return $this->sendError('Já existe um credor cadastrado com este CNPJ ou e-mail');
                // return $this->sendError($e->getMessage());
            }

            return $this->sendResponse($created_model);
        } else {
            return $this->sendError('Verifique se há o campo \'empresa_id\'.');
        }

    }

    public function destroy($id){
        $dados = $this->credorRepository->find($id);
        if(!$dados){
            return $this->sendError($this->NAO_LOCALIZADO);
        }
        try {
            $dados->delete();
            return $this->sendResponse($this->SUCESSO);
        } catch (\Exception $e) {
            return $this->sendError('Verifique se não existem registros relacionados a este credor antes');
        }
    }

    public function create(){
        $credores = $this->credorRepository->all();
        $cidades = Cidade::all();
        $empresas = Empresa::all();
        return $this->sendResponse(['empresas' => $credores, 'cidades'=>$cidades, 'empresas'=>$empresas]);
    }

    public function edit($id){
        $credores = $this->credorRepository->find($id);
        $credores->parametros_config = json_decode($credores->parametros_config);
        $cidades = $credores->cidade()->getQuery()->get();
        $empresas = $credores->empresa()->getQuery()->get();

        if(!$credores){
            return $this->sendError($this->NAO_LOCALIZADO);
        }

        return $this->sendResponse(['credores' => $credores, 'cidades' => $cidades, 'empresas' => $empresas]);
    }

    public function listar($todos = false){
        return $this->sendResponse($this->credorRepository->ListarCredores(filter_var($todos, FILTER_VALIDATE_BOOL)));
    }

    public function clientes($id) {
        try {
            //code...
            $cliente = Cliente::select(DB::raw('cliente.*'))
                ->join('operacoes', 'operacoes.cliente_id', '=','cliente.id' )
                ->join('remessas', 'operacoes.remessa_id', '=', 'remessas.id')
                ->join('credores', 'remessas.credor_id', '=', 'credores.id')
                ->where('credores.id', $id)
                ->orderBy('cliente.nome')
                ->distinct(DB::raw('cliente.nome'))
                ->get();
        } catch (\Throwable $th) {
            return $this->sendError($th);
        }

        return $this->sendResponse($cliente);
    }

    public function remessas($id) {
        return Credor::find($id)->remessas()->get();
    }

    public function store_parametros(Request $request, $id) {
        try {
            $credor = $this->credorRepository->find($id);
            $json = $request->all();
            $credor->parametros_config = $json;
            $credor->save();

            return $this->sendResponse($credor);
        } catch (\Throwable $th) {
            return $this->sendError('Falha ao atualizar o parametro. ', [$th->getMessage()]);
        }
    }

    public function clienteCredor(){
        $cliente =  DB::table('operacoes')
            ->join('remessas', 'remessas.id', '=', 'operacoes.remessa_id')
            ->join('credores', 'credores.id', '=', 'remessas.credor_id')
            ->join('cliente', 'cliente.id', '=', 'operacoes.cliente_id')
            ->select('cliente.*')
            ->where('credores.id','=',auth()->user()->credor_id)
            ->distinct('cliente.nome')
            ->orderBy('cliente.nome')
            ->get();

            if ($cliente->isEmpty()){
                return $this->sendError($this->NAO_LOCALIZADO);
            }

            foreach ($cliente as $value) {
                $value->status_cliente_id = json_decode($value->status_cliente_id);
            }

            return $this->sendResponse($cliente);
    }

    public function gerar_estatisticas(Request $request) {
        $stats_repo = $this->credorRepository->stats_operacao($request->input('credor') ? $request->input('credor') : null);

        $json_response = [
            "data" => Carbon::now()->format('d-m-Y h:m:s'),
            "registros" => [
                $stats_repo
            ]
        ];

        return $this->sendResponse($json_response);
    }

    public function ListarAcordos($id){
        return $this->sendResponse($this->credorRepository->listarAcordosCredor($id));
    }

    public function credorCpf($id){
        try {
            $cliente = Cliente::select(DB::raw('cliente.id as cliente_id'),DB::raw('cliente.nome as cliente'),'credores.*')
                ->join('operacoes', 'operacoes.cliente_id', '=','cliente.id' )
                ->join('remessas', 'operacoes.remessa_id', '=', 'remessas.id')
                ->join('credores', 'remessas.credor_id', '=', 'credores.id')
                ->where('cliente.cpf_cnpj', $id)
                ->distinct(DB::raw('credores.nome'))
                ->get();
            if ($cliente != '[]'){
                return $this->sendResponse($cliente);
            }else{
                return $this->sendError(false);
            }
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function parametrosCredor($id){
        try {
            $credor = Credor::find($id);

            if ($credor){
                return $this->sendResponse(json_decode($credor->parametros_config));
            }else{
                return $this->sendError(false);
            }
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function principal(){
        try {
            return $this->sendResponse(Credor::all());
        } catch (\Throwable $th) {
            throw $th;
        }
    }
}
