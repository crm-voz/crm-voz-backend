<?php

namespace App\Http\Controllers\Api\v1\Backoffice;

use App\Repository\Backoffice\ClientesRepository;
use App\Repository\Backoffice\OperacaoRepository;
use App\Http\Controllers\Controller;
use App\Http\Requests\OperacoesRequest;
use App\Models\BackOffice\Credor;
use App\Models\BackOffice\Operacao;
use App\Models\BackOffice\Remessa;
use App\Models\Core\ClassificacaoNegativacao;
use App\Models\Core\MotivoPendencia;
use App\Models\Core\StatusOperacao;
use App\Repository\Administrador\LogPendenciasRepository;
use App\Repository\Backoffice\CampanhasRepository;
use App\Repository\Cobranca\AcordoRepository;
use App\Services\OperacaoServices as ServicesOperacaoServices;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;


class OperacaoController extends Controller
{
    protected $operacaoRepository;
    protected $clientesRepository;
    protected $acordoRepository;
    protected $model;
    protected $logPendenciasRepository;

    public function __construct(OperacaoRepository $operacaoRepository,
                                ClientesRepository $clientesRepository,
                                AcordoRepository $acordo,
                                CampanhasRepository $campanhasRepository,
                                LogPendenciasRepository $logPendencia)
    {
        $this->operacaoRepository = $operacaoRepository;
        $this->clientesRepository = $clientesRepository;
        $this->acordoRepository = $acordo;
        $this->campanhasRepository = $campanhasRepository;
        $this->logPendenciasRepository = $logPendencia;
    }

    public function index(){
        $operacoes = $this->operacaoRepository->getIndex();
        return $this->sendResponse(['operacoes' => $operacoes]);
    }

    public function filtro(Request $request){
        $operacoes = $this->operacaoRepository->filtrar($request->input('filtros'))->limit(1000)->get();
        $operacaoServices = new ServicesOperacaoServices($operacoes);
        return $this->sendResponse(['operacoes' =>  $operacaoServices->getLista()]);
    }

    public function create($id = null){
        $cliente = $this->clientesRepository->get($id);
        $MotivoPendencia = MotivoPendencia::all();
        $classificacaoNegativacao = ClassificacaoNegativacao::all();
        $statusOperacao = StatusOperacao::all();
        $credores = Credor::all();
        $remessas = Remessa::all();

        return $this->sendResponse([
            'cliente' => $cliente,
            'MotivoPendencia'=> $MotivoPendencia,
            'classificacaoNegativacao'=> $classificacaoNegativacao,
            'statusOperacao' => $statusOperacao,
            'remessas' => $remessas,
            'credores' => $credores
        ]);
    }

    public function store(OperacoesRequest $request){
        try {
            $dados = $request->all();
            $dados['data_processamento'] = Carbon::now();
            $dados['usuario_empresas_id'] = auth()->user()->usuarios_empresas[0]->id;
            $dados['tipo_operacao_id'] = 1; //temporario
            $op = $this->operacaoRepository->create($dados);
            return $this->sendResponse($op);

        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }
    }

    public function edit($id){
        $operacao = $this->operacaoRepository->find($id);
        $operacao->cliente;
        $operacao->status_operacao;

        if(!$operacao){
            return $this->sendError($this->NAO_LOCALIZADO);
        }
        return $this->sendResponse(['operacao' =>  $operacao]);
    }

    public function update(OperacoesRequest $request, $id){
        $dados = $request->all();
        $operacao = $this->operacaoRepository->find($id);

        if(!$operacao){
            return $this->sendError($this->NAO_LOCALIZADO);
        }
        try {
            $operacao->update($dados);
            return $this->sendResponse(["mensagem" => $this->SUCESSO, "cliente_id" => $dados['cliente_id']]);
        } catch (\Exception $e) {
            return $this->sendError($this->ERRO);
        }

    }

    public function destroy($id){
        $cliente = $this->operacaoRepository->find($id);
        if(!$cliente){
            return $this->sendError($this->NAO_LOCALIZADO);
        }
        try {
            $cliente->delete();
            return $this->sendResponse($this->SUCESSO);
        } catch (\Exception $e) {
            return $this->sendError($this->ERRO);
        }
    }

    public function update_lot_motivo_pendencia(Request $request){
        $motivo_pendencia_id  = $request->input('motivo_pendencia_id');
        $ops_ids    = $request->input('ops_ids');
        $description = $request->input('description');
        $motivoId    = $request->input('motivoId');
        if(!empty($ops_ids)) {
            $this->logPendenciasRepository->criarLogPadrao($ops_ids);
            $count = $this->operacaoRepository->updateLoteMotivoPendencia($ops_ids,$motivo_pendencia_id, $description, $motivoId);
            return $this->sendResponse('Operações atualizadas: ' . $count);
        } else {
            return $this->sendError('Nenhum ID de operação foi enviado');
        }
    }

    public function update_lot_status_operacao(Request $request){
        $status_operacao_id  = $request->input('status_operacao_id');
        $ops_ids    = $request->input('ops_ids');
        $description = $request->input('description');
        $motivoId    = $request->input('motivoId');

        if(!empty($ops_ids)) {
            $count = $this->operacaoRepository->updateLoteStatusOperacao($ops_ids,$status_operacao_id, $description, $motivoId);
            $this->sendResponse('Operações atualizadas: ' . $count);
        } else {
            return $this->sendError('Nenhum ID de operação foi enviado');
        }
    }

    public function update_lot_negativado_operacao(Request $request){
        $classificacao_negativacao_id  = $request->input('classificacao_negativacao_id');
        $ops_ids    = $request->input('ops_ids');
        $description = $request->input('description');
        $motivoId    = $request->input('motivoId');

        if(!empty($ops_ids)) {
            $count = $this->operacaoRepository->updateLoteNegativadoOperacao($ops_ids,$classificacao_negativacao_id, $description, $motivoId);
            $this->sendResponse('Operações atualizadas: ' . $count);
        } else {
            return $this->sendError('Nenhum ID de operação foi enviado');
        }
    }

    public function operacoesCredor(){
        $id = auth()->user()->credor_id;
        $operacoes = Operacao::whereHas('remessa', function(Builder $query) use($id) {
            $query->where('credor_id', $id);
        })->with('cliente','motivo_pendencia','status_operacao')->get();

        if ($operacoes->isEmpty()){
            return $this->sendError($this->NAO_LOCALIZADO);
        }

        return $this->sendResponse(['operacoes' => $operacoes]);
    }

    public function remessasCredor(){
        $remessas = Remessa::where('credor_id',auth()->user()->credor_id)->get();

        if ($remessas->isEmpty()){
            return $this->sendError($this->NAO_LOCALIZADO);
        }

        return $this->sendResponse(['remessas' => $remessas]);
    }

    public function campanhasCredor(){
        $campanhas = $this->campanhasRepository->campanhasCredor();

        if ($campanhas->isEmpty()){
            return $this->sendError($this->NAO_LOCALIZADO);
        }

        return $this->sendResponse(['campanhas' => $campanhas]);
    }

    public function acordosCredor(){
        return $this->sendResponse($this->acordoRepository->acordoCredor());
    }

    public function motivo_pendencia(){
        return $this->sendResponse($this->operacaoRepository->motivo_pendencia_chart());
    }

    public function motivo_devolucao(){
        return $this->sendResponse($this->operacaoRepository->motivo_devolucao_chart());
    }

    public function status_operacao(){
        return $this->sendResponse($this->operacaoRepository->status_operacao_chart());
    }

    public function pendencia(){
        return $this->operacaoRepository->pendencia_chart();
    }

    public function valor_nominal_ano_vencimento(){
        return $this->sendResponse($this->operacaoRepository->valor_nominal_ano_vencimento_chart());
    }

    public function titulo_aberto_periodo(Request $request){
        return $this->sendResponse($this->operacaoRepository->titulo_aberto_periodo_chart($request));
    }

    public function negativados(){
        return $this->sendResponse($this->operacaoRepository->negativados());
    }

    public function operacoes_negativados_cliente($id){
        return $this->sendResponse($this->operacaoRepository->operacoes_negativados_cliente($id));
    }

    public function totalOperacoesCredor(){
        $id = auth()->user()->credor_id;
        $operacoes = Operacao::whereHas('remessa', function(Builder $query) use($id) {
            $query->where('credor_id', $id);
        })->with('cliente','motivo_pendencia','status_operacao')->count();

        if (!$operacoes){
            return $this->sendError($this->NAO_LOCALIZADO);
        }

        return $this->sendResponse(['total_operacoes' => $operacoes]);
    }

    public function operacaoCpf(Request $request,$id){
        try {
            $cliente = $this->clientesRepository->operacoesPorCpf($id,$request->credor_id);
            if ($cliente != '[]'){
                return $this->sendResponse($cliente);
            }else{
                return $this->sendError(false);
            }
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function listarOperacoesPendentes(){
       return $this->sendResponse($this->operacaoRepository->listarOperacoesPendente());
    }


}
