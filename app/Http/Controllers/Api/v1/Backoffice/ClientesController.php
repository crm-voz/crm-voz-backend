<?php

namespace App\Http\Controllers\Api\v1\Backoffice;

use App\Gateway\Dados\IEnriquecimento;
use App\Http\Controllers\Controller;
use App\Http\Requests\ClientesRequest;
use App\Models\BackOffice\Credor;
use App\Repository\Backoffice\ClientesRepository;
use App\Models\BackOffice\Operacao;
use App\Models\Core\Eventos;
use App\Models\Core\StatusLocalizacao;
use App\Repository\Backoffice\EventosRepository;
use Illuminate\Http\Request;
use Target;

class ClientesController extends Controller
{
    protected $clientesRepository;
    public function __construct(ClientesRepository $clientesRepository,
                                StatusLocalizacao $status_cliente,
                                EventosRepository $eventosRepository,
                                Operacao $operacao)
    {
        $this->modelStatusCliente = $status_cliente;
        $this->eventosRepository = $eventosRepository;
        $this->modelOperacao = $operacao;
        $this->clientesRepository = $clientesRepository;

    }

    public function index(){
        $clientes = $this->clientesRepository->get();
        return $this->sendResponse( $clientes );
    }

    public function filtroGeral(Request $request){
        $filtro_arr = $request->all();
        $clientes = $this->clientesRepository->filtrar($filtro_arr);
        return $this->sendResponse($clientes);
    }

    public function filtroFull(Request $request){
        $filtro_arr = $request->all();
        $clientes = $this->clientesRepository->pesquisar($filtro_arr); //filtrar
        return $this->sendResponse($clientes);
    }

    public function filtro(Request $request){
        $result = $this->clientesRepository->search($request->input('filtro'));
        return $this->sendResponse($result);
    }

    public function create(){
        return $this->sendResponse($this->clientesRepository->getCreate());
    }

    public function created(){
        return $this->sendResponse($this->clientesRepository->created());
    }

    public function store(Request $request){
        $dados = $request->all();

        try {
            $created_model = $this->clientesRepository->create($dados);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }

        return $this->sendResponse($created_model);
    }

    public function update(ClientesRequest $request, $id){
        $dados = $request->all();
        $cliente = $this->clientesRepository->find($id);
        if(!$cliente){
            return $this->sendError($this->NAO_LOCALIZADO);
        }
        try {
            $this->clientesRepository->enderecoAnterior($cliente, $dados);
            $cliente_updated = $cliente->update($dados);
            return $this->sendResponse($cliente_updated);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }
    }

    public function data_cadastro_confirmado(Request $request, $id){
        $cliente = $this->clientesRepository->find($id);
        if(!$cliente){
            return $this->sendError($this->NAO_LOCALIZADO);
        }
        try {
            if ($request->confirmado == true){
                $cliente_updated = $cliente->update(['data_cadastro_confirmado' => now()]);
                $this->clientesRepository->criarEvento($id,' Cadastro Confirmado ' . $cliente->nome, 8);
                return $this->sendResponse($cliente_updated);
            }
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }

    }

    public function updateLoteLocalizacao(Request $request){
        $localizacao    = $request->input('localizacao');
        $clientes_ids   = $request->input('clientes_ids');

        if(!empty($clientes_ids)) {
            try {
                $count = $this->clientesRepository->updatestatusLocalizacao($clientes_ids,$localizacao);
                return $this->sendResponse('Operações atualizadas: ' . $count);
            } catch (\Throwable $th) {
                return $this->sendError('Houve um erro... ' . $th->getMessage());
            }
        } else {
            return $this->sendError('Nenhum ID de operação foi enviado');
        }
    }

    public function edit($id)
    {
        $cliente =  $this->clientesRepository->find($id);
        if(!$cliente){
            return $this->sendError($this->NAO_LOCALIZADO);
        }
        $cliente->cidades;
        $cliente->status_cliente_id = json_decode($cliente->status_cliente_id);

        return $this->sendResponse(['cliente' => $cliente]);
    }

    public function destroy($id){
        $cliente = $this->clientesRepository->find($id);
        if(!$cliente){
            return $this->sendError($this->NAO_LOCALIZADO);
        }
        try {
            $cliente->delete();
            return $this->sendResponse($this->SUCESSO);
        } catch (\Exception $e) {
            return $this->sendError($this->ERRO . $e->getMessage());
        }
    }

    public function operacoes($id) {
        $ops = $this->modelOperacao->where('cliente_id',$id)->with('status_operacao')->get();
        return $this->sendResponse($ops);
    }

    public function operacoesAcordo($id) {
        return $this->sendResponse($this->clientesRepository->operacoesAcordo($id));
    }

    public function updateLote(Request $request){
        $status_id      = $request->input('status_id');
        $clientes_ids   = $request->input('clientes_ids');

        if(!empty($clientes_ids)) {
            try {
                $count = $this->clientesRepository->updateLote($clientes_ids, $status_id);

                return $this->sendResponse('Operações atualizadas: ' . $count);
            } catch (\Throwable $th) {
                return $this->sendError('Houve um erro... ' . $th->getMessage());
            }
        } else {
            return $this->sendError('Nenhum ID de operação foi enviado');
        }
    }

    public function updateLoteStatusClienteMult(Request $request){
        $cliente_status_id  = $request->input('status_cliente_id');
        $clientes_ids   = $request->input('clientes_ids');

        if(!empty($clientes_ids)) {
            try {
                $count = $this->clientesRepository->updateClassificacaoMultiplosClientes($clientes_ids, $cliente_status_id);
                return $this->sendResponse('Operações atualizadas: ' . $count);
            } catch (\Throwable $th) {
                return $this->sendError('Houve um erro... ' . $th->getMessage());
            }
        } else {
            return $this->sendError('Nenhum ID de operação foi enviado');
        }
    }

    public function enriquecerDadosCliente(Request $request)
    {
        $tipo = $request['tipo']; //se é por cpf, nome, telefone ou email
        $value = $request['valor'];
        return $this->sendResponse($this->clientesRepository->enriquecerClientes($tipo, $value));
    }

    public function clienteLocalizacaoChart(){
        return $this->clientesRepository->localizacaoChart();
    }

    public function statusCliente(){
        return $this->sendResponse($this->clientesRepository->statusClienteChart());
    }

    public function totalClientePeriodo(Request $request){
        return $this->sendResponse($this->clientesRepository->totalClientePeriodoChart($request));
    }

    public function eventosCliente($id){
        return $this->sendResponse($this->eventosRepository->get($id));
    }

    public function eventosClient($id){
        $eventos = Eventos::where('cliente_id',$id)->get();
        $event = collect();
        foreach($eventos as $evento){
            $event->add([
                'id' => $evento->id,
                'descricao' => $evento->descricao,
                'created_at' => $evento->created_at,
                'usuario_empresas' => [
                    'id' => $evento->usuario_empresas->user->id,
                    'nome' => $evento->usuario_empresas->user->name,
                ],
                'motivo_evento' => [
                    'id' => $evento->motivo_evento->id,
                    'nome' => $evento->motivo_evento->nome
                ]
            ]);
        }
        return ['eventos' => $event];
    }

    public function totalClientesCredor(){
        $cliente =  Operacao::join('remessas', 'remessas.id', '=', 'operacoes.remessa_id')
            ->join('credores', 'credores.id', '=', 'remessas.credor_id')
            ->join('cliente', 'cliente.id', '=', 'operacoes.cliente_id')
            ->select('cliente.*')
            ->where('credores.id','=',auth()->user()->credor_id)
            ->distinct('cliente.nome')
            ->orderBy('cliente.nome')
            ->count();

            if (!$cliente){
                return $this->sendError($this->NAO_LOCALIZADO);
            }

            return $this->sendResponse(['total_clientes' => $cliente]);
    }


}
