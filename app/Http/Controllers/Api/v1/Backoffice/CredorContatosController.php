<?php

namespace App\Http\Controllers\Api\v1\Backoffice;

use App\Http\Controllers\Controller;
use App\Models\BackOffice\CredorContato;
use Illuminate\Http\Request;

class CredorContatosController extends Controller
{
    protected $model;

    public function __construct(CredorContato $credorContato)
    {
        $this->model = $credorContato;
    }

    public function index($id){
        try {
            $credorContato = $this->model->where('credor_id',$id)->get();
            foreach ($credorContato as $value) {
                $value->relatorios = json_decode($value->relatorios);
            }

            if (!$credorContato->isEmpty()){
                return $this->sendResponse($credorContato);
            }else{
                return $this->sendError($this->NAO_LOCALIZADO);
            }
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function store(Request $request){
        try {
            $CredorContato = new CredorContato();
            $CredorContato->nome = $request->nome;
            $CredorContato->telefone = $request->telefone;
            $CredorContato->email = $request->email;
            $CredorContato->observacoes = $request->observacoes;
            $CredorContato->credor_id = $request->credor_id;
            $CredorContato->departamento = $request->departamento;
            $CredorContato->relatorios = json_encode($request->relatorios);
            $CredorContato->save();
            return $this->sendResponse($this->SUCESSO);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }
    }

    public function update(Request $request, $id){
        $dados = $request->all();
        $credorContato = $this->model->find($id);
        if(!$credorContato){
            return $this->sendError($this->NAO_LOCALIZADO);
        }
        try {
            $credorContato_update = $credorContato->update($dados);
            return $this->sendResponse($credorContato_update);
        } catch (\Exception $e) {
            return $this->sendError($this->ERRO);
        }
    }

    public function destroy($id){
        $dados = $this->model->where('id', $id);
        if(!$dados){
            return $this->sendError($this->NAO_LOCALIZADO);
        }
        try {
            $dados->delete();
            return $this->sendResponse($this->SUCESSO);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }
    }

    public function edit(Request $request, $id){

        $credorContato = $this->model->find($id);
        $credorContato->relatorios = json_decode($credorContato->relatorios);
        if(!$credorContato){
            return $this->sendError($this->NAO_LOCALIZADO);
        }

        return $this->sendResponse($credorContato);
    }
}
