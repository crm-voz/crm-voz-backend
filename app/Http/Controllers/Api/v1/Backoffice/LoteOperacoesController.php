<?php

namespace App\Http\Controllers\Api\v1\Backoffice;

use App\Http\Controllers\Controller;
use App\Repository\Backoffice\LoteOperacoesRepository;
use Exception;
use Illuminate\Http\Request;

class LoteOperacoesController extends Controller
{
    protected $repository;
    public function __construct(LoteOperacoesRepository $loteoperacoesrepository)
    {
       $this->repository = $loteoperacoesrepository;
    }

    public function importar(Request $request){
        if( $request->file() ) {
            try {
                $file_arr = $request->file();
                $file = array_pop($file_arr);
                if (!$this->repository->importar($file->getPathName())) {
                    throw new Exception("Falha ao processar planilha de Lote de Operações.", 1);
                }
                return $this->sendResponse([true]);
            } catch (\Exception $err) {
                return $this->sendError('Houve um erro... ' . $err->getMessage());
            }
        } else {
            return $this->sendError('O arquivo não foi recebido');
        }
    }
}
