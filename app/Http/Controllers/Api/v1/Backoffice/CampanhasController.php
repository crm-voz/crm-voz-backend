<?php

namespace App\Http\Controllers\Api\v1\Backoffice;

use App\Http\Controllers\Controller;
use App\Repository\Backoffice\CampanhasRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CampanhasController extends Controller
{
    protected $repository;
    protected $operacaoResistory;
    public function __construct(CampanhasRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index()
    {
        return $this->sendResponse($this->repository->listarCampanhas());
    }

    public function edit($id)
    {
        $campanha = $this->repository->find($id);
        $campanha->filtro = json_decode($campanha->filtro);
        if(!$campanha){
            return $this->sendResponse($this->NAO_LOCALIZADO);
        }
        return $this->sendResponse(['dados' => $campanha]);
    }

    public function store(Request $request)
    {
        $dados = $request->all();
        try {
            //converte o filtro em string para persistir no banco
            $dados['filtro'] = json_encode($request->filtro);
            DB::beginTransaction();
            $campanha = $this->repository->create($dados);
            $this->repository->criarClientesCampanha($campanha);
            DB::commit();
            return $this->sendResponse($campanha);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->sendError($e->getMessage());
        }
   }

   public function update(Request $request, $id)
   {
        $dados = $request->all();
        $campanha = $this->repository->find($id);

        if(!$campanha){
            return $this->sendError($this->NAO_LOCALIZADO);
        }
        try {
            $campanha->update($dados);
            return $this->sendResponse($this->SUCESSO);
        } catch (\Exception $e) {
            return $this->sendError($this->ERRO);
        }
   }

   public function detalharCampamnha($id)
   {
        $campanha = $campanha = $this->repository->find($id);
        if(!$campanha){
            return $this->sendError($this->NAO_LOCALIZADO);
        }
        return $this->sendResponse($this->repository->detalharCampanha($campanha));
   }

   public function operacoesCampanha($id)
   {
        $campanha = $campanha = $this->repository->find($id);
        if(!$campanha){
            return $this->sendError($this->NAO_LOCALIZADO);
        }
        return $this->sendResponse($this->repository->operacoesCampanha($campanha));
    }

    public function clientesCampanha($id)
    {
        $campanha = $campanha = $this->repository->find($id);
        if(!$campanha){
            return $this->sendError($this->NAO_LOCALIZADO);
        }
        return $this->sendResponse($this->repository->clientesCampanha($campanha));
    }


    public function acordosCampanha($id)
    {
        $campanha = $campanha = $this->repository->find($id);
        if(!$campanha){
            return $this->sendError($this->NAO_LOCALIZADO);
        }
        return $this->sendResponse($this->repository->listarAcordosCampanhas($campanha));
    }

    public function destroy($id)
    {
        $dados = $this->repository->find($id);
        $dados->delete();
        return $this->sendResponse($this->SUCESSO);
    }

    public function lista(Request $request, $id)
    {
        $extensao = $request->extensao;
        return $this->repository->gerar_lista($id, $extensao);
    }

    public function totalCampanhaCredor()
    {
        try {
            $campanhas = $this->repository->totalCampanhaCredor();
            return $this->sendResponse(['total_campanhas' => $campanhas]);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function totalizadores(Request $request){
        $filtro_arr = $request->all();
        $clientes = $this->repository->totalizadores($filtro_arr);
        return $this->sendResponse($clientes);
    }
}
