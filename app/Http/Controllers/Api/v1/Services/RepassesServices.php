<?php

namespace App\Http\Controllers\Api\v1\Services;

use App\Helpers\Repasses\RepasseDiadia;
use App\Helpers\Repasses\RepasseRedeDamas;

class RepassesServices{

    protected $diadia, $rededamas;
    public function __construct(RepasseDiadia $diadia, RepasseRedeDamas $rededamas) {
        $this->diadia = $diadia;
        $this->rededamas = $rededamas;
    }

    public function gerarRepasses($dados,$tipo_execel){

        if($tipo_execel['dia_dia'] == true){
            $this->diadia->diaDia($dados);
        }
        elseif($tipo_execel['sintetico'] == true){
            return "sintético";
        }
        elseif($tipo_execel['analitico'] == true){
            return "analítico";
        }
        elseif($tipo_execel['rede_damas'] == true){
            $this->rededamas->redeDamas($dados);
        }
        elseif($tipo_execel['backoffice'] == true){
            return "backoffice";
        }
        elseif($tipo_execel['parcelas_1_n'] == true){
            return "parcelas_1_n";
        }else{
            return true;
        }

    }


}
