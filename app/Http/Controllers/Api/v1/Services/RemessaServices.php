<?php

namespace App\Http\Controllers\Api\v1\Services;

use App\Models\BackOffice\Remessa;
use Carbon\Carbon;
use Exception;

class RemessaServices {

    protected $operacoes;
    protected $cliente;
    protected $credor;

    public function __construct(array $dados, $credor){
        $this->cliente = $dados['cliente'];
        $this->operacoes = $this->cliente['operacoes'];
        $this->credor = $credor;
    }

    public function novaRemessa($credor){
        return Remessa::create([
            //"numero_remessa" => uniqid($this->credor->id),
            "numero_remessa" => Carbon::now()->format('Ymdhs'),
            "data_remessa" => Carbon::now(),
            "nome_arquivo" => "",
            "credor_id" => $credor->id,
            "usuario_empresa_id" => auth()->user()->usuario_empresas[0]->id,
            "data_entrada" => Carbon::now()
        ]);
    }

    public function salvar(){
        $cliente = new ClienteServices($this->cliente);
        $cliente->salvar(); // Aqui é verificado se existe cliente, caso não exista então grava (clientes, telefones e emails).

        $remessa = $this->novaRemessa($this->credor);
        if (!$remessa){
            throw new Exception("Erro ao gerar remessa", 1);
        }
        $operacaoService = new OperacoesServices($this->operacoes, $this->cliente, $remessa);
        if (!$operacaoService->salvar()) {
            throw new Exception("Erro ao registrar operações", 1);
        }
        return $remessa;
    }

    public function salvar_com_remessa($remessa) {
        $cliente = new ClienteServices($this->cliente);
        $cliente->salvar(); // Aqui é verificado se existe cliente, caso não exista então grava (clientes, telefones e emails).

        $operacaoService = new OperacoesServices($this->operacoes, $this->cliente, $remessa);
        return $operacaoService->salvar();
    }



}
