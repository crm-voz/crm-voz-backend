<?php

namespace App\Http\Controllers\Api\v1\Services;

use App\Models\BackOffice\Cliente;
use App\Models\BackOffice\Operacao;
use App\Models\BackOffice\Remessa;
use Carbon\Carbon;

class OperacoesServices {

    protected Cliente $cliente;
    protected array $operacoes;
    protected Remessa $remessa;

    public function __construct(array $operacoes, array $cliente, Remessa $remessa){
        $this->cliente = Cliente::where('cpf_cnpj', $cliente['cpf_cnpj'])->first();
        $this->operacoes = $operacoes;
        $this->remessa = $remessa;
    }

    public function salvar(){
        try {
            foreach($this->operacoes as $operacao){
                $ops = Operacao::select('operacoes.*', 'remessas.credor_id')
                                ->join('remessas', 'operacoes.remessa_id', '=', 'remessas.id')
                                ->where('remessas.credor_id', $this->remessa->credor_id)
                                ->where('cliente_id', $this->cliente->id)
                                ->where('numero_operacao', $operacao['numero_operacao'])
                                ->get();

                if(count($ops) === 0) {
                    Operacao::create([
                        "numero_operacao" => $operacao['numero_operacao'],
                        "descricao" => $operacao['descricao'],
                        "data_processamento" => Carbon::now(),
                        "data_vencimento" => formatDateTime($operacao['data_vencimento'],'Y-m-d'),
                        "valor_nominal" =>  $operacao['valor_operacao'],
                        "valor_atualizado" => $operacao['valor_operacao'],
                        "aluno" => $operacao['aluno'],
                        "competencia" => $operacao['competencia'],
                        "cliente_id" => $this->cliente->id,
                        "usuario_empresas_id" => $this->remessa->usuario_empresa_id,
                        "remessa_id" => $this->remessa->id,
                        'status_operacao_id' => 1, //em cobrança
                        "desconto" => $operacao['desconto'],
                        "adicionais" => json_encode($operacao['adicionais'])
                    ]);
                } else {
                    $id_op = $ops[0]->id;
                    $op = Operacao::find($id_op);
                    $op->desconto = $operacao['desconto'];
                    $op->adicionais = json_encode($operacao['adicionais']);
                    $op->save();
                    // return false;
                }
            }

            return true;
        } catch (\Throwable $th) {
            throw $th;
        }
    }
}
