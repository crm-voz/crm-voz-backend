<?php

namespace App\Http\Controllers\Api\v1\Services;

use App\Models\BackOffice\Cliente;
use App\Models\BackOffice\Telefone;

class TelefonesServices {
    protected $telefones;
    protected Cliente $cliente;

    public function __construct(array $dados, Cliente $cliente){
        $this->telefones = $dados;
        $this->cliente = $cliente;
    }

    public function salvar(){
        foreach($this->telefones as $telefone){
            if($telefone['telefone'] != '') {
                $tel = Telefone::where('cliente_id', $this->cliente->id)
                                ->where('telefone', $telefone['telefone'])
                                ->get();

                if(count($tel) === 0) {
                    Telefone::create([
                        'cliente_id' => $this->cliente->id,
                        'telefone' => $telefone['telefone'],
                        'origem' => 'Credor',
                    ]);
                }
            }
        }
    }

}
