<?php

namespace App\Http\Controllers\Api\v1\Services;

use App\Models\BackOffice\Remessa;
use Carbon\Carbon;
use Exception;

class RemessaAvulsaServices {

    protected $operacoes;
    protected $cliente;
    protected $credor_id;
    protected $data_remessa;

    public function __construct(array $dados, $data_remessa=null, $credor_id=null){
        $this->cliente = $dados['cliente'];
        $this->operacoes = $this->cliente['operacoes'];
        $this->credor_id = $credor_id;
        $this->data_remessa = $data_remessa;
    }

    public function salvarRemessa($remessa){

        $cliente = new ClienteServices($this->cliente);
        $cliente->salvar();

        $operacaoService = new OperacoesServices($this->operacoes, $this->cliente, $remessa);
        if (!$operacaoService->salvar()) {
            throw new Exception("Erro ao registrar operações", 1);
        }
        return $remessa;

    }

}
