<?php

namespace App\Http\Controllers\Api\v1\Services;

use App\Gateway\Email\Avulso;
use App\Models\Cobranca\Parcela;
use App\Models\Core\Boleto;
use App\Helpers\Boletos\BoletoBrasil;
use App\Gateway\Sms\BestVoice;
use App\Helpers\Boletos\BoletoBradesco;
use App\Helpers\Boletos\BoletoCaixa;
use App\Helpers\Boletos\BoletoItau;
use App\Helpers\Boletos\BoletoSantander;
use App\Helpers\Boletos\BoletoSicoob;

class BoletosServices {

    protected $boletoModel;

    public function __construct(Boleto $boleto) {
        $this->boletoModel = $boleto;
    }

    public function salvar_boleto($dados) {
        try{
            $boleto = $this->boletoModel->create($dados);
            return $boleto;
        }catch(\Exception $e) {
            throw $e;
        }
    }

   protected function limparCpf($cpf){
        $valor = trim($cpf);
        $valor = str_replace('.', '', $valor);
        $valor = str_replace(',', '', $valor);
        $valor = str_replace('-', '', $valor);
        $valor = str_replace('/', '', $valor);
        return $valor;
   }

    public function viewBoleto($identificador){
        switch($identificador->codigo_banco) {
            case '001':
                $boleto = new BoletoBrasil;
                break;
            case '033':
                $boleto = new BoletoSantander;
                break;
            case '341':
                $boleto = new BoletoItau;
                break;
            case '237':
                $boleto = new BoletoBradesco;
                break;
            case '104':
                $boleto = new BoletoCaixa;
                break;
            case '756':
                $boleto = new BoletoSicoob;
                break;
        }

        $boleto->escrever_dados_beneficiario($identificador->nome_beneficiario,
                                             $identificador->cnpj_beneficiario,
                                             $identificador->endereco_beneficiario)
                ->escrever_vencimento(date('d/m/Y', strtotime($identificador->vencimento)))
                ->escrever_valor($identificador->valor, 1)
                ->escrever_nosso_numero($identificador->nosso_numero)
                ->escrever_carteira($identificador->carteira) /*ok*/
                ->escrever_agencia($identificador->agencia) /*ok*/
                ->escrever_convenio($identificador->convenio) /*ok*/
                ->escrever_data_doc(date('d/m/Y', strtotime($identificador->created_at)))
                ->escrever_num_doc($identificador->numero_documento) /*ok*/
                ->escrever_desconto($identificador->desconto)
                ->escrever_deducoes($identificador->deducoes) /*ok*/
                ->escrever_juros($identificador->juros) /*ok*/
                ->escrever_acrescimos($identificador->acrescimos) /*ok*/
                ->escrever_cobrado($identificador->valor_cobrado)
                ->escrever_pagador($identificador->nome_pagador,
                                   $this->limparCpf($identificador->cpf_pagador),
                                   $identificador->nome_avaliador)/*ok*/
                ->escrever_cod_credor($identificador->codigo_credor)/*ok*/
                ->escrever_linha_digitavel($identificador->linha_digitavel)
                ->escrever_cod_barras($identificador->codigo_barra) /*ok*/
                ->escrever_qr_code($identificador->chave_pix)
                ->gerar('I');

    }

    public function enviar_boleto($boleto) {
        $acordo = Parcela::find($boleto['parcelas_id'])->acordos;
        $op = $acordo->operacoes_acordo->first()->operacao;
        $credor = $op->remessa->credor;
        $cliente = $op['cliente'];

        try {
            $dados = [
                'boleto' => $boleto,
                'credor' => $credor,
                'cliente'=> $cliente
            ];
            try {
                $contatos = [];
                foreach($cliente->emails as $em) {
                    $contatos[] = [
                        'nome' => $cliente['nome'],
                        'email'=> $em['email']
                    ];
                }

                $email_arr = [
                    'contatos' => $contatos,
                    'parametros' => [
                        'mensagem' => 'Aqui está o seu boleto!'
                    ]
                ];

                $bol_gen = (new BoletoBrasil())->criar_array($dados)->gerar();
                (new Avulso)->enviar_email($email_arr, $bol_gen);
            } catch (\Throwable $th) {
                $dados['Email'] = 'Houve um problema ao enviar o email com o boleto: ' . $th->getMessage();
            }

            try {
                $tel_arr = [];
                foreach($cliente->telefones as $tel) {
                    $tel_arr[] = [
                        'celular' => $tel['telefone'],
                        'mensagem'=> 'Olá ' . $cliente['nome'] . ', aqui está o seu boleto: ' . $boleto['linha_digitavel']
                    ];
                }

                (new BestVoice())->enviar_sms($tel_arr);
            } catch (\Throwable $th) {
                $dados['Sms'] = 'Houve um problema ao enviar os SMS com o boleto: ' . $th->getMessage();
            }
            return $dados;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function visualizarBoleto($IdParcela){
        return $this->boletoModel->where('parcelas_id', $IdParcela)->get();
    }
}
