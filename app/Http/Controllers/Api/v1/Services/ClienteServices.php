<?php

namespace App\Http\Controllers\Api\v1\Services;

use App\Models\BackOffice\Cliente;
use App\Models\Core\Cidade;
use Exception;

class ClienteServices {

    protected $cliente;
    protected $telefones;
    protected $emails;

    public function __construct(array $dados){
        $this->cliente = $dados;
        $this->telefones = $dados['telefones'];
        $this->emails = $dados['emails'];
    }

    public function salvar(){
        $cliente = Cliente::where('cpf_cnpj', 'like', $this->cliente['cpf_cnpj'])->first();
        if (!$cliente){
           //TODO: Refatorar, remover a tabela de cidades do banco e usar somente o retorno do endpoint dos correios.
            $cidade = Cidade::where('nome', $this->cliente['cidade'])->where('uf', $this->cliente['uf'])->first();
            if (!$cidade){
               $cidade = Cidade::create(['nome' => $this->cliente['cidade'], 'uf' => $this->cliente['uf']]);
            }

            $cliente = Cliente::create([
                            "nome" => $this->cliente['nome'],
                            "cpf_cnpj" => $this->cliente['cpf_cnpj'],
                            "endereco" => $this->cliente['endereco'],
                            "bairro" => $this->cliente['bairro'],
                            "cep" => $this->cliente['cep'],
                            "cidades_id" => $cidade->id,
                        ]);
           if (!$cliente){
              throw new Exception("Erro ao criar novo cliente", 1);
           }
        }else{
            $cliente->update($this->cliente);
        }

        $telefoneService = new TelefonesServices($this->telefones, $cliente);
        $telefoneService->salvar();

        $emailService = new EmailServices($this->emails, $cliente);
        $emailService->salvar();

        return $cliente;
    }
}
