<?php

namespace App\Http\Controllers\Api\v1\Services;

use App\Models\BackOffice\Cliente;
use App\Models\BackOffice\Email;

class EmailServices {
    protected $emails;
    protected Cliente $cliente;

    public function __construct(array $dados, Cliente $cliente){
        $this->emails = $dados;
        $this->cliente = $cliente;
    }

    public function salvar(){
        foreach($this->emails as $email){
            if($email['email'] != '') {
                $em = Email::where('cliente_id', $this->cliente->id)
                            ->where('email', $email['email'])
                            ->get();

                if(count($em) === 0) {
                    Email::create([
                        'cliente_id' => $this->cliente->id,
                        'email' => $email['email'],
                    ]);
                }
            }
        }
    }

}
