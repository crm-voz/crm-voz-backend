<?php

namespace App\Http\Controllers\Api\v1\Services;

use App\Models\Cobranca\OperacaoAcordo;
use App\Models\Core\LogPendencias;
use App\Repository\Backoffice\ClientesRepository;
use Illuminate\Support\Facades\DB;

class NegociacaoServices
 {
    public function __construct(ClientesRepository $clienteRepository)
    {
        $this->clienteRepository = $clienteRepository;
    }

    private function telefones($telefones)
    {
        $lista = collect();
        foreach($telefones as $telefone){
            $lista->add([
                'id' => $telefone->id,
                'telefone' => $telefone->telefone,
                'status' => $telefone->status,
                'origem' => $telefone->origem,
                'tags' => $telefone->tags
            ]);
        }
        return $lista;
    }

    private function emails($emails)
    {
        $lista = collect();
        foreach($emails as $email){
            $lista->add([
                'id' => $email->id,
                'email' => $email->email,
            ]);
        }
        return $lista;
    }

    private function operacoes($operacoes)
    {
        $lista = collect();
        foreach($operacoes as $operacao){
            $lista->add([
                'id' => $operacao->id,
                'numero_operacao' => $operacao->numero_operacao,
                'data_atualizacao' => $operacao->data_atualizacao,
                'data_vencimento' => $operacao->data_vencimento,
                'data_processamento' => $operacao->data_processamento,
                'valor_nominal' => $operacao->valor_nominal,
                'valor_atualizado' => $operacao->valor_atualizado,
                'descricao' => $operacao->descricao,
                'divida_reconhecida' => $operacao->divida_reconhecida,
                'desconto' => $operacao->desconto,
                'aluno' => $operacao->aluno,
                'competencia' => $operacao->competencia,
                'created_at' => $operacao->created_at,
                'adicionais' => $operacao->adicionais,
                'status_operacao' => [
                    'id' => $operacao->status_operacao?->id,
                    'nome' => $operacao->status_operacao?->nome,
                    'sigla' => $operacao->status_operacao?->sigla,
                ],
                'status_pendencia' =>
                    $this->status_pendencia($operacao->id)
                ,
                'classificacao_negativacao' => [
                    'id' => $operacao->classificacao_negativacao?->id,
                    'nome' => $operacao->classificacao_negativacao?->nome,
                ],
                'motivo_pendencia' => [
                    'id' => $operacao->motivo_pendencia?->id,
                    'nome' => $operacao->motivo_pendencia?->nome,
                ],
                'usuario_empresas' => [
                    'id' => $operacao->usuario_empresas?->user->id,
                    'nome' => $operacao->usuario_empresas?->user->name,
                ],
                'credor' => [
                    'id' => $operacao->remessa->credor->id,
                    'nome' => $operacao->remessa->credor->nome,
                ],
                'acordo' =>
                    $this->operacoesId($operacao->id),


            ]);
        }
        return $lista;
    }

    private function status_pendencia($opId){
        return LogPendencias::select(DB::raw('log_pendencias.status, MAX(log_pendencias.created_at) created_at'))
            ->join('operacoes', 'operacoes.id', '=', 'log_pendencias.operacao_id')
            ->join('remessas', 'remessas.id', '=', 'operacoes.remessa_id')
            ->join('credores', 'credores.id', '=', 'remessas.credor_id')
            ->leftJoin('motivo_pendencia','motivo_pendencia.id','=','operacoes.motivo_pendencia_id')
            ->whereIn('log_pendencias.operacao_id',[$opId])
            ->orderBy('log_pendencias.created_at','desc')
            ->groupBy(DB::raw('log_pendencias.operacao_id, log_pendencias.status, log_pendencias.created_at'))->first();

    }

    private function operacoesId($opId){
        return OperacaoAcordo::join('operacoes', 'operacoes.id', '=', 'operacao_acordo.operacoes_id')
            ->join('acordo', 'acordo.id', '=', 'operacao_acordo.acordos_id')
            ->select('acordo.id', 'acordo.numero_acordo','acordo.status')
            ->whereIn('operacao_acordo.operacoes_id',[$opId])
            ->Where('acordo.status','<>','Cancelado')
            ->first();
    }

    private function credores($operacoes)
    {
        $lista = collect();
        foreach($operacoes as $operacao){
            $lista->add([
                'id' => $operacao->remessa->credor->id,
                'nome' => $operacao->remessa->credor->nome,
                'parametros' => json_decode($operacao->remessa->credor->parametros_config),
            ]);
        }
        $credores = collect();
        $lista->unique()->each(function($item) use ($credores){
            $credores->add($item);
        });

        return $credores;
    }

    //TODO: ENCONTRAR FORMA MAIS ELEGANTE PARA FORMATAR A DATA NO FORMATO QUE O FRONTEND PRECISA
    private function eventos($eventos){
        $lista = collect();
        foreach($eventos as $evento){
            $lista->add([
                'id' => $evento->id,
                'descricao' => $evento->descricao,
                'created_at' => $evento->created_at->format('Y-m-d').'T'.$evento->created_at->format('H:i:s'),
                'parametros_contato' => json_decode($evento->parametros_contato),
                'usuario_empresas' => [
                    'id' => $evento->usuario_empresas->user->id,
                    'nome' => $evento->usuario_empresas->user->name,
                ],
                'motivo_evento' => [
                    'id' => $evento->motivo_evento->id,
                    'nome' => $evento->motivo_evento->nome,
                    'ocultar' => $evento->motivo_evento->ocultar,
                ]
            ]);
        }
        return $lista;
    }

    private function boletoParcela($boletos)
    {
        $lista = collect();
        foreach($boletos as $boleto){
            $lista->add([
                'id' => $boleto->id,
                'linha_digitavel' => $boleto->linha_digitavel,
                'nosso_numero' => $boleto->nosso_numero,
                'vencimento' => $boleto->vencimento,
                'baixa' => $boleto->baixa,
                'valor' => $boleto->valor,
                'valor_pago' => $boleto->valor_pago,
                'multa' => $boleto->multa,
                'desconto' => $boleto->desconto,
                'id_pix' => $boleto->id_pix,
                'chave_pix' => $boleto->chave_pix,
                'nome_beneficiario' => $boleto->nome_beneficiario,
                'cnpj_beneficiario' => $boleto->cnpj_beneficiario,
                'endereco_beneficiario' => $boleto->endereco_beneficiario,
                'carteira' => $boleto->carteira,
                'agencia' => $boleto->agencia,
                'numero_documento' => $boleto->numero_documento,
                'deducoes' => $boleto->deducoes,
                'juros' => $boleto->juros,
                'acrescimos' => $boleto->acrescimos,
                'nome_pagador' => $boleto->nome_pagador,
                'cpf_pagador' => $boleto->cpf_pagador,
                'nome_avaliador' => $boleto->nome_avaliador,
                'codigo_credor' => $boleto->codigo_credor,
                'codigo_barra' => $boleto->codigo_barra,
                'convenio' => $boleto->convenio,
                'cod_banco' => $boleto->codigo_banco,
            ]);
        }
        return isset($lista[0]) ? ($lista[0]) : null;

    }

    private function parcelasAcordo($parcelas)
    {
        $lista = collect();
        foreach($parcelas as $parcela){
            $lista->add([
                'id' => $parcela->id,
                'valor_nominal' => $parcela->valor_nominal,
                'valor_previsto' => $parcela->valor_previsto,
                'data_vencimento' => $parcela->data_vencimento,
                'numero_parcela' => $parcela->numero_parcela,
                'valor_baixado' => $parcela->valor_baixado,
                'data_baixa' => $parcela->data_baixa,
                'valor_juros' => $parcela->valor_juros,
                'valor_multa' => $parcela->valor_multa,
                'valor_honorario' => $parcela->valor_honorario,
                'forma_pagamento' => $parcela->forma_pagamento,
                'valor_nominal_original' => $parcela->valor_nominal_original,
                'vencimento_original' => $parcela->vencimento_original,
                'valor_desconto' => $parcela->valor_desconto,
                'status' => $parcela->status,
                'boleto' => $this->boletoParcela($parcela->boleto),
                'operacoes_parcelas' => $parcela->operacoes_parcelas,
            ]);
        }
        return $lista;
    }


    private function operacoesAcordo($acordo)
    {
        $lista = collect();
        foreach($acordo->operacoes_acordo as $operacao){
            $lista->add([
                'id' => $operacao->operacoes_id,
            ]);
        }
        return $lista;

    }

    private function credorId($acordo)
    {
        $lista = collect();
        foreach($acordo->operacoes_acordo as $operacao){
            $lista->add(
                DB::table('remessas')->select('credor_id')->where('id',$operacao->operacao->remessa_id)->first(),
            );
        }
        return $lista;

    }

    private function acordos($acordos)
    {
        $lista = collect();
        foreach($acordos as $acordo){
            $lista->add([
                'id' => $acordo->id,
                'numero_acordo' => $acordo->numero_acordo,
                'status' => $acordo->status,
                'valor_nominal' => $acordo->valor_nominal,
                'juros' => $acordo->juros,
                'juros_porcento' => $acordo->juros_porcento,
                'multas' => $acordo->multas,
                'multas_porcento' => $acordo->multas_porcento,
                'honorarios' => $acordo->honorarios,
                'honorarios_porcento' => $acordo->honorarios_porcento,
                'desconto' => $acordo->desconto,
                'valor_entrada' => $acordo->valor_entrada,
                'quantidade_parcelas' => $acordo->quantidade_parcelas,
                'valor_parcela' => $acordo->valor_parcela,
                'data_cancelou' => $acordo->data_cancelou,
                'descricao_cancelamento' => $acordo->descricao_cancelamento,
                'executivo_cobranca' => [
                    'id' => $acordo->executivo_cobranca->id,
                    'nome' => $acordo->executivo_cobranca->funcionarios->nome,
                    'cpf' => $acordo->executivo_cobranca->funcionarios->cpf,
                ],
                'usuario_cancelou' => [
                    'id' => $acordo->usuario_cancelou?->user->id,
                    'nome' => $acordo->usuario_cancelou?->user->name,
                ],
               'parcelas' => $this->parcelasAcordo($acordo->parcelas),
                'operacoes' => $this->operacoesAcordo($acordo),
                'credor' => $this->credorId($acordo),
                'created_at' => $acordo->created_at,
                'situacao' => $acordo->situacao,
            ]);
        }
        return $lista;
    }

    public function negociacaoByCliente($id)
    {
        try {
            $cliente = $this->clienteRepository->find($id);
            if($cliente){
                $negociacao = collect();
                $negociacao->add([
                    'cliente' => [
                        'id' => $cliente->id,
                        'nome' => $cliente->nome,
                        'cpf_cnpj' => $cliente->cpf_cnpj,
                        'endereco' => $cliente->endereco,
                        'bairro' => $cliente->bairro,
                        'numero' => $cliente->numero,
                        'cep' => $cliente->cep,
                        'localizacao' => $cliente->localizacao,
                        'observacoes' => $cliente->observacoes,
                        'data_cadastro_confirmado' => $cliente->data_cadastro_confirmado,
                        'cidade' => [
                            'id' => $cliente->cidades->id,
                            'nome' => $cliente->cidades->nome,
                            'uf' => $cliente->cidades->uf,
                        ],
                        'classificacao_cliente' => json_decode( $cliente->status_cliente_id ),
                        'telefones' => $this->telefones($cliente->telefones),
                        'emails' => $this->emails($cliente->emails),
                        'operacoes' => $this->operacoes($cliente->operacoes),
                        'credores' => $this->credores($cliente->operacoes),
                        'eventos' => $this->eventos($cliente->eventos),
                        'acordos' => $this->acordos($cliente->acordos),
                    ]
                ]);

                return $negociacao[0];
            }else{
                return false;
            }
        } catch (\Throwable $th) {
            throw $th;
        }

    }
 }
