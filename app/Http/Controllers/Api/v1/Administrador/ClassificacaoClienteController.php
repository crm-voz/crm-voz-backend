<?php

namespace App\Http\Controllers\Api\v1\Administrador;

use App\Http\Controllers\Controller;
use App\Models\Core\ClassificacaoCliente;
use Illuminate\Http\Request;

class ClassificacaoClienteController extends Controller
{
    protected $modelClassificacaoCliente;

    /**
     * @author Welliton Cunha
     * @since 14.07.2021 às 15:48
     *
     * @see Classificação de Clientes
     */

    public function __construct(ClassificacaoCliente $classificacaoCliente)
    {
        $this->modelClassificacaoCliente = $classificacaoCliente;
    }

    public function index($id){
        $classificacaoCliente = $this->modelClassificacaoCliente->find($id);

        if(!$classificacaoCliente){
            return $this->sendError($this->NAO_LOCALIZADO);
        }

        return $this->sendResponse(['classificacaoCliente' => $classificacaoCliente]);
    }

    public function store(Request $request){
        try {

            $classificacaoCliente = $request->all();
            $this->modelClassificacaoCliente->create($classificacaoCliente);

            return $this->sendResponse($this->SUCESSO);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }
    }

    public function update(Request $request, $id){
        $dados = $request->all();
        $classificacaoCliente = $this->modelClassificacaoCliente::find($id);

        if (!$classificacaoCliente){
            return $this->sendError($this->NAO_LOCALIZADO);
        }

        try {
            $classificacaoCliente->update($dados);
            return $this->sendResponse($this->SUCESSO);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }
    }

    public function destroy($id){
        $classificacaoCliente = $this->modelClassificacaoCliente->where('id', $id);

        if(!$classificacaoCliente){
            return $this->sendError($this->NAO_LOCALIZADO);
        }
        try {
            $classificacaoCliente->delete();
            return $this->sendResponse($this->SUCESSO);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }
    }

    public function edit($id){
        $classificacaoCliente = $this->modelClassificacaoCliente->find($id);

        if(!$classificacaoCliente){
            return $this->sendError($this->NAO_LOCALIZADO);
        }

        return $this->sendResponse(['classificacaoCliente' => $classificacaoCliente]);
    }
}
