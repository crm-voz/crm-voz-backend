<?php

namespace App\Http\Controllers\Api\v1\Administrador;

use Illuminate\Http\Request;
use App\Models\Core\StatusLocalizacao;
use App\Http\Controllers\Controller;
use App\Http\Requests\StatusLocalizacaoRequest;

class StatusLocalizacaoController extends Controller
{
    protected $model;


    public function __construct(StatusLocalizacao $statusLocalizacao){
        $this->model = $statusLocalizacao;
    }

    public function index(Request $request){
        $items_por_pag = intval($request->input('items_pag', 15));
        $item_pag = ( $request->hasAny(['pag']) ) ? (intval($request->input('pag')) - 1) * $items_por_pag : 0;
        return $this->sendResponse(['dados' => $this->model->offset($item_pag)->limit($items_por_pag)->get()]);
    }

    public function store(StatusLocalizacaoRequest $request){
        try {
            $dados = $request->all();
            $this->model->create($dados);
            return $this->sendResponse($this->SUCESSO);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }
    }

    public function update(StatusLocalizacaoRequest $request, $id){
        $motivo_devolucao = $this->model->find($id);
        if (!$motivo_devolucao){
            return $this->sendError($this->NAO_LOCALIZADO);
        }

        try {
            $dados = $request->all();
            $dados['status'] = $dados['status'] ? true : false;
            $motivo_devolucao->fill($dados)->save();
            return $this->sendResponse($this->SUCESSO);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }
    }

    public function destroy($id){
        $dados = $this->model->find($id);
        try {
            $dados->delete();
            return $this->sendResponse($this->SUCESSO);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }
    }

    public function edit($id){
        $status = $this->model->find($id);
        if(!$status){
            return $this->sendError($this->NAO_LOCALIZADO);
        }
        return $this->sendResponse(['status_localizacao' => $status]);
    }
}
