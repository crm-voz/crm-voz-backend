<?php

namespace App\Http\Controllers\Api\v1\Administrador;

use App\Repository\Administrador\PerfilRepository;
use App\Http\Controllers\Controller;
use App\Http\Requests\EmpresasRequest;
use App\Models\Core\Cidade;
use App\Models\Core\Empresa;
use Illuminate\Http\Request;

class EmpresasController extends Controller
{
    protected $model;
    protected $perfil;

    public function __construct(Empresa $empresa, PerfilRepository $profile)
    {
        $this->model = $empresa;
        $this->perfil = $profile;

        // $this->middleware(function ($request, $next) {
        //     $this->perfil->show();
        //     if ($request->route()->getName() =='api.admin.empresas.index'){
        //         if ($this->perfil->index == false){
        //             return redirect()->route('naoautorizado');
        //         }
        //     }
        //     if ($request->route()->getName() =='api.admin.empresas.create'){
        //         if ($this->perfil->index == false){
        //             return redirect()->route('naoautorizado');
        //         }
        //     }
        //     if ($request->route()->getName() =='api.admin.empresas.store'){
        //         if ($this->perfil->store == false){
        //             return redirect()->route('naoautorizado');
        //         }
        //     }
        //     if ($request->route()->getName() =='api.admin.empresas.update'){
        //         if ($this->perfil->update == false){
        //             return redirect()->route('naoautorizado');
        //         }
        //     }
        //     if ($request->route()->getName() =='api.admin.empresas.destroy'){
        //         if ($this->perfil->destroy == false){
        //             return redirect()->route('naoautorizado');
        //         }
        //     }
        //     if ($request->route()->getName() =='api.admin.empresas.edit'){
        //         if ($this->perfil->index == false){
        //             return redirect()->route('naoautorizado');
        //         }
        //     }

        //     return $next($request);

        // });
    }

    public function index() {
        $empresas = Empresa::all();
        $cidades = Cidade::all();
         return $this->sendResponse(['empresa' => $empresas, 'cidades'=>$cidades]);
    }

    public function store(EmpresasRequest $request){
        $dados = $request->all();

        try {
            $this->model->create($dados);
            return $this->sendResponse($this->SUCESSO);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }
    }

    public function update(EmpresasRequest $request, $id){
        $dados = $request->all();

        $empresas = $this->model::find($id);
        if (!$empresas){
            return $this->sendResponse($this->NAO_LOCALIZADO);
        }

        if ($request->status == null){
            $empresas->status = false;
        }else{
            $empresas->status = $request->status;
        }

        try {
            $empresas->update($dados);
            return $this->sendResponse($this->SUCESSO);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }
    }

    public function destroy($id){
        $dados = $this->model::where('id', $id);
        if(!$dados){
            return $this->sendResponse($this->NAO_LOCALIZADO);
        }
        try {
            $dados->delete();
            return $this->sendResponse($this->SUCESSO);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }
    }

    public function create(){
        $empresas = Empresa::all();
        $cidades = Cidade::all();
        return $this->sendResponse(['empresas' => $empresas, 'cidades'=>$cidades]);
    }

    public function edit($id){
        $empresa = Empresa::where('id',$id)->with('cidade')->get();

        if(!$empresa){
            return $this->sendResponse($this->NAO_LOCALIZADO);
        }
        return $this->sendResponse(['empresa' => $empresa]);
    }
}
