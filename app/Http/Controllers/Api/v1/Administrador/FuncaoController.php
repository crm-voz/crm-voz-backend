<?php

namespace App\Http\Controllers\Api\v1\Administrador;

use App\Models\Core\Funcao;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\FuncaoRequest;

class FuncaoController extends Controller
{
    public $acao;
    public $id_funcao;
    protected $model;

    public function __construct(Funcao $funcao)
    {
        $this->model = $funcao;
    }

    public function index(){
        return $this->sendResponse($this->model->all());
    }

    public function store(FuncaoRequest $request){
        try {
            $dados = $request->all();
            $this->model->create($dados);
            return $this->sendResponse($this->SUCESSO);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }
    }

    public function update(FuncaoRequest $request, $id){
        $dados = $request->all();
        $funcao = $this->model->find($id);
        if(!$funcao){
            return $this->sendError($this->NAO_LOCALIZADO);
        }
        try {
            $funcao_update = $funcao->update($dados);
            return $this->sendResponse($funcao_update);
        } catch (\Exception $e) {
            return $this->sendError($this->ERRO);
        }
    }

    public function destroy($id){
        $dados = $this->model->where('id', $id);
        if(!$dados){
            return $this->sendError($this->NAO_LOCALIZADO);
        }
        try {
            $dados->delete();
            return $this->sendResponse($this->SUCESSO);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }
    }

    public function create(){
        return $this->sendResponse($this->model->all());
    }

    public function edit(Request $request, $id){
        $funcao = $this->model->find($id);
        if(!$funcao){
            return $this->sendError($this->NAO_LOCALIZADO);
        }

        return $this->sendResponse($funcao);
    }
}
