<?php
namespace App\Http\Controllers\Api\v1\Administrador;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\MotivoDevolucaoRequest;
use App\Models\Core\MotivoDevolucao;

class MotivoDevolucaoController extends Controller
{
    protected $model;

    public function __construct(MotivoDevolucao $motivo){
        $this->model = $motivo;
    }

    public function index(){
        $motivo_devolucao = MotivoDevolucao::all();
        return $this->sendResponse($motivo_devolucao);
    }

    public function store(MotivoDevolucaoRequest $request){
        try {
            $dados = $request->all();
            $this->model->create($dados);
            return $this->sendResponse($this->SUCESSO);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }
    }

    public function update(MotivoDevolucaoRequest $request, $id){
        $motivo_devolucao = MotivoDevolucao::find($id);
        if (!$motivo_devolucao){
            return $this->sendError($this->NAO_LOCALIZADO);
        }

        try {
            $dados = $request->all();
            $dados['status'] = (!isset($dados['status']))? 0 : 1;
            $motivo_devolucao->fill($dados)->save();
            return $this->sendResponse($this->SUCESSO);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }
    }

    public function destroy($id){
        $dados = MotivoDevolucao::where('id', $id);
        try {
            $dados->delete();
           return $this->sendResponse($this->SUCESSO);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }
    }


    public function create(){
        return view('admin.administrador.motivo_devolucao.create');
    }

    public function edit($id){
        $motivo_devolucao = MotivoDevolucao::find($id);

        if(!$motivo_devolucao){
            return $this->sendError($this->NAO_LOCALIZADO);
        }
        return $this->sendResponse(['motivo_devolucao' => $motivo_devolucao]);
    }
}
