<?php

namespace App\Http\Controllers\Api\v1\Administrador;

use App\Http\Controllers\Controller;
use App\Models\Core\Endpoints;
use Illuminate\Http\Request;

class EndpointsController extends Controller
{
    protected $model;

    public function __construct(Endpoints $endpoints){
        $this->model = $endpoints;
    }

    public function index(){
        return $this->sendResponse($this->model->all());
    }

    public function store(Request $request){
        try {
            $dados = $request->all();
            $this->model->create($dados);
            return $this->sendResponse($this->SUCESSO);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }
    }

    public function update(Request $request, $id){
        $endpoints = $this->model->find($id);
        if (!$endpoints){
            return $this->sendError($this->NAO_LOCALIZADO);
        }

        try {
            $dados = $request->all();
            $endpoints->fill($dados)->save();
            return $this->sendResponse($this->SUCESSO);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }
    }

    public function destroy($id){
        $dados = $this->model->find($id);
        try {
            $dados->delete();
            return $this->sendResponse($this->SUCESSO);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }
    }

    public function edit($id){
        $endpoints = $this->model->find($id);

        if(!$endpoints){
            return $this->sendError($this->NAO_LOCALIZADO);
        }

        return $this->sendResponse($endpoints);

    }
}
