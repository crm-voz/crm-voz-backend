<?php

namespace App\Http\Controllers\Api\v1\Administrador;

use App\Repository\Administrador\PerfilRepository;
use App\Http\Controllers\Controller;
use App\Models\Core\ParametroBoleto;
use Illuminate\Http\Request;

class ParametroBoletoController extends Controller
{
    protected $modelParametroBoleto;
    protected $perfil;

    /**
     * @author Welliton Cunha
     * @since 22.07.2021 às 09:49
     *
     * @see Parametro de Boleto
     */

    public function __construct(ParametroBoleto $parametroBoleto, PerfilRepository $profile)
    {
        $this->modelParametroBoleto = $parametroBoleto;
        $this->perfil = $profile;
    }

    public function index(){
        $parametroBoleto = $this->modelParametroBoleto::all();

        if(!$parametroBoleto){
            return $this->sendError($this->NAO_LOCALIZADO);
        }

        return $this->sendResponse($parametroBoleto);
    }

    public function store(Request $request){
        try {

            $parametroBoleto = $request->all();
            $this->modelParametroBoleto->create($parametroBoleto);

            return $this->sendResponse($this->SUCESSO);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }
    }

    public function update(Request $request, $id){
        $dados = $request->all();
        $parametroBoleto = $this->modelParametroBoleto::find($id);

        if (!$parametroBoleto){
            return $this->sendError($this->NAO_LOCALIZADO);
        }

        try {
            $parametroBoleto->update($dados);
            return $this->sendResponse($this->SUCESSO);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }
    }

    public function destroy($id){
        $parametroBoleto = $this->modelParametroBoleto->where('id', $id);

        if(!$parametroBoleto){
            return $this->sendError($this->NAO_LOCALIZADO);
        }
        try {
            $parametroBoleto->delete();
            return $this->sendResponse($this->SUCESSO);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }
    }

    public function edit($id){
        $parametroBoleto = $this->modelParametroBoleto->find($id);

        if(!$parametroBoleto){
            return $this->sendError($this->NAO_LOCALIZADO);
        }

        return $this->sendResponse($parametroBoleto);
    }
}
