<?php

namespace App\Http\Controllers\Api\v1\Administrador;

use App\Console\Commands\SnapshotServices\Snapshot;
use App\Http\Controllers\Controller;
use App\Repository\Backoffice\SnapshotRepository;

class SnapshotController extends Controller {
    public function __construct(SnapshotRepository $snapshotRepository){
        $this->snapshotRepository = $snapshotRepository;
    }

    public function index(){
        return $this->sendResponse(['dados' => $this->snapshotRepository->all()]);
    }

    public function gerar(){
        $snapshot = app(Snapshot::class);
        try {
            $snapshot->generate();
            $this->sendResponse(['dados' => true]);
        } catch (\Exception $e) {
            $this->sendError($e->getMessage());
        }
    }

}

