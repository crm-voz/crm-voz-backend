<?php

namespace App\Http\Controllers\Api\v1\Administrador;

use App\Models\Core\ClassificacaoNegativacao;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\GenericRequest;

class ClassificacaoNegativacaoController extends Controller
{
    protected $model;

    public function __construct(ClassificacaoNegativacao $model)
    {
        $this->model = $model;
    }

    public function index(){
        return $this->sendResponse(['dados' => $this->model::paginate()]);
    }

    public function filtro(Request $request){
        $models = $this->model->where('nome', 'like', $request->search.'%')->paginate();
        return $this->sendResponse(['dados' => $models]);
    }

    public function create(){
        return view('admin.administrador.classificacao_negativacao.create');
    }

    public function store(GenericRequest $request){
        $dados = $request->all();

        try {
            $this->model->create($dados);
            return $this->sendResponse($this->SUCESSO);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }

    }

    public function edit($id){
        $obj = $this->model->find($id);
        if(!$obj){
            return $this->sendError($this->NAO_LOCALIZADO);
        }
        return $this->sendResponse(['dados' => $obj]);
    }

    public function update(GenericRequest $request, $id){
        $dados = $request->all();
        $cliente = $this->model->find($id);
        if(!$cliente){
            return $this->sendError($this->NAO_LOCALIZADO);
        }
        try {
            $cliente->update($dados);
            return $this->sendResponse($this->SUCESSO);
        } catch (\Exception $e) {
            return $this->sendError($this->ERRO);
        }

    }

    public function destroy($id){
        $cliente = $this->model->find($id);
        if(!$cliente){
            return $this->sendError($this->NAO_LOCALIZADO);
        }
        try {
            $cliente->delete();
            return $this->sendResponse($this->SUCESSO);
        } catch (\Exception $e) {
            return $this->sendError($this->ERRO);
        }
    }
}
