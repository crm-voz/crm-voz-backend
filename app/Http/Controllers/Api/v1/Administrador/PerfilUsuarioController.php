<?php

namespace App\Http\Controllers\Api\v1\Administrador;

use App\Http\Controllers\Controller;
use App\Models\Core\Endpoints;
use App\Models\Core\PerfilUsuario;
use App\Models\Core\PermissoesPerfil;
use Illuminate\Http\Request;

class PerfilUsuarioController extends Controller
{
    public function __construct(PerfilUsuario $perfilUsuario,
                                PermissoesPerfil $permissoesPerfil,
                                Endpoints $endpoints){
        $this->model = $perfilUsuario;
        $this->permissoes = $permissoesPerfil;
        $this->endpoints = $endpoints;
    }

    public function index(){
        return $this->sendResponse($this->model->all());
    }

    public function store(Request $request){
        try {
            $dados = $request->all();
            $endpoints = $this->endpoints->all();

            $perfil = $this->model->where('descricao','ilike',$dados['perfil'])->first();
            if (!$perfil) {
             $perfil = $this->model->create([
                    'descricao' => $dados['perfil']
                ]);
            }

            foreach($endpoints as $endpoint){
                $this->permissoes->create([
                    'perfil_usuario_id' => $perfil->id,
                    'endpoints_id' => $endpoint->id,
                    'modificar' => in_array($endpoint->id, $dados['endpoints']),
                ]);
            }
            return $this->sendResponse($this->SUCESSO);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }
    }

    public function update(Request $request, $id){
        $perfilUsuario = $this->model->find($id);

        if (!$perfilUsuario){
            return $this->sendError($this->NAO_LOCALIZADO);
        }
        try {
            $dados = $request->all();
            $endpoints = $this->endpoints->all();

            $perfilUsuario->descricao = $dados['perfil'];
            $perfilUsuario->save();
            foreach($endpoints as $p){
                $permissao = app(PermissoesPerfil::class);
                $permissao->updateOrCreate([
                    'perfil_usuario_id' => $perfilUsuario->id,
                    'endpoints_id' => $p->id,
                    'modificar' => in_array($p->id, $dados['endpoints'])
                ]);
            }
            return $this->sendResponse($this->SUCESSO);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }
    }

    public function destroy($id){
        $dados = $this->model->find($id);
        try {
            $dados->delete();
            return $this->sendResponse($this->SUCESSO);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }
    }

    public function edit($id){
        $perfilUsuario = $this->model->find($id);

        if(!$perfilUsuario){
            return $this->sendError($this->NAO_LOCALIZADO);
        }

        $endpoints = [];
        foreach ($perfilUsuario->permissoesPerfil as $p){
            if ($p->modificar == true){
                $endpoints[] = ([
                   'id' => $p->endpoints_id,
                  // 'status' => $p->modificar
                ]);

            }

        }
        return $this->sendResponse(['perfil' => $perfilUsuario->descricao, 'endpoints' => $endpoints]);

    }
}
