<?php

namespace App\Http\Controllers\Api\v1\Administrador;

use App\Repository\Administrador\PerfilRepository;
use App\Models\Core\Funcionario;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\FuncionariosRequest;
use App\Models\Core\Cidade;
use App\Models\Core\Funcao;

class FuncionariosController extends Controller
{
    protected $model;
    protected $perfil;

    public function __construct(Funcionario $funcionario, PerfilRepository $profile)
    {
        $this->model = $funcionario;
        $this->perfil = $profile;
    }

    public function index(){
        $funcionarios = $this->model::all();
         return $this->sendResponse($funcionarios);
    }

    public function store(FuncionariosRequest $request){
        $dados = $request->all();

        try {
            $this->model->create($dados);
            return $this->sendResponse($this->SUCESSO);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }
    }

    public function update(FuncionariosRequest $request, $id){
        $dados = $request->all();

        $funcionarios = $this->model->find($id);
        if(!$funcionarios){
            return $this->sendError($this->NAO_LOCALIZADO);
        }

        try {

            $funcionarios->update($dados);
            return $this->sendResponse($this->SUCESSO);
        } catch (\Exception $e) {
            return $this->sendError($this->ERRO);
        }

    }

    public function destroy($id){
        try {
            $dados = $this->model::where('id', $id);
            $dados->delete();
            return $this->sendResponse($this->SUCESSO);
        } catch (\Exception $e) {
            return $this->sendResponse($this->SUCESSO);
        }
    }

    public function edit($id){
        $funcionario = $this->model::find($id);
        if(!$funcionario){
            return $this->sendResponse($this->NAO_LOCALIZADO);
        }
        $funcionario->cidade;
        $funcionario->funcao;
        $funcionario->usuario_empresas;

        return $this->sendResponse(['funcionario' => $funcionario]);
    }
}
