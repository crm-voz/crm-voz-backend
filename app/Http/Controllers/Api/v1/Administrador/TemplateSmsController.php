<?php

namespace App\Http\Controllers\Api\v1\Administrador;

use App\Http\Controllers\Controller;
use App\Models\BackOffice\Cliente;
use App\Models\BackOffice\Credor;
use App\Models\Core\TemplateSms;
use App\Repository\Administrador\TemplateSMSRepository;
use Illuminate\Http\Request;

class TemplateSmsController extends Controller
{
    protected $modelTemplateSms;
    protected $modelCliente;
    protected $modelCredor;
    protected $repository;

    public function __construct(TemplateSms $TemplateSms, Cliente $cliente, Credor $credor, TemplateSMSRepository $templateSms)
    {
        $this->modelTemplateSms = $TemplateSms;
        $this->modelCliente = $cliente;
        $this->modelCredor = $credor;
        $this->repository = $templateSms;
    }

    public function store(Request $request){
         $dados = $request->all();

        try {
            $this->modelTemplateSms->create($dados);
            return $this->sendResponse($this->SUCESSO);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }
    }

    public function update(Request $request, $id){
        $dados = $request->all();

        $TemplateSms = $this->modelTemplateSms->find($id);
        if(!$TemplateSms){
            return $this->sendError($this->NAO_LOCALIZADO);
        }

        try {

            $TemplateSms->update($dados);
            return $this->sendResponse($this->SUCESSO);
        } catch (\Exception $e) {
            return $this->sendError($this->ERRO);
        }

    }

    public function destroy($id){
        try {
            $dados = $this->modelTemplateSms::where('id', $id);
            $dados->delete();
            return $this->sendResponse($this->SUCESSO);
        } catch (\Exception $e) {
            return $this->sendResponse($this->SUCESSO);
        }
    }

    public function index(){
        $TemplateSms = $this->modelTemplateSms::with('credores')->get();
         return $this->sendResponse(['TemplateSms' => $TemplateSms]);
    }

    public function create(){
        $TemplateSms = $this->modelTemplateSms::all();
        return $this->sendResponse(['TemplateSms' => $TemplateSms]);
    }

    public function edit($id){
        $TemplateSms = $this->modelTemplateSms::find($id);
        if(!$TemplateSms){
            return $this->sendResponse($this->NAO_LOCALIZADO);
        }

        return $this->sendResponse(['TemplateSms' => $TemplateSms]);
    }

    public function preenchersms(Request $request){
        $filtro_arr = $request->all();
        $string = $this->repository->preencherSms($filtro_arr);
        return $this->sendResponse($string);
    }
}
