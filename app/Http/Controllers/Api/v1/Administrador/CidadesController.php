<?php

namespace App\Http\Controllers\Api\v1\Administrador;

use Illuminate\Http\Request;
use App\Models\Core\Cidade;
use App\Http\Controllers\Controller;
use App\Http\Requests\CidadesRequest;

class CidadesController extends Controller
{
    protected $model;

    public function __construct(Cidade $cidade)
    {
        $this->model = $cidade;
    }

    public function index(){
        $cidade = Cidade::all();
        return $this->sendResponse($cidade);
    }

    public function store(CidadesRequest $request){
        $dados = $request->all();
        try {
            $this->model->create($dados);
            return $this->sendResponse($this->SUCESSO);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }
    }

    public function update(CidadesRequest $request, $id){
        $dados = $request->all();
        $cidade = $this->model::find($id);
        if (!$cidade){
            return $this->sendError($this->NAO_LOCALIZADO);
        }

        try {
            $cidade->update($dados);
            return $this->sendResponse($this->SUCESSO);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }

    }

    public function destroy($id){
        $dados = $this->model::where('id', $id);

        if (!$dados){
            return $this->sendError($this->NAO_LOCALIZADO);
        }
        try {
            $dados->delete();
            return $this->sendResponse($this->SUCESSO);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }
    }


    public function edit(Request $request, $id){
        $cidade = $this->model::find($id);
        if(!$cidade){
            return $this->sendError($this->NAO_LOCALIZADO);
        }
        return $this->sendResponse(['cidade' => $cidade]);
    }

    public function filtro(Request $request){
        $cidade = $this->model::where('nome','like','%'.$request->nome.'%')->get();
        if(!$cidade){
            return $this->sendError($this->NAO_LOCALIZADO);
        }
        return $this->sendResponse(['cidade' => $cidade]);
    }

}
