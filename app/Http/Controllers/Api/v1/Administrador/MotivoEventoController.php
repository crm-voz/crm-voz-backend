<?php

namespace App\Http\Controllers\Api\v1\Administrador;

use App\Http\Controllers\Controller;
use App\Http\Requests\MotivoEventoRequest;
use App\Models\Core\MotivoEvento;
use Illuminate\Http\Request;

class MotivoEventoController extends Controller
{
    protected $model;

    public function __construct(MotivoEvento $motivoEvento)
    {
        $this->model = $motivoEvento;
    }


    public function index(){
        $motivoEvento = $this->model::all();
         return $this->sendResponse($motivoEvento);
    }

    public function store(MotivoEventoRequest $request){
         $dados = $request->all();

        try {
            $this->model->create($dados);
            return $this->sendResponse($this->SUCESSO);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }
    }

    public function update(MotivoEventoRequest $request, $id){

        $motivoEvento = MotivoEvento::find($id);
        if (!$motivoEvento){
            return $this->sendError($this->NAO_LOCALIZADO);
        }

        try {
            $dados = $request->all();
            $dados['status'] = (!isset($dados['status']))? 0 : 1;
            $dados['ocultar'] = (!isset($dados['ocultar']))? 0 : 1;
            $motivoEvento->fill($dados)->save();
            return $this->sendResponse($this->SUCESSO);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }
    }

    public function destroy($id){
        try {
            $dados = $this->model::where('id', $id);
            $dados->delete();
            return $this->sendResponse($this->SUCESSO);
        } catch (\Exception $e) {
            return $this->sendResponse($this->SUCESSO);
        }
    }

    public function create(){
        return view('admin.administrador.motivo_evento.create');
    }

    public function edit($id){
        $motivoEvento = $this->model::find($id);
        if(!$motivoEvento){
            return $this->sendError($this->NAO_LOCALIZADO);
        }
        return $this->sendResponse(['motivoEvento' => $motivoEvento]);
    }
}
