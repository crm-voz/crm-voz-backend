<?php

namespace App\Http\Controllers\Api\v1\Administrador;

use App\Helpers\Boletos\BaixaBoletos\BaixaBoletoBB;
use App\Helpers\Boletos\BaixaBoletos\BaixaBoletoBradesco;
use App\Helpers\Boletos\BaixaBoletos\BaixaBoletoCaixa;
use App\Helpers\Boletos\BaixaBoletos\BaixaBoletoItau;
use App\Helpers\Boletos\BaixaBoletos\BaixaBoletoSantander;
use App\Helpers\Boletos\BaixaBoletos\BaixaBoletoSicob;
use App\Http\Controllers\Controller;
use App\Models\Financeiro\BaixaBoleto;
use App\Repository\Administrador\BaixarBoletoRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class BaixarBoletoController extends Controller
{
    protected $modelBaixaBoleto, $baixarBoletoRepository;
    public $nome_arquivo;

    public function __construct(BaixaBoleto $baixaBoleto, BaixarBoletoRepository $baixarBoletoRepository)
    {
        $this->modelBaixaBoleto = $baixaBoleto;
        $this->baixarBoletoRepository = $baixarBoletoRepository;
    }

    public function index(){
        return  $this->sendResponse($this->modelBaixaBoleto->all());
    }

    public function boleto(Request $request){
        $file = $request->file('remessa');
        $arquivo = file($file);
        $banco400 = substr($arquivo[0], 76, 3);
        $banco240 = substr($arquivo[0], 0, 3);
        //dd($arquivo[0]);
        $lenght = $arquivo[0];
        $cnab = Str::length($lenght)-2;
        $retorno = false;
        if(($banco400 == '341') && ($cnab == '400')){
            $baixaBoleto = app(BaixaBoletoItau::class);
            $retorno = $baixaBoleto->baixar($request);
        }

        if(($banco400 == '237') && ($cnab == '400')){
            $baixaBoleto = app(BaixaBoletoBradesco::class);
            $retorno = $baixaBoleto->baixar($request);
        }

        if(($banco400 == '756')){
            $baixaBoleto = app(BaixaBoletoSicob::class);
            $retorno = $baixaBoleto->baixar($request);
        }

        if(($banco240 == '001') && ($cnab == '240')){
            $baixaBoleto = app(BaixaBoletoBB::class);
            $retorno = $baixaBoleto->baixar($request);
        }

        if(($banco240 == '033') && ($cnab == '240')){
            $baixaBoleto = app(BaixaBoletoSantander::class);
            $retorno = $baixaBoleto->baixar($request);
        }
        if(($banco240 == '104') && ($cnab == '240')){
            $baixaBoleto = app(BaixaBoletoCaixa::class);
            $retorno = $baixaBoleto->baixar($request);
        }

        if ($retorno['result'])
          return $this->sendResponse(true);
        else
          return $this->sendError($retorno['erro'],500);
    }



}
