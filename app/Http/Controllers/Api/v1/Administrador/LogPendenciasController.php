<?php

namespace App\Http\Controllers\Api\v1\Administrador;

use App\Http\Controllers\Controller;
use App\Repository\Administrador\LogPendenciasRepository;
use Illuminate\Http\Request;

class LogPendenciasController extends Controller
{
    public function __construct(LogPendenciasRepository $logPendenciasRepository)
    {
        $this->logPendenciasRepository = $logPendenciasRepository;
    }

    public function filtrar(Request $request)
    {
        return $this->sendResponse($this->logPendenciasRepository->filtrar($request['operacao_id']));
    }

    public function store(Request $request)
    {
        return $this->sendResponse($this->logPendenciasRepository->criarEmLote($request->all()));
    }

}
