<?php

namespace App\Http\Controllers\Api\v1\Administrador;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\v1\Services\BoletosServices;
use App\Repository\Financeiro\BoletoRepository;
use Illuminate\Http\Request;

class BoletosController extends Controller
{
    protected $boletoService;
    public function __construct(BoletoRepository $boletoRepository,
                                BoletosServices $boletoService)
    {
        $this->boletoRepository = $boletoRepository;
        $this->boletoService = $boletoService;
    }

    public function index(){
        $boletos = $this->boletoRepository->getAll();
        if(!$boletos){
            return $this->sendError($this->NAO_LOCALIZADO);
        }
        return $this->sendResponse(['boleto' => $boletos]);
    }

    public function store(Request $request){
        $dados = $request->all();
        try {
            $boleto = $this->boletoRepository->create($dados);
            $res = $this->boletoService->enviar_boleto($boleto);
            return $this->sendResponse($res);
        } catch (\Throwable $th) {
            return $this->sendError("Erro: " . $th->getMessage());
        }
    }

    public function update(Request $request, $id){
        $dados = $request->all();
        $boleto = $this->boletoRepository->find($id);
        if(!$boleto){
            return $this->sendError($this->NAO_LOCALIZADO);
        }

        try {
            $boleto->update($dados);
            return $this->sendResponse($dados);
        } catch (\Exception $e) {
            return $this->sendError($this->ERRO);
        }

    }

    public function data_boleto(Request $request){
        $result = $this->boletoRepository->getDataBoleto($request->data_inicial, $request->data_final);
        if(!$result){
            return $this->sendError($this->NAO_LOCALIZADO);
        }
        return $this->sendResponse(['filtro' => $result]);
    }

    public function destroy($id){
        try {
            $boleto = $this->boletoRepository->find($id);
            $boletoDel = $boleto->delete();
            return $this->sendResponse($boletoDel);
        } catch (\Exception $e) {
            return $this->sendResponse($this->ERRO);
        }
    }

    public function create(){
        $boletos = $this->boletoRepository->all();
        return $this->sendResponse(['boleto' => $boletos]);
    }

    public function edit($id){
        $boleto = $this->boletoRepository->find($id);
        if(!$boleto){
            return $this->sendResponse($this->NAO_LOCALIZADO);
        }
        return $this->sendResponse(['boleto' => $boleto]);
    }

    public function salvar_boleto($dados) {
        try {
            $boleto = $this->boletoRepository->create($dados);
            return $boleto;
        } catch(\Exception $e) {
            throw $e;
        }
    }

    public function viewBoleto(Request $request){
        $boleto = $this->boletoRepository->find($request->boleto);
        if (!$boleto){
            return $this->sendResponse($this->NAO_LOCALIZADO);
        }
        $this->boletoService->viewBoleto($boleto);
    }

    public function visualizarBoletoDaParcela($idParcela){
        $parcela = $this->boletoRepository->where('parcelas_id','=', $idParcela)->first();
        if (!$parcela){
            return $this->sendResponse($this->NAO_LOCALIZADO);
        }
        return $this->sendResponse($parcela);
    }

}
