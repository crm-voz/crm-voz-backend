<?php

namespace App\Http\Controllers\Api\v1\Administrador;

use App\Http\Controllers\Controller;
use App\Models\BackOffice\Cliente;
use App\Models\BackOffice\Credor;
use App\Models\Core\TemplateEmail;
use Illuminate\Http\Request;

class TemplateEmailController extends Controller
{
    protected $modelTemplateEmail;
    protected $modelCliente;
    protected $modelCredor;

    public function __construct(TemplateEmail $TemplateEmail, Cliente $cliente, Credor $credor)
    {
        $this->modelTemplateEmail = $TemplateEmail;
        $this->modelCliente = $cliente;
        $this->modelCredor = $credor;
    }

    public function store(Request $request){
         $dados = $request->all();

        try {
            $this->modelTemplateEmail->create($dados);
            return $this->sendResponse($this->SUCESSO);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }
    }

    public function update(Request $request, $id){
        $dados = $request->all();

        $TemplateEmail = $this->modelTemplateEmail->find($id);
        if(!$TemplateEmail){
            return $this->sendError($this->NAO_LOCALIZADO);
        }

        try {

            $TemplateEmail->update($dados);
            return $this->sendResponse($this->SUCESSO);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }

    }

    public function destroy($id){
        try {
            $dados = $this->modelTemplateEmail::where('id', $id);
            $dados->delete();
            return $this->sendResponse($this->SUCESSO);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }
    }

    public function index(){
        $TemplateEmail = $this->modelTemplateEmail::with('credores')->get();
         return $this->sendResponse(['TemplateEmail' => $TemplateEmail]);
    }

    public function create(){
        $TemplateEmail = $this->modelTemplateEmail::all();
        return $this->sendResponse(['TemplateEmail' => $TemplateEmail]);
    }

    public function edit($id){
        $TemplateEmail = $this->modelTemplateEmail::find($id);
        if(!$TemplateEmail){
            return $this->sendResponse($this->NAO_LOCALIZADO);
        }

        return $this->sendResponse(['TemplateEmail' => $TemplateEmail]);
    }

    public function preencheremail(Request $request){
        try {
            $TemplateEmail = $this->modelTemplateEmail::find($request->templateemail_id);
            $cliente = $this->modelCliente::find($request->cliente_id);
            $credor = $this->modelCredor::find($request->credores_id);

            $texto = $TemplateEmail->mensagem;
            $stringCorrigida1 = str_replace('$CLIENTE$', $cliente->nome, $texto);
            $stringCorrigida2 = str_replace('$CPF$', $cliente->cpf_cnpj, $stringCorrigida1);
            $stringCorrigida3 = str_replace('$DATA_RETIRADA$', $request->data_retirada, $stringCorrigida2);
            $stringCorrigida4 = str_replace('$CREDOR$', $credor->nome, $stringCorrigida3);
        return $this->sendResponse($stringCorrigida4);
        } catch (\Exception $e) {
            return $this->sendError($this->NAO_LOCALIZADO.' '.$e->getMessage());
        }
    }
}
