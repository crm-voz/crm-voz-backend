<?php

namespace App\Http\Controllers\Api\v1\Administrador;

use App\Repository\Administrador\PerfilRepository;
use App\Http\Controllers\Controller;
use App\Models\Core\Eventos;
use App\Repository\Backoffice\EventosRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EventosController extends Controller
{
    protected $eventosRepository;
    protected $perfil;

    /**
     * @author Welliton Cunha
     * @since 08.07.2021 às 09:39
     * @see Cadastro e Filtros de Eventos
     * @see Perfil de Acesso
     */

    public function __construct(EventosRepository $eventosRepository, PerfilRepository $profile)
    {
        $this->eventosRepository = $eventosRepository;
        $this->perfil = $profile;
    }

    public function index($id){
        $evento = $this->eventosRepository->find($id);
        $evento?->cliente;
        $evento?->usuario_empresas;
        $evento?->usuario_empresas?->user;
        $evento?->motivo_evento;
        $evento?->credores;

        if(!$evento){
            return $this->sendError($this->NAO_LOCALIZADO);
        }

        return $this->sendResponse(['eventos' => $evento]);
    }

    public function store(Request $request){
        try {
            $this->eventosRepository->createEventos($request->cliente_id,
                                             $request->descricao,
                                             $request->motivo_evento_id);

            return $this->sendResponse(true);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }
    }

    public function update(Request $request, $id){
        $dados = $request->all();
        $evento = $this->eventosRepository->find($id);

        if (!$evento){
            return $this->sendError($this->NAO_LOCALIZADO);
        }

        try {
            $evento->update($dados);
            return $this->sendResponse($this->SUCESSO);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }
    }

    public function destroy($id){
        $evento = $this->eventosRepository->find($id);

        if(!$evento){
            return $this->sendError($this->NAO_LOCALIZADO);
        }
        try {
            $evento->delete();
            return $this->sendResponse($this->SUCESSO);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }
    }

    public function edit($id){
        $evento = $this->eventosRepository->find($id);
        $evento->cliente;
        $evento->usuario_empresas;
        $evento->usuario_empresas->user;
        $evento->motivo_evento;
        $evento->credores;

        if(!$evento){
            return $this->sendError($this->NAO_LOCALIZADO);
        }

        return $this->sendResponse(['eventos' => $evento]);
    }
}
