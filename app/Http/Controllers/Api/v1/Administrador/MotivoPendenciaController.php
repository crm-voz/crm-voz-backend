<?php

namespace App\Http\Controllers\Api\v1\Administrador;

use App\Models\Core\MotivoPendencia;
use App\Http\Controllers\Controller;
use App\Http\Requests\MotivoPendenciaRequest;

class MotivoPendenciaController extends Controller
{
    protected $model;

    public function __construct(MotivoPendencia $motivo){
        $this->model = $motivo;
    }

    public function index(){
        return $this->sendResponse(['dados' => $this->model->all()]);
    }

    public function store(MotivoPendenciaRequest $request){
        try {
            $dados = $request->all();
            $this->model->create($dados);
            return $this->sendResponse($this->SUCESSO);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }
    }

    public function update(MotivoPendenciaRequest $request, $id){
        $motivo_devolucao = $this->model->find($id);
        if (!$motivo_devolucao){
            return $this->sendError($this->NAO_LOCALIZADO);
        }

        try {
            $dados = $request->all();
            $dados['status'] = $dados['status'] ? true : false;
            $motivo_devolucao->fill($dados)->save();
            return $this->sendResponse($this->SUCESSO);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }
    }

    public function destroy($id){
        $dados = $this->model->find($id);
        try {
            $dados->delete();
            return $this->sendResponse($this->SUCESSO);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }
    }


    public function create(){
        return view('admin.administrador.motivo_pendencia.create');
    }

    public function edit($id){
        $motivo = $this->model->find($id);

        if(!$motivo){
            return $this->sendError($this->NAO_LOCALIZADO);
        }

        return $this->sendResponse(['motivo_pendencia' => $motivo]);
    }
}
