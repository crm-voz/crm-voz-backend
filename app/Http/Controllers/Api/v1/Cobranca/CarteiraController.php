<?php

namespace App\Http\Controllers\Api\v1\Cobranca;

use App\Http\Controllers\Controller;
use App\Repository\Cobranca\CarteiraRepository;
use Illuminate\Http\Request;

class CarteiraController extends Controller
{
    public function __construct(CarteiraRepository $carteiraRepository)
    {
        $this->carteiraRepository = $carteiraRepository;
    }

    public function index(){
        return $this->sendResponse($this->carteiraRepository->listarCarteiras());
    }

    public function get($id){
        return $this->sendResponse($this->carteiraRepository->getCarteira($id));
    }

    public function create(){
        return $this->sendResponse($this->carteiraRepository->getDadosCreate());
    }

    public function store(Request $request){
        try {
            $carteira = $request->all();
            $novaCarteira = $this->carteiraRepository->criarCarteira($carteira);
            return $this->sendResponse($novaCarteira);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }
    }

    public function update(Request $request, $id){
        $dados = $request->all();
        $carteira = $this->carteiraRepository->find($id);
        if (!$carteira){
            return $this->sendError($this->NAO_LOCALIZADO);
        }
        try {
            $carteira->update($dados);
            return $this->sendResponse($dados);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }
    }

    public function destroy($id){
        $carteira = $this->carteiraRepository->find($id);
        if(!$carteira){
            return $this->sendError($this->NAO_LOCALIZADO);
        }
        try {
            $carteira->delete();
            return $this->sendResponse($carteira);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }
    }

    public function edit($id){
        $carteira = $this->carteiraRepository->find($id);
        $carteira->supervisor;
        $carteira->credoresCarteira;
        $carteira->executivos;

        if(!$carteira){
            return $this->sendError($this->NAO_LOCALIZADO);
        }

        return $this->sendResponse(['carteira' => $carteira]);
    }


}
