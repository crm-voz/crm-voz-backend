<?php

namespace App\Http\Controllers\Api\v1\Cobranca;

use App\Http\Controllers\Controller;
use App\Repository\Cobranca\OperacoesParcelasRepository;
use Illuminate\Http\Request;

class OperacoesParcelasController extends Controller
{
    public function __construct(OperacoesParcelasRepository $operacaoParcelasRepository)
    {
        $this->operacaoParcelasRepository = $operacaoParcelasRepository;
    }

    public function filtrar(Request $request)
    {
        $filtros = $request->all();
        return $this->sendResponse($this->operacaoParcelasRepository->filter($filtros));
    }
}
