<?php

namespace App\Http\Controllers\Api\v1\Cobranca;

use App\Http\Controllers\Controller;
use App\Models\Cobranca\Acordo;
use App\Models\Cobranca\ExecutivoCobranca;
use App\Repository\Backoffice\ClientesRepository;
use App\Repository\Backoffice\ExecutivoCobrancaRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ExecutivoCobrancaController extends Controller
{

    protected $model;
    protected $clientesRepository;
    protected $executivoCobrancaRepository;
    public function __construct
    (
        ExecutivoCobrancaRepository $executivoCobrancaRepository,
        ExecutivoCobranca $executivo,
        ClientesRepository $cliente
    )
    {
        $this->executivoCobrancaRepository = $executivoCobrancaRepository;
        $this->model = $executivo;
        $this->clientesRepository = $cliente;
    }

    public function index(){
        $executivo = $this->model->with('funcionarios')->get();
        foreach ($executivo as $value) {

            $exec[] = [
                'id' => $value->id,
                'nome' => $value->funcionarios->nome,
                'meta' => $value->meta
            ];
        }
        return $exec;
    }

    public function edit($id){
        try {
            $exe = $this->model->find($id)->with('funcionarios')->get();
            if ($exe){
                return $this->sendResponse($exe);
            }else{
                return $this->sendError($this->NAO_LOCALIZADO);
            }

        } catch (\Throwable $th) {
            throw $th;
        }

    }

    public function store(Request $request){
        $executivo = $this->model->where('funcionarios_id',$request->funcionarios_id)->first();
        if(!$executivo){
            try {
                if ($this->model->create($request->all())){
                    return $this->sendResponse($this->SUCESSO);
                }else{
                    return $this->sendError($this->ERRO);
                }
            } catch (\Exception $e) {
                throw $e;
            }
        }else{
            return $this->sendError('Funcionário já é Executivo de Cobrança.');
        }
    }

    public function update(Request $request, $id){
        try {
            $exe = $this->model->find($id);
            if ($exe){
                $exe->update($request->all());
                return $this->sendResponse($this->SUCESSO);
            }else{
                return $this->sendError($this->NAO_LOCALIZADO);
            }
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function delete($id){
        try {
            $exe = $this->model->find($id);
            if ($exe){
                $exe->delete();
                return $this->sendResponse($this->SUCESSO);
            }else{
                return $this->sendError($this->NAO_LOCALIZADO);
            }
        } catch (\Throwable $th) {
            throw $th;
        }

    }

    public function trocarExecutivo(Request $request, $id){
        $acordo = Acordo::find($id);
        $executivoAntigo = $acordo?->executivo_cobranca_id;
        if(!$acordo){
            return $this->sendError($this->NAO_LOCALIZADO);
        }
        try {
            DB::beginTransaction();
            $executivo = ExecutivoCobranca::where('id',$request->executivo_cobranca_id)->with('funcionarios')->first();
            $executivo_updated = $acordo->update(['executivo_cobranca_id' => $request->executivo_cobranca_id]);
            $this->clientesRepository->criar_evento($acordo->cliente_id,'TROCA DE EXECUTIVO DE COBRANÇA DO ACORDO '.$acordo->numero_acordo, $executivoAntigo, $executivo->funcionarios->nome);
            DB::commit();
            return $this->sendResponse($executivo_updated);
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->sendError($this->ERRO);
        }
    }

    public function listaParcelasStatus($executivoId,$status){
        return $this->sendResponse($this->executivoCobrancaRepository->parcelasAcordoPorExecutivo($executivoId, $status));
    }

    public function acordosExecutivo($executivoId){
        return $this->sendResponse( $this->executivoCobrancaRepository->acordos($executivoId) );
    }

    public function acordosDoMes($executivoId){
        return $this->sendResponse( $this->executivoCobrancaRepository->acordosDoMes($executivoId));
    }

    public function rankExecutivo(Request $request){
        return $this->sendResponse($this->executivoCobrancaRepository->rankExecutivo($request));
    }

    public function getExecutivo($id){
        return $this->sendResponse($this->model->find($id));
    }
}
