<?php

namespace App\Http\Controllers\Api\v1\Cobranca;

use App\Http\Controllers\Controller;
use App\Models\Cobranca\OperacoesParcelas;
use App\Repository\Cobranca\ParcelaRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ParcelasController extends Controller
{
    /**
     * @author Welliton Cunha
     * @since 09.07.2021 às 08:48
     *
     * @see Parcela
     */

    protected $parcelaRepository;
    public function __construct(ParcelaRepository $parcelaRepository)
    {
        $this->parcelaRepository = $parcelaRepository;
    }

    public function index($id){
        $parcela = $this->parcelaRepository->find($id);
        if(!$parcela){
            return $this->sendError($this->NAO_LOCALIZADO);
        }
        $parcela->acordos;
        $parcela->usuario_empresas->user;

        return $this->sendResponse(['parcela' => $parcela]);
    }

    public function indexAcordo($id){
        $parcela = $this->parcelaRepository->where('acordos_id','=', $id)->get();

        if(!$parcela){
            return $this->sendError($this->NAO_LOCALIZADO);
        }

        return $this->sendResponse(['parcela' => $parcela]);
    }

    public function store(Request $request){
        try {

            $parcela = $request->all();
            $this->parcelaRepository->create($parcela);

            return $this->sendResponse($parcela);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }
    }

    public function update(Request $request, $id){
        $dados = $request->all();
        $parcela = $this->parcelaRepository->find($id);

        if (!$parcela){
            return $this->sendError($this->NAO_LOCALIZADO);
        }

        try {
            DB::beginTransaction();
            foreach($dados['operacoesParcelas'] as $op){
                OperacoesParcelas::find($op['id'])->update([
                        'juros' => $op['juros'],
                        'multa' => $op['multa'],
                        'honorarios' => $op['honorarios'],
                        'desconto' => $op['desconto'],
                        'vencimento' => $op['vencimento'],
                        'realizado' => $op['realizado'],
                        'repasse' => $op['repasse'],
                    ]);
            }
            $parcela->update($dados);
            DB::commit();
            return $this->sendResponse($dados);
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->sendError($e->getMessage());
        }
    }

    public function destroy($id)
    {
        $parcela = $this->parcelaRepository->where('id','=', $id);

        if(!$parcela){
            return $this->sendError($this->NAO_LOCALIZADO);
        }
        try {
            $parcela->delete();
            return $this->sendResponse($parcela);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }
    }

    public function edit($id)
    {
        $parcela = $this->parcelaRepository->find($id);
        $parcela->acordos;
        $parcela->usuario_empresas->user;

        if(!$parcela){
            return $this->sendError($this->NAO_LOCALIZADO);
        }

        return $this->sendResponse(['parcela' => $parcela]);
    }

    public function incluirBaixa(Request $request)
    {
        $dados = $request->all();
        $parcelas = $this->parcelaRepository->getParcelas($dados['ids']);
        if (!$parcelas){
            return $this->sendResponse($this->ERRO);
        }

        $return = $this->parcelaRepository->incluirBaixa($parcelas, $dados['data_baixa'], $dados['valor_comissao'], $dados['valor_pago'], true);
        if ($return['result'])
            return $this->sendResponse(true);
        else
            return $this->sendError($return['erro']);
    }

    public function removerBaixa(Request $request)
    {
        $dados = $request->all();
        $parcelas = $this->parcelaRepository->getParcelas($dados['ids']);
        if (!$parcelas){
            return $this->sendResponse($this->ERRO);
        }

        $retorno = $this->parcelaRepository->retirarBaixa($parcelas);
        if ($retorno['result'])
            return $this->sendResponse(true);
        else
          return $this->sendError($retorno['erro']);
    }


    public function listagem(Request $request)
    {
        try {
            return $this->parcelaRepository->filtrarParcelas($request);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function filtro(Request $request)
    {
        $filtro = $request->all();
        return $this->sendResponse($this->parcelaRepository->filtrar($filtro));
    }

}
