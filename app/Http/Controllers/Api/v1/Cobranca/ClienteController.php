<?php

namespace App\Http\Controllers\Api\v1\Cobranca;

use App\Http\Controllers\Controller;
use App\Models\BackOffice\Cliente;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ClienteController extends Controller
{
    public function __construct(Cliente $cliente)
    {
        $this->cliente = $cliente;
    }

    public function editCliente($id){
        $cliente = $this->cliente->find($id);
        return $this->sendResponse(['cliente' => $cliente]);
    }

    public function update(Request $request, $id){
        $dados = $request->all();
        $cliente = $this->cliente::find($id);
        if(!$cliente){
            return $this->sendError($this->NAO_LOCALIZADO);
        }
        try {
            $cliente->update($dados);
            return $this->sendResponse($cliente);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }
    }

    public function clienteCpf($id){
        $cliente = $this->cliente->where('cpf_cnpj',$id)->first(DB::raw('id,nome'));
        if ($cliente){
            return $this->sendResponse($cliente);
        }else{
            return $this->sendError(false);
        }
    }

}
