<?php

namespace App\Http\Controllers\Api\v1\Cobranca;

use App\Http\Controllers\Controller;
use App\Repository\Cobranca\PropostaRepository;
use Illuminate\Http\Request;

class PropostaController extends Controller
{
    public function __construct(PropostaRepository $propostaRepository)
    {
        $this->propostaRepository = $propostaRepository;
    }

    public function propostasPorSupervisor(Request $request, $id){
        $status = $request['status'];
        return $this->sendResponse($this->propostaRepository->listarPropostaPorSupervisor($status, $id));
    }

    public function propostasPorExecutivo(Request $request, $id){
        $status = $request['status'];
        return $this->sendResponse($this->propostaRepository->listarPropostaPorExecutivo($status, $id));
    }

    public function store(Request $request){
        try {
            $proposta = $request->all();
            $proposta = $this->propostaRepository->createProposta($proposta);
            return $this->sendResponse($proposta);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }
    }

    public function updateStatus(Request $request, $id){
        $proposta = $this->propostaRepository->find($id);
        $dados = $request->all();
        if (!$proposta){
            return $this->sendError($this->NAO_LOCALIZADO);
        }
        try {
            $proposta->status = $dados['status'];
            $proposta->save();
            return $this->sendResponse($proposta);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }
    }
}
