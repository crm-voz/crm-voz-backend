<?php

namespace App\Http\Controllers\Api\v1\Cobranca;

use App\Helpers\Relatorios\DemonstrativoDetalhado;
use App\Helpers\Relatorios\DemonstrativoSimples;
use App\Http\Controllers\Controller;
use App\Repository\Cobranca\AcordoRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Api\v1\Services\BoletosServices;
use App\Http\Controllers\Api\v1\Services\NegociacaoServices;
use App\Models\BackOffice\Cliente;
use App\Models\BackOffice\Operacao;
use App\Repository\Backoffice\ClientesRepository;
use App\Repository\Backoffice\EventosRepository;
use Illuminate\Support\Facades\DB;


class AcordoController extends Controller
{

    /**
     * @author Welliton Cunha
     * @since 09.07.2021 às 08:48
     *
     * @see Acordo
     */
    protected $acordoRepository;
    protected const EVENTO_TROCA_EXECUTIVO = 41;

    public function __construct(
        AcordoRepository $repository,
        BoletosServices $service,
        ClientesRepository $clientesRepository,
        EventosRepository $eventosRepository)
    {
        $this->acordoRepository = $repository;
        $this->boletosService = $service;
        $this->clientesRepository = $clientesRepository;
        $this->eventosRepository = $eventosRepository;
    }

    public function index($id){
        $acordo = $this->acordoRepository->find($id);
        if(!$acordo){
            return $this->sendError($this->NAO_LOCALIZADO);
        }

        $acordo->cliente;
        $acordo->executivo_cobranca->funcionarios;
        $acordo->usuario_cancelou;
        $acordo->campanhas;
        $acordo->parcelas;
        $operacoes = $acordo->operacoes_acordo()->get()->map(function($item, $key) {
            return $item->operacao;
        });

        return $this->sendResponse(['acordo' => $acordo, 'operacoes' => $operacoes]);
    }

    public function filtrar(Request $request)
    {
        $filtro = $request->all();
        return $this->sendResponse($this->acordoRepository->filtro($filtro));
    }

    public function cliente($id){
        $acordoByCliente = $this->acordoRepository
                            ->where('cliente_id','=', $id)
                            ->get();

        if(!$acordoByCliente){
            return $this->sendError($this->NAO_LOCALIZADO);
        }

        return $this->sendResponse(['acordo_cliente' => $acordoByCliente]);
    }

    public function pesquisaAvancada(Request $request){
        $dados = $request->all();
        return $this->sendResponse($this->clientesRepository->pesquisaAvancada($dados));
    }

    public function nome_aluno(Request $request){
        $result = $this->acordoRepository->pesquisaNomeAluno($request->nome_aluno);
        if(!$result){
            return $this->sendError($this->NAO_LOCALIZADO);
        }

        return $this->sendResponse(['filtro' => $result]);
    }

    public function data_acordo(Request $request){
        $result = $this->acordoRepository->acordosPorData($request->data_inicial, $request->data_final);
        if(!$result){
            return $this->sendError($this->NAO_LOCALIZADO);
        }

        return $this->sendResponse(['filtro' => $result]);
    }

    public function filtroAcordo(Request $request){
        $result = $this->acordoRepository->filtroAcordo($request->all());
        if(!$result){
            return $this->sendError($this->NAO_LOCALIZADO);
        }

        return $this->sendResponse(['filtro' => $result]);
    }

    //Todo: Refatorar store do excesso de responsabilidade
    public function store(Request $request){
        try {
            $acordo = $this->acordoRepository->criar_acordo($request->all());
            $sms = $this->acordoRepository->enviar_sms($acordo);
            return $this->sendResponse(['acordo' => $acordo, 'sms' => $sms]);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }
    }

    public function update(Request $request, $id){
        $dados = $request->all();
        $acordo = $this->acordoRepository->find($id);

        if (!$acordo){
            return $this->sendError($this->NAO_LOCALIZADO);
        }

        try {
            $acordo->update($dados);
            return $this->sendResponse($dados);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }
    }

    //todo::Remover metodo updateExecutivoCobranca do acordoController, deve ser usado uma delegação para um metodo correspondente no ExecutivoCobrancaController
    public function updateExecutivoCobranca(Request $request, $acordoId){
        $dados = $request->all();
        $acordo = $this->acordoRepository->find($acordoId);

        if (!$acordo){
            return $this->sendError($this->NAO_LOCALIZADO);
        }
        try {
            DB::beginTransaction();
            $update =  $acordo->update([
                'executivo_cobranca_id' => $dados['executivo_cobranca']
            ]);
            $this->eventosRepository->createEventos($acordo->cliente_id,'ALTERAÇÃO DO EXECUTIVO DE COBRANÇA', self::EVENTO_TROCA_EXECUTIVO);
            DB::commit();
            return $this->sendResponse($update);
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->sendError($e->getMessage());
        }
    }

    public function destroy($id){
        try {
            return $this->sendResponse($this->acordoRepository->cancelar_acordo($id));
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }
    }

    public function edit($id){
        $acordo = $this->acordoRepository->find($id);
        if(!$acordo){
            return $this->sendError($this->NAO_LOCALIZADO);
        }

        $acordo->cliente;
        $acordo->executivo_cobranca->funcionarios;
        $acordo->usuario_cancelou;
        $acordo->campanhas;
        $acordo->parcelas;
        $operacoes = $acordo->operacoes_acordo()->get()->map(function($item, $key) {
            return $item->operacao;
        });

        return $this->sendResponse(['acordo' => $acordo, 'operacoes' => $operacoes]);
    }

    //todo::REfatorar
    public function executivo($id) {
        try {
            $acordo = $this->acordoRepository->model->select('acordo.*','cliente.nome as nome_cliente','credores.nome as nome_credor')
                    ->join('cliente','cliente.id','=','acordo.cliente_id')
                    ->join('operacoes','operacoes.cliente_id','=','acordo.cliente_id')
                    ->join('remessas','remessas.id','=','operacoes.remessa_id')
                    ->join('credores','credores.id','=','remessas.credor_id')
                    ->where('executivo_cobranca_id',$id)
                    ->distinct(DB::raw('id'))->get();

            return $this->sendResponse($acordo);
        } catch (\Exception $th) {
            return $this->sendError($th->getMessage());
        }
    }

    public function negociacao($id){
        try {
            $negociacao = app(NegociacaoServices::class);
            $info = $negociacao->negociacaoByCliente($id);
            if ( $info == false){
                return $this->sendError($this->NAO_LOCALIZADO);
            }

            return $this->sendResponse(["info" => $info]);

        } catch (\Throwable $th) {
            return $this->sendError($th->getMessage());
        }

    }

    public function acordo_periodo(Request $request){
        try {
            return $this->sendResponse($this->acordoRepository->acordo_periodo_chart($request));
        } catch (\Throwable $th) {
            return $this->sendError($th->getMessage());
        }

    }

    public function decodificar_hash($hash){
        try {
            return $this->sendResponse($this->acordoRepository->decodificar_hash($hash));
        } catch (\Throwable $th) {
            return $this->sendError($th->getMessage());
        }

    }

    public function acordosVencidos(){
        try {
            return $this->sendResponse($this->acordoRepository->acordos_vencidos());
        } catch (\Throwable $th) {
            return $this->sendError($th->getMessage());
        }

    }

    public function quebraAcordo($id){
        try {
            if ($this->acordoRepository->quebra_acordo($id) == true){
                return $this->sendResponse($this->SUCESSO);
            }else{
                return $this->sendError('Acordo não localizado.');
            }
            // return $this->acordoRepository->quebra_acordo($id);
        } catch (\Throwable $th) {
            return $this->sendError($th->getMessage());
        }

    }

    public function totalAcordoCredor(){
        try {
            return $this->sendResponse(['total_acordos' => $this->acordoRepository->totalAcordosCredor()]);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function viewDemoSimples(Request $request, $id=null){
        if($request->simulacao == true){
            $demoSimples = new DemonstrativoSimples('simulacao-demo-Simples.png');
            $nomeCliente = Cliente::select('nome')->find($request->acordo['cliente_id']);
            $nomeCredor = Operacao::where('id',$request->operacoes[0]['id'])->first();
            $acordo = (object)($request->acordo);
            $operacoes = $request->operacoes;
            $parcelas = (object)$request->parcelas;
            $demoSimples->escrever_dados_cliente($nomeCliente->nome)
                            ->escrever_dados_credor($nomeCredor->remessa?->credor?->nome)
                            ->escrever_dados_acordo($acordo)
                            ->escrever_dados_operacao($operacoes)
                            ->escrever_dados_parcelas($parcelas)
                            ->gerar('I');

        }else{
            $demoSimples = new DemonstrativoSimples('demo-Simples.png');
            $acordo = $this->acordoRepository->find($id);
            if(!$acordo){
                return $this->sendError($this->NAO_LOCALIZADO);
            }

            $acordo->cliente;
            $acordo->executivo_cobranca->funcionarios;
            $acordo->usuario_cancelou;
            $acordo->campanhas;
            $acordo->parcelas;
            $operacoes = $acordo->operacoes_acordo()->get()->map(function($item, $key) {
                return $item->operacao;
            });

            $demoSimples->escrever_dados_cliente($acordo->cliente->nome)
                            ->escrever_dados_credor($operacoes[0]->remessa?->credor?->nome)
                            ->escrever_dados_acordo($acordo)
                            ->escrever_dados_operacao($operacoes)
                            ->escrever_dados_parcelas($acordo->parcelas)
                            ->gerar('I');
        }

    }

    public function viewDemoDetalhado(Request $request, $id=null){
        if($request->simulacao == true){
            $demoDetalhado = new DemonstrativoDetalhado('simulacao-demostrativo-detalhado.png');
            $nomeCliente = Cliente::select('nome')->find($request->acordo['cliente_id']);
            $nomeCredor = Operacao::where('id',$request->operacoes[0]['id'])->first();
            $acordo = (object)($request->acordo);
            $operacoes = $request->operacoes;
            $parcelas = (object)$request->parcelas;
            $demoDetalhado->escrever_dados_cliente($nomeCliente->nome)
                            ->escrever_dados_credor($nomeCredor->remessa?->credor?->nome)
                            ->escrever_dados_acordo($acordo, $operacoes)
                            ->escrever_dados_operacao($operacoes)
                            ->escrever_dados_parcelas($parcelas)
                            ->gerar('I');


        }else{
            $demoDetalhado = new DemonstrativoDetalhado('demostrativo-detalhado.png');
            $acordo = $this->acordoRepository->find($id);
            if(!$acordo){
                return $this->sendError($this->NAO_LOCALIZADO);
            }

            $acordo->cliente;
            $acordo->executivo_cobranca->funcionarios;
            $acordo->usuario_cancelou;
            $acordo->campanhas;
            $acordo->parcelas;
            $operacoes = $acordo->operacoes_acordo()->get()->map(function($item, $key) {
                return $item->operacao;
            });

            $demoDetalhado->escrever_dados_cliente($acordo->cliente->nome)
                            ->escrever_dados_credor($operacoes[0]->remessa?->credor?->nome)
                            ->escrever_dados_acordo($acordo)
                            ->escrever_dados_operacao($operacoes)
                            ->escrever_dados_parcelas($acordo->parcelas)
                            ->gerar('I');
        }

    }

}
