<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Api\v1\Services\RemessaServices;
use App\Http\Controllers\Backoffice\Repository\RemessaRepository;
use App\Http\Controllers\Controller;
use App\Models\BackOffice\Credor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RemessaController extends Controller
{
    protected RemessaRepository $remessaRepository;

    public function __construct(RemessaRepository $remessaRepository)
    {
        $this->remessaRepository = $remessaRepository;
    }


    public function store(Request $request){
       $credor = Credor::where('cnpj', $request->cnpj)->first();
       if (!$credor){
         return $this->sendError($this->NAO_LOCALIZADO);
       }

       try {
            DB::beginTransaction();
            $remessaServices = new RemessaServices($request->all(), $credor);
            $remessa = $remessaServices->salvar();
            DB::commit();
            return $this->sendResponse($remessa, $this->SUCESSO);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->sendError($e->getMessage(), $this->ERRO);
       }
    }
}
