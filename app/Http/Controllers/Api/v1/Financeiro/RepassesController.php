<?php

namespace App\Http\Controllers\Api\v1\Financeiro;

use App\Http\Controllers\Controller;
use App\Repository\Financeiro\RepassesRepository;
use Illuminate\Http\Request;

class RepassesController extends Controller
{
    protected $repassesRepository;
    public function __construct(RepassesRepository $repassesRepository)
    {
        $this->repassesRepository = $repassesRepository;
    }
    public function index(Request $request){
        return $this->repassesRepository->index($request);
    }
}
