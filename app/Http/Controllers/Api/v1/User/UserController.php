<?php

namespace App\Http\Controllers\Api\v1\User;

use App\Http\Controllers\Controller;
use App\Models\Core\Funcionario;
use App\Models\Core\PerfilUsuario;
use App\Models\Core\UsuarioEmpresa;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Codedge\Fpdf\Fpdf\Fpdf;

class UserController extends Controller
{
    protected $model;
    protected $modelUserEmpresas;
    protected $modelFuncionario;

    /**
     * @author Welliton Cunha
     * @since 06.07.2021 às 17:56
     *
     * @see Cadastro e atualização de [users] e tabela pivô [usuario_empresas]
     */

    public function __construct(User $usuario, UsuarioEmpresa $usuario_empresas, Funcionario $funcionario)
    {
        /** Construtor da tabela de [users] e [usuario_empresas] */
        $this->model = $usuario;
        $this->modelUserEmpresas = $usuario_empresas;
        $this->modelFuncionario = $funcionario;
    }

    public function index(){
        $usuarios = $this->model::all();
        return $this->sendResponse($usuarios->map(function ($item){
            return ['id' => $item->id,
                    'name' => $item->name,
                    'email' => $item->email,
                    'cpf' => $item->cpf,
                    'url_perfil' => $item->url_perfil,
                    'perfil' => $item->usuario_empresas->map(function ($item){
                                    return $item->perfilUsuario?->descricao;
                                })];
                }));
    }

    public function store(Request $request){
        try {
            /**
             * Estou armazenando em tempo de execução o [users] e tabela pivô [usuario_empresas]
             */

            $cpf = $this->model->where('cpf',$request->cpf)->first();
            $email = $this->model->where('email',$request->email)->first();
            $users = $request->all('name','email','cpf','password','url_perfil','usuario','senha','ramal');
            $usuario_empresas = $request->all('status','empresa_id');

            $users['password'] = bcrypt($request['password']);
            if ((!$cpf) && (!$email))
            {
                $id = $this->model->create($users)->id;
                $usuario_empresas['user_id'] = $id;
                $this->modelUserEmpresas->create($usuario_empresas);
                return $this->sendResponse($this->SUCESSO);
            }else{
                return $this->sendError('CPF ou e-mail ja cadastrado!');
            }
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }
    }

    public function update(Request $request, $id){
        /**
         * Especificando apenas os campos que iremos atualizar
         */
        $users = $request->all('name','email','cpf','password','url_perfil');
        $usuario_empresas = $request->all('status','empresa_id');

        $usuario = $this->model::find($id);
        $idImpresas = DB::table('usuario_empresas')
        ->select(DB::raw('id'))
        ->where('user_id', '=', $id)->get();
        foreach($idImpresas as $ids){
            $usuarioEmpresas = $this->modelUserEmpresas::find($ids->id);
        }

        if (!$usuario){
            return $this->sendError($this->NAO_LOCALIZADO);
        }

        try {
            $users['password'] = bcrypt($request['password']);
            $usuario->update($users);
            $usuarioEmpresas->update($usuario_empresas);
            return $this->sendResponse($this->SUCESSO);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }
    }

    public function destroy($id){
        $usuario_empresas = $this->modelUserEmpresas->where('user_id', $id);
        $users = $this->model->where('id', $id);
        if((!$usuario_empresas)|(!$users)){
            return $this->sendError($this->NAO_LOCALIZADO);
        }
        try {
            $usuario_empresas->delete();
            $users->delete();
            return $this->sendResponse($this->SUCESSO);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }
    }

    public function create(){
        return $this->sendResponse($this->model->all());
    }

    //todo: Precisamos refatorar Edt do Usuario, codigo pode ser melhorado.
    //Nonilton Alves 09/12/2021 09:00
    public function edit($id){
        $user = $this->model->find($id);
        $idImpresas = DB::table('usuario_empresas')
        ->select(DB::raw('id'))
        ->where('user_id', '=', $id)->get();
        foreach($idImpresas as $ids){
            $usuario_empresas = $this->modelUserEmpresas->find($ids->id);
        }
        $usuario_empresas->empresa;
        if((!$usuario_empresas)|(!$user)){
            return $this->sendError($this->NAO_LOCALIZADO);
        }
        return $this->sendResponse(['users' => $user,'usuario_empresas' => $user->usuario_empresas]);
    }

    public function uploadFoto(Request $request)
    {
        try {
            if ($request->hasFile('image')) {
                $destinationPath = 'users/perfil/';

                $image = $request->file('image');

                $ext = $image->guessClientExtension();
                $image->move($destinationPath, auth()->user()->usuario_empresas[0]->id.'.'.$ext);
                $user = $this->model::find(auth()->user()->id);
                $user->update(['url_perfil'=> $destinationPath . auth()->user()->usuario_empresas[0]->id.'.'.$ext]);
                return $user;
            }
        } catch (\Throwable $th) {
            return $this->sendError($th->getMessage());
        }
    }

    public function exibirFoto($id){
        try {
            $user = $this->model->find($id);
            return $this->sendResponse(['image' => url( $user->url_perfil) ]);
        } catch (\Throwable $th) {
            return $this->sendError($this->NAO_LOCALIZADO);
        }

    }

    public function atacharPerfilUsuario(Request $request){
        $dados = $request->all();
        $user = User::find($dados['usuario']);
        if(!$user){
            return $this->sendResponse('Usuário não encontrado');
        }
        $perfil = PerfilUsuario::find($dados['perfil']);
        if(!$perfil){
            return $this->sendResponse('Perfil do usuário não encontrado');
        }

        try {
            $userEmpresa = $user->usuario_empresas[0];
            $userEmpresa->perfil_usuario_id = $perfil->id;
            $userEmpresa->save();
            return $this->sendResponse(true);
        } catch (\Throwable $th) {
            return $this->sendError($th->getMessage());
        }

    }

    public function detachePerfilUsuario(Request $request){
        $dados = $request->all();
        $user = User::find($dados['usuario']);
        if(!$user){
            return $this->sendResponse('Usuário não encontrado');
        }

        try {
            $userEmpresa = $user->usuario_empresas[0];
            $userEmpresa->perfil_usuario_id = null;
            $userEmpresa->save();
            return $this->sendResponse(true);
        } catch (\Throwable $th) {
            return $this->sendError($th->getMessage());
        }
    }

}

