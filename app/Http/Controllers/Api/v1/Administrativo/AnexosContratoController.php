<?php

namespace App\Http\Controllers\Api\v1\Administrativo;

use App\Http\Controllers\Controller;
use App\Repository\Administrativo\AnexosContratoRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class AnexosContratoController extends Controller
{
    public function __construct(AnexosContratoRepository $contratos)
    {
        $this->repository = $contratos;
    }

    public function index($id = null){
         $contratos = $this->repository->where('contratos_id','=',$id);
         return $this->sendResponse($contratos);
    }

    public function store(Request $request){
        $dados = $request->all();
        try {
            if($request->has('arquivo')){
                $dados['path'] = upload($request, 'arquivo', $dados['tipo'], 'anexos_contrato');
            }
            $this->repository->create($dados);
            return $this->sendResponse($this->SUCESSO);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }
    }

    public function destroy($id){
        try {
            $dados = $this->repository->find($id);
            Storage::delete("\public\anexos_contrato\{$dados->path}");
            $dados->delete();
            return $this->sendResponse($this->SUCESSO);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }
    }
}
