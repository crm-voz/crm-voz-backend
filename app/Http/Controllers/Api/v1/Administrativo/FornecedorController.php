<?php

namespace App\Http\Controllers\Api\v1\Administrativo;

use App\Http\Controllers\Controller;
use App\Http\Requests\FornecedorRequest;
use App\Repository\Administrativo\FornecedorRepository;

class FornecedorController extends Controller
{

    public function __construct(FornecedorRepository $fornecedor)
    {
        $this->repository = $fornecedor;
    }

    public function index(){
        $fornecedores = $this->repository->all();
         return $this->sendResponse($fornecedores);
    }

    public function store(FornecedorRequest $request){
        $dados = $request->all();
        try {
            $this->repository->create($dados);
            return $this->sendResponse($this->SUCESSO);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }
    }

    public function update(FornecedorRequest $request, $id){
        $dados = $request->all();

        $fornecedor = $this->repository->find($id);
        if(!$fornecedor){
            return $this->sendError($this->NAO_LOCALIZADO);
        }

        try {

            $fornecedor->update($dados);
            return $this->sendResponse($this->SUCESSO);
        } catch (\Exception $e) {
            return $this->sendError($this->ERRO);
        }

    }

    public function destroy($id){
        try {
            $dados = $this->repository->find($id);
            $dados->delete();
            return $this->sendResponse($this->SUCESSO);
        } catch (\Exception $e) {
            return $this->sendError($this->ERRO);
        }
    }

    public function edit($id){
        $fornecedor = $this->repository->find($id);
        if(!$fornecedor){
            return $this->sendResponse($this->NAO_LOCALIZADO);
        }
        $fornecedor->cidade;

        return $this->sendResponse(['fornecedores' => $fornecedor]);
    }
}
