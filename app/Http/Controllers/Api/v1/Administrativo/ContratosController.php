<?php

namespace App\Http\Controllers\Api\v1\Administrativo;

use App\Http\Controllers\Controller;
use App\Http\Requests\ContratosRequest;
use App\Models\Administrativo\Contratos;
use App\Repository\Administrativo\ContratosRepository;
use App\Repository\Administrativo\FornecedorRepository;

class ContratosController extends Controller
{

    public function __construct(ContratosRepository $contratos,
                                FornecedorRepository $fornecedorRepository)
    {
        $this->repository = $contratos;
        $this->fornecedorRepository = $fornecedorRepository;
    }

    public function index($idfornecedor = null){
        if ($idfornecedor)
           $contratos = $this->fornecedorRepository->contratos($idfornecedor);
        else
           $contratos = $this->repository->all();

         return $this->sendResponse($contratos);
    }

    public function store(ContratosRequest $request){
        $dados = $request->all();
        try {
            $this->repository->create($dados);
            return $this->sendResponse($this->SUCESSO);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }
    }

    public function update(ContratosRequest $request, $id){
        $dados = $request->all();
        $contratos = $this->repository->find($id);
        if(!$contratos){
            return $this->sendError($this->NAO_LOCALIZADO);
        }
        try {
            $contratos->update($dados);
            return $this->sendResponse($this->SUCESSO);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }
    }

    public function destroy($id){
        try {
            $dados = $this->repository->find($id);
            $dados->delete();
            return $this->sendResponse($this->SUCESSO);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }
    }

    public function edit($id){
        $contratos = $this->repository->find($id);
        if(!$contratos){
            return $this->sendResponse($this->NAO_LOCALIZADO);
        }
        $contratos->fornecedor;
        return $this->sendResponse(['contratos' => $contratos]);
    }

    public function contrato_fornecedor($id){
        $contratos = Contratos::where('fornecedor_id',$id)->get();

        if(!$contratos){
            return $this->sendResponse($this->NAO_LOCALIZADO);
        }
        return $this->sendResponse(['contratos' => $contratos]);
    }
}
