<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Models\BackOffice\Operacao;
use App\Repository\Backoffice\OperacaoRepository;

class OperacoesController extends Controller
{
    protected $operacaoRepository, $operacao;

    public function __construct(OperacaoRepository $operacaoRepository, Operacao $operacao)
    {
        $this->operacaoRepository = $operacaoRepository;
        $this->operacao = $operacao;
    }

    public function get($idCredor){
        return $this->sendResponse($this->operacaoRepository->get($idCredor));
    }

}

