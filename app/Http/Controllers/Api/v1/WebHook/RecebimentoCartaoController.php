<?php

namespace App\Http\Controllers\Api\v1\WebHook;

use App\Http\Controllers\Controller;
use App\Repository\Cobranca\AcordoRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RecebimentoCartaoController extends Controller
{
    public function __construct(AcordoRepository $acordoRepository)
    {
        $this->acordoRepository = $acordoRepository;
    }

    /*
    * $hashId -> hash do acordo gerado no crm
    */
    public function recebimento(Request $request, $hashId){
        $dados = $request->all();
        foreach($dados['items'] as $item){
            $acordo = $this->acordoRepository->decodeHash($hashId);
            if(!$acordo){
                return $this->sendError($this->NAO_LOCALIZADO);
            }

            try {
                DB::beginTransaction();
                $acordo->transacao_cartao = $dados;
                $acordo->save();
                foreach($acordo->parcelas as $index => $parcela){
                    $parcela->transacao_parcela = $item['financial_return_dates'][$index];
                    $parcela->save();
                }

                DB::commit();
                return $this->sendResponse([true]);
            } catch (\Exception $e) {
                DB::rollback();
                return $this->sendError($e->getMessage());
            }
        }
    }
}
