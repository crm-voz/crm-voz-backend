<?php

namespace App\Http\Controllers\Api\Auth\Services;

use App\Models\Core\ExecutivosCarteira;
use App\Models\Core\Funcionario;
use App\Repository\Cobranca\CarteiraRepository;

class UserServices
{
    public function __construct(CarteiraRepository $carteiraRepository)
    {
        $this->carteiraRepository = $carteiraRepository;
    }

    public function metaExecutivo($id){
        $executivo = ExecutivosCarteira::where('executivo_cobranca_id',$id)->orderBy('id','desc')->first();
        return $executivo ? $executivo->meta : 0;
    }

    public function dadosUser(){
        $funcionario = Funcionario::where('cpf', 'like', auth()->user()->cpf)->with('executivo_cobranca')->first();
        return  [
            "id" => auth()->user()->id,
            "name" => auth()->user()->name,
            "email" => auth()->user()->email,
            "cpf" => auth()->user()->cpf,
            "funcao" => $funcionario?->funcao->nome,
            'funcinarioId' => $funcionario?->id,
            "executivo_cobranca" => isset($funcionario->executivo_cobranca->id) ? $funcionario->executivo_cobranca->id : false,
            "meta" => isset($funcionario->executivo_cobranca->id) ? $this->metaExecutivo($funcionario->executivo_cobranca->id) : 0,
            "usuario_empresas" => auth()->user()->usuario_empresas[0]->id,
        ];
    }
}
