<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Api\Auth\Services\UserServices;
use App\Http\Controllers\Controller;
use App\Http\Requests\AuthRequest;
use App\Models\BackOffice\Credor;
use App\Models\Core\Funcionario;
use Illuminate\Foundation\Auth\AuthenticatesUsers;


class AuthController extends Controller
{

    use AuthenticatesUsers;

    /**
     * @author Welliton Cunha
     * @since 05.07.2021 às 11:49
     *
     * @version 2.0
     * @see Procedimento alterado para fazer login pelo CPF.
     *
     * @see Refatorado por Nonilton Alves
     *
     * */
    public function login(AuthRequest $request){
      $credentials = $request->only('cpf','password');
      if(auth()->attempt($credentials)){
        $user = app(UserServices::class)->dadosUser();
        $credor = Credor::where('id', auth()->user()->credor_id)->first();
        if ($credor){
            $credor = [
                "id" => $credor->id,
                "cnpj" => $credor->cnpj,
                "nome" => $credor->nome,
                "razao" => $credor->razao_social
            ];
        }

        $permissionsGroup = collect();
        if(auth()->user()->usuario_empresas[0]->perfilUsuario){
            foreach(auth()->user()->usuario_empresas[0]->perfilUsuario->permissoesPerfil as $permissao){
                $permissionsGroup->add((object)[
                    'id' => $permissao->id,
                    'grupo' => $permissao->endpoints->grupo,
                    'nome' => $permissao->endpoints->grupo . '.'. $permissao->endpoints->nome,
                    'url' => env('URL_ENDPOINT') . $permissao->endpoints->url,
                    'permissao' => $permissao->modificar ? true : false,
                ]);
            }
        }

        $accessToken = auth()->user()->createToken('authToken')->accessToken;
        return response(['user' => $user,
                         'access_token' => $accessToken,
                         'credor' => $credor,
                         'acesso_id' => null,
                         'permissoes' => $permissionsGroup]);
      }else{
        return $this->sendError('Invalid Credentials', [], 400);
      }
    }
}
