<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required',
            'password' => 'required|min:9|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-_]).{6,}$/',
            'confirmar_senha' => 'required|same:password',
        ];
    }

    public function messages()
    {
        return [
            'required' => 'O campo :attribute é obrigatório.',
            'same' => 'O campo :attribute e senha não são iguais.',
            'min' => 'O campo :attribute mínimo 9 caractere.',
            'regex' => 'O campo :attribute tem que ter 1 letra maiúscula, 1 minúscula, 1 numero e 1 caractere especial.',
        ];
    }
}
