<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RemessaFileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'file' => 'required|mimes:xlx,xls,xlsx',
        ];
    }

    public function messages()
    {
        return [
            'required' => 'Certifique-se que o arquivo foi enviado, e com os formatos xlx, xls ou xlsx',
        ];
    }
}
