<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FornecedorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome_fantasia' => 'required|min:5',
            'razao_social' => 'required|min:5',
        ];

    }

    public function messages(){
        return [
            'required' => 'O campo :attribute é obrigatório.',
            'required.max' => 'O campo :attribute precisa de pelo menos 5 caracteres'
        ];
    }
}
