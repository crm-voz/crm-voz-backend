<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClientesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome' => 'required|min:3|max:100',
            'cpf_cnpj' => 'required|max:20',
            'endereco' => 'required|min:3|max:100',
            'cep' => 'required|min:8|max:10'
        ];
    }

    public function messages(){
        return [
            'required' => 'O campo :attribute é obrigatório.',
            'required.min' => 'O campo :attribute tamanho minimo é inválido.',
            'required.max' => 'O campo :attribute tamanho máximo é inválido.'
        ];
    }
}
