<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GenericRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome' => 'required|min:5',
        ];
    }

    public function messages()
    {
        return [
            'required' => 'O :attribute não pode ser vazio.',
            'min' => 'O :attribute não pode ter menos que 5 caracteres.',
        ];
    }
}
