<?php
namespace App\Gateway\Dados;

interface IEnriquecimento {
    public function pesquisarPorCpf(array $cpfs);
    public function pesquisarPorNome(array $nomes);
    public function pesquisarPorTelefone(array $telefones);
    public function pesquisarPorEmail(array $emails);
}
