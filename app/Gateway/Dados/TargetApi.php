<?php

namespace App\Gateway\Dados;

use Illuminate\Support\Facades\Http;

class TargetApi implements IEnriquecimento
{
    protected $urlBase = 'https://api.targetdata-smart.com/api';

    public function __construct()
    {
        $response = json_decode( Http::post('https://api.targetdata-smart.com/api/token',[
            'grant_type' => 'password',
            'client_id' => 2,
            'client_secret' => 'j3vEKMI37i75gFC7LabWIfgJiUmZBj1H7gRCI0En',
            'username' => 'api-ti',
            'password' => 'MvQrWFvvFa',
            'empresa' => 1465
        ]) );

        $this->token = $response->access_token;
    }

    public function pesquisarPorCpf(array $cpfs)
    {
        return json_decode( Http::withToken($this->token)
                                 ->post($this->urlBase .'/PF/CPF',$cpfs), true);
    }

    public function pesquisarPorNome(array $nomes)
    {
        return  json_decode( Http::withToken($this->token)
                                 ->post($this->urlBase .'/PF/NOME',$nomes));
    }

    public function pesquisarPorTelefone(array $telefones)
    {
        return  json_decode( Http::withToken($this->token)
                                 ->post($this->urlBase .'/PF/TELEFONE',$telefones));
    }

    public function pesquisarPorEmail(array $emails)
    {
        return  json_decode( Http::withToken($this->token)
                                 ->post($this->urlBase .'/PF/EMAIL',$emails));
    }

}
