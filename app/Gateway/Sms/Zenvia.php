<?php

namespace App\Gateway\Sms;

use Illuminate\Support\Facades\Http;

class Zenvia implements iSms {
    protected $usuario;
    protected $chave;
    protected $endpoint;

    public function __construct()
    {
        $this->usuario = env('ZENVIA_USUARIO', 'grupo.voz.corp');
        $this->chave = env('ZENVIA_CHAVE', 'if8LznpC0L');
        $this->ep_single = env('ZENVIA_ENDPOINT_SINGLE', 'https://api-rest.zenvia.com/services/send-sms');
        $this->ep_bulk = env('ZENVIA_ENDPOINT_BULK', 'https://api-rest.zenvia.com/services/send-sms-multiple');
    }

    /**
     * Array para mensagem única:
     * [
     *      "celular" => "99999999999",
     *      "mensagem" => "Teste da API single - Zenvia"
     * ]
     *
     * Array para multiplas mensagens:
     * [
     *       [
     *           "celular" => "99999999999",
     *           "mensagem" => "Teste da API bulk - Zenvia"
     *       ],
     *       [
     *           "celular" => "99999999999",
     *           "mensagem" => "Teste da API bulk - v"
     *       ]
     *   ]
    */
    public function enviar_sms($json)
    {
        if( array_key_exists('celular', $json) )
        {
            $single = true;
            $json['to'] = $json['celular'];
            $json['msg'] = $json['mensagem'];
            unset($json['celular']);
            unset($json['mensagem']);

            $json = ['sendSmsRequest' => $json];
        } else {
            $single = false;
            foreach($json as $k => $v){
                $json[$k]['to'] = $json[$k]['celular'];
                $json[$k]['msg'] = $json[$k]['mensagem'];
                unset($json[$k]['celular']);
                unset($json[$k]['mensagem']);
            }
            $json = array_values($json);
            $json = ['sendSmsMultiRequest' => ['sendSmsRequestList' => $json]];
        }

        return $this->request_api($json, $single);
    }

    public function set_credenciais($usuario, $chave)
    {
        $this->usuario = $usuario;
        $this->chave = $chave;
    }

    protected function request_api($dados, $single)
    {
        return Http::withBasicAuth($this->usuario, $this->chave)
                    ->post(( $single ) ? $this->ep_single : $this->ep_bulk, $dados);
    }
}
