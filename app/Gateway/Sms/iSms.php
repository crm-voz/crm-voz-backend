<?php

namespace App\Gateway\Sms;

interface iSms {
    /**
     * Array para mensagem única:
     * [
     *      "celular" => "99999999999",
     *      "mensagem" => "Teste da API single - BestVoice"
     * ]
     *
     * Array para multiplas mensagens:
     * [
     *       [
     *           "celular" => "99999999999",
     *           "mensagem" => "Teste da API bulk - BestVoice"
     *       ],
     *       [
     *           "celular" => "99999999999",
     *           "mensagem" => "Teste da API bulk - BestVoice"
     *       ]
     *   ]
    */
    public function enviar_sms($json);
    public function set_credenciais($usuario, $chave);
}
