<?php

namespace App\Gateway\Sms;

use Illuminate\Support\Facades\Http;

class BestVoice implements iSms {
    protected $usuario;
    protected $chave;
    protected $endpoint;

    public function __construct()
    {
        $this->usuario = env('BESTVOICE_USUARIO', 'MOTAESILVA');
        $this->chave = env('BESTVOICE_CHAVE', 'B3stV0z84');
        $this->ep_single = env('BESTVOICE_ENDPOINT_SINGLE', 'http://apishort.bestvoice.com.br/bot/single-sms.php');
        $this->ep_bulk = env('BESTVOICE_ENDPOINT_BULK', 'http://apishort.bestvoice.com.br/bot/bulk-sms.php');
    }

    /**
     * Array para mensagem única:
     * [
     *      "celular" => "99999999999",
     *      "mensagem" => "Teste da API single - BestVoice"
     * ]
     *
     * Array para multiplas mensagens:
     * [
     *       [
     *           "celular" => "99999999999",
     *           "mensagem" => "Teste da API bulk - BestVoice"
     *       ],
     *       [
     *           "celular" => "99999999999",
     *           "mensagem" => "Teste da API bulk - BestVoice"
     *       ]
     *   ]
    */
    public function enviar_sms($json)
    {
        $json = ( array_key_exists('celular', $json) ) ? $json : ['bulk' => $json];
        return $this->request_api($json, array_key_exists('celular', $json));
    }

    public function set_credenciais($usuario, $chave)
    {
        $this->usuario = $usuario;
        $this->chave = $chave;
    }

    protected function request_api($dados, $single)
    {
        return Http::withHeaders([
            'usuario' => $this->usuario,
            'chave' => $this->chave
        ])->post(( $single ) ? $this->ep_single : $this->ep_bulk, $dados);
    }
}
