<?php

namespace App\Gateway\Email;

use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Mail;
use App\Mail\GenericMail;

class Avulso implements iEmail{
    protected $chave;
    protected $endpoint;

    /**
     * Array:
     * [
     *  "contatos" => [
     *      [
     *          "email" => "email@email.com",
     *          "nome" => "Nome",
     *      ],
     *      [
     *          "email" => "email2@email.com",
     *          "nome" => "Nome2",
     *      ]
     *  ],
     * "parametros" => [
     *      "mensagem" => "A mensagem a ser enviada"
     *  ],
     * ]
     */
    public function enviar_email($json, $anexo=null)
    {
        try {
            $res = [];
            foreach($json['contatos'] as $contato)
            {
                $res[] = $this->enviar($contato, ['assunto' => $contato['nome'], 'conteudo' => $json['parametros']['mensagem']], $anexo);
            }

            return $res;
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function set_credenciais($chave)
    {
        return $this;
    }

    private function enviar($destinatario, $dados, $anexo)
    {
        return Mail::to($destinatario['email'], $destinatario['nome'])
                    ->send(new GenericMail($dados, $anexo));
    }
}
