<?php

namespace App\Gateway\Email;

interface iEmail {
    public function enviar_email($json);
    public function set_credenciais($chave);
}
