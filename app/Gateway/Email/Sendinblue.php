<?php

namespace App\Gateway\Email;

use Illuminate\Support\Facades\Http;

class Sendinblue implements iEmail{
    protected $chave;
    protected $endpoint;

    public function __construct()
    {
        $this->chave = env('SENDINBLUE_CHAVE', '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');
        $this->endpoint = env('SENDINBLUE_ENDPOINT', 'https://api.sendinblue.com/v3/smtp/email');
    }

    /**
     * Array:
     * [
     *  "contatos" => [
     *      [
     *          "email" => "email@email.com",
     *          "nome" => "Nome",
     *      ],
     *      [
     *          "email" => "email2@email.com",
     *          "nome" => "Nome2",
     *      ]
     *  ],
     * "parametros" => [
     *      "credor" => "Credor"
     *  ],
     *  "template" => 1,
     * ]
     */
    public function enviar_email($json)
    {
        $to_arr = [];
        $params_arr = [];

        foreach( $json['contatos'] as $contato ){
            $to_arr[] = $contato;
        }

        $arr_tosend = [
            'to' => $to_arr,
            'templateId' => $json['template'],
            'params' => $json['parametros']
        ];

        // return $arr_tosend;
        return $this->request_api($arr_tosend);
    }

    public function set_credenciais($chave)
    {
        $this->chave = $chave;
    }

    protected function request_api($dados)
    {
        return Http::withHeaders([
            'api-key' => $this->chave,
            'X-Mailin-custom' => "custom_header_1:custom_value_1|custom_header_2:custom_value_2|custom_header_3:custom_value_3",
            'charset' => "iso-8859-1"
        ])->post($this->endpoint, $dados);
    }
}
