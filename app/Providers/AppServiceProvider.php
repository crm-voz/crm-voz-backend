<?php

namespace App\Providers;

use App\Models\BackOffice\Remessa;
use App\Models\Core\Eventos;
use App\Observers\EventoObserver;
use App\Observers\RemessaObserver;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Eventos::observe(EventoObserver::class);
        Remessa::observe(RemessaObserver::class);
    }
}
