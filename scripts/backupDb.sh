#!/bin/bash
#!/bin/env python
# Location where you want to keep your db dump
backup_folder_path=~/db_backups
export PATH="{/usr/bin/python3}:$PATH"


# File name i.e: dump-2020-06-24.sql
file_name="dump-"`date "+%Y-%m-%d"`""


# ensure the location exists
mkdir -p ${backup_folder_path}


#change database name, username and docker container name
dbname=CRM
username=tivoz
container=backend_db_1

backup_file=${backup_folder_path}/${file_name}

docker exec ${container} pg_dump -U ${username} --role "tivoz" --format=t --blobs --encoding "UTF8" -d ${dbname} > ${backup_file}
/usr/bin/python3 sendBackup.py
