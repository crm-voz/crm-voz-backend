import os
import sys
from datetime import date

fileName = f'dump-{str(date.today())}'

commandCd = f'cd ~/db_backups'
commandPut = f'cd "Backup DB CRM" ; cd Backup-CRM-PostgreSQL ; put {fileName}'
commandConn = f"smbclient -A ~/shared/smbclient.conf //192.168.254.201/h$ -c '{commandPut}'"
commandComplete = f'{commandCd} ; {commandConn}'

os.system(commandComplete)
