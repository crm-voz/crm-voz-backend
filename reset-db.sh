#!/bin/bash

docker exec -it backend_php_1 sh -c "cd /webapp ; php artisan migrate:fresh  ; php artisan migrate ; php artisan db:seed ; php artisan passport:install"
