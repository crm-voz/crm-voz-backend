<?php

use App\Http\Controllers\Api\Auth\AuthController;
use App\Repository\Backoffice\EnvioAutomaticoSmsRepository;
use App\Http\Controllers\Api\v1\{
    DashboardController,
    Backoffice\EmailClienteController,
    Backoffice\OperacaoController,
    Backoffice\RemessaController,
    Backoffice\ClientesController,
    Backoffice\CredoresController,
    Backoffice\CredorContatosController,
    Backoffice\DevolucaoController,
    Backoffice\TelefonesClienteController,
    Backoffice\CampanhasController,
    Backoffice\DescontosController,
    Backoffice\LoteOperacoesController,
    Backoffice\SmsController,
    Backoffice\EmailController,
    Backoffice\InsucessosController,
    Administrador\CidadesController,
    Administrador\StatusLocalizacaoController,
    Administrador\FuncaoController,
    Administrador\EmpresasController,
    Administrador\FuncionariosController,
    Administrador\MotivoEventoController,
    Administrador\MotivoDevolucaoController,
    Administrador\ClassificacaoNegativacaoController,
    Administrador\MotivoPendenciaController,
    Administrador\GrupoAcessoController,
    Administrador\EndpointsController,
    Administrador\EventosController,
    Administrador\BoletosController,
    Administrador\ClassificacaoClienteController,
    Administrador\ParametroBoletoController,
    Administrador\TemplateSmsController,
    Administrador\TemplateEmailController,
    Administrador\BaixarBoletoController,
    Administrador\PerfilUsuarioController,
    Administrativo\FornecedorController,
    Administrativo\ContratosController,
    Administrativo\AnexosContratoController,
    Cobranca\AcordoController,
    Cobranca\ParcelasController,
    Cobranca\ClienteController,
    Financeiro\RepassesController,
    User\UserController,
    Info\InfoStatusOperacaoController,
    Info\InfoClassificacaoClienteController,
    Info\InfoClassificacaoNegativacaoController,
    Info\InfoMotivoPendenciaController,
    OperacoesController,
    WebHook\RecebimentoCartaoController
};
use App\Http\Controllers\Api\v1\Administrador\LogPendenciasController;
use App\Http\Controllers\Api\v1\Administrador\SnapshotController;
use App\Http\Controllers\Api\v1\Cobranca\CarteiraController;
use App\Http\Controllers\Api\v1\Cobranca\ExecutivoCobrancaController;
use App\Http\Controllers\Api\v1\Cobranca\OperacoesParcelasController;
use App\Http\Controllers\Api\v1\Cobranca\PropostaController;
use App\Http\Controllers\Api\v1\Dashboard\CredorController;
use Illuminate\Support\Facades\Route;
use App\Repository\Administrador\PerfilRepository;



/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('/login', [AuthController::class, 'login'])->name('api.login');

Route::middleware(['auth:api'])->group(function (){
    Route::prefix('v1')->group(function (){
        Route::prefix('/webhook')->group(function(){
            Route::prefix('cartao')->group(function(){
                Route::post('/recebimento/{id}', [RecebimentoCartaoController::class, 'recebimento'])->name('api.webhook.cartao.recebimento');
            });
        });

        Route::prefix('/executivo/acordos')->group(function (){
            Route::get('/lista/{id}',[ExecutivoCobrancaController::class, 'acordosExecutivo'])->name('api.executivos.acordos.lista');
            Route::get('/mes/{id}',[ExecutivoCobrancaController::class, 'acordosDoMes'])->name('api.executivos.acordos.mes');
            Route::get('/listar/parcelas/status/{executivId}/{status}',[ExecutivoCobrancaController::class, 'listaParcelasStatus'])->name('api.executivos.listar.parcelas.status.executivo');
        });

        Route::prefix('/executivo')->group(function (){
            Route::get('/',         [ExecutivoCobrancaController::class, 'index'])->name('api.executivos.index');
            Route::get('/findId/{id}',[ExecutivoCobrancaController::class, 'getExecutivo'])->name('api.executivos.findId');
            Route::get('/edit/{id}',[ExecutivoCobrancaController::class, 'edit'])->name('api.executivos.edit');
            Route::put('/update/{id}',[ExecutivoCobrancaController::class, 'update'])->name('api.executivos.update');
            Route::delete('/delete/{id}',[ExecutivoCobrancaController::class, 'delete'])->name('api.executivos.delete');
            Route::post('/store',[ExecutivoCobrancaController::class, 'store'])->name('api.executivos.store');
            Route::post('/rank',    [ExecutivoCobrancaController::class, 'rankExecutivo'])->name('api.executivos.rank');
            Route::post('/trocarExecutivo/{id}',[ExecutivoCobrancaController::class, 'trocarExecutivo'])->name('api.executivos.trocarExecutivo');

        });

        Route::prefix('snaphsot')->group(function (){
            Route::get('/listagem',[SnapshotController::class, 'index'])->name('api.snapshot.lista');
            Route::get('/novo',[SnapshotController::class, 'gerar'])->name('api.snapshot.novo');
        });

        Route::prefix('info')->group(function () {
            Route::get('/statusoperacao',               [InfoStatusOperacaoController::class, 'get'])->name('api.info.statusoperacao');
            Route::get('/classificacaonegativacao',     [InfoClassificacaoNegativacaoController::class, 'get'])->name('api.info.negativacao');
            Route::get('/classificacaocliente',         [InfoClassificacaoClienteController::class, 'get'])->name('api.info.cliente');
            Route::get('/motivopendencia',              [InfoMotivoPendenciaController::class, 'get'])->name('api.info.motivopendencia');
        });

        Route::prefix('dashboard')->group(function (){
            Route::get('/', [DashboardController::class, 'index'])->name('api.dashboard');
            Route::prefix('credor')->group(function(){
                Route::get('/totalizadores', [CredorController::class,'totalizar'])->name('api.dashboard.credor.totalizadores');
                Route::get('/acordos/listar',[CredorController::class,'listarAcordos'])->name('api.dashboard.credor.acordos.listar');
                Route::get('/campanhas',     [CredorController::class,'campanhasCredor'])->name('api.dashboard.credor.campanhas');
            });
        });

        Route::prefix('clientes')->group(function (){
            Route::get('/',                 [ClientesController::class, 'index'])->name('api.admin.clientes.index');
            Route::get('/credor/chart/totalClientesCredor',[ClientesController::class, 'totalClientesCredor'])->name('api.admin.clientes.totalClientesCredor');
            Route::get('/eventos/{id}',     [ClientesController::class, 'eventosCliente'])->name('api.admin.clientes.eventos');
            Route::get('/event/{id}',       [ClientesController::class, 'eventosClient'])->name('api.admin.clientes.event');
            Route::get('/filtro',           [ClientesController::class, 'filtro'])->name('api.admin.clientes.filtro');
            Route::post('/filtro_geral',    [ClientesController::class, 'filtroGeral'])->name('api.admin.clientes.filtro_geral');
            Route::post('/filtro_full',     [ClientesController::class, 'filtroFull'])->name('api.admin.clientes.filtro_full');
            Route::get('/created',           [ClientesController::class, 'created'])->name('api.admin.clientes.created');
            Route::get('/create',           [ClientesController::class, 'create'])->name('api.admin.clientes.create');
            Route::get('/edit/{id}',        [ClientesController::class, 'edit'])->name('api.admin.clientes.edit');
            Route::get('/operacoes/{id}',   [ClientesController::class, 'operacoes'])->name('api.admin.clientes.operacoes');
            Route::get('/operacoesacordo/{id}',[ClientesController::class, 'operacoesAcordo'])->name('api.admin.clientes.operacoes_acordo');
            Route::get('/cliente_localizacao_chart',[ClientesController::class, 'clienteLocalizacaoChart'])->name('api.admin.clientes.cliente_localizacao_chart');
            Route::get('/status_cliente_chart',[ClientesController::class, 'statusCliente'])->name('api.admin.clientes.status_localizacao');
            Route::get('/total_cliente_periodo_chart',[ClientesController::class, 'totalClientePeriodo'])->name('api.admin.clientes.total_cliente_periodo');
            Route::post('/update_data_cadastro_confirmado/{id}',        [ClientesController::class, 'data_cadastro_confirmado'])->name('api.admin.clientes.update_data_cadastro_confirmado');
            Route::post('/store',           [ClientesController::class, 'store'])->name('api.admin.clientes.store');
            Route::post('/enriquecer',      [ClientesController::class, 'enriquecerDadosCliente'])->name('api.admin.clientes.enriquecer');
            Route::put('/update/{id}',      [ClientesController::class, 'update'])->name('api.admin.clientes.update');
            Route::put('/updatelot',        [ClientesController::class, 'updateLote'])->name('api.admin.clientes.update_lot');
            Route::put('/update_lote_localizacao',        [ClientesController::class, 'updateLoteLocalizacao'])->name('api.admin.clientes.update_lote_localizacao');
            Route::put('/update_lote_status_cliente_mult',        [ClientesController::class, 'updateLoteStatusClienteMult'])->name('api.admin.clientes.update_lot_status_cliente_mult');
            Route::delete('/delete/{id}',   [ClientesController::class, 'destroy'])->name('api.admin.clientes.destroy');


            Route::prefix('emails')->group(function(){
                Route::get('/{id}',                 [EmailClienteController::class, 'index'])->name('api.admin.clientes.emails');
                Route::post('/store',               [EmailClienteController::class, 'store'])->name('api.admin.clientes.emails.store');
                Route::get('/edit/{id}',            [EmailClienteController::class, 'edit'])->name('api.admin.clientes.emails.edit');
                Route::get('/adicionar_blacklist/{id}',[EmailClienteController::class, 'adicionar_blacklist'])->name('api.admin.clientes.emails.adicionar_blacklist');
                Route::get('/retirar_blacklist/{id}',[EmailClienteController::class, 'retirar_blacklist'])->name('api.admin.clientes.emails.retirar_blacklist');
                Route::put('/update/{id}',          [EmailClienteController::class, 'update'])->name('api.admin.clientes.emails.update');
                Route::post('/enviar/{idCliente}',  [EmailClienteController::class, 'send'])->name('api.admin.clientes.email.enviar');
                Route::delete('/delete/{id}',       [EmailClienteController::class, 'delete'])->name('api.admin.clientes.emails.destroy');
            });

            Route::prefix('telefones')->group(function () {
                Route::get('/{id}',                         [TelefonesClienteController::class, 'index'])->name('api.admin.clientes.telefones');
                Route::get('/create/{id}',                  [TelefonesClienteController::class, 'create'])->name('api.admin.clientes.telefones.create');
                Route::get('/edit/{id}',                    [TelefonesClienteController::class, 'edit'])->name('api.admin.clientes.telefones.edit');
                Route::get('/adicionar_blacklist/{id}',     [TelefonesClienteController::class, 'adicionar_blacklist'])->name('api.admin.clientes.telefones.adicionar_blacklist');
                Route::get('/retirar_blacklist/{id}',       [TelefonesClienteController::class, 'retirar_blacklist'])->name('api.admin.clientes.telefones.retirar_blacklist');
                Route::put('/status/update/{telefone_id}',  [TelefonesClienteController::class, 'statusUpdate'])->name('api.admin.clientes.telefones.status.update');
                Route::put('/update/{id}',                  [TelefonesClienteController::class, 'update'])->name('api.admin.clientes.telefones.update');
                Route::post('/store',                       [TelefonesClienteController::class, 'store'])->name('api.admin.clientes.telefones.store');
                Route::delete('/delete/{id}',               [TelefonesClienteController::class, 'delete'])->name('api.admin.clientes.telefones.destroy');
            });
        });

        Route::prefix('credores')->group(function (){
            Route::get('/',                 [CredoresController::class, 'index'])->name('api.admin.credores.index');
            Route::get('/listar/{todos}',           [CredoresController::class, 'listar'])->name('api.admin.credores.listar');
            Route::get('/filtro',           [CredoresController::class, 'filtro'])->name('api.admin.credores.filtro');
            Route::post('/store',           [CredoresController::class, 'store'])->name('api.admin.credores.store');
            Route::post('/storeparametros/{id}', [CredoresController::class, 'store_parametros'])->name('api.admin.credores.store_parametros');
            Route::put('/update/{id}',      [CredoresController::class, 'update'])->name('api.admin.credores.update');
            Route::delete('/delete/{id}',   [CredoresController::class, 'destroy'])->name('api.admin.credores.destroy');
            Route::get('/create',           [CredoresController::class, 'create'])->name('api.admin.credores.create');
            Route::get('/edit/{id}',        [CredoresController::class, 'edit'])->name('api.admin.credores.edit');
            Route::get('/clientes/{id}',    [CredoresController::class, 'clientes'])->name('api.admin.credores.clientes');
            Route::get('/remessas/{id}',    [CredoresController::class, 'remessas'])->name('api.admin.credores.remessas');
            Route::get('/stats',            [CredoresController::class, 'gerar_estatisticas'])->name('api.admin.credores.stats');
        });

        Route::prefix('devolucao')->group(function(){
            Route::get('/',                 [DevolucaoController::class, 'index'])->name('api.admin.devolucao.index');
            Route::get('/create',           [DevolucaoController::class, 'create'])->name('api.admin.devolucao.create');
            Route::get('/detalhe/{id}',     [DevolucaoController::class, 'detalhar'])->name('api.admin.devolucao.detalhe');
            Route::post('/store',           [DevolucaoController::class, 'store'])->name('api.admin.devolucao.store');
            Route::post('/cancelar',        [DevolucaoController::class, 'cancelarDevolucaoOperacoes'])->name('api.admin.devolucao.cancelar');

        });

        Route::prefix('carteira')->group(function (){
            Route::get('/',               [CarteiraController::class, 'index'])->name('api.admin.carteira.index');
            Route::get('/get/{id}',       [CarteiraController::class, 'get'  ])->name('api.admin.carteira.get');
            Route::get('/create',         [CarteiraController::class, 'create'])->name('api.admin.carteira.create');
            Route::get('/edit/{id}',      [CarteiraController::class, 'edit'])->name('api.admin.carteira.edit');
            Route::post('/store',         [CarteiraController::class, 'store'])->name('api.admin.carteira.store');
            Route::put('/update/{id}',    [CarteiraController::class, 'update'])->name('api.admin.carteira.update');
            Route::delete('/delete/{id}', [CarteiraController::class, 'destroy'])->name('api.admin.carteira.destroy');
        });

        Route::prefix('operacoes')->group(function (){
            Route::get('/operacoesCredor',  [OperacaoController::class, 'operacoesCredor'])->name('api.admin.operacoes.operacoesCredor');
            Route::get('/credor/chart/totalOperacoesCredor',  [OperacaoController::class, 'totalOperacoesCredor'])->name('api.admin.operacoes.totalOperacoesCredor');
            Route::get('/remessasCredor',   [OperacaoController::class, 'remessasCredor'])->name('api.admin.operacoes.remessasCredor');
            Route::get('/campanhasCredor',  [OperacaoController::class, 'campanhasCredor'])->name('api.admin.operacoes.campanhasCredor');
            Route::get('/acordosCredor',    [OperacaoController::class, 'acordosCredor'])->name('api.admin.operacoes.acordosCredor');
            Route::get('/',                 [OperacaoController::class, 'index'])->name('api.admin.operacoes.index');
            Route::get('/create/{id?}',     [OperacaoController::class, 'create'])->name('api.admin.operacoes.create');
            Route::get('/edit/{id}',        [OperacaoController::class, 'edit'])->name('api.admin.operacoes.edit');
            Route::get('/operacoes_negativados_cliente/{id}', [OperacaoController::class, 'operacoes_negativados_cliente'])->name('api.admin.operacoes.operacoes_negativados_cliente');
            Route::get('/motivo_pendencia_chart', [OperacaoController::class, 'motivo_pendencia'])->name('api.admin.operacoes.motivo_pendencia');
            Route::get('/motivo_devolucao_chart', [OperacaoController::class, 'motivo_devolucao'])->name('api.admin.operacoes.motivo_devolucao');
            Route::get('/status_operacao_chart',  [OperacaoController::class, 'status_operacao'])->name('api.admin.operacoes.status_operacao');
            Route::get('/pendencia_chart',  [OperacaoController::class, 'pendencia'])->name('api.admin.operacoes.pendencia');
            Route::get('/valor_nominal_ano_vencimento_chart', [OperacaoController::class, 'valor_nominal_ano_vencimento'])->name('api.admin.operacoes.valor_nominal_ano_vencimento');
            Route::get('/titulo_aberto_periodo_chart', [OperacaoController::class, 'titulo_aberto_periodo'])->name('api.admin.operacoes.titulo_aberto_periodo');
            Route::get('/negativados',      [OperacaoController::class, 'negativados'])->name('api.admin.operacoes.negativados');
            Route::get('/pendencias',       [OperacaoController::class, 'listarOperacoesPendentes'])->name('api.admin.operacoes.listar.pendencias');
            Route::put('/update/lote/negativado/operacao',  [OperacaoController::class, 'update_lot_negativado_operacao'])->name('api.admin.operacoes.update_lot_negativado_operacao');
            Route::put('/update/{id}',      [OperacaoController::class, 'update'])->name('api.admin.operacoes.update');
            Route::put('/update/lote/motivo/pendencia',        [OperacaoController::class, 'update_lot_motivo_pendencia'])->name('api.admin.operacoes.update_lot_motivo_pendencia');
            Route::put('/update/lote/status/operacao',        [OperacaoController::class, 'update_lot_status_operacao'])->name('api.admin.operacoes.update.lote.status.operacao');
            Route::post('/store',           [OperacaoController::class, 'store'])->name('api.admin.operacoes.store');
            Route::post('/filtro',          [OperacaoController::class, 'filtro'])->name('api.admin.operacoes.filtro');
            Route::post('log/pendencias',   [LogPendenciasController::class, 'filtrar'])->name('api.admin.operacoes.log.filtrar');
            Route::delete('/delete/{id}',   [OperacaoController::class, 'destroy'])->name('api.admin.operacoes.destroy');
        });

        Route::prefix('log/pendencias')->group(function(){
            Route::post('store', [LogPendenciasController::class, 'store'])->name('api.admin.pendencias.log.store');
        });

        Route::prefix('remessas')->group(function(){
            Route::get('/',                 [RemessaController::class, 'index'])->name('api.admin.remessas.index');
            Route::get('/credor/chart/totalRemessasCredor', [RemessaController::class, 'totalRemessasCredor'])->name('api.admin.remessas.totalRemessasCredor');
            Route::get('/create',           [RemessaController::class, 'create'])->name('api.admin.remessas.create');
            Route::get('/edit/{id}',        [RemessaController::class, 'edit'])->name('api.admin.remessas.edit');
            Route::get('/total_remessas_periodo_chart',        [RemessaController::class, 'total_remessas_periodo'])->name('api.admin.remessas.total_remessas_periodo');
            Route::post('/store',           [RemessaController::class, 'store'])->name('api.admin.remessas.store');
            Route::post('/importar',        [RemessaController::class, 'importar'])->name('api.admin.remessas.importar');
            Route::post('/remessa_avulsa',  [RemessaController::class, 'remessa_avulsa'])->name('api.admin.remessas.remessa_avulsa');
            Route::put('/update/{id}',      [RemessaController::class, 'update'])->name('api.admin.remessas.update');
            Route::delete('/delete/{id}',   [RemessaController::class, 'destroy'])->name('api.admin.remessas.destroy');
            Route::delete('/cancelar/{id}', [RemessaController::class, 'cancelar'])->name('api.admin.remessas.cancelar');
        });

        Route::prefix('cidades')->group(function(){
            Route::get('/',                 [CidadesController::class, 'index'])->name('api.admin.cidades.index');
            Route::get('/create',           [CidadesController::class, 'create'])->name('api.admin.cidades.create');
            Route::get('/edit/{id}',        [CidadesController::class, 'edit'])->name('api.admin.cidades.edit');
            Route::post('/filtro',          [CidadesController::class, 'filtro'])->name('api.admin.cidades.filtro');
            Route::post('/store',           [CidadesController::class, 'store'])->name('api.admin.cidades.store');
            Route::put('/update/{id}',      [CidadesController::class, 'update'])->name('api.admin.cidades.update');
            Route::delete('/delete/{id}',   [CidadesController::class, 'destroy'])->name('api.admin.cidades.destroy');
        });

        Route::prefix('status_localizacao')->group(function (){
            Route::get('/', [StatusLocalizacaoController::class, 'index'])->name('api.admin.status_localizacao.index');
            Route::put('/update/{id}', [StatusLocalizacaoController::class, 'update'])->name('api.admin.status_localizacao.update');
            Route::post('/store', [StatusLocalizacaoController::class, 'store'])->name('api.admin.status_localizacao.store');
            Route::delete('/delete/{id}', [StatusLocalizacaoController::class, 'destroy'])->name('api.admin.status_localizacao.destroy');
            Route::get('/create', [StatusLocalizacaoController::class, 'create'])->name('api.admin.status_localizacao.create');
            Route::get('/edit/{id}', [StatusLocalizacaoController::class, 'edit'])->name('api.admin.status_localizacao.edit');
        });

        Route::prefix('funcao')->group(function (){
            Route::get('/', [FuncaoController::class, 'index'])->name('api.admin.funcao.index');
            Route::put('/update/{id}', [FuncaoController::class, 'update'])->name('api.admin.funcao.update');
            Route::post('/store', [FuncaoController::class, 'store'])->name('api.admin.funcao.store');
            Route::delete('/delete/{id}', [FuncaoController::class, 'destroy'])->name('api.admin.funcao.delete');
            Route::get('/create', [FuncaoController::class, 'create'])->name('api.admin.funcao.create');
            Route::get('/edit/{id}', [FuncaoController::class, 'edit'])->name('api.admin.funcao.edit');
        });

        Route::prefix('empresas')->group(function(){
            Route::get('/',                 [EmpresasController::class, 'index'])->name('api.admin.empresas.index');
            Route::get('/create',           [EmpresasController::class, 'create'])->name('api.admin.empresas.create');
            Route::get('/edit/{id}',        [EmpresasController::class, 'edit'])->name('api.admin.empresas.edit');
            Route::post('/store',           [EmpresasController::class, 'store'])->name('api.admin.empresas.store');
            Route::put('/update/{id}',      [EmpresasController::class, 'update'])->name('api.admin.empresas.update');
            Route::delete('/delete/{id}',   [EmpresasController::class, 'destroy'])->name('api.admin.empresas.destroy');
        });

        Route::prefix('funcionarios')->group(function(){
            Route::get('/',                 [FuncionariosController::class, 'index'])->name('api.admin.funcionarios.index');
            Route::get('/create',           [FuncionariosController::class, 'create'])->name('api.admin.funcionarios.create');
            Route::get('/edit/{id}',        [FuncionariosController::class, 'edit'])->name('api.admin.funcionarios.edit');
            Route::post('/store',           [FuncionariosController::class, 'store'])->name('api.admin.funcionarios.store');
            Route::put('/update/{id}',      [FuncionariosController::class, 'update'])->name('api.admin.funcionarios.update');
            Route::delete('/delete/{id}',   [FuncionariosController::class, 'destroy'])->name('api.admin.funcionarios.destroy');
        });

        Route::prefix('fornecedor')->group(function(){
            Route::get('/',                 [FornecedorController::class, 'index'])->name('api.admin.fornecedores.index');
            Route::get('/create',           [FornecedorController::class, 'create'])->name('api.admin.fornecedores.create');
            Route::get('/edit/{id}',        [FornecedorController::class, 'edit'])->name('api.admin.fornecedores.edit');
            Route::post('/store',           [FornecedorController::class, 'store'])->name('api.admin.fornecedores.store');
            Route::put('/update/{id}',      [FornecedorController::class, 'update'])->name('api.admin.fornecedores.update');
            Route::delete('/delete/{id}',   [FornecedorController::class, 'destroy'])->name('api.admin.fornecedores.destroy');
        });

        Route::prefix('contratos')->group(function(){
            Route::get('/{id?}',            [ContratosController::class, 'index'])->name('api.admin.contratos.index');
            Route::get('/create',           [ContratosController::class, 'create'])->name('api.admin.contratos.create');
            Route::get('/edit/{id}',        [ContratosController::class, 'edit'])->name('api.admin.contratos.edit');
            Route::get('/contrato_fornecedor/{id}', [ContratosController::class, 'contrato_fornecedor'])->name('api.admin.contratos.contrato_fornecedor');
            Route::post('/store',           [ContratosController::class, 'store'])->name('api.admin.contratos.store');
            Route::put('/update/{id}',      [ContratosController::class, 'update'])->name('api.admin.contratos.update');
            Route::delete('/delete/{id}',   [ContratosController::class, 'destroy'])->name('api.admin.contratos.destroy');

            Route::prefix('anexos')->group(function (){
                Route::get('/{id}',             [AnexosContratoController::class, 'index'])->name('api.admin.contratos.anexos.index');
                Route::post('/store',           [AnexosContratoController::class, 'store'])->name('api.admin.contratos.anexos.store');
                Route::delete('/delete/{id}',   [AnexosContratoController::class, 'destroy'])->name('api.admin.contratos.anexos.destroy');
            });

        });

        Route::prefix('motivo_evento')->group(function(){
            Route::get('/',                 [MotivoEventoController::class, 'index'])->name('api.admin.motivo_evento.index');
            Route::get('/create',           [MotivoEventoController::class, 'create'])->name('api.admin.motivo_evento.create');
            Route::get('/edit/{id}',        [MotivoEventoController::class, 'edit'])->name('api.admin.motivo_evento.edit');
            Route::post('/store',           [MotivoEventoController::class, 'store'])->name('api.admin.motivo_evento.store');
            Route::put('/update/{id}',      [MotivoEventoController::class, 'update'])->name('api.admin.motivo_evento.update');
            Route::delete('/delete/{id}',   [MotivoEventoController::class, 'destroy'])->name('api.admin.motivo_evento.destroy');
        });

        Route::prefix('motivo_devolucao')->group(function(){
            Route::get('/',                 [MotivoDevolucaoController::class, 'index'])->name('api.admin.motivo_devolucao.index');
            Route::get('/create',           [MotivoDevolucaoController::class, 'create'])->name('api.admin.motivo_devolucao.create');
            Route::get('/edit/{id}',        [MotivoDevolucaoController::class, 'edit'])->name('api.admin.motivo_devolucao.edit');
            Route::post('/store',           [MotivoDevolucaoController::class, 'store'])->name('api.admin.motivo_devolucao.store');
            Route::put('/update/{id}',      [MotivoDevolucaoController::class, 'update'])->name('api.admin.motivo_devolucao.update');
            Route::delete('/delete/{id}',   [MotivoDevolucaoController::class, 'destroy'])->name('api.admin.motivo_devolucao.destroy');
        });

        Route::prefix('classificacao_negativacao')->group(function(){
            Route::get('/',                 [ClassificacaoNegativacaoController::class, 'index'])->name('api.admin.classificacao_negativacao.index');
            Route::get('/create',           [ClassificacaoNegativacaoController::class, 'create'])->name('api.admin.classificacao_negativacao.create');
            Route::get('/edit/{id}',        [ClassificacaoNegativacaoController::class, 'edit'])->name('api.admin.classificacao_negativacao.edit');
            Route::post('/store',           [ClassificacaoNegativacaoController::class, 'store'])->name('api.admin.classificacao_negativacao.store');
            Route::put('/update/{id}',      [ClassificacaoNegativacaoController::class, 'update'])->name('api.admin.classificacao_negativacao.update');
            Route::delete('/delete/{id}',   [ClassificacaoNegativacaoController::class, 'destroy'])->name('api.admin.classificacao_negativacao.destroy');
        });

        Route::prefix('motivo_pendencia')->group(function(){
            Route::get('/',                 [MotivoPendenciaController::class, 'index'])->name('api.admin.motivo_pendencia.index');
            Route::get('/create',           [MotivoPendenciaController::class, 'create'])->name('api.admin.motivo_pendencia.create');
            Route::get('/edit/{id}',        [MotivoPendenciaController::class, 'edit'])->name('api.admin.motivo_pendencia.edit');
            Route::post('/store',           [MotivoPendenciaController::class, 'store'])->name('api.admin.motivo_pendencia.store');
            Route::put('/update/{id}',      [MotivoPendenciaController::class, 'update'])->name('api.admin.motivo_pendencia.update');
            Route::delete('/delete/{id}',   [MotivoPendenciaController::class, 'destroy'])->name('api.admin.motivo_pendencia.destroy');
        });

        Route::prefix('campanha')->group(function(){
            Route::get('/',                 [CampanhasController::class, 'index'])->name('api.admin.campanha.index');
            Route::get('/create',           [CampanhasController::class, 'create'])->name('api.admin.campanha.create');
            Route::get('/credor/chart/totalCampanhaCredor',[CampanhasController::class, 'totalCampanhaCredor'])->name('api.admin.campanha.totalCampanhaCredor');
            Route::get('/edit/{id}',        [CampanhasController::class, 'edit'])->name('api.admin.campanha.edit');
            Route::get('/detalhar/{id}',    [CampanhasController::class, 'detalharCampamnha'])->name('api.admin.campanha.detalhar');
            Route::get('/operacoes/{id}',   [CampanhasController::class, 'operacoesCampanha'])->name('api.admin.campanha.operacoes');
            Route::get('/acordos/{id}',     [CampanhasController::class, 'acordosCampanha'])->name('api.admin.campanha.acordos');
            Route::get('/clientes/{id}',    [CampanhasController::class, 'clientesCampanha'])->name('api.admin.campanha.clientes');
            Route::post('/totalizadores',   [CampanhasController::class, 'totalizadores'])->name('api.admin.campanha.totalizadores');
            Route::post('/store',           [CampanhasController::class, 'store'])->name('api.admin.campanha.store');
            Route::post('/lista/{id?}',     [CampanhasController::class, 'lista'])->name('api.admin.campanha.lista');
            Route::put('/update/{id}',      [CampanhasController::class, 'update'])->name('api.admin.campanha.update');
            Route::delete('/delete/{id}',   [CampanhasController::class, 'destroy'])->name('api.admin.campanha.destroy');
        });

        Route::prefix('grupo_acesso')->group(function(){
            Route::get('/',                 [GrupoAcessoController::class, 'index'])->name('api.admin.grupo_acesso.index');
            Route::get('/create',           [GrupoAcessoController::class, 'create'])->name('api.admin.grupo_acesso.create');
            Route::get('/edit/{id}',        [GrupoAcessoController::class, 'edit'])->name('api.admin.grupo_acesso.edit');
            Route::post('/store',           [GrupoAcessoController::class, 'store'])->name('api.admin.grupo_acesso.store');
            Route::put('/update/{id}',      [GrupoAcessoController::class, 'update'])->name('api.admin.grupo_acesso.update');
            Route::delete('/delete/{id}',   [GrupoAcessoController::class, 'destroy'])->name('api.admin.grupo_acesso.destroy');
        });

        Route::prefix('perfil_usuario')->group(function(){
            Route::get('/',                 [PerfilUsuarioController::class, 'index'])->name('api.admin.perfil_usuario.index');
            Route::get('/create',           [PerfilUsuarioController::class, 'create'])->name('api.admin.perfil_usuario.create');
            Route::get('/edit/{id}',        [PerfilUsuarioController::class, 'edit'])->name('api.admin.perfil_usuario.edit');
            Route::post('/store',           [PerfilUsuarioController::class, 'store'])->name('api.admin.perfil_usuario.store');
            Route::put('/update/{id}',      [PerfilUsuarioController::class, 'update'])->name('api.admin.perfil_usuario.update');
            Route::delete('/delete/{id}',   [PerfilUsuarioController::class, 'destroy'])->name('api.admin.perfil_usuario.destroy');
        });

        Route::prefix('proposta')->group(function(){
            Route::get('/executivo/listar/{id}', [PropostaController::class,'propostasPorExecutivo'])->name('api.admin.propostas.executivo.listar');
            Route::get('/supervisor/listar/{id}',[PropostaController::class,'propostasPorSupervisor'])->name('api.admin.propostas.supervisor.listar');
            Route::post('/store',                [PropostaController::class, 'store'])->name('api.admin.proposta.store');
            Route::put('/update/status/{id}',    [PropostaController::class, 'updateStatus'])->name('api.admin.proposta.update.status');
        });

        Route::prefix('endpoints')->group(function(){
            Route::get('/',                 [EndpointsController::class, 'index'])->name('api.admin.endpoints.index');
            Route::get('/create',           [EndpointsController::class, 'create'])->name('api.admin.endpoints.create');
            Route::get('/edit/{id}',        [EndpointsController::class, 'edit'])->name('api.admin.endpoints.edit');
            Route::post('/store',           [EndpointsController::class, 'store'])->name('api.admin.endpoints.store');
            Route::put('/update/{id}',      [EndpointsController::class, 'update'])->name('api.admin.endpoints.update');
            Route::delete('/delete/{id}',   [EndpointsController::class, 'destroy'])->name('api.admin.endpoints.destroy');
        });

        Route::prefix('users')->group(function(){
            Route::get('/',                 [UserController::class, 'index'])->name('api.admin.users.index');
            Route::get('/create',           [UserController::class, 'create'])->name('api.admin.users.create');
            Route::get('/edit/{id}',        [UserController::class, 'edit'])->name('api.admin.users.edit');
            Route::post('/store',           [UserController::class, 'store'])->name('api.admin.users.store');
            Route::post('/uploadFoto',      [UserController::class, 'uploadFoto'])->name('api.admin.users.uploadFoto');
            Route::get('/exibirFoto/{id}',  [UserController::class, 'exibirFoto'])->name('api.admin.users.exibirFoto');
            Route::put('/update/{id}',      [UserController::class, 'update'])->name('api.admin.users.update');
            Route::delete('/delete/{id}',   [UserController::class, 'destroy'])->name('api.admin.users.destroy');
            Route::post('/atache/perfil',   [UserController::class, 'atacharPerfilUsuario'])->name('api.admin.users.atache.perfil.user');
            Route::post('/detache/perfil',  [UserController::class, 'detachePerfilUsuario'])->name('api.admin.users.detache.perfil.user');
        });

        Route::prefix('eventos')->group(function(){
            Route::get('/{id}',             [EventosController::class, 'index'])->name('api.admin.eventos.index');
            Route::get('/create',           [EventosController::class, 'create'])->name('api.admin.eventos.create');
            Route::get('/edit/{id}',        [EventosController::class, 'edit'])->name('api.admin.eventos.edit');
            Route::post('/store',           [EventosController::class, 'store'])->name('api.admin.eventos.store');
            Route::put('/update/{id}',      [EventosController::class, 'update'])->name('api.admin.eventos.update');
            Route::delete('/delete/{id}',   [EventosController::class, 'destroy'])->name('api.admin.eventos.destroy');
        });

        Route::prefix('acordo')->group(function(){
            Route::get('/negociacao/{id}',          [AcordoController::class, 'negociacao'])->name('api.admin.acordo.negociacao');
            Route::get('/credor/chart/totalAcordoCredor',  [AcordoController::class, 'totalAcordoCredor'])->name('api.admin.acordo.totalAcordoCredor');
            Route::post('/nome_aluno',              [AcordoController::class, 'nome_aluno'])->name('api.admin.acordo.nome_aluno');
            Route::post('/pesquisa/avancada',       [AcordoController::class, 'pesquisaAvancada'])->name('api.admin.acordo.pesquisa.avancada');
            Route::post('/data_acordo',             [AcordoController::class, 'data_acordo'])->name('api.admin.acordo.data_acordo');
            Route::post('/filtroAcordo',             [AcordoController::class, 'filtroAcordo'])->name('api.admin.acordo.filtroAcordo');
            Route::get('/acordo_parcela_vencida',   [AcordoController::class, 'acordosVencidos'])->name('api.admin.acordo.acordo_vencidos');
            Route::get('/quebra_acordo/{id}',       [AcordoController::class, 'quebraAcordo'])->name('api.admin.acordo.quebra_acordo');
            Route::get('/cliente/{id}',             [AcordoController::class, 'cliente'])->name('api.admin.acordo.cliente');
            Route::get('/edit/{id}',                [AcordoController::class, 'edit'])->name('api.admin.acordo.edit');
            Route::get('/executivo/{id}',           [AcordoController::class, 'executivo'])->name('api.admin.acordo.executivo');
            Route::get('/acordo_periodo_chart',     [AcordoController::class, 'acordo_periodo'])->name('api.admin.acordo.acordo_periodo');
            Route::get('/{id}',                     [AcordoController::class, 'index'])->name('api.admin.acordo.index');
            Route::get('/fatura_cartao/{id}',       [AcordoController::class, 'decodificar_hash'])->name('api.admin.acordo.decodificar_hash');
            Route::post('/store',                   [AcordoController::class, 'store'])->name('api.admin.acordo.store');
            Route::post('/viewDemoSimples/{id?}',   [AcordoController::class, 'viewDemoSimples'])->name('api.admin.view_Boleto.viewDemoSimples');
            Route::post('/viewDemoDetalhado/{id?}', [AcordoController::class, 'viewDemoDetalhado'])->name('api.admin.view_Boleto.viewDemoDetalhado');
            Route::put('/update/{id}',              [AcordoController::class, 'update'])->name('api.admin.acordo.update');
            Route::delete('/delete/{id}',           [AcordoController::class, 'destroy'])->name('api.admin.acordo.destroy');
            //editar cliente na cobrança
            Route::get('/cliente/editar/{id}',             [ClienteController::class, 'editCliente'])->name('api.admin.acordo.cliente.editar');
            Route::put('/cliente/update/{id}',             [ClienteController::class, 'update'])->name('api.admin.acordo.cliente.update');
            Route::post('/update/executivo/cobranca/{id}', [AcordoController::class, 'updateExecutivoCobranca'])->name('api.admin.update.executivo.cobranca');
            Route::post('/filtrar',                        [AcordoController::class, 'filtrar'])->name('api.admin.acordo.filtrar');
        });

        Route::prefix('parcelas')->group(function(){
            Route::post('/listagem',         [ParcelasController::class, 'listagem'])->name('api.admin.parcelas.listagem');
            Route::get('/{id}',              [ParcelasController::class, 'index'])->name('api.admin.parcelas.index');
            Route::post('/baixaManual/{id}', [ParcelasController::class, 'baixaManual'])->name('api.admin.acordo.baixaManual');

            Route::post('/retirarBaixa', [ParcelasController::class, 'removerBaixa'])->name('api.admin.parcelas.removerBaixa');
            Route::post('/incluirBaixa', [ParcelasController::class, 'incluirBaixa'])->name('api.admin.parcelas.incluirBaixa');

            Route::get('/create',            [ParcelasController::class, 'create'])->name('api.admin.parcelas.create');
            Route::get('/edit/{id}',         [ParcelasController::class, 'edit'])->name('api.admin.parcelas.edit');
            Route::get('/indexAcordo/{id}',  [ParcelasController::class, 'indexAcordo'])->name('api.admin.parcelas.indexAcordo');
            Route::post('/store',            [ParcelasController::class, 'store'])->name('api.admin.parcelas.store');
            Route::put('/update/{id}',       [ParcelasController::class, 'update'])->name('api.admin.parcelas.update');
            Route::delete('/delete/{id}',    [ParcelasController::class, 'destroy'])->name('api.admin.parcelas.destroy');
            Route::post('/filtrar',          [ParcelasController::class, 'filtro'])->name('api.admin.parcelas.filtrar');
        });

        Route::prefix('boletos')->group(function(){
            Route::get('/',                 [BoletosController::class, 'index'])->name('api.admin.boletos.index');
            Route::get('/visualizar/{id}',  [BoletosController::class, 'visualizarBoletoDaParcela'])->name('api.admin.boletos.parcela.visualizar');
            Route::post('/data_boleto',     [BoletosController::class, 'data_boleto'])->name('api.admin.acordo.data_boleto');
            Route::get('/create',           [BoletosController::class, 'create'])->name('api.admin.boletos.create');
            Route::get('/edit/{id}',        [BoletosController::class, 'edit'])->name('api.admin.boletos.edit');
            Route::post('/store',           [BoletosController::class, 'store'])->name('api.admin.boletos.store');
            Route::put('/update/{id}',      [BoletosController::class, 'update'])->name('api.admin.boletos.update');
            Route::delete('/delete/{id}',   [BoletosController::class, 'destroy'])->name('api.admin.boletos.destroy');
            Route::post('/viewBoleto',      [BoletosController::class, 'viewBoleto'])->name('api.admin.view_Boleto.viewBoleto');
        });

        Route::prefix('classificacao_cliente')->group(function(){
            Route::get('/{id}',             [ClassificacaoClienteController::class, 'index'])->name('api.admin.classificacao_cliente.index');
            Route::get('/create',           [ClassificacaoClienteController::class, 'create'])->name('api.admin.classificacao_cliente.create');
            Route::get('/edit/{id}',        [ClassificacaoClienteController::class, 'edit'])->name('api.admin.classificacao_cliente.edit');
            Route::post('/store',           [ClassificacaoClienteController::class, 'store'])->name('api.admin.classificacao_cliente.store');
            Route::put('/update/{id}',      [ClassificacaoClienteController::class, 'update'])->name('api.admin.classificacao_cliente.update');
            Route::delete('/delete/{id}',   [ClassificacaoClienteController::class, 'destroy'])->name('api.admin.classificacao_cliente.destroy');
        });

        Route::prefix('parametro_boleto')->group(function(){
            Route::get('/',                 [ParametroBoletoController::class, 'index'])->name('api.admin.parametro_boleto.index');
            Route::get('/create',           [ParametroBoletoController::class, 'create'])->name('api.admin.parametro_boleto.create');
            Route::get('/edit/{id}',        [ParametroBoletoController::class, 'edit'])->name('api.admin.parametro_boleto.edit');
            Route::post('/store',           [ParametroBoletoController::class, 'store'])->name('api.admin.parametro_boleto.store');
            Route::put('/update/{id}',      [ParametroBoletoController::class, 'update'])->name('api.admin.parametro_boleto.update');
            Route::delete('/delete/{id}',   [ParametroBoletoController::class, 'destroy'])->name('api.admin.parametro_boleto.destroy');
        });

        Route::prefix('descontos')->group(function(){
            Route::get('/',                 [DescontosController::class, 'index'])->name('api.admin.descontos.index');
            Route::get('/create',           [DescontosController::class, 'create'])->name('api.admin.descontos.create');
            Route::get('/edit/{id}',        [DescontosController::class, 'edit'])->name('api.admin.descontos.edit');
            Route::post('/store',           [DescontosController::class, 'store'])->name('api.admin.descontos.store');
            Route::put('/update/{id}',      [DescontosController::class, 'update'])->name('api.admin.descontos.update');
            Route::delete('/delete/{id}',   [DescontosController::class, 'destroy'])->name('api.admin.descontos.destroy');
            Route::post('/importar',        [DescontosController::class, 'importar'])->name('api.admin.descontos.importar');
        });

        Route::prefix('template_sms')->group(function(){
            Route::get('/',                 [TemplateSmsController::class, 'index'])->name('api.admin.template_sms.index');
            Route::get('/create',           [TemplateSmsController::class, 'create'])->name('api.admin.template_sms.create');
            Route::get('/edit/{id}',        [TemplateSmsController::class, 'edit'])->name('api.admin.template_sms.edit');
            Route::post('/store',           [TemplateSmsController::class, 'store'])->name('api.admin.template_sms.store');
            Route::put('/update/{id}',      [TemplateSmsController::class, 'update'])->name('api.admin.template_sms.update');
            Route::delete('/delete/{id}',   [TemplateSmsController::class, 'destroy'])->name('api.admin.template_sms.destroy');
            Route::post('/preenchersms',     [TemplateSmsController::class, 'preenchersms'])->name('api.admin.template_sms.preenchersms');
        });

        Route::prefix('template_email')->group(function(){
            Route::get('/',                 [TemplateEmailController::class, 'index'])->name('api.admin.template_email.index');
            Route::get('/create',           [TemplateEmailController::class, 'create'])->name('api.admin.template_email.create');
            Route::get('/edit/{id}',        [TemplateEmailController::class, 'edit'])->name('api.admin.template_email.edit');
            Route::post('/store',           [TemplateEmailController::class, 'store'])->name('api.admin.template_email.store');
            Route::put('/update/{id}',      [TemplateEmailController::class, 'update'])->name('api.admin.template_email.update');
            Route::delete('/delete/{id}',   [TemplateEmailController::class, 'destroy'])->name('api.admin.template_email.destroy');
            Route::post('/preencheremail',  [TemplateEmailController::class, 'preencheremail'])->name('api.admin.template_sms.preencheremail');
        });

        Route::prefix('credor')->group(function(){
            Route::get('/clientes',[CredoresController::class, 'clienteCredor'])->name('api.admin.credor.index');
            Route::get('/acordos/listar/{id}',[CredoresController::class, 'ListarAcordos'])->name('api.admin.credor.acordos.listar');
        });

        Route::prefix('credor_contatos')->group(function(){
            Route::get('/{id}',             [CredorContatosController::class, 'index'])->name('api.admin.credor_contatos.index');
            Route::get('/edit/{id}',        [CredorContatosController::class, 'edit'])->name('api.admin.credor_contatos.edit');
            Route::post('/store',           [CredorContatosController::class, 'store'])->name('api.admin.credor_contatos.store');
            Route::put('/update/{id}',      [CredorContatosController::class, 'update'])->name('api.admin.credor_contatos.update');
            Route::delete('/delete/{id}',   [CredorContatosController::class, 'destroy'])->name('api.admin.credor_contatos.destroy');
        });

        Route::prefix('portal')->group(function(){
            Route::get('/cliente/cpf/{id}',[ClienteController::class, 'clienteCpf'])->name('api.admin.portal.cliente.cpf');
            Route::get('/credor/cpf/{id}',[CredoresController::class, 'credorCpf'])->name('api.admin.portal.credor.cpf');
            Route::get('/parametros/credor/{id}',[CredoresController::class, 'parametrosCredor'])->name('api.admin.portal.parametros.credor');
            Route::post('/operacao/cpf/{id}',[OperacaoController::class, 'operacaoCpf'])->name('api.admin.portal.operacao.cpf');
        });

        Route::prefix('sms')->group(function() {
            Route::post('/enviar',          [SmsController::class, 'enviar_sms_unico'])->name('api.admin.sms.enviar_sms_unico');
            Route::post('/clientes/enviar',[SmsController::class, 'enviar_sms_cliente'])->name('api.admin.sms.enviar_sms_cliente');

        });

        Route::prefix('email')->group(function() {
            Route::post('/enviar',          [EmailController::class, 'enviar_email_avulso'])->name('api.admin.email.enviar_email_avulso');
            Route::post('/enviar_acordo_sintetico',  [EmailController::class, 'enviar_acordo_sintetico'])->name('api.admin.email.enviar_acordo_sintetico');
        });

        Route::prefix('lote_operacoes')->group(function (){
            Route::post('/importar', [LoteOperacoesController::class, 'importar'])->name('api.admin.lote_operacoes.importar');
        });

        Route::prefix('baixar_boleto')->group(function(){
            Route::get('/index', [BaixarBoletoController::class, 'index'])->name('api.admin.boletos.baixa.index');
            Route::post('/boleto', [BaixarBoletoController::class, 'boleto'])->name('api.admin.boletos.boleto');
        });

        Route::prefix('insucessos')->group(function(){
            Route::post('/importar', [InsucessosController::class, 'importar'])->name('api.admin.insucessos.importar');
            Route::get('/listagem/{id}', [InsucessosController::class, 'listagem'])->name('api.admin.insucessos.importar');
        });

        Route::prefix('repasses')->group(function(){
            Route::post('/filtrar', [OperacoesParcelasController::class, 'filtrar'])->name('api.admin.repasses.filtrar');
        });

        Route::prefix('teste')->group(function(){
            Route::post('/index',  [EnvioAutomaticoSmsRepository::class, 'envio_automatico_sms'])->name('api.admin.boletos.teste');
        });

        //TODO: ainda usamos??
        Route::prefix('naoautorizado')->group(function(){
            Route::get('msg',      [PerfilRepository::class, 'msg'])->name('naoautorizado');
        });

    });

});

Route::fallback(function(){
    return response()->json(['message' => 'Recurso não encontrado.']);
});
